FROM mcr.microsoft.com/dotnet/sdk:5.0 as BuildCoreLayer

WORKDIR /Build

COPY . /Build

RUN dotnet nuget locals all --clear
RUN dotnet restore --no-cache
RUN dotnet publish -c Release -o /Build/Publish --no-self-contained --no-restore /Build/Core.Mud.sln

FROM mcr.microsoft.com/dotnet/aspnet:5.0

LABEL app="Core MUD"

WORKDIR /App

COPY --from=BuildCoreLayer /Build/Publish /App

EXPOSE 4444

ENTRYPOINT ["dotnet", "Keeper.MercuryCore.TestHarness.dll"]
