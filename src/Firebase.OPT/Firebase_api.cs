using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Interface;

namespace Firebase.OPT
{
    public class Firebase_api<T> : IFirebase<T> where T : class, IFirebaseObject
    {
        private readonly string firebase_base_url = "https://freemud4444-default-rtdb.firebaseio.com/";
        private readonly JsonSerializerOptions jsoptions;
        private readonly ILogger<Firebase_api<T>> logger;
        private readonly bool isLocal = false;

        public Firebase_api (ILogger<Firebase_api<T>> logger)
        {
            this.logger = logger;
            jsoptions = new JsonSerializerOptions ();
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.WriteIndented = true;

            bool _isLocal = false;
            bool isBool = bool.TryParse (Environment.GetEnvironmentVariable ("LocalOnly"), out _isLocal);
            if (isBool && _isLocal) this.isLocal = true;
        }

        public async Task InsertOrUpdate (T obj, bool UpperID = true)
        {
            if (isLocal) return;
            string Collection = Uri.EscapeDataString (typeof (T).Name);
            string ID = Uri.EscapeDataString (obj.ID);
            if (UpperID) ID = ID.ToUpperInvariant ();
            var json = JsonSerializer.Serialize (obj, jsoptions);
            //string path = $"{firebase_base_url}/{obj.GetType().Name}/{obj.ID}.json";
            await firebase_base_url
                .AppendPathSegments (Collection, $"{ID}.json")
                //.WithOAuthBearerToken ("my_oauth_token")
                .PutStringAsync (json);
        }

        public async Task<T> Get (string ID)
        {
            T rtn = default;
            if (isLocal) return rtn;

            string Collection = typeof (T).Name;
            try
            {
                ID = Uri.EscapeDataString (ID.ToUpperInvariant ());
                rtn = await firebase_base_url
                    .AppendPathSegments (Collection, $"{ID}.json")
                    //.WithOAuthBearerToken ("my_oauth_token")
                    .GetAsync ()
                    .ReceiveJson<T> ();
            }
            catch (Exception ex)
            {
                logger.LogInformation ("Get from firebase got exception:" + ex.Message);
            }
            return rtn;
        }

        public async Task<Dictionary<string, T>> GetALL (string Collection)
        {
            Dictionary<string, T> rtn = default;
            if (isLocal) return rtn;
            try
            {
                Collection = Uri.EscapeDataString (Collection);
                rtn = await firebase_base_url
                    .AppendPathSegments ($"{Collection}.json")
                    //.WithOAuthBearerToken ("my_oauth_token")
                    .GetAsync ()
                    .ReceiveJson<Dictionary<string, T>> ();
            }
            catch (Exception ex)
            {
                logger.LogInformation ("Get from firebase got exception:" + ex.Message);
            }
            return rtn;
        }

    }
}
