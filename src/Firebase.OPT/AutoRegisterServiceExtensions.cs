﻿using NetCore.AutoRegisterDi;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AutoRegisterServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServiceFirebaseDI (this IServiceCollection services)
        {
            services.RegisterAssemblyPublicNonGenericClasses ()
                .Where (
                    x => x.Name.StartsWith ("f_")
                )
                .AsPublicImplementedInterfaces ();
            return services;
            //put any non-standard DI registration, e.g. generic types, here
        }
    }
}