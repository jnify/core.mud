using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Elf
{
    public interface IElfManager { }

    //todo:要依頻率拆分
    public class ElfManager : IElfManager
    {
        private Dictionary<string, DateTime> ExecuteTime = new Dictionary<string, DateTime> ();
        private readonly object LockBlockTime = new object ();
        private readonly ILogger<ElfManager> logger;
        private readonly TimeSpan ts_MobMovement = TimeSpan.FromMinutes (5); //Mob移動
        private readonly TimeSpan ts_MobMurmur = TimeSpan.FromSeconds (30); //Mob說話及狀態
        private readonly TimeSpan ts_Heartbeat = TimeSpan.FromSeconds (30); //世界心跳

        private readonly Random rnd = new Random ();
        private readonly IUserManager userManager;
        private readonly IWorldManager worldManager;
        private readonly IMessageManager msgmgr;
        private readonly IActionUserEffect actionEffect;
        private readonly ILoadCommonObject loadCommonObj;

        private int _SystemHour = -1;

        public ElfManager (ILogger<ElfManager> logger, IUserManager userManager, IWorldManager wordmgr, IMessageManager msgmgr, IActionUserEffect ActionEffect, ILoadCommonObject loadCommonObj)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.worldManager = wordmgr;
            this.msgmgr = msgmgr;
            this.actionEffect = ActionEffect;
            this.loadCommonObj = loadCommonObj;
            //剛啟動的第一個世界小時，不進行動作。
            _SystemHour = worldManager.WorldInfo.SystemDateTime.Hour;

            Task.Run (() =>
            {
                while (true)
                {
                    try
                    {
                        InfinityLoop ();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError (ex.Message);
                    }
                    Thread.Sleep (500);
                }
            });

        }
        private bool CanExecute (string bT_Key, TimeSpan ts)
        {
            if (!ExecuteTime.ContainsKey (bT_Key))
            {
                lock (LockBlockTime)
                {
                    if (!ExecuteTime.ContainsKey (bT_Key))
                    {
                        ExecuteTime.Add (bT_Key, DateTime.Now);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                var latestDT = ExecuteTime[bT_Key];
                if (DateTime.Now >= latestDT.Add (ts))
                {
                    lock (LockBlockTime)
                    {
                        latestDT = ExecuteTime[bT_Key];
                        if (DateTime.Now >= latestDT.Add (ts))
                        {
                            ExecuteTime[bT_Key] = DateTime.Now;
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                }
                else
                {
                    return false;
                }
            }
        }
        private void InfinityLoop ()
        {
            var currentWordDT = worldManager.WorldInfo.SystemDateTime;
            if (_SystemHour != currentWordDT.Hour) //2.5分鐘一次
            {
                _SystemHour = currentWordDT.Hour;
                BroadcastWordMessage (); //世界訊息廣播
                AutoIncreaseStatus (); //自動恢復HP, MV, Mana
                AutoDecreaseLivelihood (); //減少，口渴、飢餓的數值
                EffectDying ();
            }
            if (CanExecute ("MobMovement", ts_MobMovement)) { MobMovement (); }
            if (CanExecute ("MobMurmur", ts_MobMurmur)) { MobMurmur (); }
            if (CanExecute ("WorldHeartbeat", ts_Heartbeat)) { Heartbeat (); }
            //每500ms一次
            SendPrompt ();
            ObjectDispearing ();
            CheckEffectBy500ms ();
        }

        //有時效的，都要Check
        private readonly List<EffectiveType> checkEffect = new List<EffectiveType> ()
        {
            EffectiveType.Hide,
            EffectiveType.Sneak
        };

        private void CheckEffectBy500ms ()
        {
            //only check the end time
            userManager.GetAllUser ().Where (user =>
                user.GetAllEffect ().Any (userEff => checkEffect.Contains (userEff.Type))
            ).ToList ().ForEach (user =>
            {
                if (user.GetAllEffect ().Any (userEff => checkEffect.Contains (userEff.Type)))
                {
                    user.GetAllEffect ().Where (userEff => userEff.EndTime < DateTime.Now && checkEffect.Contains (userEff.Type)).ToList ().ForEach (userEff =>
                    {
                        var msg = actionEffect.CompleteEffect (user, userEff.Type);
                        msgmgr.SendToPPL (msg, user.Name, false);
                    });
                }
            });
        }

        private DateTime Last_DispearDatetime = DateTime.Now;
        private void ObjectDispearing ()
        {
            var CurrentDT = DateTime.Now;
            TimeSpan ts = CurrentDT - Last_DispearDatetime;
            Last_DispearDatetime = CurrentDT;

            //處理UserInfo身上的物品，先扣使用度，再風化
            userManager.GetAllUser ().Where (user =>
                    user.Things.Any (obj => obj.DispearTS != TimeSpan.MaxValue &
                        !obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear))).ToList ()
                .ForEach (user =>
                {
                    //扣使用度
                    user.Things.Where (obj => obj.DispearTS != TimeSpan.MaxValue &&
                            !obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear)).ToList ()
                        .ForEach (x => x.DispearTS -= ts);
                    //風化
                    _DispareObjectOnUser (user, user.Name);
                });
            //處理UserInfo的EQArea，先扣使用度，再風化
            userManager.GetAllUser ().Where (user =>
                    user.EQArea.Any (dic => dic.Value.DispearTS != TimeSpan.MaxValue)).ToList ()
                .ForEach (user =>
                {
                    //扣使用度
                    user.EQArea.Where (dic => dic.Value.DispearTS != TimeSpan.MaxValue).ToList ()
                        .ForEach (dic => dic.Value.DispearTS -= ts);
                    //風化
                    List<string> objKey = user.EQArea.Where (dic => dic.Value.DispearTS < TimeSpan.FromSeconds (1)).Select (dic => dic.Key).ToList ();
                    objKey.ForEach (key =>
                    {
                        var _tmp = user.EQArea[key];
                        var objDef = loadCommonObj.GetDef (_tmp.SystemCode);
                        if (objDef != null && !string.IsNullOrWhiteSpace (objDef.DispearMessage))
                            msgmgr.SendToPPL (objDef.DispearMessage.Replace ("{ObjectName}", _tmp.ShortName), user.Name);
                        else
                            msgmgr.SendToPPL ($"{_tmp.ShortName}漸漸的消失。", user.Name);
                        user.EQArea.Remove (key);
                    });
                });

            //處理房間內物品，排除穿戴物品
            worldManager.GetRooms ()
                .Where (room => room.Things.Any (obj =>
                    obj.DispearTS != TimeSpan.MaxValue &&
                    !obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear))).ToList ()
                .ForEach (room =>
                {
                    //扣使用度
                    room.Things
                        .Where (obj => obj.DispearTS != TimeSpan.MaxValue &&
                            !obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear)).ToList ()
                        .ForEach (x => x.DispearTS -= ts);
                    //風化
                    _DispareObjectOnUser (room, "", room.RoomID);
                });
        }

        private void _DispareObjectOnUser (BaseObjectContainer Container, string UserName, int RoomID = 0)
        {
            List<string> objID = Container.Things.Where (obj => obj.DispearTS < TimeSpan.FromSeconds (1)).Select (obj => obj.ID).ToList ();
            objID.ForEach (id =>
            {
                var isSuccess = Container.RemoveByID (id, out var _tmp);
                if (isSuccess)
                {
                    var objDef = loadCommonObj.GetDef (_tmp.SystemCode);
                    if (RoomID == 0)
                    {
                        if (objDef != null && !string.IsNullOrWhiteSpace (objDef.DispearMessage))
                            msgmgr.SendToPPL (objDef.DispearMessage.Replace ("{ObjectName}", _tmp.ShortName), UserName);
                        else
                            msgmgr.SendToPPL ($"{_tmp.ShortName}漸漸的消失。", UserName);
                    }
                    else
                    {
                        if (objDef != null && !string.IsNullOrWhiteSpace (objDef.DispearMessage))
                            msgmgr.BroadcastToRoom (objDef.DispearMessage.Replace ("{ObjectName}", _tmp.ShortName), RoomID);
                        else
                            msgmgr.BroadcastToRoom ($"{_tmp.ShortName}漸漸的消失。", RoomID);
                    }
                }
                else
                {
                    var _forceRemoveObj = Container.Things.Where (obj => obj.ID == id).FirstOrDefault ();
                    Container.Things.Remove (_forceRemoveObj);
                    if (RoomID == 0)
                        msgmgr.SendToPPL ($"{_forceRemoveObj.ShortName}己被天神強制回收。", UserName);
                    else
                        msgmgr.BroadcastToRoom ($"{_forceRemoveObj.ShortName}己被天神強制回收。", RoomID);

                }
            });
        }

        private void EffectDying ()
        {
            //取出瀕死的PPL
            var users = userManager.GetAllUser ().Where (user => user.Status.HasFlag (CreatureStatus.Dying)).ToList ();
            users.ForEach (user =>
            {
                var act = actionEffect.MiddleEffect (user, EffectiveType.Dying);
                msgmgr.SendToPPL (act, user.Name, false);
            });

            //取出瀕死超過10次沒人救，要變成死亡清單。
            users = userManager.GetAllUser ().Where (user => user.Status.HasFlag (CreatureStatus.Dying) &&
                user.GetEffect (EffectiveType.Dying) != null && user.GetEffect (EffectiveType.Dying).Volume > 9).ToList ();
            users.ForEach (user =>
            {
                actionEffect.CompleteEffect (user, EffectiveType.Dying);
            });
        }

        private void Heartbeat ()
        {
            worldManager.WorldInfo.SystemDateTime.UpdateDateTime ();
            //以下為測試用，30秒一次
            //EffectDying ();
            //ObjectDispearing ();
        }

        private void SendPrompt ()
        {
            var users = userManager.GetAllUser ().Where (x => x.LastMessageDatetime != DateTime.MinValue && x.LastMessageDatetime < DateTime.Now).Select (x => x.Name).ToList ();
            if (users.Count () > 0)
                msgmgr.DoDisplayPrompt (users);
        }

        //消耗Livelihood值
        private void AutoDecreaseLivelihood ()
        {
            //要扣除瀕死及死亡的PPL
            var users = userManager.GetAllUser ().Where (x => !x.Status.HasFlag (CreatureStatus.Dying));
            foreach (var user in users)
            {
                user.Livelihood.Thirsty -= 7;
                user.Livelihood.Full -= 3;
                List<string> messages = new List<string> ();
                if (user.Livelihood.Thirsty < 0)
                    messages.Add (TextUtil.GetColorWord ("你渴死了。", TextUtil.ColorFront.Yellow));
                if (user.Livelihood.Full < 0)
                    messages.Add (TextUtil.GetColorWord ("你的肚子在Call你啊！！", TextUtil.ColorFront.Yellow));
                if (user.Livelihood.Thirsty < -50)
                {
                    user.StateCurrent.HP += UtilityExperence.DecutLivelihood (user.StateMax.HP, user.Livelihood.Thirsty);
                    messages.Add (TextUtil.GetColorWord ("你的生命力正在下降！！", TextUtil.ColorFront.Red));
                }
                else if (user.Livelihood.Full < -50)
                {
                    user.StateCurrent.HP += UtilityExperence.DecutLivelihood (user.StateMax.HP, user.Livelihood.Full);
                    messages.Add (TextUtil.GetColorWord ("你的生命力正在下降！！", TextUtil.ColorFront.Red));
                }
                if (user.StateCurrent.HP <= 0)
                {
                    var effsetting = new UserEffectiveSettings (EffectiveType.Dying);
                    var result = actionEffect.InitialEffect (user, effsetting);
                    messages.Add (result);
                }

                if (messages.Count () > 0)
                    msgmgr.SendToPPL (string.Join ("\r\n", messages), user.Name, false);

                if (user.Settings.AutoEatDrink)
                {
                    if (user.Livelihood.Full < 0) AutoEatDrink (user, ObjectActionsVerb.CanEat);
                    if (user.Livelihood.Thirsty < 0) AutoEatDrink (user, ObjectActionsVerb.CanDrink);
                }
            }
        }
        private void AutoEatDrink (UserInfo user, ObjectActionsVerb actionVerb)
        {
            var obj = user.Things.Where (x => x.AcceptVerbs.HasFlag (actionVerb)).FirstOrDefault ();
            if (obj != null)
            {
                var room = worldManager.GetRoom (user);
                ActionKeyParameter papa = new ActionKeyParameter (user, room);
                obj.ReActCommand (papa, obj);
            }
        }
        private void AutoIncreaseStatus ()
        {
            //PPL自動恢復
            var users = userManager.GetAllUser ().Where (x =>
                !x.Status.HasFlag (CreatureStatus.Dying) &&
                (x.StateCurrent.HP != x.StateMax.HP || x.StateCurrent.MV != x.StateMax.MV || x.StateCurrent.Mana != x.StateMax.Mana));
            foreach (var user in users)
            {
                float statusRate = 0.4F; //正常回復，數值20時，大約十次近全滿
                if (user.Status.HasFlag (CreatureStatus.Sleep)) statusRate = 1; //睡覺回復，數值20時，四次全滿
                if (user.Status.HasFlag (CreatureStatus.Rest)) statusRate = 0.6F; //休息回復，數值20時，大約六次近全滿
                user.StateCurrent.MV = FormulaOfHealth (user.StateCurrent.MV, user.StateMax.MV, user.Attribute.Dexterity, statusRate);
                user.StateCurrent.HP = FormulaOfHealth (user.StateCurrent.HP, user.StateMax.HP, user.Attribute.Constitution, statusRate);
                user.StateCurrent.Mana = FormulaOfHealth (user.StateCurrent.Mana, user.StateMax.Mana, user.Attribute.Wisdom, statusRate);
            }
            //Mob自動恢復
            var mobs = worldManager.AllMobs.Where (x =>
                x.StateCurrent.HP != x.StateMax.HP || x.StateCurrent.MV != x.StateMax.MV || x.StateCurrent.Mana != x.StateMax.Mana);
            foreach (var mob in mobs)
            {
                float statusRate = 0.4F; //正常回復
                if (mob.Status.HasFlag (CreatureStatus.Sleep)) statusRate = 1;
                if (mob.Status.HasFlag (CreatureStatus.Rest)) statusRate = 0.6F;
                mob.StateCurrent.MV = FormulaOfHealth (mob.StateCurrent.MV, mob.StateMax.MV, mob.Attribute.Dexterity, statusRate);
                mob.StateCurrent.HP = FormulaOfHealth (mob.StateCurrent.HP, mob.StateMax.HP, mob.Attribute.Constitution, statusRate);
                mob.StateCurrent.Mana = FormulaOfHealth (mob.StateCurrent.Mana, mob.StateMax.Mana, mob.Attribute.Wisdom, statusRate);
            }
        }
        private int FormulaOfHealth (int currentValue, int maxValue, int baseNum, float statusRate)
        {
            decimal addoffset = 10;
            decimal rate = ((baseNum + addoffset) / (baseNum + addoffset + maxValue)) * maxValue;
            var extraVolume = (int) ((maxValue * rate) / 100) * statusRate;
            currentValue += (int) extraVolume;
            if (maxValue < currentValue)
                return maxValue;
            else
                return currentValue;
        }

        private void MobMurmur ()
        {
            var mobs = worldManager.AllMobs.Where (x => x.Murmur != null && x.Murmur.Length > 0 && !x.Status.HasFlag (CreatureStatus.Fighting)).ToList ();
            mobs.ForEach (x =>
            {
                var RankingMove = rnd.Next (4);
                if (RankingMove < 1 && !x.Status.HasFlag (CreatureStatus.Fighting))
                {
                    string message = x.Murmur[rnd.Next (x.Murmur.Length)].Trim ();
                    if (!string.IsNullOrWhiteSpace (message))
                    {
                        BroadcastToRoom (message, x.Geography.RoomID);
                    }
                }
            });
        }

        #region Check is Same place
        private bool isSameArea (WorldInfra FromGeo, WorldInfra ToGeo)
        {
            return (FromGeo.World == ToGeo.World &&
                FromGeo.Region == ToGeo.Region &&
                FromGeo.Area == ToGeo.Area);
        }
        private bool isSameRegion (WorldInfra FromGeo, WorldInfra ToGeo)
        {
            return (FromGeo.World == ToGeo.World &&
                FromGeo.Region == ToGeo.Region);
        }
        private bool isSameWorld (WorldInfra FromGeo, WorldInfra ToGeo)
        {
            return (FromGeo.World == ToGeo.World);
        }
        #endregion end of Check is Same place

        private void MobMovement ()
        {
            var mobs = worldManager.AllMobs.Where (x => x.AutoMovement &&
                !(x.Status.HasFlag (CreatureStatus.Fighting) || x.Status.HasFlag (CreatureStatus.Sleep) || x.Status.HasFlag (CreatureStatus.Rest) ||
                    x.Status.HasFlag (CreatureStatus.Dying))
            ).ToList ();
            mobs.ForEach (x =>
            {
                var fromRoom = worldManager.GetRoom (x.Geography.RoomID);
                List<ExitSetting> RoomExits = fromRoom.Exits.Where (y => y.gateStatus == GateStatus.Open).ToList ();
                if (rnd.Next (4) < 1 && RoomExits.Count () > 0)
                {
                    Room toRoom = null;
                    ExitSetting exit = null;

                    exit = RoomExits.Random ().FirstOrDefault ();
                    toRoom = worldManager.GetRoom (exit.RoomID);

                    if (exit != null && toRoom != null)
                    {
                        fromRoom.Mobs.Remove (x);
                        x.Geography.RoomID = toRoom.RoomID;
                        x.Geography.Area = toRoom.Geography.Area;
                        x.Geography.Region = toRoom.Geography.Region;
                        toRoom.Mobs.Add (x);
                        var t1 = BroadcastToRoom ($"{x.UserDescriptionAndID}從{exit.Dimension.GetDescriptionText()}離開。", fromRoom.RoomID);
                        var t2 = BroadcastToRoom ($"{x.UserDescriptionAndID}從{exit.Dimension.UpSideDownName()}進來。", toRoom.RoomID);
                        Task.WhenAll (t1, t2).GetAwaiter ().GetResult ();
                    }
                }
            });
        }

        private Task BroadcastToRoom (string message, int RoomID)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.Msg = message;
            bo.ExcludeAsleepPPL = true;
            bo.WorldChannel = new WorldInfra ()
            {
                RoomID = RoomID
            };
            return msgmgr.BroadcastToRoom (message, RoomID);
        }

        //todo:要做天氣變化
        private Task BroadcastWordMessage ()
        {
            string msg = $"[1;33m{WorldSystemMessage[rnd.Next (WorldSystemMessage.Length)]}[0m";
            return msgmgr.BroadcastToWorld (msg, EWorld.亞爾斯隆世界);
        }
        private readonly string[] WorldSystemMessage = new string[]
        {
            "💥突然地動山搖、海嘯四起。",
            "🔥硝煙四起、微微的震動從遠處遠處傳來。",
            "你覺得天氣變暖和了！",
            "你覺得天氣變熱了點！",
            "雨漸漸停了...",
            "你覺得天氣變得涼快一點..",
            "你覺得天氣變涼快了！"
        };
    }
}
