using Keeper.MercuryCore.Elf;
using Rest.MercuryCore.God.Interface;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class LoaderCollectionExtensions
    {
        public static IServiceCollection LoadCoreElfObjects (this IServiceCollection services)
        {
            services.AddSingleton<IElfManager, ElfManager> ()
                .AddSingleton<IMessageManager, MessageManager> ()
                .AddSingleton<IGodOfDeath, GodOfDeath> ()
                .AddSingleton<IGodOfWar, GodOfWar> ();

            return services;
        }

    }
}
