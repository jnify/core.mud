using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.Elf
{
    public class GodOfDeath : IGodOfDeath
    {
        private readonly IMobDefinationLoader mobloader;
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IUserManager userManager;
        private readonly IMessageManager msgMgr;
        public GodOfDeath (IMobDefinationLoader mobloader, ILoadCommonObject loadCommonObj, IUserManager userManager, IMessageManager msgMgr)
        {
            this.mobloader = mobloader;
            this.loadCommonObj = loadCommonObj;
            this.userManager = userManager;
            this.msgMgr = msgMgr;
        }

        public string MakeMobCorpse (Room room, MobInfo defance)
        {
            string getFromCorpseMsg = default;
            List<CommonObject> dropThings = new List<CommonObject> ();
            var md = mobloader.GetDef (((MobInfo) defance).Name);
            Random rnd = new Random ();
            //把會掉落的物品另外取出來
            defance.Things.ForEach (x =>
            {
                var objdef = md.ThingDef.Where (y => y.SystemCode == x.SystemCode).FirstOrDefault ();
                if (!string.IsNullOrWhiteSpace (objdef.SystemCode))
                {
                    if (objdef.RateOfDrop == 100 || (objdef.RateOfDrop > 0 && rnd.Next (1, 99) < objdef.RateOfDrop))
                    {
                        dropThings.Add (x);
                    }
                }
            });
            //產生屍體，並把掉落物品放進去。
            var corpse = loadCommonObj.GenerateCommonObject ("SystemOnly-Corpse");
            corpse.ShortName = $"{defance.Settings.Name}的屍體";
            corpse.DescriptionDetail = $"經過了激烈的爭戰，{defance.Settings.Name}終就還是逃不了變成屍體的命運。";
            corpse.DispearTS = TimeSpan.FromMinutes (30);

            var attacker = userManager.GetUser (defance.KillBy);
            if (dropThings.Count () > 0 && attacker != null && attacker.Settings.AutoGet)
            {
                dropThings.ForEach (obj =>
                {
                    attacker.AddThing (obj);
                    getFromCorpseMsg += $"\r\n{attacker.UserDescriptionAndID}從{corpse.ShortName}拿出{obj.ShortName}。";
                });
                dropThings.Clear ();
            }
            corpse.Things = dropThings;
            room.AddThing (corpse);
            return getFromCorpseMsg;
        }

    }
}
