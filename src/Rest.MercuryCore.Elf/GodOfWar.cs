using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.Elf
{
    public class GodOfWar : IGodOfWar
    {
        //todo:要做自動逃跑
        private readonly object LockDiedMobs = new object ();
        private Dictionary<string, DateTime> ExecuteTime = new Dictionary<string, DateTime> ();
        private TimeSpan ts_AutoHelpAttackCheck = TimeSpan.FromMilliseconds (500); //自動幫忙攻擊檢查間隔
        private TimeSpan ts_GrudgesAttackCheck = TimeSpan.FromMilliseconds (500); //因為記仇而自動攻擊檢查間隔
        private TimeSpan ts_AutoAttackCheck = TimeSpan.FromMilliseconds (100); //Mob + PPL自動攻擊檢查間隔
        private TimeSpan ts_MobRebornLCheck = TimeSpan.FromSeconds (10); //Mob重生檢查

        private readonly object LockBlockTime = new object ();
        private readonly ILogger<GodOfWar> logger;
        private readonly Random rnd = new Random ();
        private readonly IUserManager userManager;
        private readonly IWorldManager worldManager;
        private readonly IMessageManager msgmgr;
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IMobDefinationLoader mobloader;
        private readonly MobGenerate genMob;

        public GodOfWar (ILogger<GodOfWar> logger, IUserManager userManager, IWorldManager wordmgr, IMessageManager msgmgr, ILoadCommonObject loadCommonObj,
            IMobDefinationLoader mobloader, MobGenerate genMob)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.worldManager = wordmgr;
            this.msgmgr = msgmgr;
            this.loadCommonObj = loadCommonObj;
            this.mobloader = mobloader;
            this.genMob = genMob;

            Task.Run (() =>
            {
                while (true)
                {
                    try
                    {
                        InfinityLoop ();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError (ex.Message);
                    }
                    Thread.Sleep (500);
                }
            });

        }

        private bool CanExecute (string bT_Key, TimeSpan ts)
        {
            if (!ExecuteTime.ContainsKey (bT_Key))
            {
                lock (LockBlockTime)
                {
                    if (!ExecuteTime.ContainsKey (bT_Key))
                    {
                        ExecuteTime.Add (bT_Key, DateTime.Now);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                var latestDT = ExecuteTime[bT_Key];
                if (DateTime.Now >= latestDT.Add (ts))
                {
                    lock (LockBlockTime)
                    {
                        latestDT = ExecuteTime[bT_Key];
                        if (DateTime.Now >= latestDT.Add (ts))
                        {
                            ExecuteTime[bT_Key] = DateTime.Now;
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                }
                else
                {
                    return false;
                }
            }
        }
        private void InfinityLoop ()
        {
#if Debug
            ts_MobRebornLCheck = TimeSpan.FromMilliseconds (1000);
#endif
            if (CanExecute ("Main_MobReborn", ts_MobRebornLCheck)) { Main_MobReborn (); }

            if (CanExecute ("Main_AutoHelpAttack", ts_AutoHelpAttackCheck)) { Main_AutoHelpAttack (); }

            if (CanExecute ("Main_AutoAttackCheck", ts_AutoAttackCheck)) { Main_AutoAttackCheck (); }

            if (CanExecute ("Main_GrudgesAttackCheck", ts_GrudgesAttackCheck)) { Main_GrudgesAttackCheck (); }

        }
        private void Main_AutoAttackCheck ()
        {
            //Mob一律自動攻擊，忽略Private的Message。
            var Mobs = worldManager.AllMobs.Where (x => x.Status.HasFlag (CreatureStatus.Fighting) && x.NextAutoFightingTime < DateTime.Now).ToList ();
            //List<Task> autoFightTask = new List<Task> ();
            Mobs.ForEach (x =>
            {
                if (x.FightingInfo.FightingWith.Status.HasFlag (CreatureStatus.Dying))
                {
                    //對象瀕死，不能鞭屍
                    x.EndBattle ();
                }
                else
                {
                    Task.Run (() =>
                    {
                        //先把Defance的名字取出，因為在DoAttack()之後，Defance會消失
                        string defanceName = x.FightingInfo.FightingWith.Name;
                        var result = x.DoAttack ();
                        return MessageCombat (x, defanceName, result);
                    });
                }
            });
            //開啟自動攻擊的PPL
            var PPLs = userManager.GetAllUser ().Where (x => x.Status.HasFlag (CreatureStatus.Fighting) && x.NextAutoFightingTime < DateTime.Now && x.Settings.AutoFighting).ToList ();
            PPLs.ForEach (x =>
            {
                Task.Run (() =>
                {
                    var result = x.DoAttack ();
                    return MessageCombat (x, "Mob", result);
                });
            });
        }
        public Task MessageCombat (Creature Attacker, string Defence, (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) result)
        {
            if (string.IsNullOrWhiteSpace (result.PublicMessage + result.PrivateAttackerMessage + result.PrivateDefenceMessage)) return Task.CompletedTask;
            Task taskPublic = default;
            Task taskPrivate = default;

            taskPublic = msgmgr.BroadcastToRoom (result.PublicMessage, Attacker.Geography.RoomID);

            if (Attacker.isMob)
            {
                taskPrivate = msgmgr.SendToPPL (result.PrivateDefenceMessage, Defence, false);
            }
            else
            {
                taskPrivate = msgmgr.SendToPPL (result.PrivateAttackerMessage, Attacker.Name, false);
            }
            return taskPublic.ContinueWith (x => taskPrivate);
        }
        private void Main_AutoHelpAttack ()
        {
            //取得有PPL有在戰鬥的所有房間號碼
            var PPLRoomIDs = userManager.GetAllUser ().Where (x => x.Status.HasFlag (CreatureStatus.Fighting)).Select (x => x.Geography.RoomID).Distinct ();
            var MobRoomIDs = worldManager.AllMobs.Where (x => x.Status.HasFlag (CreatureStatus.Fighting)).Select (x => x.Geography.RoomID).Distinct ();
            var RoomIDs = PPLRoomIDs.Union (MobRoomIDs).ToList ();
            RoomIDs.ForEach (RoomID =>
            {
                //取出房間成員
                var users = userManager.GetRoomUser (RoomID);

                //取出有開啟自動幫忙攻擊，且未處於攻擊的人，且有組隊，且不是處於Rest or Sleep
                var enableAutoHelpPPL = users.Where (user =>
                        !(user.Status.HasFlag (CreatureStatus.Fighting) || user.Status.HasFlag (CreatureStatus.Rest) || user.Status.HasFlag (CreatureStatus.Sleep) || user.Status.HasFlag (CreatureStatus.Dying)) &&
                        user.Settings.AutoHelpFighting && userManager.GetGroup (user.Name) != default)
                    .ToList ();
                if (enableAutoHelpPPL.Count () > 0)
                {
                    enableAutoHelpPPL.ForEach (EnablePPL =>
                    {
                        //取得同隊
                        var group = userManager.GetGroup (EnablePPL.Name);
                        //取得幫誰攻擊,房間成員，未處於戰鬥，同隊，隨機取一個
                        var mainAttacker = users.Where (groupUser =>
                            groupUser.Status.HasFlag (CreatureStatus.Fighting) && group.UserGroup.Contains (groupUser.Name)).ToList ().Random ().FirstOrDefault ();
                        if (mainAttacker != null && mainAttacker.FightingInfo != null && mainAttacker.Geography.RoomID == EnablePPL.Geography.RoomID &&
                            mainAttacker.FightingInfo.FightingWith.StateCurrent.HP > 1)
                        {
                            //取得要向哪一個Mob進行自動攻擊
                            var mob = mainAttacker.FightingInfo.FightingWith;

                            if (EnablePPL.StartBattle (mob))
                                Task.Run (() =>
                                {
                                    var result = EnablePPL.DoAttack ();
                                    string startAttMsg = $"[1;31m{EnablePPL.UserDescriptionAndID}開始攻擊(7){mob.Settings.Name}！！！[0m\r\n";
                                    result.PublicMessage = $"{startAttMsg}{result.PublicMessage}";
                                    return MessageCombat (EnablePPL, "Mob", result);
                                });
                        }
                    });
                }

                //取出房間Mobs
                var mobs = worldManager.GetRoom (RoomID).Mobs;
                //取出會自動幫忙攻擊，且未處於攻擊的Mob
                var enableAutoHelpMOb = mobs.Where (mob => !mob.Status.HasFlag (CreatureStatus.Fighting) && mob.Settings.AutoHelpFighting).ToList ();
                if (enableAutoHelpMOb.Count () > 0)
                {
                    //取得幫誰攻擊
                    var mainMob = mobs.Where (mob => mob.Status.HasFlag (CreatureStatus.Fighting)).FirstOrDefault ();
                    if (mainMob != null && mainMob.FightingInfo.FightingWith.StateCurrent.HP > 1)
                    {
                        //取得要向哪一個PPL進行自動攻擊
                        var user = mainMob.FightingInfo.FightingWith;
                        enableAutoHelpMOb.ForEach (EnableMob =>
                        {
                            var defancerName = user.Name;
                            if (EnableMob.StartBattle (user))
                                Task.Run (() =>
                                {
                                    var result = EnableMob.DoAttack ();
                                    result.PublicMessage =
                                        $"[1;31m{EnableMob.Settings.Name}開始攻擊(8){user.UserDescriptionAndID}！！！[0m\r\n{result.PublicMessage}";
                                    return MessageCombat (EnableMob, defancerName, result);
                                });
                        });
                    }
                }
            });

            //取得所有的PPL房間號碼
            PPLRoomIDs = userManager.GetAllUser ().Select (x => x.Geography.RoomID).Distinct ();
            PPLRoomIDs.ToList ().ForEach (RoomID =>
            {
                //被Mob攻擊的PPL，但未處於攻擊狀態的要開始反擊。
                //取出MOB，其攻擊為PPL，但該PPL未處於攻擊狀態的所有MOB
                var mobs = worldManager.GetRoom (RoomID).Mobs.Where (mob =>
                    mob.Status.HasFlag (CreatureStatus.Fighting) &&
                    mob.FightingInfo != null && mob.FightingInfo.FightingWith != null &&
                    !mob.FightingInfo.FightingWith.Status.HasFlag (CreatureStatus.Fighting)).ToList ();
                mobs.ForEach (mob =>
                {
                    var PPL = mob.FightingInfo.FightingWith;
                    if (!PPL.Status.HasFlag (CreatureStatus.Fighting))
                    {
                        if (PPL.StartBattle (mob))
                            msgmgr.BroadcastToRoom ($"[1;31m{PPL.UserDescriptionAndID}開始攻擊(9){mob.Settings.Name}！！！[0m", RoomID);
                    }
                });
                //取出該房間會主動攻擊的Mob，且還沒有攻擊對象的Mob
                _MobAutoAttackPPL (RoomID);
            });
        }
        private void _MobAutoAttackPPL (int RoomID)
        {
            List<MobInfo> mobs = worldManager.GetRoom (RoomID).Mobs.Where (mob => mob.MobSettings != null && mob.MobSettings.AutoAttackUser && !mob.Status.HasFlag (CreatureStatus.Fighting)).ToList ();
            mobs.ForEach (mob =>
            {
                if (!mob.Status.HasFlag (CreatureStatus.Fighting))
                {
                    //隨機取某一個PPL，要排除Dying。
                    var user = userManager.GetRoomUser (RoomID).Where (x => !x.Status.HasFlag (CreatureStatus.Dying)).ToList ().Random ().FirstOrDefault ();
                    if (user != null && mob.Geography.RoomID == user.Geography.RoomID)
                    {
                        Task.Run (() =>
                        {
                            if (mob.StartBattle (user))
                            {
                                List<string> publicMessage = new List<string> ();
                                publicMessage.Add ($"[1;31m{mob.Settings.Name}開始攻擊(A){user.UserDescriptionAndID}！！！[0m");
                                string defanceName = user.Name;
                                var result = mob.DoAttack ();
                                publicMessage.Add (result.PublicMessage);

                                if (result.Success)
                                {
                                    if (!user.Status.HasFlag (CreatureStatus.Fighting) && user.StartBattle (mob))
                                        publicMessage.Add ($"[1;31m{user.UserDescriptionAndID}開始攻擊(B){mob.Settings.Name}！！！[0m");
                                }

                                result.PublicMessage = string.Join ("\r\n", publicMessage);
                                MessageCombat (mob, defanceName, result);
                            }
                        });
                    }
                }
            });
        }
        private void Main_GrudgesAttackCheck ()
        {
            //取得所有的PPL房間號碼
            var PPLRoomIDs = userManager.GetAllUser ().Select (x => x.Geography.RoomID).Distinct ();
            PPLRoomIDs.ToList ().ForEach (RoomID =>
            {
                worldManager.GetRoom (RoomID).Mobs.Where (mob => !mob.Status.HasFlag (CreatureStatus.Fighting)).ToList ().ForEach (mob =>
                {
                    var roomUsers = userManager.GetRoomUser (RoomID);
                    var result = worldManager.CheckGrudges (mob, roomUsers.Select (user => user.Name).ToList ());
                    var defander = roomUsers.Where (user => user.Name == result.GrudgesName).FirstOrDefault ();
                    if (defander != null && mob.StartBattle (defander))
                    {
                        string startBettelMsg = $"\r\n[1;31m{mob.UserDescriptionAndID}開始攻擊(C){defander.UserDescriptionAndID}。[0m";
                        msgmgr.BroadcastToRoom (startBettelMsg, RoomID);
                    }
                });
            });
        }
        private void Main_MobReborn ()
        {
            List<string> mobIDList = new List<string> ();
            var dieList = worldManager.DiedMobs_Get ().Where (x => x.RebornTime < DateTime.Now);
            if (dieList != null && dieList.Count () > 0)
            {
                foreach (var item in dieList)
                {
                    mobIDList.Add (item.ID);
                    var md = mobloader.GetDef (item.Name);
                    if (md != null)
                    {
                        genMob.Generate (md, item.BornInRoomID);
                        msgmgr.BroadcastToRoom ($"{item.Settings.Name}突然出現在你面前。", item.BornInRoomID);
                    }
                }
            }
            mobIDList.ForEach (x =>
            {
                worldManager.DiedMobs_Remove (x);
            });
        }
    }
}
