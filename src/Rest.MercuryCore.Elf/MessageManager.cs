using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Elf
{
    public class MessageManager : IMessageManager
    {
        private Func<BroadcastObject, Task> broadcast { get; set; }
        private Func<List<string>, int, Task> displayPrompt { get; set; }
        private readonly ILogger<MessageManager> logger;
        public MessageManager (ILogger<MessageManager> logger)
        {
            this.logger = logger;
        }

        public void Bind (Func<BroadcastObject, Task> broadcast, Func<List<string>, int, Task> displayPrompt)
        {
            this.broadcast = broadcast;
            this.displayPrompt = displayPrompt;
        }
        public void BindDisplayPrompt (Func<List<string>, int, Task> displayPrompt)
        {
            this.displayPrompt = displayPrompt;
        }
        public Task BroadcastToRoom (string message, int RoomID, bool ExcludeAsleepPPL = true, string ExcludePPL = default, bool DisplayPrompt = true)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.DisplayPrompt = DisplayPrompt;
            bo.Msg = message;
            bo.ExcludeAsleepPPL = ExcludeAsleepPPL;
            bo.ExcludePPL = ExcludePPL;
            bo.WorldChannel = new WorldInfra ()
            {
                RoomID = RoomID
            };
            return broadcast (bo);
        }

        public Task BroadcastToWorld (string message, EWorld world)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.Msg = message;
            bo.ExcludeAsleepPPL = false;
            bo.WorldChannel = new WorldInfra ()
            {
                World = world.ToString ()
            };
            return broadcast (bo);
        }
        public Task SendToPPL (string message, string UserName, bool ExcludeAsleepPPL = true, bool DisplayPrompt = true)
        {
            message += "\r\n";
            var bo = new BroadcastObject ()
            {
                Msg = message,
                ReceiptName = UserName,
                ExcludeAsleepPPL = ExcludeAsleepPPL
            };
            bo.DisplayPrompt = DisplayPrompt;
            return broadcast (bo);
        }
        public Task DoDisplayPrompt (List<string> UserNames)
        {
            return displayPrompt (UserNames, 0);
        }

        public Task DisplayPrompt (int RoomID)
        {
            return displayPrompt (null, RoomID);
        }
    }
}
