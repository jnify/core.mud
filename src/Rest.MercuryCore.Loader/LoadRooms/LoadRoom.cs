using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Loader.LoadRooms
{
    public class LoadRoom : IFileLoader
    {
        public class FileRoomJsonModel
        {
            public WorldInfra AreaGeography { get; set; }
            public List<RoomModel> Rooms { get; set; }
            public RoomCoverType? DefaultCeiling { get; set; }
        }
        public class CommonObjNameWithAction
        {
            public string SystemCode { get; set; }
            public string ActionKey { get; set; }
        }

        public class RoomModel : Room
        {
            public string RoomTitle { get; set; }
            public List<CommonObjNameWithAction> CommonObjects { get; set; }
            public List<string> Equipments { get; set; }
            public RoomCoverType? DefaultCeiling { get; set; }
        }
        private readonly ILogger<LoadRoom> logger;
        private IWorldManager wordmgr;
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ();
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IActionLoader actionLoader;
        public LoadRoom (ILogger<LoadRoom> logger, IWorldManager wordmgr, ILoadCommonObject loadCommonObj, IActionLoader actionLoader)
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.WriteIndented = true;
            this.wordmgr = wordmgr;
            this.logger = logger;
            this.loadCommonObj = loadCommonObj;
            this.actionLoader = actionLoader;
            LoadFile ();
        }

        public void LoadFile ()
        {
            string path = "DocumentWorld/Data/Room";
            if (Directory.Exists (path))
            {
                string[] folders = Directory.GetDirectories (path);
                foreach (var subpath in folders)
                {
                    string LastFolderName = subpath.Split ("/").Last ();
                    var files = Directory.GetFiles (subpath, "*.json");
                    foreach (string fileName in files)
                    {
                        try
                        {
                            LoadFile (fileName);
                        }
                        catch (Exception ex)
                        {
                            this.logger.LogDebug ($"Load Room from [{LastFolderName}], Get exception:", ex.Message);
                        }
                    }
                }
            }
        }
        public void LoadFile (string FullFileName)
        {
            string fs = File.ReadAllText (FullFileName);
            var t = JsonSerializer.Deserialize<FileRoomJsonModel> (fs, jsoptions);
            t.Rooms.ForEach (roomModel =>
            {
                //處理系統的Injection
                roomModel.BindWordObject (wordmgr);
                //處理預設值
                roomModel.Geography = new WorldInfra ()
                {
                    World = t.AreaGeography.World,
                    Region = t.AreaGeography.Region,
                    Area = t.AreaGeography.Area,
                    RoomID = roomModel.RoomID,
                    RoomTitle = roomModel.RoomTitle
                };
                //處理天花板
                if (roomModel.Ceiling == RoomCoverType.None)
                    if (t.DefaultCeiling.HasValue)
                        roomModel.Ceiling = t.DefaultCeiling.Value;
                    else
                        roomModel.Ceiling = RoomCoverType.Natural;
                ///---------------------------------
                if (roomModel.Mobs == null) roomModel.Mobs = new List<MobInfo> () { };
                if (roomModel.CommonObjects != null)
                {
                    roomModel.CommonObjects.ForEach (y =>
                    {
                        var commonobj = loadCommonObj.GenerateCommonObject (y.SystemCode);
                        if (commonobj != null)
                        {
                            if (!string.IsNullOrWhiteSpace (y.ActionKey))
                            {
                                var fun = actionLoader.GetCommonObjectAction (y.ActionKey);
                                if (fun != null)
                                {
                                    commonobj.SetActionCommand (fun);
                                }
                            }
                            roomModel.Things.Add (commonobj);
                        }
                        else
                        {
                            logger.LogError ($"CommonObject defined not found {y}");
                        }
                    });
                }
                if (roomModel.Equipments != null)
                {
                    roomModel.Equipments.ForEach (y =>
                    {
                        var commonObjDef = loadCommonObj.GenerateCommonObject (y);
                        if (commonObjDef != null)
                        {
                            roomModel.Things.Add ((CommonObject) commonObjDef);
                        }
                        else
                        {
                            logger.LogError ($"CommonObject defined not found {y}");
                        }
                    });
                }
                wordmgr.SetRoom (roomModel);
            });
        }
    }
}
