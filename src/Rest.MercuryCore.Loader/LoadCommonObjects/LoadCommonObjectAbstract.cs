using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public abstract class LoadCommonObjectAbstract : IFileLoader
    {
        protected static Dictionary<string, bool> commonLoaderStatus = new Dictionary<string, bool> ();
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ();
        private readonly ILogger logger;
        protected static Dictionary<string, CommonObjectDefined> commondefineds = new Dictionary<string, CommonObjectDefined> ();

        protected LoadCommonObjectAbstract (ILogger logger)
        {
            this.logger = logger;
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.WriteIndented = true;
            commonLoaderStatus.Add (logger.ToString (), false);
        }
        public abstract string Folder { get; }
        public abstract Func<CommonObjectDefined, CommonObjectDefined> ConvertDefination { get; }
        public void LoadFile ()
        {
            LoadFile (Folder);
            commonLoaderStatus[logger.ToString ()] = true;
        }
        public void LoadFile (string path)
        {
            if (Directory.Exists (path))
            {
                string[] folders = Directory.GetDirectories (path);
                foreach (var subpath in folders)
                {
                    LoadFile (subpath);
                }
                string LastFolderName = path.Split ("/").Last ();
                var files = Directory.GetFiles (path, "*.json");
                foreach (string fileName in files)
                {
                    try
                    {
                        LoadFileIntoCache (fileName);
                    }
                    catch (Exception ex)
                    {
                        this.logger.LogInformation ($"Load CommonObject Start [{LastFolderName}], Get exception:", ex.Message);
                    }
                }
            }
        }
        private void LoadFileIntoCache (string FullFileName)
        {
            string fs = File.ReadAllText (FullFileName);
            var t = JsonSerializer.Deserialize<List<CommonObjectDefined>> (fs, jsoptions);
            t.ForEach (x =>
            {
                CommonObjectDefined tmpdef = ConvertDefination (x);
                if (commondefineds.ContainsKey (tmpdef.SystemCode.ToUpperInvariant ()))
                {
                    Console.WriteLine ($"Duplicate SystemCode: [{tmpdef.SystemCode}] in File: {FullFileName}");
                }
                else
                {
                    commondefineds.Add (tmpdef.SystemCode.ToUpperInvariant (), tmpdef);
                }
            });
        }

        public CommonObjectDefined GetDef (string systemCode)
        {
            string UpperSystemCode = systemCode.ToUpperInvariant ();
            if (commondefineds.ContainsKey (UpperSystemCode))
                return commondefineds[UpperSystemCode];
            else
                return null;
        }

    }
}
