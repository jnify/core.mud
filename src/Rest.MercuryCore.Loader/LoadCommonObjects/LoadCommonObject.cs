using System;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public class LoadCommonObject : ILoadCommonObject
    {
        private readonly static Random rnd = new Random ();
        private readonly CommonObjectCanDrink commonobject;
        private readonly IActionLoader actionLoader;
        public LoadCommonObject (CommonObjectCanDrink commonobject, IActionLoader actionLoader)
        {
            this.commonobject = commonobject;
            this.actionLoader = actionLoader;
        }

        private CommonObject GetCommonObject (CommonObjectDefined def)
        {
            CommonObject rtn = default;
            if (def != null)
            {
                var fun = actionLoader.GetCommonObjectAction (def.ActionKey);
                rtn = ObjectExtension.DeepClone<CommonObjectDefined, CommonObject> (def);
                rtn.ID = Guid.NewGuid ().ToString ();
                rtn.SetActionCommand (fun);
            }
            return rtn;
        }
        private CommonObject GetEquipment (CommonObjectDefined def)
        {
            if (def != null)
            {
                CommonObject rtn = default;
                rtn = ObjectExtension.DeepClone<CommonObjectDefined, CommonObject> (def);
                rtn.ID = Guid.NewGuid ().ToString ();

                int DamageCenter = def.Volume + rnd.Next (-def.Range, def.Range);
                int Offset = def.Precision / 2;
                if (def.Type == WeaponType.None)
                {
                    //防具
                    rtn.DefenceMax = DamageCenter + Offset;
                    rtn.DefenceMin = DamageCenter - Offset;
                    if (rtn.DefenceMin < 1) rtn.DefenceMin = 1;
                    if (rtn.DefenceMin > rtn.DefenceMax) rtn.DefenceMax = rtn.DefenceMin;
                }
                else
                {
                    //武器
                    rtn.DamageMax = DamageCenter + Offset;
                    rtn.DamageMin = DamageCenter - Offset;
                    if (rtn.DamageMin < 1) rtn.DamageMin = 1;
                    if (rtn.DamageMin > rtn.DamageMax) rtn.DamageMax = rtn.DamageMin;
                }
                return rtn;
            }
            else
            {
                return default;
            }
        }
        public CommonObject GenerateCommonObject (string SystemCode)
        {
            var def = commonobject.GetDef (SystemCode);
            if (def == null) return null;
            switch (def.ObjectType)
            {
                case CommonObjectType.Weapon:
                case CommonObjectType.Equipment:
                    return GetEquipment (def);
                default:
                    return GetCommonObject (def);
            }
        }
        public CommonObjectDefined GetDef (string SystemCode)
        {
            return commonobject.GetDef (SystemCode);
        }
    }
}
