using System;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public class CommonObjectCanWear : LoadCommonObjectAbstract
    {
        public CommonObjectCanWear (ILogger<CommonObjectCanWear> logger) : base (logger) { LoadFile (); }

        public override string Folder => "DocumentWorld/Definition/CommonObjectCanWear";
        public override Func<CommonObjectDefined, CommonObjectDefined> ConvertDefination => convert;

        private CommonObjectDefined convert (CommonObjectDefined x)
        {
            x.AcceptVerbs = ObjectActionsVerb.CanDrop | ObjectActionsVerb.CanGet | ObjectActionsVerb.CanWear;
            switch (x.Position)
            {
                case EquipmentAllowPosition.Ligth:
                    x.ObjectType = CommonObjectType.Tools;
                    break;
                case EquipmentAllowPosition.HandLeft:
                case EquipmentAllowPosition.HandRight:
                    x.ObjectType = CommonObjectType.Weapon;
                    break;
                default:
                    x.ObjectType = CommonObjectType.Equipment;
                    break;
            }
            return x;
        }
    }
}
