using System;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public class CommonObjectCanDrink : LoadCommonObjectAbstract
    {
        public CommonObjectCanDrink (ILogger<CommonObjectCanDrink> logger) : base (logger)
        {
            LoadFile ();
        }

        public override string Folder => "DocumentWorld/Definition/CommonObjectCanDrink";
        public override Func<CommonObjectDefined, CommonObjectDefined> ConvertDefination => convert;

        private CommonObjectDefined convert (CommonObjectDefined x)
        {
            if (string.IsNullOrWhiteSpace (x.ActionKey)) x.ActionKey = "Drink_CommonAction";
            x.AcceptVerbs = ObjectActionsVerb.CanDrop | ObjectActionsVerb.CanGet | ObjectActionsVerb.CanDrink;
            return x;
        }
    }
}
