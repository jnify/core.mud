using System;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public class CommonObjectCanEat : LoadCommonObjectAbstract
    {
        public CommonObjectCanEat (ILogger<CommonObjectCanEat> logger) : base (logger) { LoadFile (); }

        public override string Folder => "DocumentWorld/Definition/CommonObjectCanEat";
        public override Func<CommonObjectDefined, CommonObjectDefined> ConvertDefination => convert;

        private CommonObjectDefined convert (CommonObjectDefined x)
        {
            x.ActionKey = "Eat_CommonAction";
            x.AcceptVerbs = ObjectActionsVerb.CanDrop | ObjectActionsVerb.CanGet | ObjectActionsVerb.CanEat;
            x.ObjectType = CommonObjectType.Food;
            return x;
        }
    }
}
