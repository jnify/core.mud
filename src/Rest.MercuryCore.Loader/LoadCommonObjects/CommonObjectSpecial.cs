using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.Loader.LoadCommonObjects
{
    public class CommonObjectSpecial : LoadCommonObjectAbstract
    {
        private readonly IActionLoader actionLoader;
        public CommonObjectSpecial (ILogger<CommonObjectSpecial> logger, IActionLoader actionLoader) : base (logger)
        {
            this.actionLoader = actionLoader;
            LoadFile ();
            FillContainerObject ();
        }

        public override string Folder => "DocumentWorld/Definition/CommonObjectSpecial";
        public override Func<CommonObjectDefined, CommonObjectDefined> ConvertDefination => convert;

        private CommonObjectDefined convert (CommonObjectDefined x)
        {
            return x;
        }

        private Task FillContainerObject ()
        {
            return Task.Run (() =>
            {
                while (commonLoaderStatus.Any (x => x.Value == false) || commonLoaderStatus.Count () < 3)
                {
                    Thread.Sleep (1000);
                }
                var ContainerObject = commondefineds.Where (dic => dic.Value.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGetFrom) &&
                    dic.Value.ContainSystemCode != null && dic.Value.ContainSystemCode.Count () > 0).ToList ();
                ContainerObject.ForEach (dic =>
                {
                    string SystemCode = dic.Key;
                    var def = dic.Value;
                    def.ContainSystemCode.ForEach (x =>
                    {
                        CommonObject rtn = default;
                        var def = GetDef (x);
                        if (def != null)
                        {
                            var fun = actionLoader.GetCommonObjectAction (def.ActionKey);
                            rtn = ObjectExtension.DeepClone<CommonObjectDefined, CommonObject> (def);
                            rtn.ID = Guid.NewGuid ().ToString ();
                            rtn.SetActionCommand (fun);
                            commondefineds[SystemCode].Things.Add (rtn);
                        }
                        else
                        {
                            Console.WriteLine ($" { SystemCode } 還沒載入。 ");
                        }
                    });
                });
            });
        }
    }
}
