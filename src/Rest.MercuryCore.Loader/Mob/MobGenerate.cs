using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Loader.Mob
{
    public class MobGenerate
    {
        private readonly IWorldManager wordmgr;
        private readonly IMessageManager msgMgr;
        private readonly IGodOfDeath godOfDeath;
        private Random rnd = new Random ();
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IActionLoader actionLoader;

        public MobGenerate (IWorldManager wordmgr, IMessageManager msgMgr, ILoadCommonObject loadCommonObj, IGodOfDeath godOfDeath, IActionLoader actionLoader)
        {
            this.wordmgr = wordmgr;
            this.msgMgr = msgMgr;
            this.loadCommonObj = loadCommonObj;
            this.godOfDeath = godOfDeath;
            this.actionLoader = actionLoader;
        }

        public void Generate (MobCommonDefinition MobComDef, int RoomID)
        {
            if (MobComDef == null)
            {
                Console.WriteLine ("Definition is null");
                return;
            }
            var room = wordmgr.GetRoom (RoomID);
            var Geography = new WorldInfra ()
            {
                World = room.Geography.World,
                Region = room.Geography.Region,
                Area = room.Geography.Area,
                RoomID = room.Geography.RoomID,
                RoomTitle = room.Geography.RoomTitle
            };
            MobInfo rtn = ObjectExtension.DeepClone<MobCommonDefinition, MobInfo> (MobComDef);
            rtn.GodOfDead_MakeCorpse = godOfDeath.MakeMobCorpse;
            rtn.BornInRoomID = RoomID;
            rtn.BornInRegion = room.Geography.Region;
            rtn.ID = Guid.NewGuid ().ToString ();
            rtn.Attribute = rtn.Attribute ?? new Attr () { Strength = 15, Wisdom = 15, Intelligent = 15, Constitution = 15, Dexterity = 15 };
            rtn.Geography = Geography;
            rtn.SettingRebornTime = new TimeSpan (0, 0, MobComDef.SettingRebornTimeInSecond);
            rtn.SetGlobalManager (msgMgr, wordmgr);
            rtn.StateCurrent = rtn.StateMax.Copy;
            List<CommonObject> things = new List<CommonObject> ();
            if (rtn.MobSettings == null) rtn.MobSettings = new Mob_Settings ();
            //經驗值為浮動上下3%
            rtn.StateMax.Experience += rnd.Next (rtn.StateMax.Experience * -3 / 100, rtn.StateMax.Experience * 3 / 100);

            if (MobComDef.ThingDef != null)
            {
                MobComDef.ThingDef.ForEach (x =>
                {
                    if (rnd.Next (1, 100) < x.RateOfAppear)
                    {
                        var commonobj = loadCommonObj.GenerateCommonObject (x.SystemCode);
                        if (commonobj != null)
                        {
                            things.Add (commonobj);
                        }
                        else
                        {
                            Console.WriteLine ($"CommonObject defined not found {x.SystemCode}");
                        }
                    }
                });
                rtn.Things = things;
            }

            if (MobComDef.EquipmentDef != null)
            {
                MobComDef.EquipmentDef.ForEach (x =>
                {
                    if (rnd.Next (1, 100) < x.RateOfAppear)
                    {
                        var commonobj = loadCommonObj.GenerateCommonObject (x.SystemCode);
                        if (commonobj != null)
                        {
                            if (commonobj.Type != WeaponType.None)
                            {
                                WearWeapon (rtn, commonobj);
                            }
                            else
                            {
                                WareDefence (rtn, commonobj);
                            }
                        }
                        else
                        {
                            Console.WriteLine ($"CommonObject defined not found {x.SystemCode}");
                        }
                    }
                });
                rtn.Things = things;

            }
            //加入遇見PPL事件
            if (!string.IsNullOrWhiteSpace (MobComDef.MeetPPLActionKey))
            {
                rtn.MeetPPLAction = actionLoader.GetMobAction (MobComDef.MeetPPLActionKey);
                if (rtn.MeetPPLAction == null)
                {
                    Console.WriteLine ($"MeetPPLActionKey:{MobComDef.MeetPPLActionKey}未定義。");
                    return;
                }
            }
            room.Mobs.Add (rtn);
        }

        private void WareDefence (MobInfo mob, CommonObject obj)
        {
            //移除身上現有的裝備
            switch (obj.Position)
            {
                case EquipmentAllowPosition.Head:
                case EquipmentAllowPosition.Ligth:
                case EquipmentAllowPosition.Body:
                case EquipmentAllowPosition.Gloves:
                case EquipmentAllowPosition.Cape:
                case EquipmentAllowPosition.Belt:
                case EquipmentAllowPosition.Leggings:
                    RemoveEquipmentFromBodyToThings (mob, obj.Position);
                    MoveEquipmentFromThingsToEQarea (mob, obj);
                    break;
                case EquipmentAllowPosition.Ear:
                    RemoveEquipmentHave2Position (mob, obj, EquipmentAllowPosition.Ear_1);
                    MoveEquipmentFromThingsToEQarea (mob, obj, EquipmentAllowPosition.Ear_1);
                    break;
                case EquipmentAllowPosition.Neck:
                    RemoveEquipmentHave2Position (mob, obj, EquipmentAllowPosition.Neck_1);
                    MoveEquipmentFromThingsToEQarea (mob, obj, EquipmentAllowPosition.Neck_1);
                    break;
                case EquipmentAllowPosition.Finger:
                    RemoveEquipmentHave2Position (mob, obj, EquipmentAllowPosition.Finger_1);
                    MoveEquipmentFromThingsToEQarea (mob, obj, EquipmentAllowPosition.Finger_1);
                    break;
                case EquipmentAllowPosition.Bracelet:
                    RemoveEquipmentHave2Position (mob, obj, EquipmentAllowPosition.Bracelet_1);
                    MoveEquipmentFromThingsToEQarea (mob, obj, EquipmentAllowPosition.Bracelet_1);
                    break;
                default:
                    Console.WriteLine ($"沒寫到{obj.Position}這個部位。");
                    break;
            }
            mob.EQArea = mob.EQArea.OrderBy (x => x.Key).ToDictionary (o => o.Key, p => p.Value);
        }

        private void RemoveEquipmentHave2Position (MobInfo mob, CommonObject obj, EquipmentAllowPosition copy)
        {
            //如果兩個位置都滿，則移除右邊
            if (mob.EQArea.ContainsKey (obj.Position.ToString ()) && mob.EQArea.ContainsKey (copy.ToString ()))
                RemoveEquipmentFromBodyToThings (mob, obj.Position);
        }

        private void MoveEquipmentFromThingsToEQarea (MobInfo mob, CommonObject obj, EquipmentAllowPosition copyPosition = EquipmentAllowPosition.None)
        {
            if (!mob.EQArea.ContainsKey (obj.Position.ToString ()))
            {
                mob.RemoveByID (obj.ID, out var nouse);
                mob.EQArea.Add (obj.Position.ToString (), obj);
            }
            else if (copyPosition != EquipmentAllowPosition.None && !mob.EQArea.ContainsKey (copyPosition.ToString ()))
            {
                mob.RemoveByID (obj.ID, out var nouse);
                mob.EQArea.Add (copyPosition.ToString (), obj);
            }
            else
            {
                Console.WriteLine ($"{mob.Settings.Name}沒有多餘的{obj.Position.GetDescriptionText()}可以裝備。");
            }
        }

        private void WearWeapon (MobInfo mob, CommonObject obj)
        {
            int count = mob.EQArea.Where (x => x.Value.Type != WeaponType.None).Count ();
            bool is2HandWeapon = _is2HandWeapon (obj.Type);

            if (count > 1)
                RemoveEquipmentFromBodyToThings (mob, EquipmentAllowPosition.HandRight);

            if (is2HandWeapon)
            {
                RemoveEquipmentFromBodyToThings (mob, EquipmentAllowPosition.HandRight);
                RemoveEquipmentFromBodyToThings (mob, EquipmentAllowPosition.HandLeft);
            }

            if (!mob.EQArea.Keys.Contains (EquipmentAllowPosition.HandRight.ToString ()))
            {
                MoveWeaponFromThingsToBody (mob, obj, EquipmentAllowPosition.HandRight);
            }
            else if (!mob.EQArea.Keys.Contains (EquipmentAllowPosition.HandLeft.ToString ()))
            {
                MoveWeaponFromThingsToBody (mob, obj, EquipmentAllowPosition.HandLeft);
            }
            else
            {
                Console.WriteLine ($"{mob.Settings.Name}沒有第三隻手可以拿武器！！");
            }
            mob.EQArea = mob.EQArea.OrderBy (x => x.Key).ToDictionary (o => o.Key, p => p.Value);
        }
        private bool _is2HandWeapon (WeaponType t)
        {
            switch (t)
            {
                case WeaponType.Sword2hd:
                case WeaponType.Hammer:
                case WeaponType.Staff:
                    return true;
                default:
                    return false;
            }
        }
        private void RemoveEquipmentFromBodyToThings (MobInfo mob, EquipmentAllowPosition _position)
        {
            if (mob.EQArea.ContainsKey (_position.ToString ()))
            {
                var removeObject = mob.EQArea[_position.ToString ()];
                mob.AddThing (removeObject);
                mob.EQArea.Remove (_position.ToString ());
            }
        }
        private void MoveWeaponFromThingsToBody (MobInfo mob, CommonObject obj, EquipmentAllowPosition targetPosition)
        {
            mob.RemoveByID (obj.ID, out var nouse);
            mob.EQArea.Add (targetPosition.ToString (), obj);
        }

    }
}
