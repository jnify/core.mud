using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Loader.Mob
{
    public class LoadMobDefinition : IMobDefinationLoader
    {
        private static Dictionary<string, MobCommonDefinition> mobdeflist = new Dictionary<string, MobCommonDefinition> ();
        private readonly ILogger<LoadMobDefinition> logger;
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ();

        public LoadMobDefinition (ILogger<LoadMobDefinition> logger)
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
            jsoptions.WriteIndented = true;
            this.logger = logger;
            LoadFile ();
        }

        public void LoadFile ()
        {
            string folder = "DocumentWorld/Definition/Mob";
            LoadFile (folder);
        }

        public void LoadFile (string folder)
        {
            if (Directory.Exists (folder))
            {
                string[] subFolder = Directory.GetDirectories (folder);
                foreach (var subpath in subFolder)
                {
                    LoadFile (subpath);
                }

                var files = Directory.GetFiles (folder, "*.json");
                foreach (string fileName in files)
                {
                    try
                    {
                        LoadFileInCache (fileName);
                    }
                    catch (Exception ex)
                    {
                        this.logger.LogDebug ($"Get exception: {ex.Message}");
                    }
                }
            }
        }
        public void LoadFileInCache (string FullFileName)
        {
            string fs = File.ReadAllText (FullFileName);
            var t = JsonSerializer.Deserialize<List<MobCommonDefinition>> (fs, jsoptions);
            t.ForEach (x =>
            {
                //設定重生時間，預設5分鐘(300秒)
                x.SettingRebornTimeInSecond = (x.SettingRebornTimeInSecond == 0) ? 300 : x.SettingRebornTimeInSecond;
                Set (x);
            });
        }
        public MobCommonDefinition GetDef (string mobname)
        {
            var uppermobname = mobname.ToUpperInvariant ();
            if (mobdeflist.ContainsKey (uppermobname))
            {
                return mobdeflist[uppermobname];
            }
            else
            {
                return default;
            }
        }
        private void Set (MobCommonDefinition def)
        {
            if (mobdeflist.ContainsKey (def.Name.ToUpperInvariant ()))
            {
                Console.WriteLine ($"Duplicate mob name: [{def.Name}].");
            }
            else
            {
                mobdeflist.Add (def.Name.ToUpperInvariant (), def);
            }

        }
    }

    public class EquipmentAllowPositionEnumKeyConverter : JsonConverter<EquipmentAllowPosition>
    {
        public override EquipmentAllowPosition Read (ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options = null)
        {
            EquipmentAllowPosition rtn = EquipmentAllowPosition.None;
            if (options == null) options = new JsonSerializerOptions ();
            options.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            options.WriteIndented = true;
            var value = reader.GetString ();
            if (Enum.TryParse<EquipmentAllowPosition> (value, true, out rtn))
            {
                return rtn;
            }
            else
            {
                throw new Exception ($"{value}轉換成[EquipmentAllowPosition]失敗");
            }
        }

        public override void Write (Utf8JsonWriter writer, EquipmentAllowPosition value, JsonSerializerOptions options)
        {
            writer.WriteStringValue (value.ToString ());
        }
    }

}
