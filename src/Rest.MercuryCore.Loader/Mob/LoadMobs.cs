using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.Interface;

namespace Rest.MercuryCore.Loader.Mob
{
    public class LoadMobs : IFileLoader
    {
        private class MobData
        {
            public int RoomID { get; set; }
            public string Name { get; set; }
        }
        private readonly ILogger<LoadMobs> logger;
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ();
        private readonly MobGenerate mobGen;
        private readonly IMobDefinationLoader mobloader;
        public LoadMobs (ILogger<LoadMobs> logger, MobGenerate mobGen, IMobDefinationLoader mobloader)
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
            jsoptions.WriteIndented = true;
            this.logger = logger;
            this.mobGen = mobGen;
            this.mobloader = mobloader;
            LoadFile ();
        }

        public void LoadFile ()
        {
            string path = "DocumentWorld/Data/Mob";
            _LoadFile (path);
        }

        private void _LoadFile (string folder)
        {
            if (Directory.Exists (folder))
            {
                string[] subFolder = Directory.GetDirectories (folder);
                foreach (var subpath in subFolder)
                {
                    _LoadFile (subpath);
                }

                var files = Directory.GetFiles (folder, "*.json");
                foreach (string fileName in files)
                {
                    try
                    {
                        LoadFile (fileName);
                    }
                    catch (Exception ex)
                    {
                        this.logger.LogDebug ($"Get exception: {ex.Message}");
                    }
                }
            }
        }

        public void LoadFile (string FullFileName)
        {
            string fs = File.ReadAllText (FullFileName);
            var t = JsonSerializer.Deserialize<List<MobData>> (fs, jsoptions);
            t.ForEach (x =>
            {
                var dm = mobloader.GetDef (x.Name);
                mobGen.Generate (dm, x.RoomID);
            });
        }

    }
}
