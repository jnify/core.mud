using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.LoadCommonObjects;
using Rest.MercuryCore.Loader.LoadRooms;
using Rest.MercuryCore.Loader.Mob;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class LoaderCollectionExtensions
    {
        public static IServiceCollection LoadWorldObjects (this IServiceCollection services)
        {
            //需依照順序
            services
                .AddSingleton<CommonObjectCanDrink> ()
                .AddSingleton<CommonObjectCanEat> ()
                .AddSingleton<CommonObjectCanWear> ()
                .AddSingleton<CommonObjectSpecial> ()
                .AddSingleton<ILoadCommonObject, LoadCommonObject> ()
                .AddSingleton<IMobDefinationLoader, LoadMobDefinition> ()
                .AddSingleton<LoadRoom> ()
                .AddSingleton<LoadMobs> ();

            services.AddSingleton<MobGenerate> ();

            return services;
        }

    }
}
