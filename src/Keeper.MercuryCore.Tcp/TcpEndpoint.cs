﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.Pipeline;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Tcp
{
    public class TcpEndpoint : IEndpoint
    {
        private readonly ILogger<TcpEndpoint> logger;
        private readonly TcpListener listener;
        public readonly List<TcpConnection> connections = new List<TcpConnection> ();
        private readonly object connectionsLock = new object ();
        private readonly X509Certificate serverCertificate;
        private readonly IOptionsFactory<TcpOptions> optionsFactory;

        private Task DisplayPrompt (List<string> Users, int RoomID)
        {
            if (RoomID == 0) return _DoDisplayPrompt (Users);

            IEnumerable<TcpConnection> conn = null;
            conn = connections.Where (x => x.ConnUser != null && x.ConnUser.Geography.RoomID == RoomID);
            if (conn != null && conn.Count () > 0)
            {
                foreach (var co in conn) co.ConnUser.LastMessageDatetime = co.ConnUser.LastMessageDatetime.LastMessageDatetimeExtend (TimeSpan.FromMilliseconds (-100));
            }
            return Task.CompletedTask;
        }
        private Task _DoDisplayPrompt (List<string> Users)
        {
            if (Users == null || Users.Count () == 0) return Task.CompletedTask;

            IEnumerable<TcpConnection> conn = null;
            conn = connections.Where (x => x.ConnUser != null && Users.Contains (x.ConnUser.Name));
            if (conn != null && conn.Count () > 0)
            {
                foreach (var co in conn) co.ConnUser.LastMessageDatetime = co.ConnUser.LastMessageDatetime.LastMessageDatetimeComplete ();
                var tasks = conn.Select (x => SendRawMessage (x, x.ConnUser.PromptMessage));
                return Task.WhenAll (tasks);
            }
            return Task.CompletedTask;
        }

        public async Task DoBroadcast (BroadcastObject item)
        {
            IEnumerable<TcpConnection> conn = null;
            if (item.WorldChannel != null)
            {
                conn = GetConnectionByWorldChannel (item);
            }
            else if (!string.IsNullOrWhiteSpace (item.ReceiptName))
            {
                conn = connections.Where (x => x.ConnUser != null && string.Compare (x.ConnUser.Name, item.ReceiptName, true) == 0);
            }

            if (conn != null && conn.Count () > 0)
            {
                IEnumerable<Task> tasks = new List<Task> ();
                if (item.ExcludeAsleepPPL)
                {
                    conn = conn.Where (x => !(x.ConnUser.Status.HasFlag (CreatureStatus.Sleep) ||
                        x.ConnUser.Status.HasFlag (CreatureStatus.Dying)));
                }
                if (conn != null && conn.Count () > 0)
                {
                    if (!item.DisplayPrompt)
                        foreach (var co in conn)
                            co.ConnUser.LastMessageDatetime = co.ConnUser.LastMessageDatetime.LastMessageDatetimeComplete ();
                    else
                        foreach (var co in conn)
                            co.ConnUser.LastMessageDatetime = co.ConnUser.LastMessageDatetime.LastMessageDatetimeExtend (TimeSpan.FromMilliseconds (100));
                    tasks = conn.Select (x => SendMsgAsync (x, item.Msg));
                    await Task.WhenAll (tasks);
                }
            }
            await Task.CompletedTask;
        }
        private IEnumerable<TcpConnection> GetConnectionByWorldChannel (BroadcastObject item)
        {
            IEnumerable<TcpConnection> conn = null;
            IEnumerable<TcpConnection> conntmp = null;
            if (string.IsNullOrWhiteSpace (item.ExcludePPL))
            {
                conntmp = connections.Where (x => x.ConnUser != null);
            }
            else
            {
                conntmp = connections.Where (x => x.ConnUser != null &&
                    string.Compare (x.ConnUser.Name, item.ExcludePPL, true) != 0);
            }

            if (item.WorldChannel.RoomID > 0)
            {
                conn = conntmp.Where (x =>
                    !x.ConnUser.Status.HasFlag (CreatureStatus.Dying) &&
                    (x.ConnUser.Geography.RoomID == item.WorldChannel.RoomID));
            }
            else if (!string.IsNullOrWhiteSpace (item.WorldChannel.Area))
            {
                conn = conntmp.Where (x =>
                    !x.ConnUser.Status.HasFlag (CreatureStatus.Dying) &&
                    x.ConnUser.Geography.World == item.WorldChannel.World &&
                    x.ConnUser.Geography.Region == item.WorldChannel.Region &&
                    x.ConnUser.Geography.Area == item.WorldChannel.Area);
            }
            else if (!string.IsNullOrWhiteSpace (item.WorldChannel.Region))
            {
                conn = conntmp.Where (x =>
                    !x.ConnUser.Status.HasFlag (CreatureStatus.Dying) &&
                    x.ConnUser.Geography.World == item.WorldChannel.World &&
                    x.ConnUser.Geography.Region == item.WorldChannel.Region);
            }
            else if (!string.IsNullOrWhiteSpace (item.WorldChannel.World))
            {
                conn = conntmp.Where (x =>
                    !x.ConnUser.Status.HasFlag (CreatureStatus.Dying) &&
                    x.ConnUser.Geography.World == item.WorldChannel.World);
            }

            if (item.ExcludeAsleepPPL && conn != null)
            {
                var excludePPL = conn.ToList ().Where (x => x.ConnUser.Status.HasFlag (CreatureStatus.Sleep));
                conn = conn.Except (excludePPL);
            }
            return conn;
        }
        private async Task SendRawMessage (TcpConnection conn, string message)
        {
            var data = Encoding.UTF8.GetBytes (message + "\r\n");
            var Msg = new ArraySegment<byte> (data);
            await conn.SendAsync (Msg);
        }
        private async Task SendMsgAsync (TcpConnection conn, string message)
        {
            if (string.IsNullOrWhiteSpace (message)) return;
            string pattern = @"(\(%%([\S ]*?)\|([\S ]*?)%%\))";
            foreach (Match match in Regex.Matches (message, pattern))
            {
                string tp = "";
                if (match.Groups[2].Value == conn.ConnUser.Name)
                {
                    tp = match.Value.Replace ($"(%%{match.Groups[2].Value}", "你").Replace ($"|{match.Groups[3].Value}%%)", "");
                }
                else
                {
                    tp = match.Value.Replace ($"(%%{match.Groups[2].Value}", "").Replace ($"|{match.Groups[3].Value}%%)", $"{match.Groups[3].Value}");
                }
                message = message.Replace (match.Value, tp);
            }
            var data = Encoding.UTF8.GetBytes ($"{message}\r\n");
            var Msg = new ArraySegment<byte> (data);
            await conn.SendAsync (Msg);
        }
        public TcpEndpoint (IOptionsFactory<TcpOptions> _optionsFactory, ILogger<TcpEndpoint> logger, IMessageManager msgMgr, string name)
        {
            msgMgr.Bind (DoBroadcast, DisplayPrompt);
            optionsFactory = _optionsFactory;
            TcpOptions options = optionsFactory.Create (name);

            this.logger = logger;
            this.listener = new TcpListener (options.Address, options.Port);
            this.Name = name;

            this.logger.LogInformation ("TCP Endpoint configured on port {Port}", options.Port);

            if (options.SslCertValue != null)
            {
                using (var certStore = new X509Store (StoreName.My, StoreLocation.LocalMachine))
                {
                    certStore.Open (OpenFlags.OpenExistingOnly);

                    this.serverCertificate = certStore.Certificates.Find (options.SslCertFind, options.SslCertValue, false) [0];

                    this.logger.LogInformation ("TCP Endpoint secured for subject {CertificateSubject}", this.serverCertificate.Subject);
                }
            }
        }

        public string Name
        {
            get;
        }

        public event Func<IConnection, Task> NewConnection;

        public void Start ()
        {
            this.listener.Start ();

            this.BeginAccept ();
        }

        public void Stop ()
        {
            this.listener.Stop ();

            lock (this.connectionsLock)
            {
                var connectionsToClose = this.connections.ToArray ();

                foreach (var connection in connectionsToClose)
                {
                    connection.Close ();
                }
            }

            this.connections.Clear ();
        }
        private readonly object userLock = new object ();
        private void BeginAccept ()
        {
            Task.Run (async () =>
            {
                TcpConnection newConnection = null;

                try
                {
                    TcpClient client;
                    lock (userLock)
                    {
                        client = this.listener.AcceptTcpClient ();
                        this.BeginAccept ();

                        newConnection = new TcpConnection (this.Name, client, DoBroadcast, this.serverCertificate);
                        TcpOptions options = optionsFactory.Create (this.Name);
                        this.connections.Add (newConnection);
                        Console.WriteLine ($"Connection count:{this.connections.Count()}");
                    }
                    await this.NewConnection?.Invoke (newConnection);
                }
                catch (Exception ex)
                {
                    this.logger.LogError ("Exception thrown from TCP Endpoint listener {Exception}", ex);
                }

                if (newConnection != null)
                {
                    lock (this.connectionsLock)
                    {
                        this.connections.Remove (newConnection);
                    }
                }
            });
        }

    }
}
