﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Pipeline
{
    public interface IConnection
    {
        ITargetBlock<ArraySegment<byte>> Send { get; }
        Func<BroadcastObject, Task> Broadcast { get; }

        IReceivableSourceBlock < (ArraySegment<byte>, bool) > Receive { get; }

        Task Closed { get; }

        void Close ();

        string EndpointName { get; }

        string UniqueIdentifier { get; }

        ConnectionType Type { get; }

        Task SetUser (UserInfo User);
        UserInfo ConnUser { get; }
        EndPoint RemoteEndPoint { get; }

    }

}
