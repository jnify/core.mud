﻿using System;
using System.Threading.Tasks;
using Keeper.MercuryCore.Pipeline;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Keeper.MercuryCore.Middleware
{
    //可以拿來做公告用TODO:Nice to have
    public class MotdMiddleware : IMiddleware
    {
        private readonly MotdOptions options;

        public MotdMiddleware (IOptions<MotdOptions> options)
        {
            this.options = options.Value;
        }

        public Func<Task> BuildHandler (IServiceProvider serviceProvider, Func<Task> next)
        {
            var channel = serviceProvider.GetService<ITextChannel> ();

            return async () =>
            {
                await channel.SendLineAsync (options.Message);

                await next ();
            };
        }
    }
}
