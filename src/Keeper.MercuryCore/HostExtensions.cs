﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore
{
    public static class HostExtensions
    {
        public static void Run (this IHost host)
        {
            using (var tokenSource = new CancellationTokenSource ())
            {
                Console.CancelKeyPress += (sender, e) =>
                {
                    host.ShutdownService ();
                    tokenSource.Cancel ();
                };
                System.Runtime.Loader.AssemblyLoadContext.Default.Unloading += (ctx) =>
                {
                    host.ShutdownService ();
                    tokenSource.Cancel ();
                };

                var hostTask = host.RunAsync (tokenSource.Token);
                var consoleTask = Task.Run (() =>
                {
                    Console.WriteLine ("Input Exit to close the program.");
                    while (Console.ReadLine ().ToUpperInvariant () != "EXIT") { }
                    tokenSource.Cancel ();
                });
                Task.WaitAny (hostTask, consoleTask);
                hostTask.Wait ();
            }
        }
        private static void ShutdownService (this IHost host)
        {
            var userinfo = host.serviceProvider.GetService<IFirebase<UserInfo>> ();
            var usrMng = host.serviceProvider.GetService<IUserManager> ();
            var msgMgr = host.serviceProvider.GetService<IMessageManager> ();
            msgMgr.BroadcastToWorld ("再10秒要關機了。", EWorld.亞爾斯隆世界);
            Thread.Sleep (7000);
            for (int i = 0; i < 3; i++)
            {
                msgMgr.BroadcastToWorld ($"再{3-i}秒要關機了。", EWorld.亞爾斯隆世界);
                Thread.Sleep (1000);
            }
            var quitmsg = TextUtil.GetFromFile ("DocumentHelp", "QuitMessage.hlp");
            msgMgr.BroadcastToWorld (quitmsg, EWorld.亞爾斯隆世界);

            List<Task> saveJob = new List<Task> ();
            usrMng.GetAllUser ().ForEach (user =>
            {
                saveJob.Add (userinfo.InsertOrUpdate (user));
            });
            Task.WaitAll (saveJob.ToArray ());

            msgMgr.BroadcastToWorld ("世界的入口已關閉。", EWorld.亞爾斯隆世界);

        }
    }
}
