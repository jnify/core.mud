﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.Pipeline.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace Keeper.MercuryCore.Internal
{
    internal class Host : IHost
    {
        private IServiceCollection hostServices;

        public Host (IServiceCollection hostServices)
        {
            this.hostServices = hostServices;
        }

        public IServiceProvider serviceProvider { get; set; }

        public Task RunAsync (CancellationToken token)
        {
            return Task.Run (() =>
            {
                var cancellationHandle = new ManualResetEventSlim (false);

                token.Register (() => cancellationHandle.Set ());

                serviceProvider = this.hostServices.BuildServiceProvider ();

                foreach (var startup in serviceProvider.GetServices<IHostStartup> ())
                {
                    startup.Run (serviceProvider);
                }

                var pipelines = serviceProvider.GetServices<IPipelineFactory> ()
                    .Select ((factory, index) => factory.Create (serviceProvider, index)).ToList ();

                foreach (var pipeline in pipelines)
                {
                    pipeline.Start ();
                }

                cancellationHandle.Wait ();

                foreach (var pipeline in pipelines)
                {
                    pipeline.Stop ();
                }
            });
        }
    }
}
