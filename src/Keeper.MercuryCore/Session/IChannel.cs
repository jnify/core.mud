﻿using System;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Session
{
    public interface IChannel
    {
        string RemoteIP { get; set; }

        void Handle (byte datum, Action<byte> nextHandle, Action<SignalType> nextSignal);

        void Signal (SignalType type, Action<SignalType> next);

        Func<ArraySegment<byte>, Task> Bind (Func<ArraySegment<byte>, Task> send);
        Func<BroadcastObject, Task> BindBroadcast (Func<BroadcastObject, Task> Broadcast);
        Func<UserInfo, Task> BindUser (Func<UserInfo, Task> SetUser);

    }
}
