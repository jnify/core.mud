﻿using System.Collections.Generic;

namespace Keeper.MercuryCore.Session
{
    public interface ISession
    {
        T Get<T> (string Name);

        void Remove<T> (string Name);

        void Set<T> (string Name, T value);

    }

    public class UserCommonSession : ISession
    {
        private Dictionary<string, object> SessionObj = new Dictionary<string, object> ();

        T ISession.Get<T> (string Name)
        {
            T rtn = (T) this.SessionObj[typeof (T).ToString () + Name];
            return rtn;
        }

        void ISession.Remove<T> (string Name)
        {
            this.SessionObj.Remove (typeof (T).ToString () + Name);
        }

        void ISession.Set<T> (string Name, T value)
        {
            this.SessionObj[typeof (T).ToString () + Name] = value;
        }
    }
}