﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.Session
{
    public interface ICommonTools
    {
        string DescriptionBuilder (Room room, UserInfo user, List<UserInfo> users);
        Task MovementAsync (IMessageManager msgMgr, IWorldManager wordMng, List<UserInfo> NextRoomUsers, List<UserInfo> CurrentRoomUsers, UserInfo user, Room room, ExitName exit, bool isRunaway = false);
    }
}
