﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Session.Internal
{
    internal partial class PlainTextChannel : ITextChannel, IChannel, ICommonTools
    {
        private readonly ILogger<PlainTextChannel> logger;
        private readonly Encoding textEncoding;
        private readonly IPropagatorBlock<ArraySegment<byte>, string> lineAccumulator;
        private Func<ArraySegment<byte>, Task> send;
        private Func<BroadcastObject, Task> broadcast;
        private Func<UserInfo, Task> setuser;
        private UserInfo getuser;

        public UserInfo ChannelUser => getuser;

        public string RemoteIP { get; set; }

        public string IP => RemoteIP;

        public PlainTextChannel (ILogger<PlainTextChannel> logger, Encoding textEncoding)
        {
            this.logger = logger;
            this.textEncoding = textEncoding;

            var encodingBuffer = new BufferBlock<string> ();
            var characterBuffer = new Queue<byte> ();

            var encodingAction = new ActionBlock<ArraySegment<byte>> (async data =>
            {
                var chars = this.textEncoding.GetChars (data.Array, data.Offset, data.Count);

                foreach (char character in chars)
                {
                    if (character == '\n')
                    {

                        var line = this.textEncoding.GetString (characterBuffer.ToArray ());

                        //this.logger.LogTrace ("Received line {Line}", line);

                        await encodingBuffer.SendAsync (line);

                        characterBuffer.Clear ();
                    }
                    else if (character == '\r')
                    {
                        //Ignore
                    }
                    else
                    {
                        foreach (var ch in data.Array)
                        {
                            characterBuffer.Enqueue (ch);
                        }
                    }
                }
            });

            this.lineAccumulator = DataflowBlock.Encapsulate (encodingAction, encodingBuffer);
        }

        public Func<ArraySegment<byte>, Task> Bind (Func<ArraySegment<byte>, Task> send) => this.send = send;
        public Func<BroadcastObject, Task> BindBroadcast (Func<BroadcastObject, Task> Broadcast) => this.broadcast = Broadcast;

        public Func<UserInfo, Task> BindUser (Func<UserInfo, Task> SetUser) => setuser = SetUser;

        public void Handle (byte datum, Action<byte> nextHandle, Action<SignalType> nextSignal)
        {
            this.lineAccumulator.Post (new ArraySegment<byte> (new [] { datum }));
        }

        public async Task<string> ReceiveLineAsync (TimeSpan ts = default)
        {
            if (ts == default)
            {
                try
                {
                    //每五分鐘提醒一次
                    ts = TimeSpan.FromMilliseconds (300000);
                    if (getuser.Level > 95) ts = TimeSpan.FromSeconds (30000);
                    return await this.lineAccumulator.ReceiveAsync (ts);
                }
                catch
                {
                    return await Task.Run (() => { return "SYSTEM_WAITING_COMMAND_TIMEOUT"; });
                }
            }
            else
            {
                return await this.lineAccumulator.ReceiveAsync (ts);
            }
        }

        public Task SendLineAsync (string message)
        {
            //發給自己的訊息，一律顯示Prompt, 並修改LastMessageDatetime，之後的不顯示Prompt。
            //if (ChannelUser != null && ChannelUser.LastMessageDatetime != DateTime.MaxValue && DateTime.Now > ChannelUser.LastMessageDatetime)
            if (ChannelUser != null)
            {
                ChannelUser.LastMessageDatetime = ChannelUser.LastMessageDatetime.LastMessageDatetimeComplete ();
                if (string.IsNullOrWhiteSpace (message))
                {
                    message = ChannelUser.PromptMessage;
                }
                else
                {
                    message = $"{message}\r\n\r\n{ChannelUser.PromptMessage}";
                }
            }
            var data = this.textEncoding.GetBytes ($"{message}\r\n");
            return send (new ArraySegment<byte> (data));
        }

        private Task Broadcast (BroadcastObject bo)
        {
            return broadcast (bo);
        }

        public Task BroadcastToRoom (string Message, int RoomID, string ExcludeUser = default, bool ExcludeAsleepPPL = true)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.ExcludeAsleepPPL = ExcludeAsleepPPL;
            bo.ExcludePPL = ExcludeUser;
            bo.Msg = Message;
            bo.WorldChannel = new WorldInfra ()
            {
                RoomID = RoomID
            };
            return Broadcast (bo);
        }

        public Task SendMessageToUser (string Message, string UserName, bool ExcludeAsleepPPL = true)
        {
            BroadcastObject bo = new BroadcastObject ()
            {
            Msg = Message,
            ExcludeAsleepPPL = ExcludeAsleepPPL
            };
            bo.ReceiptName = UserName;
            return broadcast (bo);
        }

        public void Signal (SignalType type, Action<SignalType> next)
        {
            if (type == SignalType.ConnectionClosed)
            {
                this.lineAccumulator.Complete ();
            }

            next (type);
        }

        public Task SetUser (UserInfo User)
        {
            getuser = User;
            return setuser (User);
        }

        public Task BroadcastToUsers (string Message, List<string> UserNames, bool ExcludeAsleepPPL = true)
        {
            if (UserNames == null || UserNames.Count == 0) return Task.CompletedTask;
            List<Task> task = new List<Task> ();
            UserNames.ForEach (x => { task.Add (SendMessageToUser (Message, x, ExcludeAsleepPPL)); });
            return Task.WhenAll (task);
        }

        public Task BroadcastToWorld (string Message, EWorld world)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.ExcludeAsleepPPL = false;
            bo.Msg = Message;
            bo.WorldChannel = new WorldInfra ()
            {
                World = world.GetDescriptionText ()
            };
            return Broadcast (bo);
        }

        public Task BroadcastToArea (string Message, Creature creature, bool ExcludeAsleepPPL = true)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.ExcludeAsleepPPL = ExcludeAsleepPPL;
            bo.Msg = TextUtil.GetColorWord (Message, TextUtil.ColorFront.Green);
            bo.WorldChannel = new WorldInfra ()
            {
                Area = creature.Geography.Area,
                Region = creature.Geography.Region,
                World = creature.Geography.World
            };
            return Broadcast (bo);
        }
    }
}
