﻿using System;
using System.Text;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Session.Internal
{
    internal class VirtualTerminalChannel : IVirtualTerminalChannel, IChannel
    {
        private Func<ArraySegment<byte>, Task> send;
        private Func<BroadcastObject, Task> broadcast;
        private Func<UserInfo, Task> setuser;

        public string RemoteIP { get; set; }

        public event Action EscapeReceived;

        public Func<ArraySegment<byte>, Task> Bind (Func<ArraySegment<byte>, Task> send) => this.send = send;

        public Func<BroadcastObject, Task> BindBroadcast (Func<BroadcastObject, Task> Broadcast) => this.broadcast = Broadcast;

        public Func<UserInfo, Task> BindUser (Func<UserInfo, Task> SetUser) => setuser = SetUser;

        public Task Broadcast (string Message, int RoomID, string World = null, string Region = null, string Area = null)
        {
            BroadcastObject bo = new BroadcastObject ();
            bo.Msg = Message;
            bo.ExcludeAsleepPPL = true;
            bo.WorldChannel = new WorldInfra ()
            {
                World = World,
                Region = Region,
                Area = Area,
                RoomID = RoomID
            };
            return broadcast (bo);
        }

        public void Handle (byte datum, Action<byte> nextHandle, Action<SignalType> nextSignal)
        {
            if (datum == 0x1b)
            {
                this.EscapeReceived?.Invoke ();
            }
            else
            {
                nextHandle (datum);
            }
        }

        public async Task SendEscapeSequenceAsync (string sequence) => await send (Encoding.ASCII.GetBytes ((char) 0x1b + sequence));

        public void Signal (SignalType type, Action<SignalType> next) => next (type);

    }
}
