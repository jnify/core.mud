﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Session.Internal
{
    internal partial class PlainTextChannel : ITextChannel, IChannel, ICommonTools
    {
        private readonly Random rnd = new Random ();

        //Visibility能見度愈高，看得愈清楚，100是全顯示，0是完全不顯示
        public bool canDisplay (int Visibility)
        {
            if (Visibility > 99) return true;
            return Visibility > rnd.Next (99);
        }
        public string DescriptionBuilder (Room room, UserInfo user, List<UserInfo> RoomUsers)
        {
            //晚上要配照明用品才能看到東西
            int Visibility = room.RoomVisibility;
            EquipmentAllowPosition eqpos = EquipmentAllowPosition.Ligth;
            if (user.EQArea.ContainsKey (eqpos.ToString ()) &&
                (user.EQArea[eqpos.ToString ()].DispearTS != TimeSpan.MaxValue)) Visibility = 100;

            StringBuilder roomSB = new StringBuilder ().Clear ();
            StringBuilder contentObjects = new StringBuilder ().Clear ();
            StringBuilder contentMob = new StringBuilder ().Clear ();
            StringBuilder contentPPL = new StringBuilder ().Clear ();

            bool isAdmin = user.Level > 95 || user.Name == "Roy";
            string adminInfo = default;
            if (room != null && room.Geography != null && room.Description != null)
            {
                if (isAdmin)
                    roomSB.Append ($"{room.Geography.Area}-{room.Geography.RoomTitle}[1;31m({room.RoomID})[0m");
                else
                    roomSB.Append ($"{room.Geography.RoomTitle}");
                roomSB.Append ("\n\r明顯的出口有：");
                if (room.Exits != null && room.Exits.Length > 0)
                {
                    string tmp = default;
                    foreach (var exit in room.Exits)
                    {
                        if (isAdmin) tmp = $"[1;31m({exit.RoomID})[0m";
                        if (canDisplay (Visibility))
                        {
                            var exitName = exit.Dimension.GetDescriptionText ();
                            exitName = (exit.gateStatus == GateStatus.Open) ? exitName : $"[1;30m{exitName}[0m";
                            roomSB.Append ($"{exitName}{tmp}　");
                        }
                    }
                }
                else
                {
                    roomSB.Append ("沒有任何明顯的出口。");
                }
                if (canDisplay (Visibility))
                    roomSB.Append ($"\r\n{room.Description}");
                else
                    roomSB.Append ($"\r\n你伸手不見五指。");

                //todo:物品可以是隱形的
                if (room.Things != null && room.Things.Count () > 0)
                {
                    //Key為Description, Value為數量
                    var objDictionary = room.Things.GroupBy (obj => obj.GetDescriptionOnTheGround ()).
                    ToDictionary (group => group.Key, group => group.Count ()).ToList ();
                    //取出最大數量
                    int maxNumLength = objDictionary.Max (x => x.Value).ToString ().Length;
                    objDictionary.ForEach (dic =>
                    {
                        string objDesc = dic.Key;
                        string numStr = dic.Value.ToAlignRightString (maxNumLength);
                        if (dic.Value > 1)
                            objDesc = $"({numStr}){objDesc}";
                        else
                            objDesc = dic.Key.PadLeft (dic.Key.Length + maxNumLength + 2);

                        if (canDisplay (Visibility))
                            contentObjects.Append ($"\r\n{objDesc}");
                        else
                            contentObjects.Append ("\r\n某不明物體。");
                    });
                }

                //todo:要有偵測隱䁥才能看到隱䁥的人
                bool ExcludeHideCreature = true;
                //列出所有沒隱暱的Mob
                var mobs = room.Mobs;
                if (ExcludeHideCreature)
                    mobs = mobs.Where (mob => !mob.Status.HasFlag (CreatureStatus.Hide)).ToList ();
                mobs.ForEach (mob =>
                {
                    //顯示Mob
                    if (isAdmin) adminInfo = $"[1;31m({mob.ID.Substring(0,5)})[0m";
                    string mobmsg = mob.NameForDisplay ();
                    if (mob.Status.HasFlag (CreatureStatus.Hide)) mobmsg = $"[30m（隱匿）[0m{mobmsg}";
                    if (mob.Status.HasFlag (CreatureStatus.Fighting))
                    {
                        string defendername = (mob.FightingInfo.FightingWith.Name.ToUpperInvariant () == user.Name.ToUpperInvariant ()) ?
                            "你" : mob.FightingInfo.FightingWith.NameForDisplay ();
                        mobmsg = $"\r\n{adminInfo}{mob.Settings.Name}正在攻擊{defendername}";
                    }
                    else
                    {
                        mobmsg = $"\r\n{adminInfo}{mobmsg}";
                    }
                    if (!canDisplay (Visibility)) mobmsg = $"\r\n{adminInfo}某不明人物。";
                    contentMob.Append (mobmsg);
                });

                //列出所有沒隱暱的PPL，要扣掉自己
                if (RoomUsers != null && RoomUsers.Any (x => x.Name != user.Name))
                {
                    var listUser = RoomUsers.Where (roomUser => roomUser.Name != user.Name);
                    if (ExcludeHideCreature) listUser = listUser.Where ((roomUser => !roomUser.Status.HasFlag (CreatureStatus.Hide)));

                    listUser.ToList ().ForEach (x =>
                    {
                        string pplMsg;
                        if (x.Status.HasFlag (CreatureStatus.Fighting))
                        {
                            pplMsg = $"\r\n{x.NameForDisplay ()}正在攻擊{x.FightingInfo.FightingWith.Settings.Name}({x.FightingInfo.FightingWith.Name})";
                        }
                        else
                        {
                            pplMsg = $"\r\n{x.NameForDisplay ()}";
                        }
                        if (!canDisplay (Visibility)) pplMsg = "\r\n某不明人物。";
                        contentPPL.Append (pplMsg);
                    });
                }
            }
            else
            {
                roomSB.Append ("這裏一片虛無");
            }
            return roomSB.Append (contentObjects).Append (contentMob).Append (contentPPL).ToString ();
        }

        public Task MovementAsync (IMessageManager msgMgr, IWorldManager wordMng, List<UserInfo> NextRoomUsers, List<UserInfo> CurrentRoomUsers, UserInfo userLeader, Room RoomFrom, ExitName exit, bool isRunaway = false)
        {
            //建議可以改成StringBuilder, TODO: Nice to have
            List<string> MessageToFromRoom = new List<string> (); //原房間最後剩餘人員訊息
            List<string> MessageToNextRoom = new List<string> (); //原本目的房間所有人員
            List<string> MessageToFollower = new List<string> (); //給所有跟隨者訊息，不含Leader
            List<string> MessageToLeader = new List<string> (); //給Leader的訊息

            List<Task> moveTask = new List<Task> ();

            //Check Room Status
            var result = CheckRoomStatus (wordMng, RoomFrom, exit, out var RoomTo);
            if (!result.isSuccess) return msgMgr.SendToPPL (result.Message, userLeader.Name);

            //確認Leader是否可以移動
            result = CheckCanMoveToNext (userLeader, RoomTo, exit);
            if (!result.isSuccess) return msgMgr.SendToPPL (result.Message, userLeader.Name);

            //移動Leader
            userLeader.ChangeRoom (RoomTo);

            //對原房間所有人顯示Leader離開的訊息
            string verb = isRunaway ? "逃離！" : "離開。";
            MessageToFromRoom.Add (result.Message + verb);
            //對目的房間所有有人進來的訊息
            MessageToNextRoom.Add (result.Message + "進來。");
            MessageToFollower.Add ("你跟著" + result.Message + verb);

            //顯示Room information
            msgMgr.SendToPPL (DescriptionBuilder (RoomTo, userLeader, NextRoomUsers), userLeader.Name);

            //處理跟隨者
            List<UserInfo> follouserlist = new List<UserInfo> ();
            if (CurrentRoomUsers.Any (x => x.Actions.FollowWith == userLeader.Name))
            {
                follouserlist = CurrentRoomUsers.Where (x => x.Actions.FollowWith == userLeader.Name).ToList ();
                List<string> followerFailed = new List<string> ();
                //取出限隨者
                follouserlist.ForEach (x =>
                {
                    var result = CheckCanMoveToNext (x, RoomTo, exit);
                    if (result.isSuccess)
                    {
                        //對原房間離開的訊息
                        MessageToFromRoom.Add (result.Message + "離開。");
                        //對目的房間進來的訊息
                        MessageToNextRoom.Add (result.Message + "進來。");
                        //對Leader及Foller不要顯示
                        x.ChangeRoom (RoomTo); //移動PPL
                        //顯示Room information
                        msgMgr.SendToPPL (DescriptionBuilder (RoomTo, x, NextRoomUsers), x.Name);
                    }
                    else
                    {
                        //對原PPL直接顯示無法移動的訊息。
                        msgMgr.SendToPPL (result.Message, x.Name);
                        //對Leader顯示有人沒跟上。
                        MessageToLeader.Add ($"你發現 { x.Name } 沒跟上！");
                        //沒跟上的要扣掉，先放在follouserlist
                        followerFailed.Add (x.Name);
                    }
                });
                follouserlist.RemoveAll (x => followerFailed.Contains (x.Name));
            }

            //原房間剩餘PPL列表
            List<string> UsersInFromRoom = CurrentRoomUsers.Where (x => x.Name != userLeader.Name).Except (follouserlist)
                .Select (x => x.Name).ToList ();
            if (UsersInFromRoom.Count () > 0)
            {
                string messageforFromRoom = string.Join ("\r\n", MessageToFromRoom);
                UsersInFromRoom.ForEach (username =>
                {
                    moveTask.Add (msgMgr.SendToPPL (messageforFromRoom, username));
                });
            }

            //本次移動的所有人員
            List<string> UsersInMovement = new List<string> () { userLeader.Name };
            UsersInMovement.AddRange (follouserlist.Select (x => x.Name));

            //取得目的Mob未處於攻擊者，要處理"MeetPPLAction"
            DealWithMetPPLAction (userLeader, RoomTo, follouserlist);

            //處理 MessageToNextRoom
            if (NextRoomUsers.Count () > 0)
            {
                string message = string.Join ("\r\n", MessageToNextRoom);
                NextRoomUsers.ForEach (u =>
                {
                    moveTask.Add (msgMgr.SendToPPL (message, u.Name));
                });
            }

            //處理 MessageToFollower
            if (follouserlist.Count () > 0 && MessageToFollower.Count () > 0)
            {
                string message = string.Join ("\r\n", MessageToFollower);
                follouserlist.ForEach (u =>
                {
                    moveTask.Add (msgMgr.SendToPPL (message, u.Name));
                });
            }

            //處理 MessageToLeader
            moveTask.Add (msgMgr.SendToPPL (string.Join ("\r\n", MessageToLeader), userLeader.Name));

            return Task.WhenAll (moveTask);
        }

        private static void DealWithMetPPLAction (UserInfo userLeader, Room RoomTo, List<UserInfo> follouserlist)
        {
            var MobsNotInFighting = RoomTo.Mobs.Where (x => !x.Status.HasFlag (CreatureStatus.Fighting) && x.MeetPPLAction != null).ToList ();
            MobsNotInFighting.ForEach (mobx =>
            {
                Task.Run (() =>
                {
                    ActionKeyParameter param = new ActionKeyParameter (userLeader, mobx, RoomTo);
                    mobx.MeetPPLAction (param, null);
                });

                //跟隨著
                follouserlist.ForEach (userx =>
                {
                    ActionKeyParameter param = new ActionKeyParameter (userx, mobx, RoomTo);
                    Task.Run (() =>
                    {
                        mobx.MeetPPLAction (param, null);
                    });
                });
            });
        }
        private (bool isSuccess, string Message) CheckRoomStatus (IWorldManager wordMng, Room roomFrom, ExitName exit, out Room roomTo)
        {
            roomTo = null;
            if (!roomFrom.Exits.Any (x => x.Dimension == exit)) return (false, exit.GetDescriptionText () + "沒有路。");

            var doorSettings = roomFrom.Exits.Where (x => x.Dimension == exit).FirstOrDefault ();
            if (doorSettings.gateStatus != GateStatus.Open)
                return (false, $"{ doorSettings.Dimension.GetDescriptionText () } 的門是關著的。");

            //Check is the Room number is exist
            roomTo = wordMng.GetRoom (doorSettings.RoomID);
            if (roomTo == null) return (false, exit.GetDescriptionText () + "的房子還沒蓋好。");

            return (true, "");
        }
        private (bool isSuccess, string Message) CheckCanMoveToNext (UserInfo user, Room roomTo, ExitName exit)
        {
            var result = CheckUserStatus (user);
            if (!result.isSuccess) return result;

            result = CheckAdventureSkill (user, roomTo);
            if (!result.isSuccess) return result;

            return (true, $"{user.UserDescriptionAndID}從{exit.GetDescriptionText ()}");
        }
        private (bool isSuccess, string Message) CheckUserStatus (UserInfo user)
        {
            if (user.Status.HasFlag (CreatureStatus.Fighting))
                return (false, "你還在戰鬥中，無法離開。");
            if (user.Status.HasFlag (CreatureStatus.Rest))
                return (false, "嗯...你太舒服起不來了...");
            if (user.Status.HasFlag (CreatureStatus.Sleep))
                return (false, "哦？ 在睡夢中嗎？");
            if (user.Status.HasFlag (CreatureStatus.Dying))
                return (false, "你傷得太重，動彈不得！");
            return (true, "");
        }
        private (bool isSuccess, string Message) CheckAdventureSkill (UserInfo user, Room roomTo)
        {
            int skillResult = 1;
            PracticedSkill skillAttr = default;
            BaseSkill selectedSkill = null;
            string failedmessage = "你太累了。";
            switch (roomTo.Terrain)
            {
                case Topography.Beach:
                    skillAttr = user.GetSkill (SkillName.Swimming);
                    if (skillAttr == null) return (false, "你游不過去！！");
                    selectedSkill = new SkillSwimming (skillAttr);
                    skillResult = selectedSkill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);
                    failedmessage = skillResult.GetSkillFailedMessage (selectedSkill);
                    break;
                case Topography.Mountain:
                    skillAttr = user.GetSkill (SkillName.Climbing);
                    if (skillAttr == null) return (false, "你爬不過去！！");
                    selectedSkill = new SkillSwimming (skillAttr);
                    skillResult = selectedSkill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);
                    failedmessage = skillResult.GetSkillFailedMessage (selectedSkill);
                    break;
                default:
                    if (user.StateCurrent.MV < 1)
                        skillResult = -3;
                    else
                        user.StateCurrent.MV--;
                    break;
            }
            if (skillResult != 1) return (false, failedmessage);
            return (true, "");
        }
    }
}
