﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Session
{
    public interface ITextChannel
    {
        Task SendLineAsync (string message);
        Task BroadcastToRoom (string Message, int RoomID, string ExcludeUser = default, bool ExcludeAsleepPPL = true);
        Task BroadcastToArea (string Message, Creature creature, bool ExcludeAsleepPPL = true);
        Task BroadcastToWorld (string Message, EWorld world);
        Task BroadcastToUsers (string Message, List<string> UserNames, bool ExcludeAsleepPPL = true);
        Task SendMessageToUser (string Message, string UserName, bool ExcludeAsleepPPL = true);
        Task SetUser (UserInfo User);
        Task<string> ReceiveLineAsync (TimeSpan ts = default);
        UserInfo ChannelUser { get; }
        string IP { get; }
    }
}
