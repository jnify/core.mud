﻿var serializeJSONOpt = {
    skipFalsyValuesForTypes: ["number"],
};
var sysJs = {
    isNull: function (obj) {
        return (!obj && typeof (obj) != undefined && obj != 0);
    },
    colorArray: {
        '000000': '0',
        '900900': '31',
        '090090': '32',
        '990990': '33',
        '009009': '34',
        '909909': '35',
        '099099': '36',
        'BBBBBB': '37',
        '666666': '1;30',
        'f66f66': '1;31',
        '6f66f6': '1;32',
        'ff6ff6': '1;33',
        '66f66f': '1;34',
        'f6ff6f': '1;35',
        '6ff6ff': '1;36',
        'ffffff': '1;37'
    },
    MakeUpIfNull: function (obj) {
        if (this.isNull(obj)) return [];
        return obj;
    },
    ConvertToANSIColor: function (str) {
        if (str === undefined || str.length == 0) return "";
        var reg = /<span\sstyle=\"color:\s#(\w{6});\">/gm;
        var regSub = /(\w{6})/gm;
        var allmatch = str.match(reg);
        var rtn = str;
        if (!sysJs.isNull(allmatch)) {
            allmatch.forEach(span => {
                var submatch = span.match(regSub);
                if (!sysJs.isNull(submatch)) {
                    submatch.forEach(color => {
                        rtn = rtn.replace(span, "[" + sysJs.colorArray[color] + "m");
                    })
                }
            });
        }
        rtn = rtn.replaceAll("</span>", "[0m");
        return rtn;
    },
    InjectFormAction: function () {
        jQuery.each($("Form"), function (index, obj) {
            var functionName = 'formJs.' + obj.id;
            var fun = eval(functionName);
            if (typeof fun === "function") {
                $(obj).bind("submit", fun);
                $(obj).find('input, select').on("change", function () {
                    $(obj).submit();
                });
            }
        });
    },
    showExecuteMessage: function (message) {
        var boxID = "MessageBoxIn3Sec";
        var box = '<button type="button" id="' + boxID + '" class="btn btn-outline-success btn-block">' + message + '</button>';
        $('header').append(box);
        $('#' + boxID).fadeOut(1000, function () {
            $(this).remove();
        });
    },
    ajax: function (url, jsonDataString, callBack) {
        $.ajax({
            async: true,
            cache: false,
            contentType: "application/json",
            data: jsonDataString,
            url: url,
            type: "POST",
            success: function (res) {
                if (typeof callBack === "function") {
                    callBack();
                }
                sysJs.showExecuteMessage(res);
            },
            error: function (res) {
                alert("出錯囉！！");
            }
        });
    },
    dialog: function (title, message, submitCallback) {
        $('#dialogModelTitle').text(title);
        $('#dialogModelMessage').text(message);
        $('#dialogSubmit').on("click", function () {
            $('#dialogSubmit').unbind();
            $('#modal-confirmation').modal('hide');
            submitCallback();
        });
        $('#modal-confirmation').modal('show');
    }
};

var formJs = {
    fileRoomJsonModel: function () {
        var formID = "fileRoomJsonModel";
        var url = "/Room/_SaveRoomModel?key=" + key + "&subkey=" + subkey;

        var obj = $('#' + formID).serializeJSON(serializeJSONOpt);
        obj["Rooms"] = [];
        var jsonString = JSON.stringify(obj);
        if (jsonString.includes('<span style')) { alert('請檢查文字段落，有Html Tag！'); return false;}
        sysJs.ajax(url, jsonString);

        return false;
    }
};

var roomJs = {
    outPutFiles: function () {
        url = "/Room/_Output";
        sysJs.ajax(url, "");
    },
    roomJsModel: function (addExit) {
        tinymce.triggerSave();
        var desc = sysJs.ConvertToANSIColor($($('textarea').val()).html());
        var formID = "roomJsModel";
        var obj = $('#' + formID).serializeJSON();
        obj["Description"] = desc;
        obj["CommonObjects"] = sysJs.MakeUpIfNull(obj["CommonObjects"]);
        obj["Equipments"] = sysJs.MakeUpIfNull(obj["Equipments"]);
        obj["Exits"] = sysJs.MakeUpIfNull(obj["Exits"]);
        //Todo:改成用Checkbox了
        if (!sysJs.isNull(obj["Exits"]) && obj["Exits"].length > 0) {
            for (var i = 0; i < obj["Exits"].length; i++) {
                if (!sysJs.isNull(obj["Exits"][i].AcceptVerbs) && obj["Exits"][i].AcceptVerbs[0] == "None") {
                    obj["Exits"][i].AcceptVerbs.shift();
                    if (obj["Exits"][i].AcceptVerbs.length == 0) {
                        delete obj["Exits"][i].AcceptVerbs;
                    }
                }
                if (!sysJs.isNull(obj["Exits"][i].AcceptVerbs)) {
                    obj["Exits"][i].AcceptVerbs = obj["Exits"][i].AcceptVerbs.join();
                }
            }
        }
        if (addExit === true) {
            obj["Exits"].push({
                "RoomID": 0,
                "AcceptVerbs": "none",
                "Dimension": "south",
                "gateStatus": "open",
                "KeyNumber": 0
            });
        }
        var jsonString = JSON.stringify(obj);
        if (jsonString.includes('<span style')) { alert('請檢查文字段落，有Html Tag！'); return false;}
        var UpdateOppositeDoor = $('#UpdateOppositeDoor').prop('checked');
        var url = "/Room/_SaveRoomModelDetail?key=" + key + "&subkey=" + subkey + "&RoomID=" + RoomID + "&UpdateOppositeDoor=" + UpdateOppositeDoor;
        sysJs.ajax(url, jsonString, function () {
            RoomID = obj.RoomID;
            if (!$('input[name="RoomID"]').hasClass("d-none")) {
                $('input[name="RoomID"]').addClass("d-none").after(RoomID);
            }
        });
        return false;
    },
    displayChangeRoomID: function () {
        $this = $(this);
        $('#modal-updateRoomID').modal('show');
        $('#oldRoomID').val($this.data('roomid'));
        return false;
    },
    saveChangeRoomID: function () {
        var obj = $('#formChangeRoomID').serializeJSON(serializeJSONOpt);
        var url = "/Room/_ChangeRoomID?oldRoomID=" + obj.oldRoomID + "&newRoomID=" + obj.newRoomID;
        sysJs.ajax(url, "", function () {
            var $span = $('.btnChangeRoomID').parent().find('span:contains("' + obj.oldRoomID + '")');
            $span.text(obj.newRoomID);
            $span.parent().find('a').data('roomid', obj.newRoomID);
            $('#modal-updateRoomID').modal('hide');
        });
    },
    displayChangeExitRoomID: function () {
        $this = $(this);
        $('#modal-updateExitRoomID').modal('show');
        $('#ExitDimation').val($this.data('dimension'));
        $('#ExitOnRoomID').val($this.data('roomid'));
        return false;
    },
    saveChangeExitRoomID: function () {
        var obj = $('#formChangeExitRoomID').serializeJSON(serializeJSONOpt);
        var url = "/Room/_ChangeExitRoomID?RoomID=" + obj.ExitOnRoomID + "&Dimation=" + obj.ExitDimation + "&newExitRoomID=" + obj.newExitRoomID + "&UpdateOppositeDoor=" + obj.UpdateOppositeDoor;
        sysJs.ajax(url, "", function () {
            var htmlTR = $('tr').parent().find('span:contains("' + obj.ExitOnRoomID + '")').parent().parent();
            var descA = htmlTR.find("[data-dimension='" + obj.ExitDimation + "']").prev();
            var dimationWord = descA.text().substring(0, 2) + obj.newExitRoomID;
            descA.text(dimationWord);
            $('#modal-updateExitRoomID').modal('hide');
        });
    },
    ModifyAcceptVerb: function () {
        var $verb = $('#AcceptVerbs');
        if ($verb.val().includes('CanLock') && !$('#KeyNumberRow').is(":visible"))
            $('#KeyNumberRow').show();
        if (!$verb.val().includes('CanLock') && $('#KeyNumberRow').is(":visible"))
            $('#KeyNumberRow').hide();

        if ($verb.val().includes('CanOpenClose') && !$('#gateStatusRow').is(":visible"))
            $('#gateStatusRow').show();
        if (!$verb.val().includes('CanOpenClose') && $('#gateStatusRow').is(":visible"))
            $('#gateStatusRow').hide();
    },
    AddCommonObjects: function () {
        var innerHtml = '<div><input type="text" name="CommonObjects[][SystemCode]"> <input type="text" name="CommonObjects[][ActionKey]"> <a href="#" class="bi bi-x-circle text-danger btnRemoveCommonObject"></a></div>';
        $('.btnAddCommonObjects').parent().next().append(innerHtml);
        $('.btnRemoveCommonObject').unbind().on("click", function () { $(this).parent().remove(); return false; });
        return false;
    },
    AddEquipments: function () {
        var innerHtml = '<div> <input type="text" name="Equipments[]"> <a href="#" class="bi bi-x-circle text-danger btnRemoveEquipments"></a></div>';
        $('.btnAddEquipments').parent().next().append(innerHtml);
        $('.btnRemoveEquipments').unbind().on("click", function () { $(this).parent().remove(); return false; });
        return false;
    },
}

var mobJs = {
    outPutFiles: function () {
        url = "/MobDefination/_Output";
        sysJs.ajax(url, "");
    },
    mobSaveModel: function () {
        //處理tinymce
        tinymce.triggerSave();

        var tinymceCount = $('.tinymcetextarea, .tinymcetext').length;
        if (tinymceCount > 0) {
            var tinymceObj = $('.tinymcetextarea, .tinymcetext');
            tinymceObj.each(
                function () {
                    var content = "";
                    var hval = $(this).val().replace('</p>\n<p>', '<br />');
                    if (hval.length > 0) {
                        var hcon = $(hval).html().replace('<br>', '\r\n');
                        content += sysJs.ConvertToANSIColor(hcon);

                        $(this).val(content);
                    }
                }
            );
        }

        var formID = "JsModel";
        var obj = $('#' + formID).serializeJSON(serializeJSONOpt);

        //處理Multiple selector
        var MultpleCount = $('.MultipleSelect').length;
        var MultipleObj = $('.MultipleSelect');
        if (MultpleCount > 0) {
            for (var i = 0; i < MultpleCount; i++) {
                var selectorName = $(MultipleObj[i]).attr('name');
                obj[selectorName] = $('[name="' + selectorName + '"]').val()
            }
        }

        var jsonString = JSON.stringify(obj);
        if (jsonString.includes('<span style')) { alert('請檢查文字段落，有Html Tag！'); return false;}

        var url = "/MobDefination/_Save?SubFolder=" + SubFolder + "&FileName=" + FileName + "&Name=" + creatureName;
        sysJs.ajax(url, jsonString, function () {
            creatureName = obj.Name;
            if (!$('input[name="Name"]').hasClass("d-none")) {
                $('input[name="Name"]').addClass("d-none").after(creatureName);
            }
        });
        return false;
    },

}

var commonObjJs = {
    outPutFiles: function (objType) {
        url = "/CommonObjectCanEatDrink/_Output?type=" + objType;
        sysJs.ajax(url, "");
    },
    SaveCanEatDrinkModel: function (kind) {
        //處理tinymce
        tinymce.triggerSave();

        var tinymceCount = $('.tinymcetextarea, .tinymcetext').length;
        if (tinymceCount > 0) {
            var tinymceObj = $('.tinymcetextarea, .tinymcetext');
            tinymceObj.each(
                function () {
                    var content = "";
                    var hval = $(this).val().replace('</p>\n<p>', '<br />');
                    if (hval.length > 0) {
                        var hcon = $(hval).html().replace('<br>', '\r\n');
                        content += sysJs.ConvertToANSIColor(hcon);

                        $(this).val(content);
                    }
                }
            );
        }

        var formID = "JsModel";
        var obj = $('#' + formID).serializeJSON(serializeJSONOpt);

        //處理Multiple selector
        var MultpleCount = $('.MultipleSelect').length;
        var MultipleObj = $('.MultipleSelect');
        if (MultpleCount > 0) {
            for (var i = 0; i < MultpleCount; i++) {
                var selectorName = $(MultipleObj[i]).attr('name');
                obj[selectorName] = $('[name="' + selectorName + '"]').val()
            }
        }
        //end

        //處理Multiple Flag selector變String, MultipleSelectFlag
        var MultipleFlagObj = $('.MultipleSelectFlag');
        if (MultipleFlagObj.length > 0) {
            var selectorName = MultipleFlagObj.attr('name');
            obj[selectorName] = MultipleFlagObj.val().join(", ");
        }
        //end

        var jsonString = JSON.stringify(obj);
        if (jsonString.includes('<span style')) { alert('請檢查文字段落，有Html Tag！'); return false;}

        var url = "/CommonObjectCanEatDrink/_" + kind + "Save?key=" + key;
        sysJs.ajax(url, jsonString, function () {
            if (!$('input[name="SystemCode"]').hasClass("d-none")) {
                $('input[name="SystemCode"]').addClass("d-none").after(obj.SystemCode);
            }
        });
        return false;
    }
}
var tinymceJs = {
    tinymceInit: function (selector, height) {
        if (isNaN(height, 10)) height = 300;
        tinymce.init({
            selector: selector,
            plugins: 'quickbars ',
            toolbar: false,
            quickbars_selection_toolbar: 'forecolor',
            height: height,
            color_map: [
                '000000', '黑',
                '900900', '暗紅',
                '090090', '深綠',
                '990990', '土黃',
                '009009', '深藍',
                '909909', '紫色',
                '099099', '藍綠',
                'BBBBBB', '灰白',
                '666666', '銀灰',
                'f66f66', '大紅',
                '6f66f6', '淺綠',
                'ff6ff6', '亮黃',
                '66f66f', '正藍',
                'f6ff6f', '粉紅',
                '6ff6ff', '淺藍',
                'ffffff', '純白'
            ],
            menubar: false,
            skin: "oxide-dark",
            content_css: "dark"
        });
    },
    tinymceBinding: function () {
        tinymceJs.tinymceInit('.tinymcetextarea', 200);
        tinymceJs.tinymceInit('.tinymcetext', 80);
    }
}


$().ready(function () {
    sysJs.InjectFormAction();
    $('.MultipleSelect,.MultipleSelectFlag').multiselect();
    tinymceJs.tinymceBinding();
})