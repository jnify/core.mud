using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using static Rest.MercuryCore.Loader.LoadRooms.LoadRoom;

namespace Rest.MercuryCore.ManagementConsole.Models
{
    public class RoomDetailViewModel
    {
        private ExistsViewModel[] acceptverbs;
        public FileRoomJsonModel fileRoomJsonModel { get; set; }
        public RoomModel roomModel { get; set; }

        public ExistsViewModel[] Exits
        {
            get
            {
                if (acceptverbs == null && roomModel.Exits != null)
                {
                    acceptverbs = roomModel.Exits.Select (x =>
                        ObjectExtension.DeepClone<ExitSetting, ExistsViewModel> (x).Init (x)).ToArray ();
                }
                if (acceptverbs == null) return new ExistsViewModel[] { };
                return acceptverbs;
            }
        }
    }

    public class ExistsViewModel : ExitSetting
    {
        public bool canLock { get; set; }
        public bool hasDoor { get; set; }
        public ExistsViewModel Init (ExitSetting obj)
        {
            canLock = obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock);
            hasDoor = obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose);
            return this;
        }
    }
}
