using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json.Serialization;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.ManagementConsole.Models.JsonFileObject
{
    public interface IMobObjectBasic
    {
        public string SystemCode { get; set; }
    }

    public class CommonObjectCanEatDrinkJsonBasicModel : IMobObjectBasic
    {
        public CommonObjectCanEatDrinkJsonBasicModel ()
        {
            Weight = 1;
        }

        public string Name { get; set; }
        public string SystemCode { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public string DescriptionDetail { get; set; }
        public string Unit { get; set; }
        public CommonObjectType ObjectType { get; set; }
        public int Weight { get; set; }
        public int Pricing { get; set; }
        public List<CommonObjectDecoration> Decoration { get; set; }
    }

    public class CommonObjectCanEatDrinkJsonModel : CommonObjectCanEatDrinkJsonBasicModel
    {
        public CommonObjectCanEatDrinkJsonModel () : base ()
        {
            DestroyWhenZeroQuota = true;
            AllowExecuteTimes = 1;
        }

        public string ActionKey { get; set; }
        public bool DestroyWhenZeroQuota { get; set; }
        public int AllowExecuteTimes { get; set; }
        public string SuccessMessage { get; set; }
        public string ExtraInformation { get; set; }

    }

    public class CommonObjectCanWearJsonModel : CommonObjectCanEatDrinkJsonModel
    {
        public EquipmentAllowPosition Position { get; set; }
        public WeaponType Type { get; set; }
        public int Volume { get; set; }
        public int Range { get; set; }
        public int Precision { get; set; }
        public AttackPoint ExtraDamageDefance { get; set; }
        public Attr ExtraAttribute { get; set; }

        public CommonObjectCanWearJsonModel () : base ()
        {

        }
    }

    public class CommonObjectSpecialJsonModel : CommonObjectCanEatDrinkJsonModel
    {
        public ObjectActionsVerb AcceptVerbs { get; set; }
        public int KeyNumber { get; set; }
        public GateStatus gateStatus { get; set; }
        public string[] ContainSystemCode { get; set; }
    }
}
