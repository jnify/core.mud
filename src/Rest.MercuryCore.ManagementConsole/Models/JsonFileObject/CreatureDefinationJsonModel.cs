using System.Collections.Generic;
using System.Text.Json.Serialization;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.ManagementConsole.BLL.Loader;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.ManagementConsole.Models.JsonFileObject
{
    //it's partical of UserConfiguration object.
    public class MobBehaivorSettings
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DetailDescription { get; set; }
        public bool AutoHelpFighting { get; set; }
        public int Wimpy { get; set; }

    }

    public class MobLocateInfoJsonModel : IMobDefinationBasic
    {
        public string Name { get; set; }
        public int RoomID { get; set; }
    }
    public class CreatureDefinationBasicJsonModel : IMobDefinationBasic
    {
        public CreatureDefinationBasicJsonModel ()
        {
            //Attribute = new Attr () { };
            StateMax = new BasicAndUpgradeSetting ();
            Settings = new MobBehaivorSettings ();
        }

        public string Name { get; set; }
        public bool AutoMovement { get; set; }
        public int Level { get; set; }
        public CreatureRace Race { get; set; }
        public CreatureGender Gender { get; set; }
        public CreatureCareer Career { get; set; }
        public Dictionary<string, CommonObject> EQArea { get; set; }
        public Attr Attribute { get; set; }
        public BasicAndUpgradeSetting StateMax { get; set; }
        public int BasicAttack { get; set; }
        public int BasicDefence { get; set; }
        public MobBehaivorSettings Settings { get; set; }
        public int Coins { get; set; }
        public MobRole Role { get; set; }
        public int SettingRebornTimeInSecond { get; set; }
        public List<CommonThingDefinition> ThingDef { get; set; }
        public List<CommonThingDefinition> EquipmentDef { get; set; }
        public int TrainingFeeBase { get; set; }
        public string MeetPPLActionKey { get; set; }
        public string[] GreetingMessages { get; set; }
        public List<CommonObjectType> AllowSellingTypes { get; set; }
        public Dictionary<string, string[]> QuestScript { get; set; }

        public string QuestScriptName { get; set; }
        public string[] Murmur { get; set; }
        public List<VendorMenu> Menu { get; set; }
        public Mob_Settings MobSettings { get; set; }
        public List<PracticedSkill> PracticeSkills { get; set; }
        public List<LearnedSkillCategory> LearnedSkillSystem { get; set; }
        public List<KeyValuePair<string, string[]>> ViewQuestScript { get; set; }
    }
}
