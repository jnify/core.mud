using Rest.MercuryCore.ManagementConsole.BLL.Loader;

namespace Rest.MercuryCore.ManagementConsole.Views.Models
{
    public class MobDataViewModel : MobData
    {
        public int RecordCount { get; set; }
    }
}
