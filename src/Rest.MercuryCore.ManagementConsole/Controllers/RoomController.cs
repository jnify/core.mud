﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.ManagementConsole.BLL.Loader;
using Rest.MercuryCore.ManagementConsole.Models;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using static Rest.MercuryCore.Loader.LoadRooms.LoadRoom;

namespace Rest.MercuryCore.ManagementConsole.Controllers
{
    public class RoomController : Controller
    {
        private readonly ILogger<RoomController> _logger;

        public RoomController (ILogger<RoomController> logger)
        {
            _logger = logger;
        }

        public IActionResult RoomParentList (bool reload = false)
        {
            var loadRoomModel = new LoadRoomModel ();
            return View (loadRoomModel.GetRoomModelList (reload));
        }

        public IActionResult RoomFileList (string key)
        {
            var loadRoomModel = new LoadRoomModel ();
            ViewBag.RoomDataKey = key;
            return View (loadRoomModel.GetRoomModelList (key));
        }

        public IActionResult RoomList (string key, string subkey)
        {
            var loadRoomModel = new LoadRoomModel ();
            ViewBag.RoomDataKey = key;
            ViewBag.RoomSubDataKey = subkey;
            return View (loadRoomModel.GetRoomModelList (key, subkey));
        }

        public IActionResult RoomDetail (string key, string subkey, int id)
        {
            var loadRoomModel = new LoadRoomModel ();
            ViewBag.RoomDataKey = key;
            ViewBag.RoomSubDataKey = subkey;

            RoomDetailViewModel model = new RoomDetailViewModel ();
            model.fileRoomJsonModel = loadRoomModel.GetRoomModelList (key, subkey);
            model.roomModel = loadRoomModel.GetRoomModelList (key, subkey, id);
            if (model.roomModel == null)
            {
                model.roomModel = new RoomModel ();
            }
            return View (model);
        }

        public IActionResult NewRoomDetail (string key, string subkey, int fromRoomID, string dimensionFrom)
        {
            var loadRoomModel = new LoadRoomModel ();
            ViewBag.RoomDataKey = key;
            ViewBag.RoomSubDataKey = subkey;
            ViewBag.isNew = true;

            RoomDetailViewModel model = new RoomDetailViewModel ();
            model.fileRoomJsonModel = loadRoomModel.GetRoomModelList (key, subkey);

            //Get from roomModel
            var fromRoomModel = loadRoomModel.GetRoomModelList (key, subkey, fromRoomID);
            model.roomModel = new RoomModel ();
            if (model.fileRoomJsonModel.Rooms != null && model.fileRoomJsonModel.Rooms.Count () > 0)
                model.roomModel.RoomID = model.fileRoomJsonModel.Rooms.Max (r => r.RoomID) + 1;
            if (!string.IsNullOrWhiteSpace (dimensionFrom))
            {
                model.roomModel.Exits = new ExitSetting[]
                {
                    new ExitSetting ()
                    {
                    RoomID = fromRoomID,
                    Dimension = Enum.Parse<ExitName> (dimensionFrom).UpSideDown ()
                    }
                };
            }
            var fromRoomInfo = loadRoomModel.findRoomByRoomID (fromRoomID);
            if (fromRoomInfo.model != null)
            {
                model.roomModel.RoomTitle = fromRoomInfo.model.RoomTitle;
                model.roomModel.Description = fromRoomInfo.model.Description;
            }

            return View ("~/Views/Room/RoomDetail.cshtml", model);
        }

        public IActionResult _SaveRoomModel ([FromBody] FileRoomJsonModel para, [FromQuery] string key, [FromQuery] string subkey)
        {
            var loadRoomModel = new LoadRoomModel ();
            loadRoomModel.UpdateFileRoomJsonModel (para, key, subkey);
            return Ok ("OK");
        }

        public IActionResult _ChangeRoomID ([FromQuery] int oldRoomID, [FromQuery] int newRoomID)
        {
            var loadRoomModel = new LoadRoomModel ();
            if (loadRoomModel.ChangeRoomID (oldRoomID, newRoomID))
            {
                return Ok ("OK");
            }
            else
            {
                return StatusCode (400, "Failed");
            }
        }

        public IActionResult _ChangeExitRoomID ([FromQuery] int RoomID, [FromQuery] ExitName Dimation, [FromQuery] int newExitRoomID, [FromQuery] bool UpdateOppositeDoor)
        {
            var loadRoomModel = new LoadRoomModel ();
            if (loadRoomModel.ChangeExitRoomID (RoomID, Dimation, newExitRoomID, UpdateOppositeDoor))
            {
                return Ok ("OK");
            }
            else
            {
                return StatusCode (400, "Failed");
            }
        }

        public IActionResult _SaveRoomModelDetail ([FromBody] RoomModel para, [FromQuery] string key, [FromQuery] string subkey, [FromQuery] int RoomID, [FromQuery] bool UpdateOppositeDoor)
        {
            var loadRoomModel = new LoadRoomModel ();
            if (loadRoomModel.UpdateRoomJsonModel (para, key, subkey, RoomID, UpdateOppositeDoor))
            {
                return Ok ("OK");
            }
            else
            {
                return StatusCode (400, "Failed");
            }
        }

        public IActionResult _Output ()
        {
            var loadRoomModel = new LoadRoomModel ();
            loadRoomModel.OutPutFilesAsync ().Wait ();
            return Ok ("OK");
        }

        [ResponseCache (Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error ()
        {
            return View (new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
