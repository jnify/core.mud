﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.ManagementConsole.BLL.Loader;
using Rest.MercuryCore.ManagementConsole.Models.JsonFileObject;
using Rest.MercuryCore.ManagementConsole.Views.Models;

namespace Rest.MercuryCore.ManagementConsole.Controllers
{
    public class MobDefinationController : Controller
    {
        private readonly ILogger<MobDefinationController> _logger;
        private readonly IActionLoader actionLoader;

        public MobDefinationController (ILogger<MobDefinationController> logger, IActionLoader actionLoader)
        {
            _logger = logger;
            this.actionLoader = actionLoader;
        }

        public IActionResult FirstLayer (bool reload = false)
        {
            var loadobject = new LoadMobDefinition ();
            //Load mob locate
            new LoadMobLocateInfoModel (reload);
            return View (loadobject.GetFromFirstLayer (reload));
        }

        public IActionResult SecondLayer (string SubFolder)
        {
            ViewBag.SubFolder = SubFolder;
            var loadobject = new LoadMobDefinition ();
            return View (loadobject.GetFromSecondLayer (SubFolder));
        }

        public IActionResult ModelList (string SubFolder, string FileName)
        {
            ViewBag.SubFolder = SubFolder;
            ViewBag.FileName = FileName;
            var loadobject = new LoadMobDefinition ();
            var viewModel = loadobject.GetFromFileName (SubFolder, FileName);

            //get mob located
            var loadmoblocate = new LoadMobLocateInfoModel ();
            Dictionary<string, string> moblocate = new Dictionary<string, string> ();
            var mobNames = viewModel.Select (x => x.Name).ToList ();
            moblocate = loadmoblocate.GetSumByNames (mobNames);
            ViewBag.MobLocate = moblocate;

            return View (viewModel);
        }

        public IActionResult ModelDetail (string SubFolder, string FileName, string Name)
        {
            ViewBag.SubFolder = SubFolder;
            ViewBag.FileName = FileName;
            ViewBag.AllMobActions = actionLoader.GetAllMobAction ().Select (x => x.Key).ToList ();
            var loadobject = new LoadMobDefinition ();
            var model = loadobject.GetByName (Name);
            if (string.IsNullOrWhiteSpace (model.Name))
            {
                model = new CreatureDefinationBasicJsonModel ();
            }
            return View (model);
        }

        public IActionResult _Save ([FromBody] CreatureDefinationBasicJsonModel para, [FromQuery] string SubFolder, [FromQuery] string FileName, [FromQuery] string Name)
        {
            var loadobject = new LoadMobDefinition ();
            if (para.ViewQuestScript != null && para.ViewQuestScript.Count () > 0)
            {
                para.QuestScript = para.ViewQuestScript.ToDictionary (x => x.Key, x => x.Value);
            }
            para.ViewQuestScript = null;
            loadobject.Update (Name, SubFolder, FileName, para);
            return Ok ("OK");
        }
        public IActionResult _Output ()
        {
            var obj = new LoadMobDefinition ();
            obj.OutPutFilesAsync ().Wait ();
            return Ok ("OK");
        }

    }
}
