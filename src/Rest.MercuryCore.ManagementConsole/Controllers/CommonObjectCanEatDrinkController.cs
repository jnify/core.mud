﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.ManagementConsole.BLL.Loader;
using Rest.MercuryCore.ManagementConsole.Models.JsonFileObject;

namespace Rest.MercuryCore.ManagementConsole.Controllers
{
    public class CommonObjectCanEatDrinkController : Controller
    {
        private readonly ILogger<CommonObjectCanEatDrinkController> _logger;
        private readonly IActionLoader actionLoader;

        public CommonObjectCanEatDrinkController (ILogger<CommonObjectCanEatDrinkController> logger, IActionLoader actionLoader)
        {
            _logger = logger;
            this.actionLoader = actionLoader;
        }

        public IActionResult _Output (string type)
        {
            var LoadObjName = $"Load{type}";
            ILoadCommonObject comObj;
            switch (LoadObjName)
            {
                case "LoadCanEat":
                    comObj = new LoadCanEat ();
                    break;
                case "LoadCanDrink":
                    comObj = new LoadCanDrink ();
                    break;
                case "LoadCanWear":
                    comObj = new LoadCanWear ();
                    break;
                default:
                    comObj = new LoadSpecialObj ();
                    break;
            }
            comObj.OutPutFilesAsync ().Wait ();
            return Ok ("OK");
        }

        ////////////////////////////////////////////////////////////////
        public IActionResult CanEatFiles (bool reload = false)
        {
            var loadobject = new LoadCanEat ();
            return View (loadobject.GetCommonObjectModelALL (reload));
        }

        public IActionResult CanEatList (string key)
        {
            ViewBag.Key = key;
            var loadobject = new LoadCanEat ();
            return View (loadobject.GetCommonObjectModelList (key));
        }

        public IActionResult CanEatDetail (string key, string systemcode)
        {
            ViewBag.Key = key;
            ViewBag.Systemcode = systemcode;
            var loadobject = new LoadCanEat ();
            return View (loadobject.GetCommonObjectDetail (key, systemcode));
        }

        public IActionResult _CanEatSave ([FromBody] CommonObjectCanEatDrinkJsonModel para, [FromQuery] string key)
        {
            var loadobject = new LoadCanEat ();
            var result = loadobject.UpdateCommonObject (key, para);
            if (result)
                return Ok ("OK");
            else
                return StatusCode (400, "Failed");
        }
        ////////////////////////////////////////////////////////////////
        public IActionResult CanDrinkFiles (bool reload = false)
        {
            var loadobject = new LoadCanDrink ();
            return View (loadobject.GetCommonObjectModelALL (reload));
        }

        public IActionResult CanDrinkList (string key)
        {
            ViewBag.Key = key;
            var loadobject = new LoadCanDrink ();
            return View (loadobject.GetCommonObjectModelList (key));
        }
        public IActionResult CanDrinkDetail (string key, string systemcode)
        {
            ViewBag.Key = key;
            ViewBag.Systemcode = systemcode;
            var loadobject = new LoadCanDrink ();
            return View (loadobject.GetCommonObjectDetail (key, systemcode));
        }

        public IActionResult _CanDrinkSave ([FromBody] CommonObjectCanEatDrinkJsonModel para, [FromQuery] string key)
        {
            var loadobject = new LoadCanDrink ();
            var result = loadobject.UpdateCommonObject (key, para);
            if (result)
                return Ok ("OK");
            else
                return StatusCode (400, "Failed");
        }
        ////////////////////////////////////////////////////////////////
        public IActionResult CanWearFiles (bool reload = false)
        {
            var loadobject = new LoadCanWear ();
            return View (loadobject.GetCommonObjectModelALL (reload));
        }

        public IActionResult CanWearList (string key)
        {
            ViewBag.Key = key;
            var loadobject = new LoadCanWear ();
            return View (loadobject.GetCommonObjectModelList (key));
        }
        public IActionResult CanWearDetail (string key, string systemcode)
        {
            ViewBag.Key = key;
            ViewBag.Systemcode = systemcode;
            var loadobject = new LoadCanWear ();
            return View (loadobject.GetCommonObjectDetail (key, systemcode));
        }
        public IActionResult _CanWearSave ([FromBody] CommonObjectCanWearJsonModel para, [FromQuery] string key)
        {
            var loadobject = new LoadCanWear ();
            var result = loadobject.UpdateCommonObject (key, para);
            if (result)
                return Ok ("OK");
            else
                return StatusCode (400, "Failed");
        }
        ////////////////////////////////////////////////////////////////
        public IActionResult SpecialObjFiles (bool reload = false)
        {
            var loadobject = new LoadSpecialObj ();
            return View (loadobject.GetCommonObjectModelALL (reload));
        }

        public IActionResult SpecialObjList (string key)
        {
            ViewBag.Key = key;
            var loadobject = new LoadSpecialObj ();
            return View (loadobject.GetCommonObjectModelList (key));
        }
        public IActionResult SpecialObjDetail (string key, string systemcode)
        {
            ViewBag.Key = key;
            ViewBag.Systemcode = systemcode;
            var loadobject = new LoadSpecialObj ();
            return View (loadobject.GetCommonObjectDetail (key, systemcode));
        }
        public IActionResult _SpecialObjSave ([FromBody] CommonObjectSpecialJsonModel para, [FromQuery] string key)
        {
            var loadobject = new LoadSpecialObj ();
            var result = loadobject.UpdateCommonObject (key, para);
            if (result)
                return Ok ("OK");
            else
                return StatusCode (400, "Failed");
        }
    }
}
