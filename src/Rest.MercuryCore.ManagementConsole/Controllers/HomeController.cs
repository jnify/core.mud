﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.ManagementConsole.Models;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.ManagementConsole.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IActionLoader actionLoader;

        private static bool ActionMobIsEmpty = true;

        public HomeController (ILogger<HomeController> logger, IActionLoader actionLoader)
        {
            _logger = logger;
            this.actionLoader = actionLoader;
        }

        public IActionResult Index ()
        {
            if (ActionMobIsEmpty)
            {
                var thisType = typeof (Rest.MercuryCore.EventActions.ActionMob);
                var methods = thisType.GetMethods ();
                string FuctionPrefixName = "Action";
                int FuctionPrefixNameLength = FuctionPrefixName.Length;
                foreach (var method in methods)
                {
                    if (method.Name.StartsWith (FuctionPrefixName))
                    {
                        Func<ActionKeyParameter, CommonObject[], string> actionFunction = null;
                        string ActionKey = method.Name.Substring (FuctionPrefixNameLength);
                        actionLoader.AddMobAction (ActionKey, actionFunction);
                    }
                }
                ActionMobIsEmpty = false;
            }
            return View ();
        }

        [ResponseCache (Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error ()
        {
            return View (new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
