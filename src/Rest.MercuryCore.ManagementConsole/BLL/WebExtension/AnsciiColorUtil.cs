using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Rest.MercuryCore.ManagementConsole.BLL.WebExtension
{
    public class AnsciiColorUtil
    {
        private static Dictionary<string, string> colormapping = new Dictionary<string, string> ()
        { { "0", "000000" }, { "31", "900900" }, { "32", "090090" }, { "33", "990990" }, { "34", "009009" }, { "35", "909909" }, { "36", "099099" }, { "37", "BBBBBB" }, { "1;30", "666666" }, { "1;31", "f66f66" }, { "1;32", "6f66f6" }, { "1;33", "ff6ff6" }, { "1;34", "66f66f" }, { "1;35", "f6ff6f" }, { "1;36", "6ff6ff" }, { "1;37", "ffffff" }
        };
        private static string pattern = @"\x1b\[(\d;?\d*)m";

        public static string ConvertToHtmlSpan (string msg)
        {
            string rtn = default;
            if (!string.IsNullOrWhiteSpace (msg))
            {
                rtn = msg.Replace ("\x1b[0m", "</span>");
                var matchGroup = Regex.Matches (rtn, pattern);
                foreach (Match match in matchGroup)
                {
                    string ansciColor = match.Groups[1].Value;
                    string span = "<span style=\"color: #" + colormapping[ansciColor] + ";\">";
                    rtn = rtn.Replace (match.Groups[0].Value, span);
                }
                rtn = rtn.Replace ("\r\n", "<br>");
            }
            return rtn;
        }
    }
}
