using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.ManagementConsole.BLL.WebExtension
{
    public static class EnumExtensionWeb
    {
        public static IEnumerable<SelectListItem> GetEnumDescriptionSelectList<TEnum> (this IHtmlHelper htmlHelper) where TEnum : struct, IConvertible
        {
            return new SelectList (Enum.GetValues (typeof (TEnum)).OfType<Enum> ()
                .Select (x =>
                    new SelectListItem
                    {
                        Text = Enum.Parse<TEnum> (x.ToString ()).GetDescriptionText (true),
                            Value = x.ToString ()
                    }), "Value", "Text");
        }

        public static IEnumerable<SelectListItem> GetEnumDescriptionSelectListSkill<TEnum> (this IHtmlHelper htmlHelper) where TEnum : struct, IConvertible
        {
            SkillName.Circle.GetAttribute<SkillAttribute> ();
            return new SelectList (Enum.GetValues (typeof (TEnum)).OfType<Enum> ()
                .Select (x =>
                    new SelectListItem
                    {
                        Text = Enum.TryParse<SkillName> (x.ToString (), true, out var skillname) ? skillname.GetAttribute<SkillAttribute> ().Description : "Not a Skill Name",
                            Value = x.ToString ()
                    }), "Value", "Text");
        }
    }
}
