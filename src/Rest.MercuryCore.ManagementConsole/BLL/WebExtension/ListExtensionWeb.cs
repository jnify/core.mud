using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.ManagementConsole.BLL.WebExtension
{
    public static class ListExtensionWeb
    {
        public static T Pop<T> (this List<T> source)
        {
            T rtn = default;
            if (source != null && source.Count () > 0)
            {
                rtn = source.First ();
                source.RemoveAt (0);
            }
            return rtn;
        }
    }
}
