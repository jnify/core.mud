using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using static Rest.MercuryCore.Loader.LoadRooms.LoadRoom;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public class LoadMobModel
    {
        private static Dictionary<string, Dictionary<string, List<MobCommonDefinition>>> mobData = new Dictionary<string, Dictionary<string, List<MobCommonDefinition>>> ();
        private string path { get; set; }
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ()
        {
            Encoder = JavaScriptEncoder.Create (UnicodeRanges.All),
            WriteIndented = true,
            IgnoreNullValues = true
        };

        public LoadMobModel ()
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
            this.path = $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/Mob";
        }

        internal Dictionary<string, Dictionary<string, List<MobCommonDefinition>>> GetMobModelList (bool reload)
        {
            if (mobData.Count () == 0 || reload)
            {
                mobData.Clear ();
                if (Directory.Exists (path))
                {
                    string[] folders = Directory.GetDirectories (path);
                    foreach (var subpath in folders)
                    {
                        string LastFolderName = subpath.Split ("/").Last ();
                        mobData.Add (LastFolderName, new Dictionary<string, List<MobCommonDefinition>> ());

                        var files = Directory.GetFiles (subpath, "*.json");
                        foreach (string fileName in files)
                        {
                            try
                            {
                                string LastFileName = fileName.Split ("/").Last ();
                                string fs = File.ReadAllText (fileName);
                                var t = JsonSerializer.Deserialize<List<MobCommonDefinition>> (fs, jsoptions);
                                mobData[LastFolderName].Add (LastFileName, t);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine (ex.Message);
                            }
                        }
                    }
                }
            }
            return mobData;
        }

        internal async Task OutPutFilesAsync ()
        {
            foreach (var key in mobData)
            {
                foreach (var subkey in key.Value)
                {
                    string fileFullName = Path.Combine (path, key.Key, subkey.Key);
                    var JsonModel = subkey.Value;
                    JsonModel = JsonModel.OrderBy (mob => mob.Name).ToList ();
                    var jsonString = JsonSerializer.Serialize<List<MobCommonDefinition>> (JsonModel, jsoptions);
                    await File.WriteAllTextAsync (fileFullName, jsonString, Encoding.UTF8);
                }
            }
        }

        internal void UpdateMobCommonDefinition (MobCommonDefinition para, string key, string subkey)
        {
            var mob = mobData[key][subkey].Where (mob => mob.Name == para.Name).FirstOrDefault ();
            mobData[key][subkey].Remove (mob);
            mobData[key][subkey].Add (para);
        }

        internal Dictionary<string, List<MobCommonDefinition>> GetMobModelList (string fileName)
        {
            return mobData[fileName];
        }

        internal MobCommonDefinition GetMobModel (string key, string subkey, string name)
        {
            var rtn = mobData[key][subkey].Where (def => def.Name == name).FirstOrDefault ();
            if (rtn == null) rtn = new MobCommonDefinition ();
            return rtn;
        }

        internal List<MobCommonDefinition> GetMobListModel (string key, string subkey)
        {
            return mobData[key][subkey];
        }
    }
}
