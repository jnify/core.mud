using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public class MobData
    {
        public int RoomID { get; set; }
        public string Name { get; set; }
    }

    public class LoadMobLocateInfoModel
    {
        Dictionary<string, Dictionary<string, List<MobData>>> LocateCache = new Dictionary<string, Dictionary<string, List<MobData>>> ();
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ();
        private string DataFullFolderPath = $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Data/Mob";

        public LoadMobLocateInfoModel (bool reload = false)
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.WriteIndented = true;
            if (LocateCache.Count () == 0 || reload == true)
            {
                LocateCache.Clear ();
                _LoadFile (DataFullFolderPath);
            }
        }

        public List<MobData> GetAllByName (string name)
        {
            List<MobData> rtn = new List<MobData> ();
            foreach (var subfolder in LocateCache)
            {
                foreach (var filename in subfolder.Value)
                {
                    rtn.AddRange (filename.Value.Where (x => x.Name == name));
                }
            }
            return rtn;
        }

        public Dictionary<string, string> GetSumByNames (List<string> MobNames)
        {
            Dictionary<string, string> rtn = new Dictionary<string, string> ();
            List < (string MobName, string RoomID, int Count) > mobGrouped = new List < (string MobName, string RoomID, int Count) > ();
            foreach (var subfolder in LocateCache)
            {
                foreach (var filename in subfolder.Value)
                {
                    var mobInRoomData = filename.Value.Where (x => MobNames.Contains (x.Name)).ToList ();
                    var test = mobInRoomData.GroupBy (obj => new { MobName = obj.Name, RoomID = obj.RoomID })
                        .Select (x => (x.Key.MobName, x.Key.RoomID.ToString (), Count : x.Count ())).ToList ();
                    mobGrouped.AddRange (test);
                }
            }
            MobNames.ForEach (InputMobName =>
            {
                if (rtn.ContainsKey (InputMobName))
                {
                    Console.WriteLine ("Dulpitle Mob Name: " + InputMobName);
                }
                else
                {
                    List<string> roomInfo = new List<string> ();
                    mobGrouped.Where (x => x.MobName == InputMobName).ToList ().ForEach (groupedInfo =>
                    {
                        if (groupedInfo.Count > 1)
                        {
                            roomInfo.Add ($"{groupedInfo.RoomID}({groupedInfo.Count})");
                        }
                        else
                        {
                            roomInfo.Add (groupedInfo.RoomID);
                        }
                    });
                    rtn.Add (InputMobName, string.Join (",", roomInfo));
                }
            });
            return rtn;
        }

        private void _LoadFile (string folder)
        {
            if (Directory.Exists (folder))
            {
                string[] subFolder = Directory.GetDirectories (folder);
                foreach (var subpath in subFolder)
                {
                    _LoadFile (subpath);
                }

                var files = Directory.GetFiles (folder, "*.json");
                foreach (string fileName in files)
                {
                    try
                    {
                        LoadFile (fileName, folder);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine (ex.Message);
                    }
                }
            }
        }
        public void LoadFile (string FullFileName, string SubFolder)
        {
            if (!LocateCache.ContainsKey (SubFolder))
                LocateCache.Add (SubFolder, new Dictionary<string, List<MobData>> ());

            if (!LocateCache[SubFolder].ContainsKey (FullFileName))
                LocateCache[SubFolder].Add (FullFileName, new List<MobData> ());

            string fs = File.ReadAllText (FullFileName);
            var t = JsonSerializer.Deserialize<List<MobData>> (fs, jsoptions);
            LocateCache[SubFolder][FullFileName].AddRange (t);
        }

        public async Task OutPutFilesAsync ()
        {
            if (LocateCache.Count () > 0)
            {
                foreach (var data in LocateCache)
                {
                    string folderName = data.Key;
                    foreach (var filedata in data.Value)
                    {
                        string fileFullName = filedata.Key;
                        var JsonModel = filedata.Value.OrderBy (x => x.Name).ThenBy (x => x.RoomID).ToList ();
                        var jsonString = JsonSerializer.Serialize<List<MobData>> (JsonModel, jsoptions);
                        await File.WriteAllTextAsync (fileFullName, jsonString, Encoding.UTF8);
                    }
                }
            }
        }
    }
}
