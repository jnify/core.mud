using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.ManagementConsole.Models.JsonFileObject;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public class LoadAll : LoadCommonObject_ABS<CommonObjectCanEatDrinkJsonBasicModel>
    {
        public override string Path => $"Fake";
        public void GetAll ()
        {
            Dictionary<string, string> rtn = new Dictionary<string, string> ();
            foreach (var d1 in GetCacheAll)
            {
                foreach (var d2 in d1.Value)
                {
                    var objList = d2.Value.ToDictionary (x => x.SystemCode, x => $"{x.ShortName}({x.Name})");
                    rtn = rtn.Union (objList).ToDictionary (t => t.Key, t => t.Value);
                }
            }
        }

    }

    public class LoadCanEat : LoadCommonObject_ABS<CommonObjectCanEatDrinkJsonModel>
    {
        public override string Path => $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/CommonObjectCanEat";
    }

    public class LoadCanDrink : LoadCommonObject_ABS<CommonObjectCanEatDrinkJsonModel>
    {
        public override string Path => $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/CommonObjectCanDrink";
    }

    public class LoadCanWear : LoadCommonObject_ABS<CommonObjectCanWearJsonModel>
    {
        public override string Path => $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/CommonObjectCanWear";
    }

    public class LoadSpecialObj : LoadCommonObject_ABS<CommonObjectSpecialJsonModel>
    {
        public override string Path => $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/CommonObjectSpecial";
    }

    public class LoadMobDefinition : LoadJsonFilesRecursive_ABS<CreatureDefinationBasicJsonModel>
    {
        public override string RootPath => $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/Mob";
    }
}
