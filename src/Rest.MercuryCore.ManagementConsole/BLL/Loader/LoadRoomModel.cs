using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using static Rest.MercuryCore.Loader.LoadRooms.LoadRoom;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public class LoadRoomModel
    {
        private static Dictionary<string, Dictionary<string, FileRoomJsonModel>> roomdata = new Dictionary<string, Dictionary<string, FileRoomJsonModel>> ();
        private string path { get; set; }
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ()
        {
            Encoder = JavaScriptEncoder.Create (UnicodeRanges.All),
            WriteIndented = true,
            IgnoreNullValues = true
        };

        public LoadRoomModel ()
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
            this.path = $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Data/Room";
        }

        public Dictionary<string, FileRoomJsonModel> GetRoomModelList (string fileName)
        {
            return roomdata[fileName];
        }

        public FileRoomJsonModel GetRoomModelList (string key, string subkey)
        {
            return roomdata[key][subkey];
        }

        public RoomModel GetRoomModelList (string key, string subkey, int id)
        {
            if (roomdata[key][subkey].Rooms != null)
                return roomdata[key][subkey].Rooms.Where (x => x.RoomID == id).FirstOrDefault ();
            else
                return default;
        }

        public Dictionary<string, Dictionary<string, FileRoomJsonModel>> GetRoomModelList (bool reload = false)
        {
            if (roomdata.Count () == 0 || reload)
            {
                roomdata.Clear ();
                if (Directory.Exists (path))
                {
                    string[] folders = Directory.GetDirectories (path);
                    foreach (var subpath in folders)
                    {
                        string LastFolderName = subpath.Split ("/").Last ();
                        roomdata.Add (LastFolderName, new Dictionary<string, FileRoomJsonModel> ());

                        var files = Directory.GetFiles (subpath, "*.json");
                        foreach (string fileName in files)
                        {
                            try
                            {
                                string LastFileName = fileName.Split ("/").Last ();
                                string fs = File.ReadAllText (fileName);
                                var t = JsonSerializer.Deserialize<FileRoomJsonModel> (fs, jsoptions);
                                roomdata[LastFolderName].Add (LastFileName, t);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine (ex.Message);
                            }
                        }
                    }
                }
            }
            return roomdata;

        }

        internal bool UpdateRoomJsonModel (RoomModel para, string key, string subkey, int roomID, bool UpdateOppositeDoor)
        {
            if (para == null) return false;
            if (roomID == 0)
            {
                //新增，要先確認RoomID是否已存在
                foreach (var l1 in roomdata)
                {
                    string KeyL1 = l1.Key;
                    foreach (var l2 in l1.Value)
                    {
                        string SubKeyL2 = l2.Key;
                        if (l2.Value.Rooms == null) l2.Value.Rooms = new List<RoomModel> ();
                        //RoomID不能重覆
                        if (l2.Value.Rooms.Any (room => room.RoomID == para.RoomID)) return false;
                    }
                }
            }
            roomdata[key][subkey].Rooms.RemoveAll (room => room.RoomID == roomID);
            roomdata[key][subkey].Rooms.Add (para);
            roomdata[key][subkey].Rooms = roomdata[key][subkey].Rooms.OrderBy (x => x.RoomID).ToList ();
            if (UpdateOppositeDoor)
            {
                foreach (var exit in para.Exits)
                {
                    InsUpdateUpsideDoorSetting (roomID, exit.Dimension, exit.RoomID);
                }
            }
            OutPut (key, subkey).Wait ();
            return true;
        }

        internal bool ChangeExitRoomID (int roomID, ExitName dimation, int newExitRoomID, bool UpdateOppositeDoor)
        {
            if (roomID == 0 || dimation == ExitName.None) return false;
            var roomInfo = findRoomByRoomID (roomID);
            var exit = roomInfo.model.Exits.Where (ex => ex.Dimension == dimation).FirstOrDefault ();
            if (newExitRoomID == 0)
            {
                if (exit != null)
                {
                    if (UpdateOppositeDoor)
                    {
                        //找出對向門，並刪除
                        var posExit = roomInfo.model.Exits.Where (x => x.Dimension == dimation).FirstOrDefault ();
                        if (posExit != null)
                        {
                            var posRoom = findRoomByRoomID (posExit.RoomID).model;
                            if (posRoom != null)
                            {
                                posRoom.Exits = posRoom.Exits.Where (x => x.RoomID != roomID).ToArray ();
                            }
                        }
                    }
                    roomInfo.model.Exits = roomInfo.model.Exits.Where (x => x.Dimension != dimation).ToArray ();
                }
                OutPut (roomInfo.Key, roomInfo.SubKey).Wait ();
                return true;
            }
            else
            {
                if (exit != null)
                {
                    exit.RoomID = newExitRoomID;
                }
                else
                {
                    roomInfo.model.Exits = roomInfo.model.Exits.Push (new ExitSetting ()
                    {
                        RoomID = newExitRoomID,
                            AcceptVerbs = ObjectActionsVerb.None,
                            Dimension = dimation
                    });
                }
                if (UpdateOppositeDoor)
                {
                    InsUpdateUpsideDoorSetting (roomID, dimation, newExitRoomID);
                }
                OutPut (roomInfo.Key, roomInfo.SubKey).Wait ();
                return true;
            }
        }

        private void InsUpdateUpsideDoorSetting (int CurrentRoomID, ExitName CurrentRoomDimation, int LinkToRoomID)
        {
            var posRoom = findRoomByRoomID (LinkToRoomID).model;
            var upsideExit = CurrentRoomDimation.UpSideDown ();
            if (posRoom != null)
            {
                var postExit = posRoom.Exits.Where (x => x.Dimension == upsideExit).FirstOrDefault ();
                if (postExit != null)
                {
                    postExit.RoomID = CurrentRoomID;
                }
                else
                {
                    posRoom.Exits = posRoom.Exits.Push (new ExitSetting ()
                    {
                        RoomID = CurrentRoomID,
                            AcceptVerbs = ObjectActionsVerb.CanOpenClose,
                            Dimension = upsideExit

                    });
                }
            }
        }

        public (string Key, string SubKey, RoomModel model) findRoomByRoomID (int roomID)
        {
            foreach (var l1 in roomdata)
            {
                string KeyL1 = l1.Key;
                foreach (var l2 in l1.Value)
                {
                    string SubKeyL2 = l2.Key;
                    if (l2.Value.Rooms.Any (room => room.RoomID == roomID))
                        return (KeyL1, SubKeyL2, l2.Value.Rooms.Where (room => room.RoomID == roomID).First ());
                }
            }
            return (null, null, null);
        }

        internal bool ChangeRoomID (int oldRoomID, int newRoomID)
        {
            //先檢查，newRoomID有沒有重覆
            foreach (var l1 in roomdata)
            {
                foreach (var l2 in l1.Value)
                {
                    string SubKeyL2 = l2.Key;
                    if (l2.Value.Rooms.Any (room => room.RoomID == newRoomID)) return false;
                }
            }

            //沒有重覆，改掉RoomID
            foreach (var l1 in roomdata)
            {
                foreach (var l2 in l1.Value)
                {
                    if (l2.Value.Rooms.Any (room => room.RoomID == oldRoomID))
                    {
                        var room = roomdata[l1.Key][l2.Key].Rooms.Where (room => room.RoomID == oldRoomID).First ();
                        room.RoomID = newRoomID;
                        OutPut (l1.Key, l2.Key).Wait ();
                        return true;
                    }
                }
            }
            return false;
        }

        internal async Task OutPutFilesAsync ()
        {
            foreach (var key in roomdata)
            {
                foreach (var subkey in key.Value)
                {
                    string fileFullName = Path.Combine (path, key.Key, subkey.Key);
                    await OutPut (key.Key, subkey.Key);
                }
            }
        }

        private async Task OutPut (string Key, string Subkey)
        {
            string fileFullName = Path.Combine (path, Key, Subkey);
            if (roomdata.ContainsKey (Key) && roomdata[Key].ContainsKey (Subkey) && roomdata[Key][Subkey].Rooms != null)
            {
                var fileRoomJsonModel = roomdata[Key][Subkey];
                fileRoomJsonModel.Rooms.ForEach (room =>
                {
                    if (room.Exits != null)
                        room.Exits = room.Exits.OrderBy (exit => exit.Dimension).ToArray ();
                });
                fileRoomJsonModel.Rooms = fileRoomJsonModel.Rooms.OrderBy (room => room.RoomID).ToList ();
                fileRoomJsonModel.Rooms.ForEach (x =>
                {
                    if (x.Exits != null && x.Exits.Count () > 0)
                        x.Exits = x.Exits.OrderBy (exit => exit.Dimension).ToArray ();
                });
                var jsonString = JsonSerializer.Serialize<FileRoomJsonModel> (fileRoomJsonModel, jsoptions);
                await File.WriteAllTextAsync (fileFullName, jsonString, Encoding.UTF8);
            }
        }

        public void UpdateFileRoomJsonModel (FileRoomJsonModel input, string key, string subkey)
        {
            roomdata[key][subkey].AreaGeography = input.AreaGeography;
            roomdata[key][subkey].DefaultCeiling = input.DefaultCeiling;
            OutPut (key, subkey).Wait ();
        }
    }
}
