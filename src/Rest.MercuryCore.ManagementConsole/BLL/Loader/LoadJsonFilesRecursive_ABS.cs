using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.ManagementConsole.Models.JsonFileObject;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public interface IMobDefinationBasic
    {
        public string Name { get; set; }
    }

    public interface ILoadJsonFiles<T> where T : IMobDefinationBasic
    {
        Dictionary<string, Dictionary<string, List<T>>> GetFromFirstLayer (bool reload);
        Dictionary<string, List<T>> GetFromSecondLayer (string Key);
        T GetByName (string Name);
        Task OutPutFilesAsync ();
    }

    public abstract class LoadJsonFilesRecursive_ABS<T> : ILoadJsonFiles<T> where T : IMobDefinationBasic, new ()
    {
        public class ObjectDictionary<X> where X : IMobDefinationBasic, new ()
        {
            public string SubFolderName { get; set; }
            public string FileName { get; set; }
            public string Name => Content.Name;
            public X Content { get; set; }
        }

        abstract public string RootPath { get; }

        private static Dictionary<string, List<ObjectDictionary<T>>> ObjectInDictionary = new Dictionary<string, List<ObjectDictionary<T>>> ();
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ()
        {
            Encoder = JavaScriptEncoder.Create (UnicodeRanges.All),
            WriteIndented = true,
            IgnoreNullValues = true
        };

        public LoadJsonFilesRecursive_ABS ()
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
        }

        public async Task OutPutFilesAsync ()
        {
            if (ObjectInDictionary.ContainsKey (RootPath))
            {
                var dicObj = ObjectInDictionary[RootPath];
                if (dicObj != null)
                {
                    Dictionary<string, List<T>> fileSource = new Dictionary<string, List<T>> ();
                    dicObj.ForEach (x =>
                    {
                        string fullFilePath = System.IO.Path.Combine (RootPath, x.SubFolderName, x.FileName);
                        if (!fileSource.ContainsKey (fullFilePath)) fileSource.Add (fullFilePath, new List<T> ());
                        fileSource[fullFilePath].Add (x.Content);
                    });

                    foreach (var key in fileSource)
                    {
                        string fileFullName = key.Key;
                        var JsonModel = key.Value.OrderBy (x => x.Name).ToList ();
                        var jsonString = JsonSerializer.Serialize<List<T>> (JsonModel, jsoptions);
                        await File.WriteAllTextAsync (fileFullName, jsonString, Encoding.UTF8);
                    }
                }
            }
            LoadMobLocateInfoModel loadmoblocate = new LoadMobLocateInfoModel ();
            await loadmoblocate.OutPutFilesAsync ();
        }

        private void LoadFromFile (bool reload)
        {
            if (!ObjectInDictionary.ContainsKey (RootPath) || ObjectInDictionary[RootPath].Count () == 0 || reload)
            {
                if (!ObjectInDictionary.ContainsKey (RootPath)) ObjectInDictionary.Add (RootPath, new List<ObjectDictionary<T>> ());
                ObjectInDictionary[RootPath].Clear ();
                if (Directory.Exists (RootPath))
                {
                    string[] folders = Directory.GetDirectories (RootPath);
                    foreach (var subpath in folders)
                    {
                        string SubFolderName = subpath.Split ("/").Last ();
                        var files = Directory.GetFiles (subpath, "*.json");
                        foreach (string fileName in files)
                        {
                            try
                            {
                                string LastFileName = fileName.Split ("/").Last ();
                                string fs = File.ReadAllText (fileName);
                                var t = JsonSerializer.Deserialize<List<T>> (fs, jsoptions);
                                var dicObjList = t.Select (x => new ObjectDictionary<T> ()
                                {
                                    SubFolderName = SubFolderName,
                                        FileName = LastFileName,
                                        Content = x
                                }).ToList ();
                                ObjectInDictionary[RootPath].AddRange (dicObjList);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine (ex.Message);
                            }
                        }
                    }
                }
            }
        }

        //NewBie, NewbieImmArea
        public Dictionary<string, Dictionary<string, List<T>>> GetFromFirstLayer (bool reload)
        {
            LoadFromFile (reload);
            var dicObj = ObjectInDictionary[RootPath];
            var rtn = dicObj.GroupBy (x => x.SubFolderName).ToDictionary (x => x.Key,
                x => x.GroupBy (y => y.FileName).ToDictionary (y => y.Key, y => y.Select (z => z.Content).ToList ())
            );
            return rtn;
        }

        //NewbieMobDefined.json, QuestMobDefined.json
        public Dictionary<string, List<T>> GetFromSecondLayer (string SubFolder)
        {
            if (ObjectInDictionary.ContainsKey (RootPath))
            {
                var dicObj = ObjectInDictionary[RootPath].Where (x => x.SubFolderName == SubFolder);
                var rtn = dicObj.GroupBy (y => y.FileName).ToDictionary (y => y.Key, y => y.Select (z => z.Content).ToList ());
                return rtn;
            }
            else
            {
                return new Dictionary<string, List<T>> ();
            }
        }

        public List<T> GetFromFileName (string SubFolder, string FileName)
        {
            if (ObjectInDictionary.ContainsKey (RootPath))
            {
                var dicObj = ObjectInDictionary[RootPath].Where (x => x.SubFolderName == SubFolder && x.FileName == FileName);
                var rtn = dicObj.Select (x => x.Content).ToList ();
                return rtn;
            }
            else
            {
                return new List<T> ();
            }
        }

        public T GetByName (string Name)
        {
            var obj = ObjectInDictionary[RootPath].Where (x => x.Name == Name).FirstOrDefault ();
            if (obj == null) return new T ();
            return obj.Content;
        }

        internal void Update (string Name, string SubFolderName, string FileName, T para)
        {
            if (para != null)
            {
                ObjectDictionary<T> dicObj = new ObjectDictionary<T> () { };
                dicObj.SubFolderName = SubFolderName;
                dicObj.FileName = FileName;
                dicObj.Content = para;
                if (ObjectInDictionary[RootPath].Any (x => x.Name == Name))
                {
                    ObjectInDictionary[RootPath].RemoveAll (x => x.Name == Name);
                }
                ObjectInDictionary[RootPath].Add (dicObj);
                ObjectInDictionary[RootPath] = ObjectInDictionary[RootPath].OrderBy (x => x.Name).ToList ();

                var jsonOriginalObj = ObjectInDictionary[RootPath].Where (x => x.FileName == FileName && x.SubFolderName == SubFolderName).Select (x => x.Content).ToList ();
                string FullFilePath = Path.Combine (RootPath, SubFolderName, FileName);
                var jsonString = JsonSerializer.Serialize<List<T>> (jsonOriginalObj, jsoptions);
                File.WriteAllTextAsync (FullFilePath, jsonString, Encoding.UTF8).Wait ();
            }
        }
    }
}
