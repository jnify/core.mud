using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.ManagementConsole.Models.JsonFileObject;

namespace Rest.MercuryCore.ManagementConsole.BLL.Loader
{
    public interface ILoadCommonObject
    {
        public Task OutPutFilesAsync ();
    }
    public abstract class LoadCommonObject_ABS<T> : ILoadCommonObject where T : IMobObjectBasic, new ()
    {
        abstract public string Path { get; }
        /// <summary>
        /// (Path, (key, list<object>))
        /// </summary>
        private static Dictionary<string, Dictionary<string, List<T>>> CommonObjectDic = new Dictionary<string, Dictionary<string, List<T>>> ();
        private JsonSerializerOptions jsoptions = new JsonSerializerOptions ()
        {
            Encoder = JavaScriptEncoder.Create (UnicodeRanges.All),
            WriteIndented = true,
            IgnoreNullValues = true
        };

        public LoadCommonObject_ABS ()
        {
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.Converters.Add (new EquipmentAllowPositionEnumKeyConverter ());
        }

        protected Dictionary<string, Dictionary<string, List<T>>> GetCacheAll => CommonObjectDic;
        public async Task OutPutFilesAsync ()
        {
            var dicObj = CommonObjectDic[Path];
            if (dicObj != null)
            {
                foreach (var key in dicObj)
                {
                    string fileFullName = System.IO.Path.Combine (Path, key.Key);
                    var JsonModel = key.Value;
                    JsonModel = JsonModel.OrderBy (comObj => comObj.SystemCode).ToList ();
                    var jsonString = JsonSerializer.Serialize<List<T>> (JsonModel, jsoptions);
                    await File.WriteAllTextAsync (fileFullName, jsonString, Encoding.UTF8);
                }
            }
        }

        internal Dictionary<string, List<T>> GetCommonObjectModelALL (bool reload)
        {
            LoadCommonObject (reload);
            return CommonObjectDic[Path];
        }

        internal List<T> GetCommonObjectModelList (string Key)
        {
            if (CommonObjectDic[Path].ContainsKey (Key))
            {
                return CommonObjectDic[Path][Key];
            }
            else
            {
                return new List<T> ();
            }
        }

        private void LoadCommonObject (bool reload)
        {
            if (!CommonObjectDic.ContainsKey (Path))
            {
                CommonObjectDic.Add (Path, new Dictionary<string, List<T>> ());
            }
            if (CommonObjectDic[Path].Count () == 0 || reload)
            {
                CommonObjectDic[Path].Clear ();
                if (Directory.Exists (Path))
                {
                    var files = Directory.GetFiles (Path, "*.json");
                    foreach (string fileName in files)
                    {
                        try
                        {
                            string LastFileName = fileName.Split ("/").Last ();
                            string fs = File.ReadAllText (fileName);
                            var t = JsonSerializer.Deserialize<List<T>> (fs, jsoptions);
                            CommonObjectDic[Path].Add (LastFileName, t);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine (ex.Message);
                        }
                    }
                }
            }
        }

        internal bool UpdateCommonObject (string Key, T para)
        {
            if (para != null)
            {
                var path = CommonObjectDic.Keys.Where (p => p.EndsWith (Key)).FirstOrDefault ();
                if (string.IsNullOrWhiteSpace (path))
                {
                    string basic = $"./../Keeper.MercuryCore.CommandLoop/DocumentWorld/Definition/";
                    string fullpath = basic + Key;
                    CommonObjectDic.Add (fullpath, new Dictionary<string, List<T>> () { });
                }

                bool isSameFile = false;
                //找其它的File有沒有同SystemCode，有的話要return false
                foreach (var d1 in CommonObjectDic)
                {
                    if (isSameFile) break;
                    var d1Path = d1.Key;
                    foreach (var d2 in d1.Value)
                    {
                        var innerObj = d2.Value.Where (x => x.SystemCode == para.SystemCode).FirstOrDefault ();
                        if (innerObj != null)
                        {
                            if (d1Path == Path)
                            {
                                isSameFile = true;
                                break;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }

                var obj = CommonObjectDic[Path][Key].Where (x => x.SystemCode == para.SystemCode).FirstOrDefault ();
                if (obj != null) CommonObjectDic[Path][Key].Remove (obj);
                CommonObjectDic[Path][Key].Add (para);

                CommonObjectDic[Path][Key] = CommonObjectDic[Path][Key].OrderBy (x => x.SystemCode).ToList ();

                var jsonOriginalObj = CommonObjectDic[Path][Key];
                string FullFilePath = System.IO.Path.Combine (Path, Key);
                var jsonString = JsonSerializer.Serialize<List<T>> (jsonOriginalObj, jsoptions);
                File.WriteAllTextAsync (FullFilePath, jsonString, Encoding.UTF8).Wait ();

                return true;
            }
            return false;
        }

        internal T GetCommonObjectDetail (string key, string systemCode)
        {
            foreach (var item in CommonObjectDic[Path])
            {
                if (item.Value.Any (x => x.SystemCode == systemCode))
                {
                    var obj = item.Value.Where (x => x.SystemCode == systemCode).First ();
                    if (key != null && item.Key != key)
                    {
                        item.Value.Remove (obj);
                        if (!CommonObjectDic[Path].ContainsKey (key)) CommonObjectDic[Path].Add (key, new List<T> ());
                        CommonObjectDic[Path][key].Add (obj);
                    }
                    return obj;
                }
            }
            return new T () { };
        }
    }
}
