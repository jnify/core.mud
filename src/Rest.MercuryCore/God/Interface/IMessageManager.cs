using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.God.Interface
{
    public interface IMessageManager
    {
        void Bind (Func<BroadcastObject, Task> broadcast, Func<List<string>, int, Task> displayPrompt);
        Task BroadcastToRoom (string message, int RoomID, bool ExcludeAsleepPPL = true, string ExcludePPL = default, bool DisplayPrompt = true);
        Task BroadcastToWorld (string message, EWorld world);
        Task SendToPPL (string message, string UserName, bool ExcludeAsleepPPL = true, bool DisplayPrompt = true);
        Task DoDisplayPrompt (List<string> UserNames);
        //修改LastMessageTime而己
        Task DisplayPrompt (int RoomID);
    }
}
