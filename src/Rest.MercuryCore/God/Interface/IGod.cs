using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.God.Interface
{
    public interface IGod { }
    public interface IGodOfLive
    {
        //List<UserInfo> GetAllUser (bool InUserLogin = false);
    }

    public interface IGodOfDeath : IGod
    {
        string MakeMobCorpse (Room room, MobInfo defance);
    }
    public interface IGodOfWar : IGod
    {
        Task MessageCombat (Creature Attacker, string Defence, (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) result);
    }
}
