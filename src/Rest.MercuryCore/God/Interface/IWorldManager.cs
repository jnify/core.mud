using System;
using System.Collections.Generic;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.God.Interface
{
    public interface IWorldManager
    {
        List<Room> GetRooms ();
        Room GetRoom (int roomId);
        Room GetRoom (UserInfo user);
        void SetRoom (Room room);
        WorldInformation WorldInfo { get; }
        List<MobInfo> AllMobs { get; }

        //Mob 死亡
        List<MobInfo> DiedMobs_Get ();
        void DiedMobs_Add (MobInfo mob);
        void DiedMobs_Remove (string ID);

        //組隊
        Func<string, List<UserInfo>> GetGroupUsers { get; set; }

        //記仇 Grudges
        void Grudges_Add (MobInfo mob, string UserName);
        void Grudges_Remove (MobInfo mob, string UserName);
        (bool IsGrudges, string GrudgesName) CheckGrudges (MobInfo mob, List<string> UserNames);
    }
}
