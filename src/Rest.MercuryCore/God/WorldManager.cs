using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Quest;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.God
{
    public class WorldManager : IWorldManager
    {
        private Dictionary<int, Room> Rooms = new Dictionary<int, Room> ();
        private readonly ILogger<WorldManager> logger;
        private readonly object roomLock = new object ();
        private readonly object diemobLock = new object ();
        private readonly object grudgesLock = new object ();
        private WorldInformation _WorldInfo = new WorldInformation () { };

        private List<MobInfo> _diemobs = new List<MobInfo> () { };

        public WorldInformation WorldInfo
        {
            get
            {
                return _WorldInfo;
            }
        }

        public List<MobInfo> AllMobs
        {
            get
            {
                List<MobInfo> rtn = new List<MobInfo> ();
                //rtn.AddRange ();
                Rooms.Where (x => x.Value.Mobs.Count () > 0).Select (x => x.Value).ToList ().ForEach (x =>
                {
                    rtn.AddRange (x.Mobs);
                });
                return rtn;
            }
        }

        public Func<string, List<UserInfo>> GetGroupUsers { get; set; }
        private readonly GrudgesInfo grud;

        public WorldManager (ILogger<WorldManager> logger, GrudgesInfo grud)
        {
            this.logger = logger;
            this.grud = grud;
        }

        public Room GetRoom (int roomId)
        {
            if (Rooms.ContainsKey (roomId))
            {
                return Rooms[roomId];
            }
            else
            {
                return default;
            }
        }

        public Room GetRoom (UserInfo user) => GetRoom (user.Geography.RoomID);

        public void SetRoom (Room room)
        {
            lock (roomLock)
            {
                if (Rooms.ContainsKey (room.RoomID))
                {
                    Rooms[room.RoomID] = room;
                    Console.WriteLine ($"SetRoom:{room.RoomID}重覆了。");
                }
                else
                {
                    Rooms.Add (room.RoomID, room);
                }
            }
        }

        public void DiedMobs_Add (MobInfo mob)
        {
            lock (diemobLock)
            {
                if (mob != null)
                {
                    mob.Geography.RoomID = -1;
                    _diemobs.Add (mob);
                };
            }
        }

        public void DiedMobs_Remove (string ID)
        {
            if (_diemobs.Any (x => x.ID == ID))
            {
                lock (diemobLock)
                {
                    if (_diemobs.Any (x => x.ID == ID))
                    {
                        _diemobs.Remove (_diemobs.Where (x => x.ID == ID).FirstOrDefault ());
                    }
                }
            }
        }

        public List<MobInfo> DiedMobs_Get ()
        {
            return _diemobs;
        }

        public void Grudges_Add (MobInfo mob, string UserName)
        {
            grud.Add (mob, UserName);
        }
        public void Grudges_Remove (MobInfo mob, string UserName)
        {
            grud.Remove (mob, UserName);
        }
        public (bool IsGrudges, string GrudgesName) CheckGrudges (MobInfo mob, List<string> UserNames)
        {
            return grud.CheckGrudges (mob, UserNames);
        }

        public List<Room> GetRooms () => Rooms.Select (x => x.Value).ToList ();
    }
}
