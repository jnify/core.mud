namespace Rest.MercuryCore.Interface
{
    public interface IFileLoader
    {
        void LoadFile ();
        void LoadFile (string FileName);
    }
}
