using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.MercuryCore.Interface
{
    public interface IFirebaseObject
    {
        string ID { get; set; }
    }
    public interface IFirebase<T> where T : class, IFirebaseObject
    {
        Task InsertOrUpdate (T obj, bool UpperID = true);
        Task<T> Get (string ID);
        Task<Dictionary<string, T>> GetALL (string Collection);
    }

}
