using Rest.MercuryCore.CreatureModel;

namespace Rest.MercuryCore.Interface
{
    public interface IActionUserEffect
    {
        string InitialEffect (UserInfo user, UserEffectiveSettings Effect);
        string MiddleEffect (UserInfo user, EffectiveType Effect);
        string CompleteEffect (UserInfo user, EffectiveType Effect);
        string CancelEffect (UserInfo user, EffectiveType Effect);
    }
}
