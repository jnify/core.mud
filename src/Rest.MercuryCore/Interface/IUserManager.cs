using System.Collections.Generic;
using System.Threading.Tasks;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Interface
{
    public interface IUserManager
    {
        UserInfo GetUser (string username);
        List<UserInfo> GetRoomUser (int RoomID);
        List<UserInfo> GetRoomUser (UserInfo User);
        List<UserInfo> GetAllUser (bool InUserLogin = false);
        bool CheckUserInSync (string username, string password = null);
        Task<bool> CreateUserAsync (UserInfo user);
        void RemoveUser (string username);

        //Group功能
        bool CreateGroup (string GroupName, string UserName);
        bool JoinGroup (string GroupName, string UserName);
        bool LeaveGroup (string UserName);
        GroupObject GetGroup (string UserName);
    }
}
