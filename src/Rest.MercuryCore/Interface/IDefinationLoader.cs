using Rest.MercuryCore.CreatureModel;

namespace Rest.MercuryCore.Interface
{
    public interface IMobDefinationLoader : IFileLoader
    {
        MobCommonDefinition GetDef (string SystemCode);
    }
}
