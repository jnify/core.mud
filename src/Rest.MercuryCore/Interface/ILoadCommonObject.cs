using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Interface
{
    public interface ILoadCommonObject
    {
        CommonObject GenerateCommonObject (string SystemCode);
        CommonObjectDefined GetDef (string SystemCode);
    }
}
