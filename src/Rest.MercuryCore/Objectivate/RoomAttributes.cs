using System.ComponentModel;

namespace Rest.MercuryCore.Objectivate
{
    public enum Topography
    {
        [Description ("平地")]
        Normal, //
        [Description ("海水、溪流")]
        Beach, //
        [Description ("山地")]
        Mountain
    }
    public class ExitSetting : BaseObjectCanLock
    {
        public int RoomID { get; set; }
        public ObjectActionsVerb AcceptVerbs { get; set; }
        public ExitName Dimension { get; set; }
    }
    public enum ExitName
    {
        [Description ("未設定")]
        None,

        [Description ("東邊")]
        East,

        [Description ("西邊")]
        West,

        [Description ("南邊")]
        South,

        [Description ("北邊")]
        North,

        [Description ("上方")]
        Up,

        [Description ("下方")]
        Down
    }
    public enum GateStatus
    {
        [Description ("開")]
        Open = 0,

        [Description ("關")]
        Close = 1,

        [Description ("鎖住")]
        Locked = 2
    }

    public enum RoomCoverType
    {
        [Description ("未設定")]
        None, //
        [Description ("自然變化")]
        Natural, //
        [Description ("永夜")]
        Dark, //
        [Description ("永晝")]
        Light,
    }

    public enum UnlockResult
    {
        NoLocked, //物品未上鎖
        Locked, //物品己上鎖
        Success, //解鎖開鎖成功
        WrongKeyNumber, //鑰匙不對
        NoKey, //沒有鑰匙
    }

}
