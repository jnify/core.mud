using System.Linq;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.Objectivate
{
    public static class CommonObjectExtension
    {
        //回傳-1表示沒價值，需要直接定價
        public static int GetBasicPrice (this CommonObject obj)
        {
            if (obj.Pricing > 0) return obj.Pricing;
            switch (obj.ObjectType)
            {
                case CommonObjectType.Weapon:
                case CommonObjectType.Equipment:
                    var basicPrice = obj.DamageMax + obj.DamageMin + obj.DefenceMax + obj.DefenceMin;
                    var extPrice = obj.ExtraDamageDefance.Physical + obj.ExtraDamageDefance.Fire + obj.ExtraDamageDefance.Wind;
                    var extReal = obj.ExtraDamageDefance.PhysicalReal + obj.ExtraDamageDefance.FireReal + obj.ExtraDamageDefance.WindReal;
                    var extAttr = obj.ExtraAttribute.Constitution + obj.ExtraAttribute.Dexterity + obj.ExtraAttribute.Intelligent + obj.ExtraAttribute.Strength + obj.ExtraAttribute.Wisdom;
                    obj.Pricing = (basicPrice + (extPrice * 2) + (extReal * 2) + (extAttr * 10)) * 100;
                    return obj.Pricing;
                default:
                    return -1;
            }
        }

        public static string GetDescriptionOnTheGround (this CommonObject obj) =>
            $"{obj.GetFullDescriptionWithDecoration()}（{obj.Name}）。";

        //生鏽的 小刀
        public static string GetShortDescription (this CommonObject obj) =>
            $"{obj.Description}{obj.ShortName}";

        //（閃閃發亮）生鏽的小刀
        public static string GetShortDescriptionWithDecoration (this CommonObject obj) =>
            $"{obj.GetDecoration()}{obj.GetShortDescription()}";

        //一把 生鏽的小刀
        public static string GetFullDescription (this CommonObject obj) =>
            $"{obj.Unit}{obj.GetShortDescription()}";

        //（閃閃發亮）一把生鏽的小刀
        private static string GetFullDescriptionWithDecoration (this CommonObject obj) =>
            $"{obj.GetDecoration()}{obj.GetFullDescription()}";

        //（閃閃發亮）
        private static string GetDecoration (this CommonObject obj)
        {
            string dec = default;
            if (obj.Decoration != null && obj.Decoration.Count () > 0)
                obj.Decoration.ForEach (x =>
                {
                    dec += $"（{x.GetDescriptionText ()}）";
                });
            return dec;
        }

        public static string GetLookDescription (this CommonObject obj)
        {
            string rtn = obj.GetFullDescription ();

            if (!string.IsNullOrWhiteSpace (obj.DescriptionDetail))
                rtn += $"\r\n{obj.DescriptionDetail}";

            //todo:可穿戴裝備要設計耐久度
            if (obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear))
                rtn += $"\r\n{obj.ShortName}看起來全新無損。";
            return rtn;
        }
    }
}
