using System.Collections.Generic;

namespace Rest.MercuryCore.Objectivate
{
    public class CommonObjectDefined : CommonObject
    {
        public string SuccessMessage { get; set; }
        public int Volume { get; set; }
        public int Range { get; set; }
        public int Precision { get; set; }
        public string ExtraInformation { get; set; }
        public string DispearMessage { get; set; }
        public List<string> ContainSystemCode { get; set; }

    }

}
