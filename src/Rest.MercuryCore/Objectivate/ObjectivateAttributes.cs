using System;
using System.ComponentModel;

namespace Rest.MercuryCore.Objectivate
{
    [Flags]
    public enum ObjectActionsVerb
    {
        None = 0,
        CanEat = 1,
        CanDrink = 2,
        CanDrop = 4,
        CanGet = 8,
        CanGetFrom = 16,
        CanPutIn = 32,
        CanWear = 64,
        CanNotRemove = 128,
        CanSearch = 256,
        CanLock = 512,
        CanOpenClose = 1024,
        CanShake = 2048
    }
    public class AttackPoint
    {
        public int Physical { get; set; }
        public int Wind { get; set; }
        public int Fire { get; set; }

        public int PhysicalReal { get; set; }
        public int WindReal { get; set; }
        public int FireReal { get; set; }

        public int Subtract (AttackPoint point)
        {
            int p = (Physical - point.Physical) + (PhysicalReal - point.PhysicalReal);
            int w = (Wind - point.Wind) + (WindReal - point.WindReal);
            int f = (Fire - point.Fire) + (FireReal - point.FireReal);
            if (p < 0) p = 1;
            if (w < 0) w = 0;
            if (f < 0) f = 0;
            return p + w + f;
        }
        public AttackPoint Augment (AttackPoint point)
        {
            this.Physical += point.Physical;
            this.PhysicalReal += point.PhysicalReal;
            this.Fire += point.Fire;
            this.FireReal = point.FireReal;
            this.Wind += point.Wind;
            this.WindReal = point.WindReal;
            return this;
        }

    }
    public class Attr
    {
        public int Strength { get; set; }

        public int Wisdom { get; set; }

        public int Intelligent { get; set; }

        public int Constitution { get; set; }

        public int Dexterity { get; set; }
    }
    public enum EquipmentAllowPosition
    {
        None,

        [Description ("照明用具")]
        Ligth,

        [Description ("頭")]
        Head,

        [Description ("身")]
        Body,

        [Description ("腿")]
        Leggings,

        [Description ("手")]
        Gloves,

        [Description ("手臂")]
        Arm,

        [Description ("身")]
        Cape,

        [Description ("腰")]
        Belt,

        [Description ("腳")]
        Boots,

        [Description ("右耳")]
        Ear,

        [Description ("左耳")]
        Ear_1,

        [Description ("右食指")]
        Finger,

        [Description ("左食指")]
        Finger_1,

        [Description ("脖子")]
        Neck,

        [Description ("脖子")]
        Neck_1,

        [Description ("右手腕")]
        Bracelet,

        [Description ("左手腕")]
        Bracelet_1,

        [Description ("右手")]
        HandRight,

        [Description ("左手")]
        HandLeft,

    }
    public enum WeaponType
    {
        [Description ("防具")]
        None = 0,

        [Description ("短刀、短劍")]
        Dagger,

        [Description ("長劍")]
        Sword,

        [Description ("雙手劍")]
        Sword2hd,

        [Description ("法杖")]
        Staff,

        [Description ("鎚")]
        Hammer,

        [Description ("盾")]
        Shield,

        [Description ("弓")]
        Bow,

        [Description ("繩、鞭")]
        Whip,

        [Description ("爪")]
        Claw,

        [Description ("矛、槍")]
        Stab,

        [Description ("樂器")]
        Musical,

        [Description ("徒手")]
        Barehand,
    }

    public enum CommonObjectType
    {
        [Description ("無法買賣")]
        CanNotSell, //
        [Description ("雜項")]
        Misc, //
        [Description ("藥水")]
        Potion, //
        [Description ("武器")]
        Weapon, //
        [Description ("防具")]
        Equipment, //
        [Description ("食物")]
        Food, //
        [Description ("酒")]
        Drink, //
        [Description ("工具")]
        Tools, //
        [Description ("解謎物品")]
        Quest, //
    }

    public enum CommonObjectDecoration
    {
        None,

        [Description ("[1;37m閃閃發亮[0m")]
        Shiny,

        [Description ("[1;34m嗡嗡作響[0m")]
        Hum,

        [Description ("[1;30m破舊[0m")]
        Shabby,

    }

}
