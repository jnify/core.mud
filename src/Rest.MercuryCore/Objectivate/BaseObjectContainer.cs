using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.Objectivate
{
    public class BaseObjectContainer : BaseObjectCanLock
    {
        private readonly object LockThings = new object ();
        public List<CommonObject> Things { get; set; }
        public bool RemoveByID (string _ID, out CommonObject rtn)
        {
            lock (LockThings)
            {
                rtn = Things.Where (x => x.ID == _ID).FirstOrDefault ();
                if (rtn != null)
                {
                    if (rtn.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGet))
                    {
                        Things.Remove (rtn);
                        return true;
                    }
                }
            }
            return false;
        }
        public void AddThing (CommonObject item)
        {
            lock (LockThings)
            {
                Things.Add (item);
            }
        }
        public CommonObject GetThing (string objname)
        {
            CommonObject rtn = null;
            int index = 0;
            if (TextUtil.GetNumberOfObject (objname, out objname, out index))
            {
                if (Things.Where (x => x.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)).Count () < index) return rtn;
                if (index > 0) --index;
            }
            if (Things.Any (x => x.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)))
            {
                rtn = Things.Where (x => x.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)).ToList () [index];
            }
            return rtn;
        }
    }

    public class BaseObjectCanLock
    {
        public GateStatus gateStatus { get; set; }
        /// <summary>
        /// 鑰匙編號，0~99保留給Pick技能用，表示所需的Pick技能等級
        /// </summary>
        public int KeyNumber { get; set; }
        public UnlockResult Unlock (Creature creature)
        {
            if (gateStatus == GateStatus.Open) return UnlockResult.NoLocked;
            if (KeyNumber == 0) return UnlockResult.Success;

            //比對身上鑰匙號碼
            if (creature.Things.Any (obj => obj.KeyNumber == KeyNumber))
                return UnlockResult.Success;

            //身上是否有鑰匙
            if (creature.Things.Any (obj => obj.KeyNumber > 0))
                return UnlockResult.WrongKeyNumber;
            else
                return UnlockResult.NoKey;
        }
    }

}
