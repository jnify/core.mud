using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Objectivate
{
    public class Room : BaseObjectContainer
    {
        private readonly object LockThings = new object ();
        private IWorldManager wordMng;

        public Room ()
        {
            Things = new List<CommonObject> ();
            Mobs = new List<MobInfo> ();
            Terrain = Topography.Normal;
            Ceiling = RoomCoverType.None;
        }

        public void BindWordObject (IWorldManager wordMng)
        {
            this.wordMng = wordMng;
        }

        public int RoomID { get; set; }
        public WorldInfra Geography { get; set; }
        public ExitSetting[] Exits { get; set; }
        public Topography Terrain { get; set; }
        public string Description { get; set; }
        public RoomCoverType Ceiling { get; set; }

        [JsonIgnore]
        public int RoomVisibility
        {
            get
            {
                if (wordMng == null) return 0;
                if (Ceiling == RoomCoverType.Dark || Ceiling == RoomCoverType.None) return 0;
                if (Ceiling == RoomCoverType.Light) return 100;
                var session = wordMng.WorldInfo.SystemDateTime.Session.GetAttribute<SessionOfDayAttribute> ();
                return session.Visibility;
            }
        }

        public MobInfo GetMob (string MobName)
        {
            MobInfo rtn = null;
            if (string.IsNullOrWhiteSpace (MobName)) return rtn;
            int index = 0;
            if (TextUtil.GetNumberOfObject (MobName, out MobName, out index))
            {
                if (Mobs.Where (x => x.Name.StartsWith (MobName, StringComparison.InvariantCultureIgnoreCase)).Count () < index) return rtn;
                if (index > 0) --index;
            }
            if (Mobs.Any (x => x.Name.StartsWith (MobName, StringComparison.InvariantCultureIgnoreCase)))
            {
                rtn = Mobs.Where (x => x.Name.StartsWith (MobName, StringComparison.InvariantCultureIgnoreCase)).ToList () [index];
            }
            return rtn;
        }

        public List<MobInfo> Mobs { get; set; }
    }
}
