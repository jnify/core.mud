using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Objectivate
{
    public class CommonObject : BaseObjectContainer
    {
        public CommonObject ()
        {
            ID = Guid.NewGuid ().ToString ();
            Things = new List<CommonObject> ();
            ExtraDamageDefance = new AttackPoint ();
            ExtraAttribute = new Attr ();
            DestroyWhenZeroQuota = true;
            AllowExecuteTimes = 1;
            DispearTS = TimeSpan.MaxValue;
            ObjectType = CommonObjectType.Misc;
        }

        public string ID { get; set; }
        public string SystemCode { get; set; }
        public string ActionKey { get; set; }
        public CommonObjectType ObjectType { get; set; }
        public bool DestroyWhenZeroQuota { get; set; }
        public int AllowExecuteTimes { get; set; }

        public int Level { get; set; }
        //物品的英文名稱(Knife)
        public string Name { get; set; }
        //物品的中文名稱(小刀)
        public string ShortName { get; set; }
        //物品的形容(生鏽的)
        public string Description { get; set; }
        //物品的單位(把)
        public string Unit { get; set; }
        //物品的詳細描述，用在Look指令
        public string DescriptionDetail { get; set; }
        public int Weight { get; set; }
        public int Pricing { get; set; }
        public ObjectActionsVerb AcceptVerbs { get; set; }
        public List<CommonObjectDecoration> Decoration { get; set; }

        [JsonIgnore]
        private Func<ActionKeyParameter, CommonObject[], string> reActCommand { get; set; }
        public void SetActionCommand (Func<ActionKeyParameter, CommonObject[], string> action) => reActCommand = action;
        public string ReActCommand (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            if (reActCommand != null)
                return reActCommand (actionpapa, commonobject);
            return default;
        }

        ///////Equipment，可裝備在哪個部位
        public EquipmentAllowPosition Position { get; set; }
        //武器種類，None則為防具
        public WeaponType Type { get; set; }
        //最小傷害值
        public int DamageMin { get; set; }
        //最大傷害值
        public int DamageMax { get; set; }
        //最小防禦值
        public int DefenceMin { get; set; }
        //最大防禦值
        public int DefenceMax { get; set; }
        //額外傷害及真實傷害及減傷
        public AttackPoint ExtraDamageDefance { get; set; }
        //額外增加屬性點數
        public Attr ExtraAttribute { get; set; }

        [JsonConverter (typeof (JsonTimeSpanConverter))]
        public TimeSpan DispearTS { get; set; }
    }
}
