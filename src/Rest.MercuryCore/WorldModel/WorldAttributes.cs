using System;
using System.ComponentModel;

namespace Rest.MercuryCore.WorldModel
{
    public class WorldInfra
    {
        public string World { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string RoomTitle { get; set; }
        public int RoomID { get; set; }
    }

    public enum EWorld
    {
        未定義 = 0,
        亞爾斯隆世界,
        普克巴納大陸
    }

    public enum ERegion
    {
        未定義 = 0,
        新手區,
        斯澤費拉界,
        尼眾拉納界
    }
    public class SessionOfDayAttribute : Attribute
    {
        public DayOrNight DayOrNight { get; private set; }
        public int Visibility { get; private set; }

        public SessionOfDayAttribute (DayOrNight DayOrNight, int Visibility)
        {
            this.DayOrNight = DayOrNight;
            this.Visibility = Visibility;
        }
    }
    public enum DayOrNight
    {
        Day,
        Night
    }
    public enum SessionOfDay
    {
        [Description ("凌晨")]
        [SessionOfDayAttribute (DayOrNight.Night, 0)]
        A,

        [Description ("拂曉")]
        [SessionOfDayAttribute (DayOrNight.Night, 25)]
        B,

        [Description ("早晨")]
        [SessionOfDayAttribute (DayOrNight.Night, 100)]
        C,

        [Description ("午前")]
        [SessionOfDayAttribute (DayOrNight.Night, 100)]
        D,

        [Description ("午後")]
        [SessionOfDayAttribute (DayOrNight.Night, 100)]
        E,

        [Description ("傍晚")]
        [SessionOfDayAttribute (DayOrNight.Night, 100)]
        F,

        [Description ("薄暮")]
        [SessionOfDayAttribute (DayOrNight.Night, 75)]
        G,

        [Description ("深夜")]
        [SessionOfDayAttribute (DayOrNight.Night, 0)]
        H
    }
    public enum MonthOfYear
    {
        [Description ("雄鷹之月")]
        a,

        [Description ("玄晶之月")]
        b,

        [Description ("寒冰之月")]
        c,

        [Description ("九星之月")]
        d,

        [Description ("芙蓉之月")]
        e,

        [Description ("金輪之月")]
        f,

        [Description ("修羅之月")]
        g,

        [Description ("照空之月")]
        h,

        [Description ("紫韻之月")]
        i,

        [Description ("蒼炎之月")]
        j,

        [Description ("緋雁之月")]
        k,

        [Description ("暗石之月")]
        l,
    }

}
