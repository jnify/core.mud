using System;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.WorldModel
{
    public class WorldInformation
    {
        private readonly DateTime WorldBuildTime = new DateTime (2020, 5, 14, 0, 0, 0);
        public WorldInformation ()
        {
            StartTime = DateTime.Now;
            SystemDateTime = new SystemWorldDate (WorldBuildTime);
            SystemDateTime.UpdateDateTime ();
        }

        public DateTime StartTime { get; private set; }
        public SystemWorldDate SystemDateTime { get; set; }
    }

    public class SystemWorldDate
    {
        private DateTime starttime;
        public SystemWorldDate (DateTime startDatetime)
        {
            starttime = startDatetime;
            Session = SessionOfDay.A;
            Year = 0;
            Month = 0;
            Day = 0;
            Hour = 0;
        }

        public void UpdateDateTime ()
        {
            UpdateDateTime (DateTime.Now);
        }

        public void UpdateDateTime (DateTime value)
        {
            //實際2.5分鐘為一個小時，但因世界心跳30秒一次，所以最多有一分鐘的誤差
            long elapsedTicks = value.Ticks - starttime.Ticks;
            TimeSpan elapsedSpan = new TimeSpan (elapsedTicks);

            var totaldays = elapsedSpan.TotalMinutes / 60;
            var totalmonth = totaldays / 30.00;
            var year = totalmonth / 12.00;

            var months = totalmonth - (Math.Truncate (year) * 12);
            var days = totaldays - (Math.Truncate (totalmonth) * 30);
            var hours = (int) ((elapsedSpan.TotalMinutes - (Math.Truncate (totaldays) * 60)) / 2.5);

            Hour = (int) hours;
            Day = (int) days;
            Month = (int) months;
            Year = (int) year;
            Session = (SessionOfDay) (hours / 3);
        }

        public int Year;
        public int Month;
        public int Day;
        public int Hour;
        public SessionOfDay Session;

        public override string ToString ()
        {
            string NameOfYear = "德隆斯";
            string NumberOfYear = Year.ToString ();
            string NameOfMonth = ((MonthOfYear) Enum.Parse<MonthOfYear> (Month.ToString ())).GetDescriptionText ();
            return $"{NameOfYear}第{NumberOfYear}年 {NameOfMonth} {Hour}點 {Session.GetDescriptionText()}";
        }
    }
}
