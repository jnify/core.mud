using System.Collections.Generic;

namespace Rest.MercuryCore.WorldModel
{
    public class GroupObject
    {
        private readonly object lockUsers = new object ();
        public List<string> UserGroup { get; private set; }
        public string Leader { get; set; }
        public string GroupName { get; set; }

        public GroupObject (string LeaderName, string GroupName = default)
        {
            Leader = LeaderName;
            UserGroup = new List<string> ();
            UserGroup.Add (LeaderName);
            if (GroupName == default)
                this.GroupName = "私人小隊";
            else
                this.GroupName = GroupName;
        }

        public bool AddMember (string userName)
        {
            if (UserGroup.Contains (userName)) return false;
            lock (lockUsers)
            {
                if (UserGroup.Contains (userName)) return false;
                UserGroup.Add (userName);
            }
            return true;
        }
        public bool RemoveMember (string userName)
        {
            if (!UserGroup.Contains (userName)) return false;
            lock (lockUsers)
            {
                if (!UserGroup.Contains (userName)) return false;
                UserGroup.Remove (userName);
            }
            return true;
        }
    }

}
