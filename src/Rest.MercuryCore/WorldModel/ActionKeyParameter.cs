using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.WorldModel
{
    public class ActionKeyParameter
    {
        public ActionKeyParameter (UserInfo user, Room room)
        {
            this.user = user;
            this.room = room;
        }
        public ActionKeyParameter (MobInfo mob, Room room)
        {
            this.room = room;
            this.mob = mob;
        }
        public ActionKeyParameter (UserInfo user, MobInfo mob, Room room)
        {
            this.user = user;
            this.room = room;
            this.mob = mob;
        }
        public UserInfo user { get; set; }
        public MobInfo mob { get; set; }
        public Room room { get; set; }
    }
}
