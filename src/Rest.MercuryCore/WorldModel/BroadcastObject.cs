using System.Collections.Generic;

namespace Rest.MercuryCore.WorldModel
{
    public class BroadcastObject
    {
        public bool ExcludeAsleepPPL = true;
        public string ExcludePPL;
        public string Msg;
        public WorldInfra WorldChannel;
        public string ReceiptName;
        public bool DisplayPrompt = true;
    }
}
