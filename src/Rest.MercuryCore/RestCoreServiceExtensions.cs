using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Quest;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class LoaderCollectionExtensions
    {
        public static IServiceCollection LoadCoreObjects (this IServiceCollection services)
        {
            services.AddSingleton<IWorldManager, WorldManager> ()
                .AddSingleton<IActionLoader, ActionLoader> ()
                .AddSingleton<ActionCommonObject> ()
                .AddSingleton<ActionMob> ()
                .AddSingleton<GrudgesInfo> ()
                .AddSingleton<QuestCollection> ()
                .AddSingleton<IActionUserEffect, ActionUserEffect> ();

            return services;
        }

    }
}
