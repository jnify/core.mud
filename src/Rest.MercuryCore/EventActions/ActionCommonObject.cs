using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.EventActions
{
    public class ActionCommonObject
    {
        private const string ThirstyIsFull = "你喝飽了，肚子再也裝不下了！！";
        private const string FullIsFull = "你吃飽了，肚子再也裝不下了！！";
        private readonly object LockBlockTime = new object ();
        private readonly object LockItemQuantity = new object ();
        private readonly Random rnd = new Random ();
        private Dictionary<string, DateTime> BlockingTime = new Dictionary<string, DateTime> ();
        private Dictionary<string, int> ItemQuantity = new Dictionary<string, int> ();
        private bool CanExecute (string bT_Key, TimeSpan ts)
        {
            if (!BlockingTime.ContainsKey (bT_Key))
            {
                lock (LockBlockTime)
                {
                    if (!BlockingTime.ContainsKey (bT_Key))
                    {
                        BlockingTime.Add (bT_Key, DateTime.Now);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                var latestDT = BlockingTime[bT_Key];
                if (DateTime.Now >= latestDT.Add (ts))
                {
                    lock (LockBlockTime)
                    {
                        latestDT = BlockingTime[bT_Key];
                        if (DateTime.Now >= latestDT.Add (ts))
                        {
                            BlockingTime[bT_Key] = DateTime.Now;
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Return the quantity of reduce 1, if totla have 15, will return 14, return -1, shows Quantity is not enough.
        /// </summary>
        /// <param name="key">Key of the function</param>
        /// <param name="MaxQuantity">Max number</param>
        /// <param name="ts">Timespan, after the ts will reset the number.</param>
        /// <returns>Result of conduct 1.</returns>
        private int ReduceItemQuantity (string key, int MaxQuantity, TimeSpan ts)
        {
            if (ItemQuantity.ContainsKey (key))
            {
                if (ItemQuantity[key] > 0)
                {
                    lock (LockItemQuantity)
                    {
                        if (ItemQuantity[key] > 0)
                        {
                            ItemQuantity[key] -= 1;
                            return ItemQuantity[key];
                        }
                    }
                }
            }
            if (CanExecute (key, ts))
            {
                lock (LockItemQuantity)
                {
                    ItemQuantity[key] = MaxQuantity - 1;
                    return ItemQuantity[key];
                }
            }
            else
            {
                return -1;
            }
        }
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IMessageManager msgMgr;
        public ActionCommonObject (ILoadCommonObject loadCommonObj, IMessageManager msgMgr, IActionLoader actionLoader)
        {
            this.loadCommonObj = loadCommonObj;
            this.msgMgr = msgMgr;
            //自動把Action開頭的放入CommonObjectActionCollection
            var thisType = this.GetType ();
            var methods = thisType.GetMethods ();
            string FuctionPrefixName = "Action";
            int FuctionPrefixNameLength = FuctionPrefixName.Length;
            foreach (var method in methods)
            {
                if (method.Name.StartsWith (FuctionPrefixName))
                {
                    var actionFunction = (Func<ActionKeyParameter, CommonObject[], string>) Delegate.CreateDelegate (typeof (Func<ActionKeyParameter, CommonObject[], string>), this, method);
                    string ActionKey = method.Name.Substring (FuctionPrefixNameLength);
                    actionLoader.AddCommonObjectAction (ActionKey, actionFunction);
                }
            }
        }
        private void _DoCommonAction (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            //Get defination
            var eatobj = commonobject[0];
            var def = loadCommonObj.GetDef (eatobj.SystemCode);
            string message = def.SuccessMessage.Replace ("{Attacker}", actionpapa.user.UserDescriptionAndID);

            if (eatobj.DestroyWhenZeroQuota && (eatobj.AllowExecuteTimes == 1))
            {
                actionpapa.user.RemoveByID (eatobj.ID, out var tmp);
                tmp = null;
            }
            eatobj.AllowExecuteTimes--;
            msgMgr.BroadcastToRoom (message, actionpapa.user.Geography.RoomID);
            if (def.ExtraInformation != default)
            {
                var actions = def.ExtraInformation.Split (",");
                if (actions.Length == 2)
                {
                    int.TryParse (actions[1], out var num);
                    switch (actions[0].ToLower ())
                    {
                        case "full":
                            actionpapa.user.Livelihood.Full += num;
                            break;
                        case "thirsty":
                            actionpapa.user.Livelihood.Thirsty += num;
                            break;
                        case "mv":
                            actionpapa.user.StateCurrent.MV += num;
                            if (actionpapa.user.StateCurrent.MV > actionpapa.user.StateMax.MV) actionpapa.user.StateCurrent.MV = actionpapa.user.StateMax.MV;
                            break;
                        case "mana":
                            actionpapa.user.StateCurrent.Mana += num;
                            if (actionpapa.user.StateCurrent.Mana > actionpapa.user.StateMax.Mana) actionpapa.user.StateCurrent.Mana = actionpapa.user.StateMax.Mana;
                            break;
                        case "hp":
                            actionpapa.user.StateCurrent.HP += num;
                            if (actionpapa.user.StateCurrent.HP > actionpapa.user.StateMax.HP) actionpapa.user.StateCurrent.HP = actionpapa.user.StateMax.HP;
                            break;
                        case "godhealth":
                            actionpapa.user.StateCurrent = actionpapa.user.StateMax.Copy;
                            break;
                        default:
                            Console.WriteLine ($"{eatobj.SystemCode} ExtraInformation 的動作({actions[0]})設定錯誤。");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine ($"{eatobj.SystemCode} ExtraInformation 設定錯誤。");
                }
            }
        }
        private bool DecutThirsty (UserInfo user, int Amount)
        {
            var tmpThirsty = user.Livelihood.Thirsty;
            if (user.Livelihood.Thirsty < 200)
            {
                if (user.Livelihood.Thirsty < 100)
                {
                    user.Livelihood.Thirsty += Amount;
                }
                else
                {
                    user.Livelihood.Thirsty += Amount;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        private string Search_Practice_Equipment (ActionKeyParameter actionpapa, string eqSystemCode)
        {
            var room = actionpapa.room;
            string BT_Key = "Search_" + eqSystemCode;
            TimeSpan st = TimeSpan.FromMinutes (15);
            if (CanExecute (BT_Key, st))
            {
                var eq = loadCommonObj.GenerateCommonObject (eqSystemCode);
                if (eq != null)
                {
                    room.AddThing (eq);
                    msgMgr.BroadcastToRoom ($"{actionpapa.user.UserDescriptionAndID}找到了{eq.ShortName}。", actionpapa.room.RoomID);
                }
                else
                {
                    msgMgr.SendToPPL ("什麼？！", actionpapa.user.Name);

                }
            }
            else
            {
                msgMgr.SendToPPL ("找啊找的，但你什麼也沒找到。", actionpapa.user.Name);
            }
            return default;
        }
        public string ActionShake_JerkyBlackBerryTree (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            int times = 15;
            string Quantity_Key = "Shake_JerkyBlackBerryTree";
            TimeSpan ts = TimeSpan.FromMinutes (15);
            int rest_quantity = ReduceItemQuantity (Quantity_Key, times, ts);
            if (rest_quantity > -1)
            {
                var obj = loadCommonObj.GenerateCommonObject ("JerkyBlackBerry");
                if (!string.IsNullOrWhiteSpace (obj.Name))
                {
                    actionpapa.room.AddThing (obj);
                    msgMgr.BroadcastToRoom ($"{actionpapa.user.UserDescriptionAndID}用力的搖了{commonobject[0].ShortName}一下，{obj.Unit}{obj.ShortName}掉了下來，差點砸到了{actionpapa.user.UserDescriptionAndID}。", actionpapa.user.Geography.RoomID);
                }
                else
                {
                    msgMgr.SendToPPL ("快找IMM說：JerkyBlackBerry 找不到。", actionpapa.user.Name);
                }
            }
            else
            {
                msgMgr.BroadcastToRoom ($"{actionpapa.user.UserDescriptionAndID}用力的搖了{commonobject[0].ShortName}一下，原本不多葉子又掉了一些下來。", actionpapa.user.Geography.RoomID);
            }
            return default;
        }
        public string ActionDrink_CommonAction (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            var user = actionpapa.user;
            var water = commonobject[0];
            if (water.AllowExecuteTimes < 1)
            {
                msgMgr.SendToPPL ($"{commonobject[0].ShortName}早就空空如也。", actionpapa.user.Name);
                return default;
            }

            if (actionpapa.user.Livelihood.Thirsty > 200)
            {
                msgMgr.SendToPPL (ThirstyIsFull, actionpapa.user.Name);
            }
            else
            {
                _DoCommonAction (actionpapa, commonobject);
            }
            return default;
        }
        public string ActionDrink_Water (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            var user = actionpapa.user;
            var water = commonobject[0];
            if (water.AllowExecuteTimes < 1)
            {
                msgMgr.SendToPPL ($"{commonobject[0].ShortName}裏頭連半滴水也沒有。", actionpapa.user.Name);
                return default;
            }

            if (DecutThirsty (user, 40))
            {
                msgMgr.BroadcastToRoom ($"{user.UserDescriptionAndID}從{water.ShortName}喝水。", user.Geography.RoomID);
                if (user.Livelihood.Thirsty < 0)
                    msgMgr.SendToPPL ("你覺得沒那麼渴了。", actionpapa.user.Name);
                if (water.DestroyWhenZeroQuota && (water.AllowExecuteTimes == 1))
                {
                    var def = loadCommonObj.GetDef (water.SystemCode);
                    string message = def.SuccessMessage.Replace ("{Attacker}", actionpapa.user.UserDescriptionAndID);
                    msgMgr.SendToPPL (message, actionpapa.user.Name);
                    user.RemoveByID (water.ID, out var tmp);
                    tmp = null;
                }
                water.AllowExecuteTimes--;
            }
            else
            {
                msgMgr.SendToPPL (ThirstyIsFull, actionpapa.user.Name);
            }
            return default;
        }
        public string ActionEat_CommonAction (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            var user = actionpapa.user;
            var water = commonobject[0];
            if (water.AllowExecuteTimes < 1)
            {
                msgMgr.SendToPPL ($"{commonobject[0].ShortName}已經空空如也。", actionpapa.user.Name);
                return default;
            }

            if (actionpapa.user.Livelihood.Full > 200)
            {
                msgMgr.SendToPPL (FullIsFull, actionpapa.user.Name);
            }
            else
            {
                _DoCommonAction (actionpapa, commonobject);
            }
            return default;
        }
        public string ActionFountainAddMV (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            if (commonobject != null && commonobject[0] != null)
            {
                var user = actionpapa.user;
                var tmpThirsty = user.Livelihood.Thirsty;
                if (user.Livelihood.Thirsty >= 200) user.Livelihood.Thirsty = 199; //永遠不會喝飽
                if (user.Livelihood.Thirsty < 200)
                {
                    if (user.Livelihood.Thirsty < 100)
                    {
                        user.Livelihood.Thirsty += 20;
                    }
                    else
                    {
                        user.Livelihood.Thirsty += 10;
                    }
                    int addNumber = user.StateMax.MV / 5;
                    user.StateCurrent.MV += addNumber;
                    if (user.StateCurrent.MV > user.StateMax.MV) user.StateCurrent.MV = user.StateMax.MV;
                    msgMgr.BroadcastToRoom ($"{user.UserDescriptionAndID}從{commonobject[0].ShortName}喝水。", actionpapa.user.Geography.RoomID);

                    if (tmpThirsty < 0)
                    {
                        if (user.Livelihood.Thirsty > 0)
                            msgMgr.SendToPPL ("你不覺得渴了。", actionpapa.user.Name);
                        else
                            msgMgr.SendToPPL ("你覺得沒那麼渴了。", actionpapa.user.Name);
                    }
                    msgMgr.SendToPPL ("你覺得沒那麼累了。", actionpapa.user.Name);
                }
                else
                {
                    msgMgr.SendToPPL (ThirstyIsFull, actionpapa.user.Name);

                }
            }
            else
            {
                msgMgr.SendToPPL ("該找IMM喝水囉～～(FountainAddMV)", actionpapa.user.Name);
            }
            return default;
        }
        public string ActionFountain (ActionKeyParameter actionpapa, params CommonObject[] commonobject)
        {
            var user = actionpapa.user;
            var tmpThirsty = user.Livelihood.Thirsty;
            if (user.Livelihood.Thirsty < 200)
            {
                if (user.Livelihood.Thirsty < 100)
                {
                    user.Livelihood.Thirsty += 20;
                }
                else
                {
                    user.Livelihood.Thirsty += 10;
                }
                msgMgr.BroadcastToRoom ($"{user.UserDescriptionAndID}從{commonobject[0].ShortName}喝水。", user.Geography.RoomID);
                if (tmpThirsty < 0)
                {
                    if (user.Livelihood.Thirsty > 0)
                        msgMgr.SendToPPL ("你不覺得渴了。", actionpapa.user.Name);
                    else
                        msgMgr.SendToPPL ("你覺得沒那麼渴了。", actionpapa.user.Name);
                }
            }
            else
            {
                msgMgr.SendToPPL (ThirstyIsFull, actionpapa.user.Name);
            }
            return default;
        }
        public string ActionSearch_PracticeHelmet (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeHelmet");
        public string ActionSearch_PracticeArmor (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeArmor");
        public string ActionSearch_PracticeLeggings (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeLeggings");
        public string ActionSearch_PracticeEar (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeEar");
        public string ActionSearch_PracticeSwrod (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeSwrod");
        public string ActionSearch_PracticeNife (ActionKeyParameter actionpapa, params CommonObject[] commonobject) => Search_Practice_Equipment (actionpapa, "PracticeNife");
    }
}
