using System;
using System.Collections.Generic;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.EventActions.Interface
{
    public interface IActionLoader
    {
        void AddCommonObjectAction (string ActionKey, Func<ActionKeyParameter, CommonObject[], string> action);
        void AddMobAction (string ActionKey, Func<ActionKeyParameter, CommonObject[], string> action);
        Func<ActionKeyParameter, CommonObject[], string> GetCommonObjectAction (string ActionKey);
        Func<ActionKeyParameter, CommonObject[], string> GetMobAction (string ActionKey);

        //for Management ViewOnly
        Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> GetAllMobAction ();
        Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> GetAllCommonObjectAction ();
    }
}
