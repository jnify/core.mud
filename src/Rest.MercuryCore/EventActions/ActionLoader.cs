using System;
using System.Collections.Generic;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.EventActions
{
    public class ActionLoader : IActionLoader
    {
        private Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> CommonObjectActionCollection = new Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> ();
        private Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> MobActionCollection = new Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> ();
        public void AddCommonObjectAction (string ActionKey, Func<ActionKeyParameter, CommonObject[], string> action) => CommonObjectActionCollection.Add (ActionKey, action);
        public void AddMobAction (string ActionKey, Func<ActionKeyParameter, CommonObject[], string> action) => MobActionCollection.Add (ActionKey, action);

        public Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> GetAllCommonObjectAction () => CommonObjectActionCollection;

        public Dictionary<string, Func<ActionKeyParameter, CommonObject[], string>> GetAllMobAction () => MobActionCollection;

        public Func<ActionKeyParameter, CommonObject[], string> GetCommonObjectAction (string ActionKey)
        {
            if (string.IsNullOrWhiteSpace (ActionKey)) return null;
            if (CommonObjectActionCollection.ContainsKey (ActionKey))
                return CommonObjectActionCollection[ActionKey];
            else
                return null;
        }

        public Func<ActionKeyParameter, CommonObject[], string> GetMobAction (string ActionKey)
        {
            if (string.IsNullOrWhiteSpace (ActionKey)) return null;
            if (MobActionCollection.ContainsKey (ActionKey))
                return MobActionCollection[ActionKey];
            else
                return null;
        }
    }
}
