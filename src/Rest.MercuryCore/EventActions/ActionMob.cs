using System;
using System.Collections.Generic;
using System.Threading;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Quest;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;
using static Rest.MercuryCore.Utilities.TextUtil;

namespace Rest.MercuryCore.EventActions
{
    public class ActionMob
    {
        private readonly Random rnd = new Random ();
        private readonly QuestCollection quest;
        private readonly ILoadCommonObject loadCommonObj;
        private readonly IMobDefinationLoader mobloader;
        private readonly IMessageManager msgMgr;
        private static Dictionary<string, DateTime> ActionScriptLocked = new Dictionary<string, DateTime> ();
        public ActionMob (QuestCollection quest, ILoadCommonObject loadCommonObj, IWorldManager wordmgr, IMobDefinationLoader mobloader, IMessageManager msgMgr, IActionLoader actionLoader)
        {
            this.quest = quest;
            this.loadCommonObj = loadCommonObj;
            this.mobloader = mobloader;
            this.msgMgr = msgMgr;
            //自動把Action開頭的放入MobActionCollection
            var thisType = this.GetType ();
            var methods = thisType.GetMethods ();
            string FuctionPrefixName = "Action";
            int FuctionPrefixNameLength = FuctionPrefixName.Length;
            foreach (var method in methods)
            {
                if (method.Name.StartsWith (FuctionPrefixName))
                {
                    var actionFunction = (Func<ActionKeyParameter, CommonObject[], string>) Delegate.CreateDelegate (typeof (Func<ActionKeyParameter, CommonObject[], string>), this, method);
                    string ActionKey = method.Name.Substring (FuctionPrefixNameLength);
                    actionLoader.AddMobAction (ActionKey, actionFunction);
                }
            }
        }
        public string ActionGreeting (ActionKeyParameter actionpapa, CommonObject[] commonobject)
        {
            string GreetingActionKey = "Greeting" + actionpapa.mob.Name;
            if (!isActionScriptLocked (GreetingActionKey))
            {
                actionpapa.mob.inAction = true;
                var md = mobloader.GetDef (actionpapa.mob.Name);
                var roomid = actionpapa.mob.Geography.RoomID;
                for (var i = 0; i < md.GreetingMessages.Length; i++)
                {
                    var message = md.GreetingMessages[i];
                    if (actionpapa.mob.Status.HasFlag (CreatureStatus.Fighting))
                    {
                        msgMgr.BroadcastToRoom ($"{actionpapa.mob.Settings.Name}說道：{actionpapa.mob.FightingInfo.FightingWith.UserDescriptionAndID}太卑鄙了，話還沒說完呢！居然偷襲我！", roomid, true, null, false);
                        break;
                    }
                    else
                    {
                        if (actionpapa.user.Geography.RoomID == actionpapa.mob.Geography.RoomID)
                        {
                            var msg = message.Replace ("{UserDescriptionAndID}", actionpapa.user.UserDescriptionAndID)
                                .Replace ("{user}", actionpapa.user.Settings.Name);
                            msgMgr.BroadcastToRoom (msg, roomid, true, null, false);
                            Thread.Sleep (1000);
                        }
                        else
                        {
                            msgMgr.BroadcastToRoom ($"{actionpapa.mob.Settings.Name}說道：{actionpapa.user.UserDescriptionAndID}真沒禮貌，話還沒說完呢！人就跑掉了。", roomid, true, null, false);
                            break;
                        }
                    }
                }
                msgMgr.DisplayPrompt (actionpapa.mob.Geography.RoomID);
                actionpapa.mob.inAction = false;
                unLockActionScript (GreetingActionKey);
            }
            return default;
        }
        private void Say (Creature Actor, string msg)
        {
            msgMgr.BroadcastToRoom ($"{Actor.Settings.Name}說道：{msg}", Actor.Geography.RoomID, true, null, false);
        }
        private void Message (string msg, int RoomID)
        {
            msgMgr.BroadcastToRoom (TextUtil.GetColorWord (msg, ColorFront.Green), RoomID, true, null, false);
        }
        private void SendObjectToUser (Creature Actor, Creature Receiver, string LineItem)
        {
            var objDetail = LineItem.SpliteComma ();
            if (objDetail.Key == "coin") //以逗號分隔，Lineitem: coin,1000,Step2
            {
                objDetail.Value = objDetail.Value.TrimAllSpace ();
                var settings = objDetail.Value.Split (",");
                int conis = int.Parse (settings[0]);
                string Step = settings[1];
                string QuestName = settings[2];
                Receiver.Coins += conis;
                msgMgr.SendToPPL (TextUtil.GetColorWord ($"{Actor.Settings.Name}拿了些銅幣給你。", ColorFront.Yellow), Receiver.Name);
                quest.InsertOrUpdate (QuestName, Receiver.Name, Step);
            }
            else
            {
                //以逗號分隔，Lineitem: PotionNewbieMV,step3,QuestName
                var commonobj = loadCommonObj.GenerateCommonObject (objDetail.Key);
                if (commonobj != null)
                {
                    var settings = objDetail.Value.TrimAllSpace ().Split (",");
                    string Step = settings[0];
                    string QuestName = settings[1];
                    Receiver.AddThing (commonobj);
                    msgMgr.SendToPPL (TextUtil.GetColorWord ($"{Actor.Settings.Name}拿了{commonobj.GetFullDescription()}給你。", ColorFront.Yellow), Receiver.Name);
                    quest.InsertOrUpdate (QuestName, Receiver.Name, Step);
                }
                else
                {
                    msgMgr.SendToPPL (TextUtil.GetColorWord ($"{Actor.Settings.Name}找不到{objDetail.Key}可以給你(001)。", ColorFront.Yellow), Receiver.Name);
                }
            }
        }
        public void ExecuteSteps (string[] steps, ActionKeyParameter actionpapa)
        {
            foreach (string step in steps)
            {
                Thread.Sleep (1000);
                var item = step.SpliteColon ();
                if (actionpapa.user.Geography.RoomID != actionpapa.mob.Geography.RoomID)
                {
                    var Actor = actionpapa.mob;
                    msgMgr.BroadcastToRoom ($"{Actor.Settings.Name}說道：{actionpapa.user.UserDescriptionAndID}真沒禮貌，話還沒說完呢！人就跑掉了。", Actor.Geography.RoomID, true, null, false);
                    break;
                }
                switch (item.Key.ToLower ())
                {
                    case "say":
                        Say (actionpapa.mob, item.Value);
                        break;
                    case "message":
                        Message (item.Value, actionpapa.mob.Geography.RoomID);
                        break;
                    case "object":
                        SendObjectToUser (actionpapa.mob, actionpapa.user, item.Value);
                        break;
                    case "completed": //completed,QuestName
                        quest.InsertOrUpdate (item.Value, actionpapa.user.Name, "Done");
                        break;
                    case "save": //save,step4,QuestName
                        var save = item.Value.SpliteComma ();
                        quest.InsertOrUpdate (save.Value, actionpapa.user.Name, save.Key);
                        break;
                    case "receive":
                        //當收到東西時，才觸發
                        return;
                    default:
                        Message ($"item.Key:{item.Key}，還沒寫。", actionpapa.mob.Geography.RoomID);
                        break;
                }
            }
            msgMgr.DisplayPrompt (actionpapa.mob.Geography.RoomID);
        }
        private static readonly object LockScript = new object ();
        public string ActionMobActionScript (ActionKeyParameter actionpapa, CommonObject[] commonobject)
        {
            var mobdef = mobloader.GetDef (actionpapa.mob.Name);
            var QuestName = mobdef.QuestScriptName;
            if (lockActionScript (QuestName, 30))
            {
                Dictionary<string, string[]> QuestSetting = new Dictionary<string, string[]> ();
                if (mobdef != null)
                    QuestSetting = mobdef.QuestScript;
                else
                    Console.WriteLine ($"{actionpapa.mob.Name} definition not found.");
                actionpapa.mob.inAction = true;
                var settings = quest.Get (QuestName, actionpapa.user.Name);
                if (string.Compare (settings, "Done", true) != 0 && QuestSetting.ContainsKey (settings))
                {
                    var steps = QuestSetting[settings];
                    ExecuteSteps (steps, actionpapa);
                }
                actionpapa.mob.inAction = false;
                unLockActionScript (QuestName);
            }
            return default;
        }
        private bool isActionScriptLocked (string key)
        {
            if (ActionScriptLocked.ContainsKey (key))
            {
                return DateTime.Now < ActionScriptLocked[key];
            }
            else
            {
                lock (LockScript)
                {
                    if (ActionScriptLocked.ContainsKey (key))
                    {
                        return DateTime.Now < ActionScriptLocked[key];
                    }
                    else
                    {
                        ActionScriptLocked.Add (key, DateTime.MinValue);
                        return false;
                    }
                }
            }
        }
        private bool lockActionScript (string key, int lockInSeconds)
        {
            if (isActionScriptLocked (key))
            {
                return false;
            }
            else
            {
                ActionScriptLocked[key] = DateTime.Now.Add (TimeSpan.FromSeconds (lockInSeconds));
                return true;
            }
        }
        private void unLockActionScript (string key) => ActionScriptLocked[key] = DateTime.MinValue;
    }
}
