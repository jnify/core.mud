using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillBurningHands : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 3000;
        public override int CooldownInMillisecondsInAfter => 2000;
        public SkillBurningHands (PracticedSkill Attr) : base (Attr) { }
        public override AttackPoint[] Damage (Creature attacker, int SkillLevel)
        {
            base.Damage (attacker, SkillLevel);
            var skill = attacker.GetSkillCategory (practicedSkill);
            int basic = UtilityExperence.GetSpellsBasicAttack (attacker, skill);
            AttackPoint tmp = GetAttackPoint (basic, attacker.GetWeaponRightHand (), attacker.GetWeaponLeftHand (), SkillLevel, 1);
            return new AttackPoint[] { tmp };
        }
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null)
        {
            return TextUtil.GetSpellsSuccessMessage (Percentage, RightHandWeapon, "唱完咒文，一個燃燒的火焰手掌出現在空中");
        }

        public override string FailedMessage (CommonObject RightHandWeapon = null) =>
            "你一時緊張念錯了咒語!!!";
    }
}
