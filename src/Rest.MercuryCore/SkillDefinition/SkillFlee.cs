using System;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillFlee : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 1000;
        public SkillFlee (PracticedSkill Attr) : base (Attr) { }
        private Random rnd = new Random ();
        public override string FailedMessage (CommonObject RightHandWeapon = default) =>
            "{Attacker}的腳不小心拌了一下，沒能逃掉。";

        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "{Attacker}成功的逃走了。";
    }

    public class SkillDodge : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 1000;
        public SkillDodge (PracticedSkill Attr) : base (Attr) { }
        private Random rnd = new Random ();
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = default) =>
            EffectMessage[rnd.Next (EffectMessage.Length)];
        private string[] EffectMessage =>
            new string[]
            {
                "{Attacker}遊刃有餘地躲過{Defance}的攻擊。",
                "{Attacker}千鈞一髮地躲過{Defance}的攻擊。",
            };
        public override string FailedMessage (CommonObject RightHandWeapon = default) =>
            "{Attacker}腳下一滑，沒能躲過{Defance}的攻擊。";
    }
}
