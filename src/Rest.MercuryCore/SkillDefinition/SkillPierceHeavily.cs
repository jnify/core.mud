using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillPierceHeavily : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 3000;
        public SkillPierceHeavily (PracticedSkill Attr) : base (Attr) { }
        public override AttackPoint[] Damage (Creature attacker, int SkillLevel)
        {
            base.Damage (attacker, SkillLevel);
            AttackPoint tmp = GetAttackPoint (attacker.BasicAttack, attacker.GetWeaponRightHand (), attacker.GetWeaponLeftHand (), SkillLevel, 2);
            return new AttackPoint[] { tmp };
        }
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) =>
            TextUtil.GetAttackSuccessMessage (Percentage, RightHandWeapon, skillAttribute.Description);
        public override string FailedMessage (CommonObject RightHandWeapon = null) =>
            TextUtil.GetAttackFailedMessage (RightHandWeapon, skillAttribute.Description);
    }
}
