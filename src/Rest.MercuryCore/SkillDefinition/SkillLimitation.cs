using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;

namespace Rest.MercuryCore.SkillDefinition
{
    public static class SkillLimitation
    {
        private static Dictionary<CreatureCareer, Dictionary<SkillCategory, int>> SkillCategoryMaxLevel = new Dictionary<CreatureCareer, Dictionary<SkillCategory, int>> ()
        {
            {
            CreatureCareer.Thief,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Adventure, 30 }, { SkillCategory.Combat, 15 }, { SkillCategory.Barehand, 20 }, { SkillCategory.Bow, 20 }, { SkillCategory.Dagger, 50 }, { SkillCategory.Sword, 20 }, { SkillCategory.Shield, 20 }, { SkillCategory.Thief, 40 }, { SkillCategory.Whip, 20 }, { SkillCategory.Sword2hd, 20 } }
            },
            {
            CreatureCareer.Ranger,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 20 }, { SkillCategory.Whip, 99 }, { SkillCategory.Dagger, 70 }, { SkillCategory.Staff, 30 }, { SkillCategory.Hammer, 20 }, { SkillCategory.Combat, 25 }, { SkillCategory.Knowledge, 50 }, { SkillCategory.Adventure, 99 }, { SkillCategory.Claw, 50 }, { SkillCategory.Bow, 99 }, { SkillCategory.Sword, 60 }, { SkillCategory.Sword2hd, 60 }, { SkillCategory.Shield, 40 }, { SkillCategory.Thief, 99 }, { SkillCategory.Ranger, 99 } }
            },
            {
            CreatureCareer.Ninja,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 25 }, { SkillCategory.Whip, 50 }, { SkillCategory.Dagger, 80 }, { SkillCategory.Staff, 30 }, { SkillCategory.Hammer, 25 }, { SkillCategory.Combat, 25 }, { SkillCategory.Ninja, 99 }, { SkillCategory.Claw, 99 }, { SkillCategory.Bow, 25 }, { SkillCategory.Sword, 40 }, { SkillCategory.Sword2hd, 40 }, { SkillCategory.Shield, 40 }, { SkillCategory.Thief, 99 }, { SkillCategory.Adventure, 50 } }
            },
            {
            CreatureCareer.Fighter,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 60 }, { SkillCategory.Claw, 70 }, { SkillCategory.Bow, 70 }, { SkillCategory.Sword, 99 }, { SkillCategory.Sword2hd, 99 }, { SkillCategory.Shield, 99 }, { SkillCategory.Warrior, 99 }, { SkillCategory.Knight, 99 }, { SkillCategory.Stab, 99 }, { SkillCategory.Whip, 70 }, { SkillCategory.Dagger, 70 }, { SkillCategory.Staff, 99 }, { SkillCategory.Hammer, 99 }, { SkillCategory.Combat, 99 }, { SkillCategory.Adventure, 50 } }
            },
            {
            CreatureCareer.Warrior,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 60 }, { SkillCategory.Claw, 70 }, { SkillCategory.Bow, 70 }, { SkillCategory.Sword, 99 }, { SkillCategory.Sword2hd, 99 }, { SkillCategory.Shield, 99 }, { SkillCategory.Warrior, 99 }, { SkillCategory.Knight, 99 }, { SkillCategory.Stab, 99 }, { SkillCategory.Whip, 70 }, { SkillCategory.Dagger, 70 }, { SkillCategory.Staff, 99 }, { SkillCategory.Hammer, 99 }, { SkillCategory.Combat, 99 }, { SkillCategory.Adventure, 50 } }
            },
            {
            CreatureCareer.Knight,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 60 }, { SkillCategory.Claw, 70 }, { SkillCategory.Bow, 70 }, { SkillCategory.Sword, 99 }, { SkillCategory.Sword2hd, 99 }, { SkillCategory.Shield, 99 }, { SkillCategory.Warrior, 99 }, { SkillCategory.Knight, 99 }, { SkillCategory.Stab, 99 }, { SkillCategory.Whip, 70 }, { SkillCategory.Dagger, 70 }, { SkillCategory.Staff, 99 }, { SkillCategory.Hammer, 99 }, { SkillCategory.Combat, 99 }, { SkillCategory.Adventure, 50 } }
            },
            {
            CreatureCareer.Paladin,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Barehand, 50 }, { SkillCategory.Claw, 60 }, { SkillCategory.Bow, 60 }, { SkillCategory.Sword, 99 }, { SkillCategory.Sword2hd, 99 }, { SkillCategory.Shield, 99 }, { SkillCategory.Saint, 38 }, { SkillCategory.Warrior, 99 }, { SkillCategory.Paladin, 99 }, { SkillCategory.Stab, 70 }, { SkillCategory.Whip, 60 }, { SkillCategory.Dagger, 60 }, { SkillCategory.Staff, 99 }, { SkillCategory.Hammer, 99 }, { SkillCategory.Water, 20 }, { SkillCategory.Combat, 70 }, { SkillCategory.Adventure, 50 } }
            },
            {
            CreatureCareer.Magic,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Dagger, 10 }, { SkillCategory.Hammer, 10 }, { SkillCategory.Wind, 99 }, { SkillCategory.Lightning, 99 }, { SkillCategory.Evil, 99 }, { SkillCategory.Earth, 99 }, { SkillCategory.Saint, 99 }, { SkillCategory.Adventure, 40 }, { SkillCategory.Wizard, 99 }, { SkillCategory.Staff, 99 }, { SkillCategory.Fire, 99 }, { SkillCategory.Ligth, 99 }, { SkillCategory.Poison, 99 }, { SkillCategory.Dark, 99 }, { SkillCategory.Water, 99 }, { SkillCategory.Combat, 10 }, { SkillCategory.Sage, 99 } }
            },
            {
            CreatureCareer.Magician,
            new Dictionary<SkillCategory, int> () { { SkillCategory.Dagger, 10 }, { SkillCategory.Hammer, 10 }, { SkillCategory.Wind, 99 }, { SkillCategory.Lightning, 99 }, { SkillCategory.Evil, 99 }, { SkillCategory.Earth, 99 }, { SkillCategory.Saint, 99 }, { SkillCategory.Adventure, 40 }, { SkillCategory.Wizard, 99 }, { SkillCategory.Staff, 99 }, { SkillCategory.Fire, 99 }, { SkillCategory.Ligth, 99 }, { SkillCategory.Poison, 99 }, { SkillCategory.Dark, 99 }, { SkillCategory.Water, 99 }, { SkillCategory.Combat, 10 }, { SkillCategory.Sage, 99 } }
            },
            {
            CreatureCareer.Musician,
            new Dictionary<SkillCategory, int> () { }
            },
            {
            CreatureCareer.Bard,
            new Dictionary<SkillCategory, int> () { }
            },
            {
            CreatureCareer.Chief,
            new Dictionary<SkillCategory, int> () { }
            },

        };

        private static SkillCategory[] AttackSkillCategory = new SkillCategory[]
        {
            SkillCategory.Dagger, SkillCategory.Sword, SkillCategory.Sword2hd, SkillCategory.Staff, SkillCategory.Hammer, SkillCategory.Shield, SkillCategory.Bow, SkillCategory.Whip, SkillCategory.Claw, SkillCategory.Stab, SkillCategory.Barehand
        };

        private static SkillCategory[] SpellsSkillCategory = new SkillCategory[]
        {
            SkillCategory.Dark, SkillCategory.Earth, SkillCategory.Evil, SkillCategory.Fire, SkillCategory.Lightning, SkillCategory.Ligth, SkillCategory.Poison, SkillCategory.Saint, SkillCategory.Water, SkillCategory.Wind
        };
        private static Dictionary<SkillCategory, int> _GetSkillCategoryLimit (Creature creature)
        {
            if (SkillCategoryMaxLevel.ContainsKey (creature.Career))
                return SkillCategoryMaxLevel[creature.Career];
            else
                return null;
        }

        private static int _GetSkillCategoryLimit (Creature creature, SkillCategory category)
        {
            var limit = _GetSkillCategoryLimit (creature);
            if (limit == null) return 0;
            if (limit.ContainsKey (category))
                return limit[category];
            else
                return 0;
        }

        public static int GetSkillCategoryLimit (Creature creature, SkillCategory category)
        {
            var limit = _GetSkillCategoryLimit (creature, category);
            switch (creature.Career)
            {
                case CreatureCareer.Fighter:
                case CreatureCareer.Warrior:
                case CreatureCareer.Knight:
                    return GetSkillCategoryLimitFighter (creature, category);
                case CreatureCareer.Paladin:
                    return GetSkillCategoryLimitPaladin (creature, category);
                case CreatureCareer.Magic:
                case CreatureCareer.Magician:
                case CreatureCareer.Archmage:
                    return GetSkillCategoryLimitMagic (creature, category);
                default:
                    return _GetSkillCategoryLimit (creature, category);
            }
        }

        private static Dictionary<SkillCategory, SkillCategory> _SpellPare = new Dictionary<SkillCategory, SkillCategory> ()
        { { SkillCategory.Fighter, SkillCategory.Water }, { SkillCategory.Ligth, SkillCategory.Dark }, { SkillCategory.Wind, SkillCategory.Lightning }, { SkillCategory.Earth, SkillCategory.Poison }, { SkillCategory.Saint, SkillCategory.Evil }
        };

        private static int GetSkillCategoryLimitMagic (Creature creature, SkillCategory category)
        {
            SkillCategory upsideSkill = SkillCategory.Adventure;
            if (SpellsSkillCategory.Contains (category))
            {
                //Get upside spell
                if (_SpellPare.ContainsKey (category))
                    upsideSkill = _SpellPare[category];
                else
                    upsideSkill = _SpellPare.Where (x => x.Value == category).First ().Key;
                if (creature.LearnedSkillSystem.Any (x => x.skillCategory == category)) return 0;

                return 99;
            }
            else
            {
                return _GetSkillCategoryLimit (creature, category);
            }

        }

        private static SkillCategory[] _FighterSkillAllow99 = new SkillCategory[] { SkillCategory.Shield, SkillCategory.Sword, SkillCategory.Sword2hd, SkillCategory.Staff, SkillCategory.Hammer, SkillCategory.Stab };

        //Shield, Sword, Sword2hd, Staff, Hammer, Stab
        //選兩項99，其餘的85
        private static int GetSkillCategoryLimitFighter (Creature creature, SkillCategory category)
        {
            var SkillList = _GetSkillCategoryLimit (creature);
            //找出是否有超‌‌‌過85的
            var Skill99 = creature.LearnedSkillSystem.Where (x => _FighterSkillAllow99.Contains (x.skillCategory) && x.Level > 85).ToList ();
            if (Skill99.Count () < 2) return SkillList[category]; //還未達兩項超過85，所以回傳最高值99
            if (Skill99.Any (x => x.skillCategory == category)) return 99; //已達兩項超過85，故兩項可回99
            if (_FighterSkillAllow99.Contains (category)) return 85; //其餘的最高只到85
            return SkillList[category]; //不在限制內的，依設定回傳。
        }

        private static SkillCategory[] _PaladinSkillAllow99 = new SkillCategory[] { SkillCategory.Shield, SkillCategory.Sword, SkillCategory.Sword2hd, SkillCategory.Staff, SkillCategory.Hammer };

        //Shield, Sword, Sword2hd, Staff, Hammer
        //選兩項99，其餘的85
        private static int GetSkillCategoryLimitPaladin (Creature creature, SkillCategory category)
        {
            var SkillList = _GetSkillCategoryLimit (creature);
            //找出是否有超‌‌‌過85的
            var Skill99 = creature.LearnedSkillSystem.Where (x => _PaladinSkillAllow99.Contains (x.skillCategory) && x.Level > 85).ToList ();
            if (Skill99.Count () < 2) return SkillList[category]; //還未達兩項超過85，所以回傳最高值99
            if (Skill99.Any (x => x.skillCategory == category)) return 99; //已達兩項超過85，故兩項可回99
            if (_PaladinSkillAllow99.Contains (category)) return 85; //其餘的最高只到85
            return SkillList[category]; //不在限制內的，依設定回傳。
        }

    }
}
