using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillHide : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillHide (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你試圖找個地方躲起來。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "你找了個地方躲了起來。";
    }

    public class SkillPickLock : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 5000;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillPickLock (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你解鎖失敗了。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "你把鎖打開了。";
    }

    public class SkillSpy : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillSpy (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "";
    }

    public class SkillSneak : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillSneak (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你試圖小心翼翼的行動。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "你開始小心翼翼的行動。";
    }
}
