using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillCircle : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 500;
        public SkillCircle (PracticedSkill Attr) : base (Attr) { }
        public override AttackPoint[] Damage (Creature attacker, int SkillLevel)
        {
            base.Damage (attacker, SkillLevel);
            AttackPoint tmp = GetAttackPoint (attacker.BasicAttack, attacker.GetWeaponRightHand (), attacker.GetWeaponLeftHand (), SkillLevel, 0.7);
            return new AttackPoint[] { tmp };
        }
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) =>
            TextUtil.GetAttackSuccessMessage (Percentage, RightHandWeapon, skillAttribute.Description);
        public override string FailedMessage (CommonObject RightHandWeapon = null) =>
            TextUtil.GetAttackFailedMessage (RightHandWeapon, skillAttribute.Description);
    }

    public class SkillPeek : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillPeek (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "";
    }

    public class SkillSteal : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 5000;
        public SkillSteal (PracticedSkill Attr) : base (Attr) { }
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你什麼也沒偷到。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "";
    }
}
