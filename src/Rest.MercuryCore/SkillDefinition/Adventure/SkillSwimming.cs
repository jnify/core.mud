using System;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.SkillDefinition
{
    public class SkillSwimming : BaseSkill
    {
        public override int CooldownInMillisecondsInAfter => 0;
        public override int CooldownInMillisecondsInBefore => 0;
        public SkillSwimming (PracticedSkill Attr) : base (Attr) { }
        private Random rnd = new Random ();
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你游不過去。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "";
    }

    public class SkillClimbing : BaseSkill
    {
        public override int CooldownInMillisecondsInBefore => 0;
        public override int CooldownInMillisecondsInAfter => 0;
        public SkillClimbing (PracticedSkill Attr) : base (Attr) { }
        private Random rnd = new Random ();
        public override string FailedMessage (CommonObject RightHandWeapon = default) => "你爬不過去。";
        public override string Message (int Percentage = 0, CommonObject RightHandWeapon = null) => "";
    }
}
