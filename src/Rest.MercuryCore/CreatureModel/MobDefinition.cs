using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.CreatureModel
{
    public class MobCommonDefinition : MobInfo
    {
        public MobCommonDefinition ()
        {
            AutoMovement = false;
            //Test1.ToDictionary (x => x.Key, x => x.Value);
        }

        public new string Name { get; set; }

        public int SettingRebornTimeInSecond { get; set; }

        //身上帶的東西，會掉落。
        public List<CommonThingDefinition> ThingDef { get; set; }
        public List<CommonThingDefinition> EquipmentDef { get; set; }
        public int TrainingFeeBase { get; set; }
        public string MeetPPLActionKey { get; set; }
        public string[] GreetingMessages { get; set; }
        public List<CommonObjectType> AllowSellingTypes { get; set; }
        public Dictionary<string, string[]> QuestScript { get; set; }
        public string QuestScriptName { get; set; }

    }

    public class CommonThingDefinition
    {
        public string SystemCode { get; set; }
        public int RateOfAppear { get; set; }
        public int RateOfDrop { get; set; }
    }
}
