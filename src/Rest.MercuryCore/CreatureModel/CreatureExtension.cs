using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public static class CreatureExtension
    {
        #region Look command
        public static string GetLookDescription (this Creature creature, bool DisplayThing, bool DisplayAttribute = false, bool isAdmin = false)
        {
            if (isAdmin)
            {
                DisplayThing = true;
                DisplayAttribute = true;
            }
            StringBuilder sb = new StringBuilder ().Clear ();
            sb.Append (creature.Settings.Name);
            sb.Append (TextUtil.GetHPDescription (creature.StateCurrent.HP, creature.StateMax.HP));
            sb.Append ("\r\n\r\n");
            if (string.IsNullOrWhiteSpace (creature.Settings.DetailDescription))
                sb.Append ("你覺得沒有什麼特別的。");
            else
                sb.Append (creature.Settings.DetailDescription);

            //Display Attribute
            if (DisplayAttribute) sb.Append ("\r\n\r\n" + creature.GetLookAttribute ());

            //Display Equipment
            if (creature.EQArea.Count () > 0)
                sb.Append ("\r\n\r\n" + creature.GetLookEquipment ());

            //Display Things
            if (DisplayThing)
                sb.Append ("\r\n\r\n" + creature.GetLookThings ());

            return sb.ToString ();
        }

        public static string GetLookAttribute (this Creature creature)
        {
            StringBuilder sb = new StringBuilder ().Clear ();
            sb.Append ($"力量：{creature.Attribute.Strength}　　");
            sb.Append ($"智力：{creature.Attribute.Intelligent}　　");
            sb.Append ($"知識：{creature.Attribute.Wisdom}　　");
            sb.Append ($"敏捷：{creature.Attribute.Dexterity}　　");
            sb.Append ($"體質：{creature.Attribute.Constitution}\r\n");
            sb.Append ($"生命：{creature.StateCurrent.HP} （{TextUtil.GetPercentage(creature.StateCurrent.HP,creature.StateMax.HP)}%）\r\n");
            sb.Append ($"精神：{creature.StateCurrent.Mana} （{TextUtil.GetPercentage(creature.StateCurrent.Mana,creature.StateMax.Mana)}%）\r\n");
            sb.Append ($"體力：{creature.StateCurrent.MV} （{TextUtil.GetPercentage(creature.StateCurrent.MV,creature.StateMax.MV)}%）\r\n");
            sb.Append ($"經驗值：{creature.StateMax.Experience}/{UtilityExperence.ConvertLevelToExperence(creature.Level+1)}　　");
            if (creature.isPPL)
            {
                sb.Append ($"練習點數：{((UserInfo)creature).Points.Practice}　　復活點數：{((UserInfo)creature).Points.Reviviscence}\r\n");
            }
            var coins = creature.Coins.ToCoinTuple ();
            sb.Append ($"現金: {coins.Gold} 枚金幣(Gold)  {coins.Silver} 枚銀幣(Silver)  {coins.Copper} 枚銅幣(Copper)\r\n");
            sb.Append ($"攜帶物品 {creature.Things.Count() + creature.EQArea.Count()} 件，目前負重 {creature.Things.Sum(x=>x.Weight) + creature.EQArea.ToList().Sum(x=>x.Value.Weight)} 磅.");
            return sb.ToString ();
        }
        public static string GetLookEquipment (this Creature creature)
        {
            StringBuilder sb = new StringBuilder ().Clear ();
            if (creature.EQArea != null && creature.EQArea.Count () > 0)
            {
                foreach (var keyvalue in creature.EQArea)
                {
                    string posWord = $"[{keyvalue.Value.Position.GetDescriptionText ().LeftPadSpace (4)}]";
                    string eqName = keyvalue.Value.GetShortDescriptionWithDecoration () + $"（{keyvalue.Value.Name}）";
                    sb.Append ($"\r\n{posWord}{eqName}");
                }
            }
            else
            {
                sb.Append ("\r\n\t唾手可得，無處不在的空氣。");
            }
            return sb.ToString ();
        }
        public static string GetLookThings (this Creature creature)
        {
            StringBuilder sb = new StringBuilder ().Clear ();
            sb.Append ("目前攜帶有:");

            if (creature.Things.Count () > 0)
            {
                //Key為Description, Value為數量
                var objDictionary = creature.Things.GroupBy (obj => obj.GetDescriptionOnTheGround ()).
                ToDictionary (group => group.Key, group => group.Count ()).ToList ();
                //取出最大數量
                int maxNumLength = objDictionary.Max (x => x.Value).ToString ().Length;
                objDictionary.ForEach (dic =>
                {
                    string objDesc = dic.Key;
                    string numStr = dic.Value.ToAlignRightString (maxNumLength);
                    if (dic.Value > 1)
                        objDesc = $"({numStr}){objDesc}";
                    else
                        objDesc = dic.Key.PadLeft (dic.Key.Length + maxNumLength + 2);

                    sb.Append ($"\r\n{objDesc}");
                });
            }
            else
            {
                sb.Append ($"\r\n\t身上兩䄂清風，什麼都沒有。");
            }
            return sb.ToString ();
        }
        #endregion

        #region Skill
        public static PracticedSkill GetSkill (this Creature user, SkillName skillName) =>
            user.PracticeSkills.Where (x => x.Skillname == skillName).FirstOrDefault ();

        public static LearnedSkillCategory GetSkillCategory (this Creature user, PracticedSkill skill)
        {
            if (skill == null) return default;
            return user.LearnedSkillSystem.Where (x => x.skillCategory == skill.SkillAttr.skillCategory).FirstOrDefault ();
        }

        public static int GetSkillCategoryMaxLevel (this Creature user, SkillCategory category)
        {
            return SkillLimitation.GetSkillCategoryLimit (user, category);
        }
        #endregion

        #region Effective start
        public static void AddEffect (this Creature user, UserEffectiveSettings Effective)
        {
            if (!user.Effectives.Any (eff => eff.Type == Effective.Type))
            {
                user.Effectives.Add (Effective);
            }
        }

        public static void RemoveEffect (this Creature user, EffectiveType effectType)
        {
            if (user.Effectives.Any (eff => eff.Type == effectType))
            {
                user.Effectives.Remove (user.Effectives.Where (eff => eff.Type == effectType).First ());
            }
        }

        public static UserEffectiveSettings GetEffect (this Creature user, EffectiveType effectiveType)
        {
            var rtn = user.Effectives.Where (eff => eff.Type == effectiveType).FirstOrDefault ();
            if (rtn == null) rtn = new UserEffectiveSettings () { };
            return rtn;
        }
        public static List<UserEffectiveSettings> GetAllEffect (this Creature user) => user.Effectives;
        public static void ClearAllEffect (this Creature user) => user.Effectives.Clear ();
        #endregion

        #region Display title
        //骯髒的流氓（MobName）......約翰·哈珀
        //Settings.Description(Name)......Settings.Name
        public static string NameForDisplay (this Creature creature)
        {
            string decoration = creature.Status.GetDecoration ();
            string desc = default;
            if (creature.isMob)
            {
                //mob不顯示Name屬性，直接拿Description的
                desc = creature.Settings.Description.Replace ("{NAME}", creature.Name);
            }
            else
            {
                desc = $"{creature.Settings.Description}{creature.Settings.Name}({creature.Name})";
            }
            return decoration + desc;
        }
        #endregion
    }
}
