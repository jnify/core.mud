using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public partial class Creature : BaseObjectContainer
    {
        public (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) DoAttack (BaseSkill InputSkill = null)
        {
            Creature defance = FightingInfo.FightingWith;
            var rtn = _DoAttack (InputSkill);
            if (isMob)
            {
                rtn.PrivateAttackerMessage = default;
            }
            else
            {
                rtn.PrivateDefenceMessage = default;
            }

            if (!string.IsNullOrWhiteSpace (rtn.PublicMessage))
                rtn.PublicMessage = rtn.PublicMessage.Replace ("{Attacker}", UserDescriptionAndID)
                .Replace ("{Defance}", defance.UserDescriptionAndID);
            if (!string.IsNullOrWhiteSpace (rtn.PrivateAttackerMessage))
                rtn.PrivateAttackerMessage = rtn.PrivateAttackerMessage.Replace ("{Attacker}", UserDescriptionAndID)
                .Replace ("{Defance}", defance.UserDescriptionAndID);
            if (!string.IsNullOrWhiteSpace (rtn.PrivateDefenceMessage))
                rtn.PrivateDefenceMessage = rtn.PrivateDefenceMessage.Replace ("{Attacker}", UserDescriptionAndID)
                .Replace ("{Defance}", defance.UserDescriptionAndID);
            return rtn;
        }

        public (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) _DoAttack (BaseSkill InputSkill)
        {
            (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) rtn = default;
            if (isStopByCheckFightingInfo (FightingInfo)) return rtn;

            //Get Weapon Type
            CommonObject WeaponRightHand = GetWeaponRightHand ();
            CommonObject WeaponLeftHand = GetWeaponLeftHand ();

            //取得所學技能，支援自動攻擊技能，換成技能實作
            BaseSkill SelectedSkill = InputSkill ?? GetAutoAttackSkill (WeaponRightHand);
            if (SelectedSkill == null)
            {
                Console.WriteLine ($"{Name}:SelectedSkill is NULL");
                return rtn;
            }

            Func<PracticedSkill, bool, bool> AttackerImproveSkillAct = null;
            if (isPPL) AttackerImproveSkillAct = ((UserInfo) this).GettingImprovement;

            //發動技能失敗，熟練度==成功率
            int ActionResult = SelectedSkill.CanExecute (CooldownAction, AttackerImproveSkillAct, StateCurrent, IncreaseSkillTimes);
            if (ActionResult < 0)
            {
                //點數不夠
                rtn.PrivateAttackerMessage = TextUtil.GetConsumeNotEnoughString (ActionResult);
                return rtn;
            }
            else if (ActionResult == 0)
            {
                //發動技能失敗
                rtn.PrivateAttackerMessage = SelectedSkill.FailedMessage (WeaponRightHand);
                return rtn;
            }
            else if (ActionResult == 9)
            {
                //發動技能失敗
                rtn.PrivateAttackerMessage = SelectedSkill.FailedMessage (WeaponRightHand) + "\r\n你從錯誤中學習進步。";
                return rtn;
            }
            //技能發動成功，就開始變更狀態為攻擊成功。不管有沒有扣HP成功。
            rtn.Success = true;

            Creature defance = FightingInfo.FightingWith;
            if (defance == null)
            {
                rtn.PrivateAttackerMessage = "沒有找到攻擊的對象！！";
                return rtn;
            }
            if (!defance.Status.HasFlag (CreatureStatus.Fighting) && defance.StartBattle (this))
                rtn.PublicMessage += $"[1;31m{defance.UserDescriptionAndID}開始攻擊(6){UserDescriptionAndID}！！！[0m\r\n";

            //取得技能等級
            var learnedSkill = LearnedSkillSystem.Where (x => x.skillCategory == SelectedSkill.skillCateogry).FirstOrDefault ();
            int SkillLevel = (learnedSkill == null) ? 0 : learnedSkill.Level;

            //取得傷害次數及數值
            AttackPoint[] damages = SelectedSkill.Damage (this, SkillLevel);

            //被攻擊的對象若在睡覺，則會起床。
            if (defance.Status.HasFlag (CreatureStatus.Sleep))
            {
                defance.Status &= ~CreatureStatus.Sleep;
                defance.Status |= CreatureStatus.Stand;
                rtn.PublicMessage += $"{defance.UserDescriptionAndID}醒過來，並且站了起來。";
            }

            Func<PracticedSkill, bool, bool> DefanceImproveSkillAct = null;
            if (defance.isPPL) DefanceImproveSkillAct = ((UserInfo) defance).GettingImprovement;
            //防守方要開始防禦
            foreach (var attPoint in damages)
            {
                BaseSkill DefanceSkill = UtilityExperence.GetDefanceSkill (defance, SelectedSkill, SkillLevel);
                if (DefanceSkill != null)
                {
                    //沒有站起來，無法使用技能。
                    if (!defance.Status.HasFlag (CreatureStatus.Stand))
                        rtn.PrivateDefenceMessage += "嗯... 你太舒服起不來了...";

                    int DefanceResult = DefanceSkill.CanExecute (defance.CooldownAction, DefanceImproveSkillAct, defance.StateCurrent, IncreaseSkillTimes);
                    if (DefanceResult < 0) //點數不夠
                        rtn.PrivateDefenceMessage += TextUtil.GetConsumeNotEnoughString (DefanceResult).SwapAttackerDefence ();
                    else if (DefanceResult == 0) //發動技能失敗
                        rtn.PrivateDefenceMessage += DefanceSkill.FailedMessage (WeaponRightHand).SwapAttackerDefence ();
                    else if (DefanceResult == 9) //發動技能失敗，但進步了
                        rtn.PrivateDefenceMessage += DefanceSkill.FailedMessage (WeaponRightHand).SwapAttackerDefence () + "\r\n你從錯誤中學習進步。";
                    else
                    {
                        //發動防守成功，不扣生命值
                        rtn.PublicMessage += DefanceSkill.Message (0, defance.GetWeaponRightHand ()).SwapAttackerDefence ();
                        continue;
                    }
                }

                ///////////////防守方無防守，攻擊方進行攻擊/////////////
                //取得守方防禦值
                AttackPoint defancePoint = defance.GetDefancePoint ();
                //扣除生命點數
                int FinalDamage = attPoint.Subtract (defancePoint);
                //扣不到生命值，可能是逃走或死了
                if (!defance.ReduceHP (FinalDamage, this)) return rtn;

                if (defance.StateCurrent.HP > 0)
                {
                    //string successMessage =
                    int Percentage = FinalDamage * 100 / defance.StateMax.HP;
                    rtn.PublicMessage += SelectedSkill.Message (Percentage, WeaponRightHand) + $" ({FinalDamage})\r\n";
                    continue;
                }

                //防守方死亡，需要停止鞭屍
                defance.EndBattle ();
                EndBattle ();
                string finalAttackMsg = SelectedSkill.Message (100, WeaponRightHand) + $" ({FinalDamage})\r\n";
                if (isPPL) //攻擊者為PPL，防守者為Mob，死亡
                {
                    //致命的一擊
                    finalAttackMsg += "{Defance} 駕鶴西歸, 仙山賣豆干去也!!\r\n";
                    finalAttackMsg += "你聽到 {Defance} 臨死前的慘叫聲：啊～～～!!！";
                    rtn.PublicMessage += finalAttackMsg;
                    rtn.PrivateAttackerMessage += ((MobInfo) defance).Died (this);
                }
                else
                {
                    //攻擊者為Mob，防守者為PPL，死亡
                    var diemsg = ((UserInfo) defance).Died (this);
                    rtn.PublicMessage += $"{finalAttackMsg}\r\n{diemsg}";
                    rtn.PrivateDefenceMessage += diemsg;
                }
                return rtn;
            }
            if (isPPL)
                rtn.PublicMessage += "\r\n" + defance.Settings.Name + TextUtil.GetHPDescription (defance.StateCurrent.HP, defance.StateMax.HP);
            return rtn;
        }

        private bool isStopByCheckFightingInfo (BattleStatus fightingInfo)
        {
            if (fightingInfo == null)
            {
                Status &= ~CreatureStatus.Fighting;
                return true;
            };

            //要有攻擊對象，且在同一房間，才能攻擊
            var defance = FightingInfo.FightingWith;
            if ((defance != null) &&
                defance.Geography.RoomID == Geography.RoomID &&
                !defance.Status.HasFlag (CreatureStatus.Dying)
            ) return false;

            //清狀態
            Status &= ~CreatureStatus.Fighting;
            return true;
        }

        private AttackPoint GetDefancePoint ()
        {
            var rtn = new AttackPoint ()
            {
                Physical = BasicDefence
            };
            EQArea.ToList ().ForEach (x =>
            {
                rtn.Physical += rnd.Next (x.Value.DefenceMin, x.Value.DefenceMax);
                rtn.PhysicalReal += x.Value.ExtraDamageDefance.PhysicalReal;
                rtn.Wind += x.Value.ExtraDamageDefance.Wind;
                rtn.WindReal += x.Value.ExtraDamageDefance.WindReal;
                rtn.Fire += x.Value.ExtraDamageDefance.Fire;
                rtn.FireReal += x.Value.ExtraDamageDefance.FireReal;
            });
            return rtn;
        }

        private BaseSkill GetAutoAttackSkill (CommonObject WeaponA)
        {
            List<PracticedSkill> skill = null;
            var defaultBarehandSkill = new PracticedSkill () { Skillname = SkillName.DefaultBarehand };
            WeaponType wt = (WeaponA == null) ? WeaponType.None : WeaponA.Type;
            skill = PracticeSkills.Where (x => x.SkillAttr.isAutoFighting (wt)).ToList ();
            //加入預設的攻擊技能
            if (!skill.Any (sk => sk.Skillname == SkillName.DefaultBarehand)) skill.Add (defaultBarehandSkill);
            PracticedSkill SelectedSkillAttr = skill.ToList ().Random ().First ();
            var rtn = GetSkillImplementation (SelectedSkillAttr.Skillname);
            return rtn;
        }

        internal BaseSkill GetSkillImplementation (SkillName skillName)
        {
            if (skillName == SkillName.DefaultBarehand)
            {
                var defaultBarehandSkill = new PracticedSkill () { Skillname = SkillName.DefaultBarehand, Proficiency = 95 };
                var classBarehand = Assembly.GetExecutingAssembly ().GetTypes ().Where (t =>
                    t.Namespace == "Rest.MercuryCore.SkillDefinition" && t.Name == "SkillDefaultBarehand").First ();
                return (BaseSkill) Activator.CreateInstance (classBarehand, defaultBarehandSkill);
            }
            PracticedSkill practSkill = PracticeSkills.Where (x => x.Skillname == skillName).FirstOrDefault ();
            if (practSkill == null) return default;
            var skillClassName = $"Skill{skillName.ToString()}";
            var classes = Assembly.GetExecutingAssembly ().GetTypes ().Where (t =>
                t.Namespace == "Rest.MercuryCore.SkillDefinition" && t.Name == skillClassName).FirstOrDefault ();
            var rtn = (BaseSkill) Activator.CreateInstance (classes, practSkill);
            return rtn;
        }

        public CommonObject GetWeaponRightHand ()
        {
            if (EQArea.ContainsKey (EquipmentAllowPosition.HandRight.ToString ()))
                return EQArea[EquipmentAllowPosition.HandRight.ToString ()];
            return default;
        }

        public CommonObject GetWeaponLeftHand ()
        {
            if (EQArea.ContainsKey (EquipmentAllowPosition.HandLeft.ToString ()))
                return EQArea[EquipmentAllowPosition.HandLeft.ToString ()];
            return default;
        }

        public ActionCollection Actions;

        public class ActionCollection
        {
            public string FollowWith = default;

            public ActionCollection () { }

            public void Follow (Creature crue)
            {
                FollowWith = crue.Name;
            }

            public void UnFollow () => FollowWith = default;
        }

    }
}
