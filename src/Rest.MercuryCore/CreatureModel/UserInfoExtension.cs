using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public static class UserInfoExtension
    {
        public static string DisplayActionEffect (this UserInfo user)
        {
            string rtn = "目前沒有任何法術效果。";
            var effects = user.GetAllEffect ();
            if (effects != null && effects.Count () > 0)
            {
                rtn = "目前法術效果：";
                effects.ForEach (eff =>
                {
                    var desc = eff.Type.GetDescriptionText<EffectiveType> ().RightPadSpace (16);
                    rtn += $"\r\n{desc}";
                    if (eff.EndTime != DateTime.MaxValue)
                    {
                        TimeSpan ts = eff.EndTime - DateTime.Now;
                        int hours = ts.Days * 24 + ts.Hours;
                        rtn += $" 持續：{hours}小時：{ts.Minutes}分：{ts.Seconds}秒";
                    }
                });
            }
            return rtn;
        }
        public static string AddEffective (this UserInfo user, UserEffectiveSettings effect) =>
            user.actEffect.InitialEffect (user, effect);
        public static void CancelEffective (this UserInfo user, EffectiveType effectType) => user.actEffect.CancelEffect (user, effectType);
        public static void ChangeRoom (this UserInfo user, Room roomTo)
        {
            user.CancelEffective (EffectiveType.Hide);
            user.Geography.World = roomTo.Geography.World;
            user.Geography.Region = roomTo.Geography.Region;
            user.Geography.Area = roomTo.Geography.Area;
            user.Geography.RoomID = roomTo.Geography.RoomID;
            user.Geography.RoomTitle = roomTo.Geography.RoomTitle;
        }
    }
}
