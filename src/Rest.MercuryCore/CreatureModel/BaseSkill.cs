using System;
using System.Threading;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public abstract class BaseSkill
    {
        private Random rnd = new Random ();
        protected PracticedSkill practicedSkill;
        abstract public int CooldownInMillisecondsInBefore { get; }
        abstract public int CooldownInMillisecondsInAfter { get; }
        protected BaseSkill (PracticedSkill Attr)
        {
            this.practicedSkill = Attr;
            skillAttribute = Attr.SkillAttr;
            spellsAttribute = Attr.Skillname.GetAttribute<SpellsAttribute> ();
        }
        public SkillCategory skillCateogry => skillAttribute.skillCategory;
        abstract public string Message (int Percentage = 0, CommonObject RightHandWeapon = default);
        abstract public string FailedMessage (CommonObject RightHandWeapon = default);
        virtual public AttackPoint[] Damage (Creature attacker, int SkillLevel)
        {
            //增加左手武器技能次數
            if (attacker.EQArea.ContainsKey (EquipmentAllowPosition.HandLeft.ToString ()))
            {
                WeaponType onHandType = attacker.EQArea[EquipmentAllowPosition.HandLeft.ToString ()].Type;
                if (Enum.TryParse<SkillCategory> (onHandType.ToString (), true, out var HandSkill))
                    attacker.IncreaseSkillTimes (HandSkill);
            }

            //增加右手武器技能次數
            if (attacker.EQArea.ContainsKey (EquipmentAllowPosition.HandRight.ToString ()))
            {
                WeaponType onHandType = attacker.EQArea[EquipmentAllowPosition.HandRight.ToString ()].Type;
                if (Enum.TryParse<SkillCategory> (onHandType.ToString (), true, out var HandSkill))
                    attacker.IncreaseSkillTimes (HandSkill);
            }

            return null;
        }
        virtual public string ExecuteAction (Creature attacker = null, Creature defence = null, CommonObject obj = null) => default;
        public SkillAttribute skillAttribute;
        public SpellsAttribute spellsAttribute;

        ///1:成功，0失敗，-1:HP不足，-2:MAMA不足, -3:MV不足, 9:熱練度增加
        public int CanExecute (Action<int, int> CooldownAction, Func<PracticedSkill, bool, bool> ImprovementSkillAction,
            BasicAndUpgrade state, Action<SkillCategory> increaseSkillTimes)
        {
            CooldownAction (CooldownInMillisecondsInBefore, CooldownInMillisecondsInAfter);
            if (CooldownInMillisecondsInBefore > 0) Thread.Sleep (CooldownInMillisecondsInBefore);
            bool canExe = rnd.Next (0, 100) < practicedSkill.Proficiency;
            BasicAndUpgrade consume = skillAttribute.Consume;
            if (state.HP < consume.HP) return -1;
            if (state.Mana < consume.Mana) return -2;
            if (state.MV < consume.MV) return -3;
            state.HP -= consume.HP;
            state.Mana -= consume.Mana;
            state.MV -= consume.MV;
            increaseSkillTimes (skillAttribute.skillCategory);
            if (!canExe)
            {
                consume = consume.Halved;
                //失敗要GettingImprovement
                if (ImprovementSkillAction != null)
                    if (ImprovementSkillAction (practicedSkill, false))
                        return 9;
                    else
                        return 0;
                else
                    return 0;
            }
            return 1;
        }
        protected AttackPoint GetAttackPoint (int basicAttack, CommonObject RightWeapon, CommonObject LeftWeapon, int SkillLevel, double Magnification)
        {
            RightWeapon = RightWeapon ?? new CommonObject () { };
            LeftWeapon = LeftWeapon ?? new CommonObject () { };

            int PhysicalRight = rnd.Next (RightWeapon.DamageMin + SkillLevel, RightWeapon.DamageMax + SkillLevel).Magnification (Magnification);
            int PhysicalLeft = rnd.Next (LeftWeapon.DamageMin, LeftWeapon.DamageMax).Magnification (Magnification) / 2; //左手武器，效能減半
            AttackPoint rtn = new AttackPoint ()
            {
                Physical = basicAttack + PhysicalRight + PhysicalLeft,

                PhysicalReal = RightWeapon.ExtraDamageDefance.PhysicalReal.Magnification (Magnification) +
                (RightWeapon.ExtraDamageDefance.PhysicalReal.Magnification (Magnification) / 2),

                WindReal = RightWeapon.ExtraDamageDefance.Wind.Magnification (Magnification) +
                (LeftWeapon.ExtraDamageDefance.Wind.Magnification (Magnification) / 2),

                FireReal = RightWeapon.ExtraDamageDefance.Fire.Magnification (Magnification) +
                (LeftWeapon.ExtraDamageDefance.Fire.Magnification (Magnification) / 2),
                Wind = 0, Fire = 0 //還沒實作
            };
            return rtn;
        }
    }
}
