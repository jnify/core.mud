namespace Rest.MercuryCore.CreatureModel
{
    public class Mob_Settings
    {
        public bool AutoAttackUser { get; set; }
        public GrudgesAndResolve Grudges { get; set; }
        public string GrudgesMessage { get; set; }
    }

    public enum MobRole
    {
        None,
        Trainer,
        Vendor
    }
    //todo:目前只驗證了AttackTheMobOnly而已
    public enum GrudgesAndResolve
    {
        Reginal, //死亡不記仇，但區域性的仇人，會主動攻擊
        AttackTheMobOnly, //被攻擊就會記仇，Mob死亡 or restart 後解除
        MobDiedToPPLDied, //Mob死亡後會記仇，記在同名身上(DB)，PPL被該Mob殺死後解除
        Reginal_MobDiedToPPLDied, //Mob死亡後會被整個Region記仇，PPL死亡後解除
    }

}
