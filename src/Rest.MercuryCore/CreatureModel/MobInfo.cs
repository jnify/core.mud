using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.CreatureModel
{
    public class MobInfo : Creature, IFirebaseObject
    {
        private IMessageManager msgMgr;
        private IWorldManager wordmgr;

        public new string Name
        {
            set
            {
                base.Name = value;
            }
            get
            {
                return base.Name;
            }
        }
        public TimeSpan SettingRebornTime { get; set; }
        public DateTime RebornTime = DateTime.MaxValue;
        public bool AutoMovement { get; set; }
        public int BornInRoomID { get; set; }
        public string BornInRegion { get; set; }
        public MobRole Role { get; set; }
        public string[] Murmur { get; set; }
        public List<VendorMenu> Menu { get; set; }
        public Mob_Settings MobSettings { get; set; }
        public bool inAction = false;
        public Func<Room, MobInfo, string> GodOfDead_MakeCorpse;

        [JsonIgnore]
        public Func<ActionKeyParameter, CommonObject[], string> MeetPPLAction { get; set; }

        [JsonIgnore]
        public string KillBy { get; set; }

        public MobInfo () : base ()
        {
            AutoMovement = false;
            SettingRebornTime = new TimeSpan (0, 10, 0); //重生時間10分鐘
            KillBy = default;
        }

        public void SetGlobalManager (IMessageManager msgMgr, IWorldManager wordmgr)
        {
            this.msgMgr = msgMgr;
            this.wordmgr = wordmgr;
        }

        public string Died (Creature attacker)
        {
            string rtn = default;

            //計算Exp
            var Attacker = (UserInfo) attacker;
            KillBy = Attacker.Name;
            //同隊，同RoomID的均分經驗值
            var GroupUsers = wordmgr.GetGroupUsers (attacker.Name).Where (x => x.Geography.RoomID == Attacker.Geography.RoomID).ToList ();
            if (GroupUsers.Count () == 0)
            {
                //沒有組隊
                rtn = $"{Attacker.AddExperience(StateMax.Experience)}";
            }
            else
            {
                List < (string UserName, string Message) > GroupPrivateMsg = new List < (string PublicMessage, string PrivateMessage) > ();
                int GroupSize = GroupUsers.Count ();
                int Experence = (StateMax.Experience / GroupSize);
                GroupUsers.ForEach (user =>
                {
                    if (string.Compare (user.Name, attacker.Name, true) == 0)
                    {
                        //本身獲得的經驗值訊息
                        rtn = $"{Attacker.AddExperience(Experence)}";
                    }
                    else
                    {
                        //隊友獲得的經驗值訊息
                        string LevelUpMsg = $"{user.AddExperience(Experence)}";
                        GroupPrivateMsg.Add ((user.Name, LevelUpMsg));
                    }
                });
                Task.Run (() =>
                {
                    Thread.Sleep (500);
                    foreach (var item in GroupPrivateMsg) msgMgr.SendToPPL (item.Message, item.UserName);
                });
            }

            var room = wordmgr.GetRoom (Geography.RoomID);

            //產生屍體及自動捨取物品訊息
            rtn += GodOfDead_MakeCorpse (room, this);

            //加到死亡清單
            wordmgr.DiedMobs_Add (this);
            //從房間移除
            room.Mobs.Remove (this);
            RebornTime = DateTime.Now.Add (SettingRebornTime);
            return rtn;
        }
    }
}
