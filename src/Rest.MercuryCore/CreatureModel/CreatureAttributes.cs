using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json.Serialization;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public class PracticedSkill
    {
        //技能設定
        public SkillName Skillname { get; set; }
        //技能熟練度
        public int Proficiency { get; set; }

        [JsonIgnore]
        public SkillAttribute SkillAttr => Skillname.GetAttribute<SkillAttribute> ();
        [JsonIgnore]
        public SpellsAttribute SpellsAttr => Skillname.GetAttribute<SpellsAttribute> ();
    }
    public class LearnedSkillCategory
    {
        //技能系統
        public LearnedSkillCategory (SkillCategory skillCategory, int level, int experience)
        {
            this.skillCategory = skillCategory;
            this.Level = level;
            this.Experience = experience;

        }
        public SkillCategory skillCategory { get; set; }
        //等級
        public int Level { get; set; }
        //經驗值
        public int Experience { get; set; }
    }

    //技能系統
    //todo:每個職業的技能上限都不一樣
    public enum SkillCategory
    {
        ////////以下為冒險技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("冒險")]
        Adventure,

        [Description ("知識鑑別")]
        Knowledge,

        ////////以下為通用技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("戰鬥")]
        Combat,

        ////////以下為武器技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("短刀、短劍")]
        Dagger,

        [Description ("長劍")]
        Sword,

        [Description ("雙手劍")]
        Sword2hd,

        [Description ("法杖")]
        Staff,

        [Description ("鎚")]
        Hammer,

        [Description ("盾")]
        Shield,

        [Description ("弓")]
        Bow,

        [Description ("繩、鞭")]
        Whip,

        [Description ("爪")]
        Claw,

        [Description ("矛、槍")]
        Stab,

        [Description ("徒手")]
        Barehand,
        ////////以下為職業技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("盜賊")]
        Thief,

        [Description ("忍者")]
        Ninja,

        [Description ("殺手")]
        Assassin,

        [Description ("流浪人")]
        Ranger,

        [Description ("戰士")]
        Fighter,

        [Description ("武士")]
        Warrior,

        [Description ("騎士")]
        Knight,

        [Description ("俠客")]
        Paladin,

        [Description ("贀者")]
        Sage,

        [Description ("巫師")]
        Wizard,

        [Description ("施法者")]
        Magic,

        [Description ("魔法使")]
        Magician,

        [Description ("大法師")]
        Archmage,

        [Description ("音樂家")]
        Musician,

        [Description ("吟遊詩人")]
        Bard,

        [Description ("領袖")]
        Chief,

        ////////以下為法術技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("火")]
        Fire,

        [Description ("水")]
        Water,

        [Description ("光")]
        Ligth,

        [Description ("闇")]
        Dark,

        [Description ("風")]
        Wind,

        [Description ("雷")]
        Lightning,

        [Description ("土")]
        Earth,

        [Description ("毒")]
        Poison,

        [Description ("聖")]
        Saint,

        [Description ("邪")]
        Evil,

        ////////以下為音樂技能//////////////////////////////////////////////////////////////////////////////////
        [Description ("樂器")]
        Musical,

    }

    public enum SkillName
    {
        //這個不需要，也無法學習，描述為空白，避免被顯示出來
        [Skill ("", 5, 0, 0,
        skillCategory = SkillCategory.Combat, LearnSkillMinLevel = 1,
        MustWearWeapon = WeaponType.None, DefanceSkills = SkillName.Dodge,
        SupportAutoFighting = true, SupportAutoDefance = false, ProficiencyMax = 95)]
        DefaultBarehand,

        ////////以下為通用技能//////////////////////////////////////////////////////////////////////////////////
        //15	balance control	平衡控制	95%
        //25	second attack	連續兩次攻擊	95%
        //30	hunt	追擊	95%*
        //40	perception	perception	90%
        [Skill ("閃躲", 5, 0, 0,
        skillCategory = SkillCategory.Combat, LearnSkillMinLevel = 1)]
        Dodge,

        [Skill ("逃跑", 5, 0, 0,
        skillCategory = SkillCategory.Combat, LearnSkillMinLevel = 1)]
        Flee,

        ////////以下為短刀、短劍技能//////////////////////////////////////////////////////////////////////////////////
        //21	double pierce	連續兩次刺擊	95%
        //41	tripple pierce	連續三次刺擊	90%
        //51	pierce evasive	閃躲	90%
        //71	quadru pierce	連續四次刺擊	85%
        [Skill ("重重一擊", 10, 0, 0,
        skillCategory = SkillCategory.Dagger, LearnSkillMinLevel = 6, MustWearWeapon = WeaponType.Dagger,
        SupportAutoFighting = true)]
        PierceHeavily,

        ////////以下為冒險技能//////////////////////////////////////////////////////////////////////////////////
        [Skill ("游泳", 2, 0, 0,
        skillCategory = SkillCategory.Adventure, LearnSkillMinLevel = 1)]
        Swimming,

        [Skill ("爬山", 3, 0, 0,
        skillCategory = SkillCategory.Adventure, LearnSkillMinLevel = 1)]
        Climbing,

        ////////以下為盜賊技能//////////////////////////////////////////////////////////////////////////////////
        [Skill ("繞刺", 2, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 1,
        MustWearWeapon = WeaponType.Dagger, ProficiencyMax = 40)]
        Circle,

        [Skill ("偷窺", 0, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 1)]
        Peek,

        [Skill ("妙手空空", 15, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 3)]
        Steal,

        [Skill ("隱匿", 5, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 5)]
        Hide,

        [Skill ("潛行", 0, 0, 0, //每個Ticket花費5MV
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 25)]
        Sneak,

        [Skill ("偵察", 2, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 30)]
        Spy,

        [Skill ("解鎖", 15, 0, 0,
        skillCategory = SkillCategory.Thief, LearnSkillMinLevel = 10, ProficiencyMax = 80)]
        PickLock,

        ////////以下為法術技能//////////////////////////////////////////////////////////////////////////////////
        [Skill ("燃燒之手", 0, 5, 0, skillCategory = SkillCategory.Fire, LearnSkillMinLevel = 1)]
        [Spells (CastTargetType.CreatureAttack, CastMessage = "口中吟唱著咒語，'የሚቃጠሉእጆች'。")]
        BurningHands,

        [Skill ("火焰箭", 0, 8, 0, skillCategory = SkillCategory.Fire, LearnSkillMinLevel = 6)]
        [Spells (CastTargetType.CreatureAttack, CastMessage = "比畫著不知名的符號，'የነበልባልቀስት'。")]
        FlameArrow,

    }
    public enum CreatureRace
    {
        [Description ("人類")]
        Human
    }
    public enum CreatureCareer
    {
        [CareerCategory ("盜賊")]
        [Description ("盜賊")]
        Thief,

        [CareerCategory ("盜賊")]
        [Description ("忍者")]
        Ninja,

        [CareerCategory ("盜賊")]
        [Description ("殺手")]
        Assassin,

        [CareerCategory ("盜賊")]
        [Description ("流浪人")]
        Ranger,

        [CareerCategory ("戰士")]
        [Description ("戰士")]
        Fighter,

        [CareerCategory ("戰士")]
        [Description ("武士")]
        Warrior,

        [CareerCategory ("戰士")]
        [Description ("騎士")]
        Knight,

        [CareerCategory ("戰士")]
        [Description ("俠客")]
        Paladin,

        [CareerCategory ("法師")]
        [Description ("施法者")]
        Magic,

        [CareerCategory ("法師")]
        [Description ("魔法使")]
        Magician,

        [CareerCategory ("法師")]
        [Description ("大法師")]
        Archmage,

        [CareerCategory ("吟遊詩人")]
        [Description ("音樂家")]
        Musician,

        [CareerCategory ("吟遊詩人")]
        [Description ("吟遊詩人")]
        Bard,

        [CareerCategory ("吟遊詩人")]
        [Description ("領袖")]
        Chief
    }
    public enum CreatureGender
    {
        [Description ("男性")]
        Male,

        [Description ("女性")]
        Female,

        [Description ("中性")]
        Neutral
    }

    [Flags]
    public enum CreatureStatus
    {
        None = 0, //用到會GG，任何HasFlag(None)的會回傳True
        Fighting = 1,
        Stand = 2,
        Rest = 4,
        Sleep = 8,
        Flying = 16,

        [Decoration ("[1;30m隱匿[0m")]
        Hide = 32,

        [Decoration ("[1;30m潛行[0m")]
        Sneak = 64,
        Dying = 524288
    }
    public class BasicAndUpgrade
    {
        public int HP { get; set; }
        public int Mana { get; set; }
        public int MV { get; set; }

        [JsonIgnore]
        public BasicAndUpgrade Copy
        {
            get
            {
                return new BasicAndUpgrade ()
                {
                    HP = HP,
                        Mana = Mana,
                        MV = MV
                };
            }
        }

        [JsonIgnore]
        public BasicAndUpgrade Halved
        {
            get
            {
                return new BasicAndUpgrade ()
                {
                    HP = HP / 2,
                        Mana = Mana / 2,
                        MV = MV / 2
                };
            }
        }
    }
    public class BasicAndUpgradeSetting : BasicAndUpgrade
    {
        public int Experience { get; set; }
    }

    [AttributeUsage (AttributeTargets.Field)]
    public class CareerCategoryAttribute : Attribute
    {
        public string Category { get; private set; }
        public CareerCategoryAttribute (string Category)
        {
            this.Category = Category;
        }
    }

    [AttributeUsage (AttributeTargets.Field)]
    public class DecorationAttribute : Attribute
    {
        public string Decoration { get; private set; }
        public DecorationAttribute (string Decoration)
        {
            this.Decoration = Decoration;
        }
    }

    public class UserConfiguration
    {
        private string name;

        public UserConfiguration ()
        {
            this.AutoFighting = true;
            this.AutoEatDrink = true;
            this.AutoGet = true;
            this.Wimpy = 20;
            this.Prompt = "*生命%h 精神%m 體力%v*";
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace (Description)) Description = value;
                name = value;
            }
        }
        public string Description { get; set; }
        public string DetailDescription { get; set; }
        public bool AutoHelpFighting { get; set; }
        public bool AutoFighting { get; set; }
        public bool AutoEatDrink { get; set; }
        public int Wimpy { get; set; }
        public string Prompt { get; set; }
        public bool AutoGet { get; set; }
    }

    //////怪怪的設計
    public class BattleStatus
    {
        public Creature FightingWith;

        public void Clear ()
        {
            FightingWith = null;
        }

    }

}
