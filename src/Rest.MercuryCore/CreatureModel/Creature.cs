using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.CreatureModel
{
    public partial class Creature : BaseObjectContainer
    {
        protected readonly object LockHP = new object ();
        protected readonly object LockIncreaseExecuteSkillTimes = new object ();
        private readonly Random rnd = new Random ();
        internal List<UserEffectiveSettings> Effectives { get; set; }

        [JsonIgnore]
        public string ID
        {
            get
            {
                if (id == default) id = Guid.NewGuid ().ToString ();
                return id;
            }

            set
            {
                id = value;
            }
        }

        private string name;
        private string id;
        [JsonIgnore]
        public Dictionary<string, int> BeAttackWith = new Dictionary<string, int> () { };
        [JsonIgnore]
        public DateTime LastMessageDatetime { get; set; }

        public int Level { get; set; }
        //所學技能
        public List<PracticedSkill> PracticeSkills { get; set; }
        //所學系統
        public List<LearnedSkillCategory> LearnedSkillSystem { get; set; }

        public CreatureRace Race { get; set; }
        public CreatureGender Gender { get; set; }
        public CreatureCareer Career { get; set; }

        public Dictionary<string, CommonObject> EQArea { get; set; }

        public WorldInfra Geography { get; set; }
        public Attr Attribute { get; set; }
        public CreatureStatus Status { get; set; }
        public BattleStatus FightingInfo { get; set; }
        public BasicAndUpgrade StateCurrent { get; set; }
        public BasicAndUpgradeSetting StateMax { get; set; }

        public int BasicAttack { get; set; }
        public int BasicDefence { get; set; }
        public UserConfiguration Settings { get; set; }
        public int Coins { get; set; }

        [JsonIgnore]
        public bool isMob => Guid.TryParse (ID, out var tmp);
        [JsonIgnore]
        public bool isPPL => !isMob;
        public void BindDelayInMilliseconds (Action<int> act)
        {
            CooldownAction = (before, after) =>
            {
                act (after);
                var ts = new TimeSpan (0, 0, 0, 0, after) + new TimeSpan (0, 0, 0, 0, before);
                NextAutoFightingTime = DateTime.Now.Add (ts);
                LastMessageDatetime = NextAutoFightingTime;
            };
        }

        [JsonIgnore]
        public Action<int, int> CooldownAction;
        [JsonIgnore]
        public Action<SkillCategory> IncreaseSkillTimes;
        [JsonIgnore]
        public DateTime NextAutoFightingTime = DateTime.MinValue;

        [JsonIgnore]
        protected Dictionary<SkillCategory, int> SkillSystemTimes { get; set; }

        [JsonIgnore]
        public string UserDescriptionAndID
        {
            get
            {
                return $"(%%{Name}|{Settings.Name}%%)";
            }
        }

        [JsonIgnore]
        public string Name
        {
            get => name;
            set
            {
                name = value;
                ID = value.ToUpper ();
                if (string.IsNullOrWhiteSpace (Settings.Name))
                {
                    Settings.Name = value;
                }
            }
        }

        public Creature ()
        {
            Level = 1;

            Geography = new WorldInfra ()
            {
                World = EWorld.亞爾斯隆世界.GetDescriptionText (),
                Region = ERegion.新手區.GetDescriptionText (),
                Area = "新手區",
                RoomID = 1
            };
            Things = new List<CommonObject> ();
            EQArea = new Dictionary<string, CommonObject> ();
            Settings = new UserConfiguration () { };
            FightingInfo = new BattleStatus () { };
            StateCurrent = new BasicAndUpgrade ();
            StateMax = new BasicAndUpgradeSetting ();
            Status = CreatureStatus.Stand;
            SkillSystemTimes = new Dictionary<SkillCategory, int> ();
            LearnedSkillSystem = new List<LearnedSkillCategory> ();

            IncreaseSkillTimes = (x) =>
            {
                //增加技能次數
                if (isPPL && LearnedSkillSystem.Any (skill => skill.skillCategory == x))
                    _Increase_SkillSystemTimes (x);
            };

            Actions = new Creature.ActionCollection ();
            CooldownAction = (before, after) => NextAutoFightingTime = DateTime.Now.Add (new TimeSpan (0, 0, 0, 0, before) + new TimeSpan (0, 0, 0, 0, after));
            PracticeSkills = new List<PracticedSkill> ()
            {
                new PracticedSkill () { Skillname = SkillName.Dodge, Proficiency = 5 }
            };
            Effectives = new List<UserEffectiveSettings> ();
        }

        private void _Increase_SkillSystemTimes (SkillCategory x)
        {
            if (SkillSystemTimes.ContainsKey (x))
                SkillSystemTimes[x]++;
            else
                lock (LockIncreaseExecuteSkillTimes)
                {
                    if (SkillSystemTimes.ContainsKey (x))
                        SkillSystemTimes[x]++;
                    else
                        SkillSystemTimes.Add (x, 1);
                }
            if (SkillSystemTimes[x] > 10) SkillSystemTimes[x] = 10;
        }

        public void Clear ()
        {
            EndBattle ();
        }

        public void EndBattle ()
        {
            if (FightingInfo != null && FightingInfo.FightingWith != null)
                FightingInfo.FightingWith.Status &= ~CreatureStatus.Fighting;
            FightingInfo = new BattleStatus () { };
            Status &= ~CreatureStatus.Fighting;
        }

        public void CancelHideSneakEffect ()
        {
            if (isPPL)
            {
                ((UserInfo) this).CancelEffective (EffectiveType.Hide);
                ((UserInfo) this).CancelEffective (EffectiveType.Sneak);
            }

        }

        public bool StartBattle (Creature Defender)
        {
            CancelHideSneakEffect ();
            if (Defender.StateCurrent.HP < 1)
            {
                return false;
            }
            else if (Status.HasFlag (CreatureStatus.Fighting))
            {
                return FightingInfo.FightingWith.Name == Defender.Name;
            }
            else
            {
                FightingInfo = new BattleStatus ()
                {
                    FightingWith = Defender
                };
                Status |= CreatureStatus.Fighting;
                return true;
            }

        }

        //取得現況屬性
        public Attr GetCreatureAttribute ()
        {
            var rtn = new Attr ()
            {
                Constitution = Attribute.Constitution,
                Strength = Attribute.Strength,
                Dexterity = Attribute.Dexterity,
                Wisdom = Attribute.Wisdom,
                Intelligent = Attribute.Intelligent,
            };
            EQArea.ToList ().ForEach (x =>
            {
                rtn.Constitution += x.Value.ExtraAttribute.Constitution;
                rtn.Strength += x.Value.ExtraAttribute.Strength;
                rtn.Dexterity += x.Value.ExtraAttribute.Dexterity;
                rtn.Wisdom += x.Value.ExtraAttribute.Wisdom;
                rtn.Intelligent += x.Value.ExtraAttribute.Intelligent;
            });
            return rtn;
        }

        public bool ReduceHP (int damage, Creature Attacker)
        {
            if (damage > 0)
            {
                lock (LockHP)
                {
                    if (StateCurrent.HP > 0)
                    {
                        StateCurrent.HP -= damage;
                        if (isMob)
                        {
                            //記錄傷害值
                            //todo:會自動轉攻擊傷害值最高的人，也可視為仇恨值
                            if (BeAttackWith.ContainsKey (Attacker.Name))
                            {
                                BeAttackWith[Attacker.Name] += damage;
                            }
                            else
                            {
                                BeAttackWith.Add (Attacker.Name, damage);
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public CommonObject GetEQThing (string objname)
        {
            CommonObject rtn = null;
            int index = 0;
            if (TextUtil.GetNumberOfObject (objname, out objname, out index))
            {
                if (EQArea.Where (x => x.Value.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)).Count () < index) return rtn;
                if (index > 0) --index;
            }
            if (EQArea.Any (x => x.Value.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)))
            {
                rtn = EQArea.Where (x => x.Value.Name.StartsWith (objname, StringComparison.InvariantCultureIgnoreCase)).ToList () [index].Value;
            }
            return rtn;
        }
    }
}
