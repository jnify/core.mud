using System;
using System.Collections.Generic;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public partial class UserInfo : Creature //, IFirebaseObject
    {
        private DateTime idelIntime = DateTime.MinValue;
        public int idelTimes = 1;
        /// <summary>
        /// PPL還可否發呆
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>True：顯示提示，False：踢除PPL</returns>
        public bool SetIdel ()
        {
            DateTime currentIDtime = DateTime.Now;
            //三次發呆五分鐘，就強制下線。斷線也包含在內。
            var ts = TimeSpan.FromMilliseconds (-300500);
            var CheckTime = currentIDtime.Add (ts); //目前的時間，減掉五分鐘多一點，預期應該小一點，為發呆中
            if (CheckTime > idelIntime)
                idelTimes = 1;
            else
                idelTimes++;

            idelIntime = currentIDtime;

            return idelTimes > 2;
        }
        public bool GettingImprovement (PracticedSkill SkillAttr, bool usePoint = false)
        {
            int maxProficiency = SkillAttr.SkillAttr.ProficiencyMax;
            if (SkillAttr.Proficiency >= maxProficiency) return false;

            var CreateAttr = GetCreatureAttribute ();
            if (usePoint || UtilityExperence.CanIncreaseProficiency (CreateAttr))
            {
                SkillAttr.Proficiency += UtilityExperence.IncreaseProficiency (CreateAttr);
                if (SkillAttr.Proficiency > maxProficiency) SkillAttr.Proficiency = maxProficiency;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
