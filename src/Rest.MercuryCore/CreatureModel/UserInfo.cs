using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;
using static Rest.MercuryCore.Utilities.TextUtil;

namespace Rest.MercuryCore.CreatureModel
{
    public partial class UserInfo : Creature, IFirebaseObject
    {
        private readonly Random rnd = new Random ();
        internal IActionUserEffect actEffect;
        public PointSystem Points { get; set; }

        public UserInfo (IActionUserEffect actEffect) : base ()
        {
            this.actEffect = actEffect;
            Points = new PointSystem ();
            AuditLog = new Audit ();
            Livelihood = new Necessities ()
            {
                Thirsty = 100,
                Full = 100
            };
        }

        public void SetEffectionAction (IActionUserEffect actEffect)
        {
            this.actEffect = actEffect;
        }
        public string Password { get; set; }
        public Necessities Livelihood { get; set; }

        public Audit AuditLog { get; set; }

        public string AddExperience (int Exp)
        {
            string msg = default;
            int SkillExp = 0;
            //未達技能上限之技能才列入計算。只要沒"學"到上限，就會吃技能經驗，todo:要避免吃技能經驗
            SkillSystemTimes = SkillSystemTimes.Where (x =>
                LearnedSkillSystem.Where (y => y.skillCategory == x.Key).FirstOrDefault ().Level <
                SkillLimitation.GetSkillCategoryLimit (this, x.Key)
            ).ToDictionary (x => x.Key, x => x.Value);

            //施用效果依次遞減
            var totalSkillcount = SkillSystemTimes.Sum (x =>
                Decimal.ToInt32 (((10m - x.Value) + 10m) * x.Value / 2m)
            );
            //本身Exp以5計算
            int SelfExpTimes = 5;
            totalSkillcount += (SelfExpTimes + 10) * SelfExpTimes / 2;

            //計算技能經驗值
            decimal perUnit = (Exp * 1m) / totalSkillcount;
            SkillSystemTimes.ToList ().ForEach (x =>
            {
                int subExp = Decimal.ToInt32 ((((10 - x.Value) + 10) * x.Value / 2) * perUnit);
                var increaseSkill = LearnedSkillSystem.Where (y => y.skillCategory == x.Key).FirstOrDefault ();
                if (increaseSkill != null) increaseSkill.Experience += subExp;
                SkillExp += subExp;
                if (UtilityExperence.CheckNextLevelExperence (increaseSkill.Level + 1, increaseSkill.Experience))
                {
                    msg += $"你的{increaseSkill.skillCategory}技能已經達到昇 {increaseSkill.Level + 1} 級的水準!!\r\n";
                }
            });

            //計算本身經驗值
            int SelfExp = Exp - SkillExp;
            if (SelfExp < 1) SelfExp = -1;
            StateMax.Experience += SelfExp;
            msg += $"你總共從這次戰鬥獲得 {SelfExp} 點經驗。";
            if (UtilityExperence.CheckNextLevelExperence (Level + 1, StateMax.Experience))
            {
                var CreateAttr = GetCreatureAttribute ();
                Level++;
                StateMax.Experience -= UtilityExperence.ConvertLevelToExperence (Level);

                //基本攻防屬性是固定的
                BasicAttack = (Level * CreateAttr.Strength / 8) + (CreateAttr.Constitution * Level / 4);
                BasicDefence = ((Level - 5) * CreateAttr.Strength / 8) + (CreateAttr.Constitution * (Level - 5) / 4);
                if (BasicDefence < 1) BasicDefence = 1;
                if (BasicAttack < 10) BasicAttack = 10;

                UtilityExperence.IncreasePoints (Level, Points, CreateAttr, out int Practice, out int Reviviscence);
                if (Reviviscence > 0)
                    msg += $"\r\n你昇級了!!你得到 {Reviviscence} 點復活點數!！";
                //HP, Mana, MV
                UtilityExperence.IncreaseState (Level, StateMax, CreateAttr, out int HP, out int Mana, out int MV);
                int perHP = Decimal.ToInt32 ((HP / (StateMax.HP - HP - 1m)) * 100);
                int perMana = Decimal.ToInt32 ((Mana / (StateMax.Mana - Mana - 1m)) * 100);
                int perMV = Decimal.ToInt32 ((MV / (StateMax.MV - MV - 1m)) * 100);
                msg += $"\r\n你增加了 {perHP} % 的生命點數, {perMana} % 的精神力, {perMV} % 的體力, {Practice} 點的練習點數.";
                //原地滿血
                StateCurrent.HP = StateMax.HP;
                StateCurrent.Mana = StateMax.Mana;
                StateCurrent.MV = StateMax.MV;
            }
            //清空
            SkillSystemTimes.Clear ();
            return msg;
        }

        [JsonIgnore]
        public string PromptMessage =>
            Settings.Prompt.Replace ("%HP%", StateCurrent.HP.ToString ())
            .Replace ("%MV%", StateCurrent.MV.ToString ())
            .Replace ("%MANA%", StateCurrent.Mana.ToString ())
            .Replace ("%AREA%", Geography.Area)
            .Replace ("%NAME%", Settings.Name)
            .Replace ("%ID%", Name)
            .Replace ("%h", TextUtil.GetColorWord (TextUtil.GetPercentage (StateCurrent.HP, StateMax.HP).ToString () + "%", ColorFront.Green))
            .Replace ("%m", TextUtil.GetColorWord (TextUtil.GetPercentage (StateCurrent.Mana, StateMax.Mana).ToString () + "%", ColorFront.Silver))
            .Replace ("%v", TextUtil.GetColorWord (TextUtil.GetPercentage (StateCurrent.MV, StateMax.MV).ToString () + "%", ColorFront.LightBlue))
            .Replace ("%r", Geography.RoomTitle);

        public string Died (Creature Attacker)
        {
            UserEffectiveSettings effsetting = new UserEffectiveSettings (EffectiveType.Dying);
            return this.AddEffective (effsetting);
        }

    }
}
