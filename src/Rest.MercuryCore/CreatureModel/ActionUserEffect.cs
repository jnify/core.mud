using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Rest.MercuryCore.CreatureModel
{
    public class ActionUserEffect : IActionUserEffect
    {
        private readonly Random rnd = new Random ();
        internal readonly IWorldManager wordmgr;
        internal readonly IMessageManager msgmgr;
        internal readonly ILoadCommonObject loadCommonObj;
        private readonly ILogger<ActionUserEffect> logger;

        private readonly Dictionary<EffectiveType, ActionEffect> effectiveList = new Dictionary<EffectiveType, ActionEffect> ();

        public ActionUserEffect (ILogger<ActionUserEffect> logger, IWorldManager wordmgr, IMessageManager msgmgr, ILoadCommonObject loadCommonObj)
        {
            this.wordmgr = wordmgr;
            this.msgmgr = msgmgr;
            this.logger = logger;
            this.loadCommonObj = loadCommonObj;
            new UserEffectiveImplementation (this);
        }

        private ActionEffect GetEffectAction (UserEffectiveSettings Effect) => GetEffectAction (Effect.Type);

        private ActionEffect GetEffectAction (EffectiveType Effect)
        {
            if (effectiveList.ContainsKey (Effect))
            {
                return effectiveList[Effect];
            }
            else
            {
                string msg = $"User effect EffectiveType not found:{Effect}";
                logger.LogError (msg);
                return null;
            }
        }

        public string InitialEffect (UserInfo user, UserEffectiveSettings Effect)
        {
            var act = GetEffectAction (Effect);
            return act.Initial (user, Effect);
        }

        public string MiddleEffect (UserInfo user, EffectiveType Effect)
        {
            var act = GetEffectAction (Effect);
            if (act != null && act.Middle != null)
                return act.Middle (user, Effect);
            else
                return default;
        }

        public string CompleteEffect (UserInfo user, EffectiveType Effect)
        {
            var act = GetEffectAction (Effect);
            if (act != null && act.Complete != null)
                return act.Complete (user, Effect);
            else
                return default;
        }

        public string CancelEffect (UserInfo user, EffectiveType Effect)
        {
            var act = GetEffectAction (Effect);
            if (act != null && act.Cancel != null)
                return act.Cancel (user, Effect);
            else
                return default;
        }

        internal void AddAction (ActionEffect act)
        {
            if (effectiveList.ContainsKey (act.Type))
            {
                Console.WriteLine ($"AddAction:{act.Type}重覆了。");
            }
            else
            {
                effectiveList.Add (act.Type, act);
            }
        }
    }
    public class ActionEffect
    {
        public ActionEffect (EffectiveType type) { this.Type = type; }
        public EffectiveType Type { get; private set; }
        public Func<UserInfo, UserEffectiveSettings, string> Initial { get; set; }
        public Func<UserInfo, EffectiveType, string> Middle { get; set; }
        public Func<UserInfo, EffectiveType, string> Complete { get; set; }
        public Func<UserInfo, EffectiveType, string> Cancel { get; set; }
    }
}
