using System;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.CreatureModel
{
    [AttributeUsage (AttributeTargets.Field)]
    public class SkillAttribute : Attribute
    {
        public WeaponType MustWearWeapon { get; set; }
        public SkillCategory skillCategory { get; set; }
        public int LearnSkillMinLevel { get; set; }
        public bool SupportAutoFighting { get; set; }
        public bool SupportAutoDefance { get; set; }
        public string Description { get; set; }
        public BasicAndUpgrade Consume { get; set; }
        public SkillName DefanceSkills { get; set; }
        public int ProficiencyMax { get; set; }

        /// <summary>
        /// MustWearWeapon: 必需裝備的武器
        /// LearnSkillMinLevel: 學習需要等級
        /// SupportAutoFighting: 是否支援自動攻擊
        /// SupportAutoDefance: 是否支援自動防禦
        /// ProficiencyMax: 熟練度上限
        /// </summary>
        public SkillAttribute (string Description, int MV, int Mana, int HP)
        {
            this.Description = Description;
            this.MustWearWeapon = WeaponType.None;
            this.SupportAutoDefance = false;
            this.SupportAutoFighting = false;
            this.ProficiencyMax = 95;
            this.DefanceSkills = SkillName.Dodge;
            Consume = new BasicAndUpgrade ()
            {
                MV = MV,
                HP = HP,
                Mana = Mana
            };
        }

        public bool isAutoFighting (WeaponType weapontype)
        {
            if (MustWearWeapon == WeaponType.None)
            {
                return SupportAutoFighting;
            }
            else
            {
                return (weapontype == MustWearWeapon && SupportAutoFighting);
            }
        }

        public bool isAutoDefance (WeaponType weapontype)
        {
            if (MustWearWeapon == WeaponType.None)
            {
                return SupportAutoDefance;
            }
            else
            {
                return (weapontype == MustWearWeapon && SupportAutoDefance);
            }
        }
    }

    [AttributeUsage (AttributeTargets.Field)]
    public class SpellsAttribute : Attribute
    {
        public string CastMessage { get; set; }
        public CastTargetType TargetType { get; set; }
        public EffectiveType effectType { get; set; }
        public SpellsAttribute (CastTargetType TargetType)
        {
            this.TargetType = TargetType;
        }
    }

    public enum CastTargetType
    {
        None,
        CreatureEffect,
        CreatureAttack,
        CommonObject
    }
}
