using System;
using System.Linq;
using Rest.MercuryCore.Utilities;

namespace Rest.MercuryCore.CreatureModel
{
    public class UserEffectiveImplementation
    {
        private readonly ActionUserEffect main;
        public UserEffectiveImplementation (ActionUserEffect main)
        {
            this.main = main;
            var thisType = this.GetType ();
            var methods = thisType.GetMethods ();
            string FuctionPrefixName = "Effect";
            int FuctionPrefixNameLength = FuctionPrefixName.Length;
            foreach (var method in methods)
            {
                if (method.Name.StartsWith (FuctionPrefixName))
                {
                    var act = (Func<ActionEffect>) Delegate.CreateDelegate (typeof (Func<ActionEffect>), this, method);
                    var actEffect = act.Invoke ();
                    main.AddAction (actEffect);
                }
            }
        }
        public ActionEffect EffectDying ()
        {
            var act = new ActionEffect (EffectiveType.Dying) { };
            act.Initial = (UserInfo user, UserEffectiveSettings Effective) =>
            {
                user.AddEffect (Effective);
                user.EndBattle ();
                user.Status &= ~CreatureStatus.Stand;
                user.Status &= ~CreatureStatus.Rest;
                user.Status |= CreatureStatus.Sleep;
                user.Status |= CreatureStatus.Dying;
                return TextUtil.GetColorWord ($"{user.UserDescriptionAndID}陷入昏迷了！！", TextUtil.ColorFront.Red);
            };
            act.Cancel = (UserInfo user, EffectiveType effectType) =>
            {
                user.RemoveEffect (effectType);
                user.Status &= ~CreatureStatus.Dying;
                return "你正在睡覺！！";
            };
            act.Middle = (UserInfo user, EffectiveType effectType) =>
            {
                var Effective = user.GetEffect (effectType);
                Effective.Volume++;
                switch (Effective.Volume)
                {
                    case 1:
                    case 2:
                        return "你動彈不得！！！";
                    case 3:
                    case 4:
                        return "你傷的很重！需要找人急救！！";
                    default:
                        return "再不找人急救，你就沒命了！";
                }
            };
            act.Complete = (UserInfo user, EffectiveType effectType) =>
            {
                //移除所有法術效果
                user.ClearAllEffect ();
                user.Clear ();
                user.Status = CreatureStatus.Stand;
                //先取出Room
                var room = main.wordmgr.GetRoom (user.Geography.RoomID);
                user.Points.Reviviscence -= 1;

                //產生屍體，並把掉落物品放進去。
                var corpse = main.loadCommonObj.GenerateCommonObject ("SystemOnly-Corpse");
                corpse.ShortName = $"{user.Settings.Name}的屍體";
                corpse.DescriptionDetail = $"經過了激烈的爭戰，{user.Settings.Name}終就還是逃不了變成屍體的命運。";
                corpse.DispearTS = TimeSpan.FromMinutes (30);
                //處理身上物品
                corpse.Things = user.Things.ToList ();
                user.Things.Clear ();
                //處理身上裝備
                corpse.Things.AddRange (user.EQArea.Select (x => x.Value).ToList ());
                user.EQArea.Clear ();
                //todo:要移除身上的效果及攻擊，防禦
                user.Geography.RoomID = 2140000001; //移到冥河的源頭//Max: 2147483647
                room.AddThing (corpse);
                //恢復戕態
                user.StateCurrent = user.StateMax.Copy;
                main.msgmgr.SendToPPL (TextUtil.GetColorWord ("噢！！不！！你死了！！", TextUtil.ColorFront.Red), user.Name, false);
                main.msgmgr.BroadcastToRoom ($"{user.Name}已經死了！！", room.RoomID, true, user.Name);
                return default;
            };
            return act;
        }
        public ActionEffect EffectHide ()
        {
            var act = new ActionEffect (EffectiveType.Hide) { };
            act.Initial = (UserInfo user, UserEffectiveSettings Effective) =>
            {
                if (user.Status.HasFlag (CreatureStatus.Fighting)) return "你正忙著呢！";
                if (user.GetEffect (Effective.Type) != null) user.RemoveEffect (Effective.Type);
                user.AddEffect (Effective);
                user.Status |= CreatureStatus.Hide;
                return "你隱匿了起來。";
            };
            act.Complete = (UserInfo user, EffectiveType effectType) =>
            {
                user.RemoveEffect (effectType);
                user.Status &= ~CreatureStatus.Hide;
                return "你慢慢的顯露了出來。";
            };
            act.Middle = (UserInfo user, EffectiveType effectType) => "";
            act.Cancel = (UserInfo user, EffectiveType effectType) =>
            {
                user.RemoveEffect (effectType);
                user.Status &= ~CreatureStatus.Hide;
                return default;
            };
            return act;
        }

        public ActionEffect EffectHSneak ()
        {
            var act = new ActionEffect (EffectiveType.Sneak) { };
            act.Initial = (UserInfo user, UserEffectiveSettings Effective) =>
            {
                if (user.Status.HasFlag (CreatureStatus.Fighting)) return "你正忙著呢！";
                if (user.GetEffect (Effective.Type) != null) user.RemoveEffect (Effective.Type);
                user.AddEffect (Effective);
                user.Status |= CreatureStatus.Sneak;
                return "你開始小心翼翼的行動。";
            };
            act.Complete = (UserInfo user, EffectiveType effectType) =>
            {
                user.RemoveEffect (effectType);
                user.Status &= ~CreatureStatus.Sneak;
                return "你恢復了正常的行動。";
            };
            act.Middle = (UserInfo user, EffectiveType effectType) =>
            {
                if (user.StateCurrent.MV > 5)
                {
                    user.StateCurrent.MV -= 5;
                    return "";
                }
                else
                {
                    user.RemoveEffect (effectType);
                    user.Status &= ~CreatureStatus.Sneak;
                    return "你太累了，恢復正常的行動了。";
                }
            };
            act.Cancel = (UserInfo user, EffectiveType effectType) =>
            {
                user.RemoveEffect (effectType);
                user.Status &= ~CreatureStatus.Sneak;
                return default;
            };
            return act;
        }
    }
}
