using System;
using System.ComponentModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.CreatureModel
{
    public class UserEffectiveSettings
    {

        public UserEffectiveSettings () { }

        /// <summary>
        /// Initial effect setting without end date.
        /// </summary>
        /// <param name="Type">EffectiveType</param>
        public UserEffectiveSettings (EffectiveType Type)
        {
            this.Type = Type;
            this.EndTime = DateTime.MaxValue;
            this.Volume = 0;
        }

        /// <summary>
        /// Initial effect setting end date.
        /// </summary>
        /// <param name="Type">EffectiveType</param>
        /// <param name="EndTime">Date time of the end of effect.</param>
        public UserEffectiveSettings (EffectiveType Type, DateTime EndTime)
        {
            this.Type = Type;
            this.EndTime = EndTime;
            this.Volume = 0;
        }

        public EffectiveType Type { get; set; }
        public DateTime EndTime { get; set; }
        public int Volume { get; set; }
    }
    public enum EffectiveType
    {
        None,
        [Description ("瀕死")]
        Dying,

        [Description ("隱匿")]
        Hide,

        [Description ("潛行")]
        Sneak,
    }

    public static class AttrExtensions
    {
        public static Attr GetInRandom (this Attr a, int strength,
            int wisdom,
            int intelligent,
            int constiutution,
            int dexterity,
            int percentage = 5)
        {
            Random rnd = new Random ();
            var ds = Convert.ToInt32 (Math.Round (strength * percentage / 100.00, 0, MidpointRounding.AwayFromZero));
            var dw = Convert.ToInt32 (Math.Round (wisdom * percentage / 100.00, 0, MidpointRounding.AwayFromZero));
            var di = Convert.ToInt32 (Math.Round (intelligent * percentage / 100.00, 0, MidpointRounding.AwayFromZero));
            var dc = Convert.ToInt32 (Math.Round (constiutution * percentage / 100.00, 0, MidpointRounding.AwayFromZero));
            var dd = Convert.ToInt32 (Math.Round (dexterity * percentage / 100.00, 0, MidpointRounding.AwayFromZero));
            Attr rtn = new Attr ()
            {
                Strength = rnd.Next (strength - ds, strength + ds),
                Wisdom = rnd.Next (wisdom - dw, wisdom + dw),
                Intelligent = rnd.Next (intelligent - di, intelligent + di),
                Constitution = rnd.Next (constiutution - dc, constiutution + dc),
                Dexterity = rnd.Next (dexterity - dd, dexterity + dd)
            };
            return rtn;
        }
    }

    public class PointSystem
    {
        public int Practice { get; set; }
        public int Reviviscence { get; set; }
    }

    public class Necessities
    {
        public int Thirsty { get; set; }
        public int Full { get; set; }
    }

    public class Audit
    {
        public string CreateDateTime { get; set; }
        public string LastLoginedDateTime { get; set; }
        public string LastLogOffdDateTime { get; set; }
        public string LastLoginedIP { get; set; }
    }

}
