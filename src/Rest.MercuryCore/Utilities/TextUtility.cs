using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Utilities
{
    internal static class AttackDefaultDescription
    {
        private static List<string> verbList = new List<string>
        {
            "使盡吃奶力氣地",
            "使勁地",
            "拼老命地",
            "奮不顧身地",
            "奮力地",
            "逮到機會地",
            "不顧一切地",
            "看準要害地",
        };

        private static Dictionary<WeaponType, string> WeaponVerbList = new Dictionary<WeaponType, string> ()
        { { WeaponType.None, "一擊" }, { WeaponType.Sword, "一砍" }, { WeaponType.Dagger, "一刺" }
        };

        internal static string WeaponVerb (CommonObject weaponObject)
        {
            WeaponType weaponType = WeaponType.None;
            if (weaponObject != null) weaponType = weaponObject.Type;

            string rtn = $"{weaponType.ToString()}還設定。";
            if (WeaponVerbList.ContainsKey (weaponType)) rtn = WeaponVerbList[weaponType];
            return rtn;
        }
        internal static string RandomVerb => verbList.Random ().FirstOrDefault ();

        internal static string WeaponHitVerb (CommonObject weaponObject, string SkillName)
        {
            string rtn = default;

            if (weaponObject != null && !string.IsNullOrWhiteSpace (weaponObject.ShortName))
                rtn = $"手持{weaponObject.ShortName}{RandomVerb}"; //手持小刀-看準要害地
            else
                rtn = $"{RandomVerb}"; //看準要害地

            if (string.IsNullOrWhiteSpace (SkillName))
                rtn += WeaponVerb (weaponObject);
            else
                rtn += SkillName;

            return rtn;
        }
    }

    public class TextUtil
    {
        private static Random random = new Random ();
        private static string NumberOFPattern = @"^(\d)\.(.*)";

        //todo:檢查看看是否可以刪掉？
        public static string GetSkillExecuteFailedMessage (BaseSkill skill, int skillResult, string UserDescriptionAndID, string DefanceName, CommonObject RightWeapon = null)
        {
            string rtn = default;
            switch (skillResult)
            {
                case -1:
                case -2:
                case -3:
                    rtn = TextUtil.GetConsumeNotEnoughString (skillResult);
                    break;
                case 0:
                    rtn = skill.FailedMessage (RightWeapon);
                    break;
                case 9:
                    rtn = skill.FailedMessage (RightWeapon) + "\r\n你從錯誤中學習進步。";
                    break;
                default:
                    return $"Result:{skillResult}未定義。";
            }
            rtn = rtn.Replace ("{Attacker}", UserDescriptionAndID).Replace ("{Defance}", DefanceName);
            return rtn;
        }

        public static string GetExitName (string exit) => exit.ToUpperInvariant ()
        switch
        {
            "N" => "North",
            "S" => "South",
            "E" => "East",
            "W" => "West",
            "U" => "UP",
            "D" => "DOWN",
            _ => exit
        };

        internal static string GetAttackSuccessMessage (int Percentage, CommonObject WeaponObject, string skillName = default)
        {
            string rtn = "{Attacker}" + AttackDefaultDescription.WeaponHitVerb (WeaponObject, skillName);

            if (Percentage > 100) Percentage = 100;
            int range = Percentage / 10;
            switch (range)
            {
                case 10:
                    string endVerb = AttackDefaultDescription.WeaponVerb (WeaponObject);
                    if (!string.IsNullOrWhiteSpace (skillName)) endVerb = skillName;
                    rtn = "{Attacker}" + AttackDefaultDescription.RandomVerb;
                    rtn += "給{Defance}致命的" + endVerb;
                    return rtn;
                case 9:
                    return $"{rtn}，造成{{Defance}}不可忽視的傷害！";
                case 8:
                    return $"{rtn}，使得{{Defance}}血流不止！";
                case 7:
                    return $"{rtn}，造成{{Defance}}很大的傷害！";
                case 6:
                    return $"{rtn}，造成{{Defance}}重傷！";
                case 5:
                    return $"{rtn}，使得{{Defance}}吐了好幾口鮮血！";
                case 4:
                    return $"{rtn}，造成{{Defance}}些微的傷害！";
                case 3:
                    return $"{rtn}，造成{{Defance}}擦傷。";
                case 2:
                    return $"{rtn}，對{{Defance}}不痛不癢的。";
                default:
                    return $"{rtn}，對{{Defance}}來說卻一點效果也沒有。";
            }
        }
        internal static string GetSpellsSuccessMessage (int Percentage, CommonObject WeaponObject, string Message)
        {
            string rtn = default;
            if (WeaponObject != null && !string.IsNullOrWhiteSpace (WeaponObject.ShortName) && WeaponObject.Type == WeaponType.Staff)
                rtn = $"{{Attacker}}手持{WeaponObject.ShortName}{Message}";
            else
                rtn = $"{{Attacker}}{Message}";

            if (Percentage > 100) Percentage = 100;
            int range = Percentage / 10;
            switch (range)
            {
                case 10:
                    return $"{rtn}，對{{Defance}}造成致命的一擊！";
                case 9:
                    return $"{rtn}，造成{{Defance}}不可忽視的傷害！";
                case 8:
                    return $"{rtn}，使得{{Defance}}血流不止！";
                case 7:
                    return $"{rtn}，造成{{Defance}}很大的傷害！";
                case 6:
                    return $"{rtn}，造成{{Defance}}重傷！";
                case 5:
                    return $"{rtn}，使得{{Defance}}吐了好幾口鮮血！";
                case 4:
                    return $"{rtn}，造成{{Defance}}些微的傷害！";
                case 3:
                    return $"{rtn}，造成{{Defance}}擦傷。";
                case 2:
                    return $"{rtn}，對{{Defance}}不痛不癢的。";
                default:
                    return $"{rtn}，對{{Defance}}來說卻一點效果也沒有。";
            }
        }
        internal static string GetAttackFailedMessage (CommonObject WeaponObject, string skillName = default) =>
            "{Attacker}" + AttackDefaultDescription.WeaponHitVerb (WeaponObject, skillName) + "，結果卻失去了準頭，沒打中{Defance}！";
        public static string EncodePassword (string username, string password)
        {
            var enStr = $"{password}4444{username.ToUpperInvariant()}";
            using (var cryptoMD5 = System.Security.Cryptography.MD5.Create ())
            {
                //將字串編碼成 UTF8 位元組陣列
                var bytes = Encoding.UTF8.GetBytes (password);

                //取得雜湊值位元組陣列
                var hash = cryptoMD5.ComputeHash (bytes);

                //取得 MD5
                var md5 = BitConverter.ToString (hash)
                    .Replace ("-", String.Empty)
                    .ToUpper ();

                return md5;
            }
        }

        public static bool IsSameCareer (CreatureCareer a, CreatureCareer b)
        {
            var aCategory = a.GetAttribute<CareerCategoryAttribute> ();
            var bCategory = b.GetAttribute<CareerCategoryAttribute> ();
            return (aCategory.Category == bCategory.Category);
        }

        public static string GetConsumeNotEnoughString (int actionResult) => actionResult
        switch
        {
            -1 => "{Attacker}快往生了，沒辦法做。", -2 => "{Attacker}頭痛到榨不出一滴精神力。", _ => "{Attacker}太累了，什麼都不想做。"
        };

        public static class ColorFront
        {
            public static string Black { get { return "[30m"; } }
            public static string Red { get { return "[31m"; } }
            public static string Green { get { return "[32m"; } }
            public static string Yellow { get { return "[33m"; } }
            public static string Blue { get { return "[34m"; } }
            public static string Purple { get { return "[35m"; } }
            public static string BlueGreen { get { return "[36m"; } }
            public static string Gray { get { return "[1;30m"; } }
            public static string Reset { get { return "[0m"; } }
            public static string Silver { get { return "[1;30m"; } }
            public static string LightBlue { get { return "[1;34m"; } }
        }

        public static string GetColorWord (bool enable)
        {
            if (enable)
                return $"{TextUtil.ColorFront.Green}啟用{TextUtil.ColorFront.Reset}";
            else
                return $"{TextUtil.ColorFront.Green}停用{TextUtil.ColorFront.Reset}";
        }

        public static string GetColorWord (string txt, string color = null, bool AddBrackets = false)
        {
            color = color ?? TextUtil.ColorFront.Green;
            if (AddBrackets)
            {
                return $"{color}（{txt}）{TextUtil.ColorFront.Reset}";
            }
            else
            {
                return $"{color}{txt}{TextUtil.ColorFront.Reset}";
            }
        }

        public static string GetFromFile (string folder, string fname)
        {
            folder = folder.Replace ("/", "").Replace (".", "").Replace ("\\", "");
            string path = Path.Combine (Path.GetDirectoryName ("."), folder, fname);
            return GetFromFile (path);
        }

        public static string GetFromFile (string FullName)
        {
            if (File.Exists (FullName))
            {
                return File.ReadAllText (FullName, Encoding.UTF8);
            }
            else
            {
                return $"Help document not found:{Path.GetFileNameWithoutExtension (FullName)}";
            }

        }

        public static bool GetNumberOfObject (string command, out string paramaterString, out int index)
        {
            bool rtn = false;
            index = 0;
            paramaterString = command;
            foreach (Match matchs in Regex.Matches (command, NumberOFPattern))
            {
                if (int.TryParse (matchs.Groups[1].Value, out index))
                {
                    paramaterString = matchs.Groups[2].Value;
                    rtn = true;
                }
            }
            return rtn;
        }

        public static int GetPercentage (int Valid, int Total)
        {
            double percent = (double) (Valid * 100) / Total;
            return (int) Math.Round (percent, 0, MidpointRounding.AwayFromZero);
        }

        public static string GetHPDescription (int Valid, int Total)
        {
            Valid = (Valid < 0) ? 0 : Valid;
            var percent = GetPercentage (Valid, Total);
            int range = percent / 10;
            switch (range)
            {
                case 0:
                case 1:
                    return "[31m要壽終正寢[0m了。";
                case 2:
                    return "[31m已接近死亡邊緣[0m。";
                case 3:
                    return "[31m受了嚴重的傷[0m。";
                case 4:
                    return "[1;31m全身是血[0m。";
                case 5:
                    return "[1;31m受了重傷[0m。";
                case 6:
                    return "[1;31m大量出血[0m。";
                case 7:
                    return "[1;35m血流不止[0m。";
                case 8:
                    return "[1;35m開始流血[0m。";
                case 9:
                    return "[36m受了點傷[0m。";
                default:
                    return "[1;36m看起來好極了[0m。";
            }
        }

    }
}
