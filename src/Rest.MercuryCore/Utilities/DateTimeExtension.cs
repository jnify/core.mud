using System;

namespace Rest.MercuryCore.Utilities
{
    public static class DateTimeExtesion
    {
        public static DateTime LastMessageDatetimeExtend (this DateTime source, TimeSpan ts = default)
        {
            DateTime newtime = DateTime.Now;
            if (ts.TotalMilliseconds > 0) newtime = DateTime.Now.Add (ts);
            return (source == DateTime.MaxValue || newtime > source) ? newtime : source;
        }

        public static DateTime LastMessageDatetimeComplete (this DateTime source)
        {
            return (source < DateTime.Now.Add (TimeSpan.FromSeconds (1))) ? DateTime.MaxValue : source;
        }
    }
}
