using System.Globalization;

namespace System.Text.Json.Serialization
{
    /// <summary>
    /// <see cref="JsonConverter"/> to convert TimeSpan to and from strings.
    /// </summary>
    public class JsonTimeSpanConverter : JsonConverter<TimeSpan>
    {
        public override TimeSpan Read (ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.String)
                throw new NotSupportedException ();
            if (typeToConvert != typeof (TimeSpan))
                throw new NotSupportedException ();

            // 使用常量 "c" 來指定用 [-][d.]hh:mm:ss[.fffffff] 作為 TimeSpans 轉換的格式
            return TimeSpan.ParseExact (reader.GetString (), "c", CultureInfo.InvariantCulture);
        }

        public override void Write (Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options)
        {
            writer.WriteStringValue (value.ToString ("c", CultureInfo.InvariantCulture));
        }
    }
}
