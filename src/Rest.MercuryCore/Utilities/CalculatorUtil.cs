using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Utilities
{
    public static class UtilityExperence
    {
        private readonly static Random rnd = new Random ();
        private readonly static List<int> ReviviscenceDef = new List<int> ();
        static UtilityExperence ()
        {
            //定義昇級時取得的復活點數
            Dictionary<int, int> range = new Dictionary<int, int> ();
            range.Add (0, 10);
            range.Add (1, 5);
            range.Add (2, 4);
            range.Add (3, 1);
            foreach (var item in range)
                for (int i = 0; i < item.Value; i++)
                    ReviviscenceDef.Add (item.Key);

        }
        /// <summary>
        /// 該等級所需之經驗值
        /// </summary>
        /// <param name="Level">等級</param>
        /// <returns>經驗值</returns>
        public static int ConvertLevelToExperence (int Level)
        {
            return (Level * Level * Level * Level + 2000);
        }
        /// <summary>
        /// 扣除提昇該等級所需的經驗值結果
        /// </summary>
        /// <param name="NextSkillLevel">提昇的等級</param>
        /// <param name="experience">目前擁有的經驗值</param>
        /// <returns></returns>
        public static int ConductSkillSystemExperence (int NextSkillLevel, int experience)
        {
            return experience - ConvertLevelToExperence (NextSkillLevel);
        }
        /// <summary>
        /// 還差多少經驗值可以昇級
        /// </summary>
        /// <param name="NextSkillLevel">提昇的等級</param>
        /// <param name="experience">目前擁有的經驗值</param>
        /// <returns></returns>
        public static int GetNextLevelNeedExperence (int NextSkillLevel, int experience)
        {
            return ConvertLevelToExperence (NextSkillLevel) - experience;
        }
        /// <summary>
        /// 是否能昇級
        /// </summary>
        /// <param name="NextSkillLevel">提昇的等級</param>
        /// <param name="experience">目前擁有的經驗值</param>
        /// <returns></returns>
        public static bool CheckNextLevelExperence (int NextSkillLevel, int experience)
        {
            if (NextSkillLevel == 1)
                return true;
            else
                return experience > ConvertLevelToExperence (NextSkillLevel);
        }
        /// <summary>
        /// 昇級增加屬性
        /// </summary>
        /// <param name="Level">昇等(前?)等級</param>
        /// <param name="stateMax">全滿的數值</param>
        /// <param name="createAttr">現行基本屬性</param>
        /// <param name="HP">HP增量</param>
        /// <param name="MANA">Mana增量</param>
        /// <param name="MV">MV增量</param>
        internal static void IncreaseState (int Level, BasicAndUpgrade stateMax, Attr createAttr, out int HP, out int MANA, out int MV)
        {
            const int edgeMin = 6;
            const int edgeMax = 7;
            int baseAttrMin = Decimal.ToInt32 (createAttr.Constitution * edgeMin);
            int baseAttrMax = Decimal.ToInt32 (createAttr.Constitution * edgeMax);
            HP = GetIncreaseNum (baseAttrMin, baseAttrMax, Level);
            stateMax.HP += HP;

            baseAttrMin = Decimal.ToInt32 (createAttr.Intelligent * edgeMin);
            baseAttrMax = Decimal.ToInt32 (createAttr.Intelligent * edgeMax);
            MANA = GetIncreaseNum (baseAttrMin, baseAttrMax, Level);
            stateMax.Mana += MANA;

            baseAttrMin = Decimal.ToInt32 (createAttr.Dexterity * edgeMin);
            baseAttrMax = Decimal.ToInt32 (createAttr.Dexterity * edgeMax);
            MV = GetIncreaseNum (baseAttrMin, baseAttrMax, Level);
            stateMax.MV += MV;
        }
        /// <summary>
        /// 增加點數
        /// </summary>
        /// <param name="Level">昇等(前?)等級</param>
        /// <param name="points"></param>
        /// <param name="createAttr">現行基本屬性</param>
        /// <param name="Practice">練習點數</param>
        /// <param name="Reviviscence">複活點數</param>
        internal static void IncreasePoints (int level, PointSystem points, Attr createAttr, out int Practice, out int Reviviscence)
        {
            Reviviscence = ReviviscenceDef.Random ().First ();
            points.Reviviscence += Reviviscence;

            const int edgeMin = 2;
            const int edgeMax = 4;
            int baseAttrMin = Decimal.ToInt32 (createAttr.Wisdom * edgeMin);
            int baseAttrMax = Decimal.ToInt32 (createAttr.Wisdom * edgeMax);
            Practice = rnd.Next (baseAttrMin, baseAttrMax) / 10;
            points.Practice += Practice;
        }

        internal static BaseSkill GetDefanceSkill (Creature defance, BaseSkill AttackerSkill, int skillLevel)
        {
            SkillName finalDefanceSkill = SkillName.DefaultBarehand; //先定一個禁用的技能。若沒有可用的，最後要改成Dodge
            List<PracticedSkill> DefanceSkills = new List<PracticedSkill> ();
            //特定防禦通用技能，或是Dodge
            DefanceSkills = defance.PracticeSkills.Where (x => x.Skillname == SkillName.Dodge ||
                x.Skillname == AttackerSkill.skillAttribute.DefanceSkills).ToList ();

            //武器技能的自動防禦技能
            var rightHandWeapon = defance.GetWeaponRightHand ();
            WeaponType wt = (rightHandWeapon == null) ? WeaponType.None : rightHandWeapon.Type;
            DefanceSkills.AddRange (defance.PracticeSkills.Where (x => x.SkillAttr.isAutoDefance (wt)).ToList ());
            //會自動檢查有沒有Dodge的技能，沒有的話，就加一個上去。等於變相的強迫一定要學Dodge。
            if (DefanceSkills.Any (x => x.Skillname == SkillName.Dodge)) DefanceSkills.Add (new PracticedSkill () { Skillname = SkillName.Dodge, Proficiency = 20 });
            DefanceSkills = DefanceSkills.Random (); //打亂順序

            var CreatureAttr = defance.GetCreatureAttribute ();
            //成功發動門檻:Dex 20時約33,Dex 30時約50。
            //總成功率會累加，但後面的技能成功率會遞減。
            var SuccessPoint = CreatureAttr.Dexterity / 1.65;
            foreach (var defanceSkill in DefanceSkills)
            {
                if (rnd.Next (1, 101) < SuccessPoint)
                {
                    finalDefanceSkill = defanceSkill.Skillname;
                    break;
                }
                SuccessPoint *= 0.7;
            }

            if (finalDefanceSkill == SkillName.DefaultBarehand) //防禦失敗
                return null;
            else
                return defance.GetSkillImplementation (finalDefanceSkill);
        }

        /// <summary>
        /// 技能熟練度進步增量
        /// </summary>
        /// <param name="createAttr">現行基本屬性</param>
        /// <returns></returns>
        public static int IncreaseProficiency (Attr createAttr)
        {
            return Convert.ToInt16 (Math.Floor (createAttr.Intelligent * 0.8));
        }
        /// <summary>
        /// 能否增加技能熟練度
        /// </summary>
        /// <param name="createAttr"></param>
        /// <returns></returns>
        public static bool CanIncreaseProficiency (Attr createAttr)
        {
            return (createAttr.Wisdom * 10) > rnd.Next (0, 300);
        }

        /// <summary>
        /// HP, Mana, MV依等級增量
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private static int GetIncreaseNum (int min, int max, int level)
        {
            decimal increase = (rnd.Next (min, max)) * (level / 2m) / 10;
            return Decimal.ToInt32 (increase);
        }
        public static int DecutLivelihood (int HPValue, int Livelihood)
        {
            return HPValue * (Livelihood * 2 / 100);
        }
        public static int GetSpellsBasicAttack (Creature creature, LearnedSkillCategory skillCategory)
        {
            var CreatureCurrentAttr = creature.GetCreatureAttribute ();
            decimal rtn = (CreatureCurrentAttr.Intelligent * creature.Level / 2m) +
                (skillCategory.Level / (50 - CreatureCurrentAttr.Intelligent) * 20m);
            return Convert.ToInt32 (rtn);
        }
    }

}
