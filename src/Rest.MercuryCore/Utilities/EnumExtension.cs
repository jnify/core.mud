using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Rest.MercuryCore.Utilities
{
    public static class EnumExtension
    {
        private static string pattern = @"\x1b\[(\d;?\d*)m";

        public static string GetDescriptionText<T> (this T source, bool TrimAnsciiColor = false) where T : struct, IConvertible
        {
            string rtn = source.ToString ();
            FieldInfo fi = source.GetType ().GetField (source.ToString ());
            DescriptionAttribute[] attributes = (DescriptionAttribute[]) fi.GetCustomAttributes (
                typeof (DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                rtn = attributes[0].Description;
                if (TrimAnsciiColor) rtn = Regex.Replace (rtn, pattern, "");
            }
            return rtn;
        }

        public static TAttribute GetAttribute<TAttribute> (this Enum value)
        where TAttribute : Attribute
        {
            var enumType = value.GetType ();
            var name = Enum.GetName (enumType, value);
            return enumType.GetField (name).GetCustomAttributes (false).OfType<TAttribute> ().SingleOrDefault ();
        }
        public static string UpSideDownName (this ExitName exit) => exit
        switch
        {
            ExitName.Down => ExitName.Up.GetDescriptionText (),
            ExitName.Up => ExitName.Down.GetDescriptionText (),
            ExitName.West => ExitName.East.GetDescriptionText (),
            ExitName.East => ExitName.West.GetDescriptionText (),
            ExitName.South => ExitName.North.GetDescriptionText (),
            ExitName.North => ExitName.South.GetDescriptionText (),
            _ => "找不到方向"
        };

        public static ExitName UpSideDown (this ExitName exit) => exit
        switch
        {
            ExitName.Down => ExitName.Up,
            ExitName.Up => ExitName.Down,
            ExitName.West => ExitName.East,
            ExitName.East => ExitName.West,
            ExitName.South => ExitName.North,
            ExitName.North => ExitName.South,
            _ => ExitName.None
        };

        public static SessionOfDayAttribute SessionSettings (this SessionOfDay session)
        {
            return session.GetAttribute<SessionOfDayAttribute> ();
        }

        public static string CreaturePositionDescription (this CreatureStatus status)
        {
            if (status.HasFlag (CreatureStatus.Dying))
            {
                return "正陷入昏迷中";
            }
            else if (status.HasFlag (CreatureStatus.Sleep))
            {
                return "正在睡覺";
            }
            else if (status.HasFlag (CreatureStatus.Rest))
            {
                return "正坐著休息";
            }
            else
            {
                if (status.HasFlag (CreatureStatus.Flying))
                {
                    return "在空中飄著";
                }
                else
                {
                    return "站在這裏";
                }
            }
        }

        public static string GetDecoration (this CreatureStatus status)
        {
            string rtn = default;
            foreach (CreatureStatus source in Enum.GetValues (typeof (CreatureStatus)))
            {
                if ((status & source) != 0)
                {
                    var decoration = source.GetAttribute<DecorationAttribute> ();
                    if (decoration != null)
                        rtn += $"（{decoration.Decoration}）";
                }
            }
            return rtn;
        }
    }

}
