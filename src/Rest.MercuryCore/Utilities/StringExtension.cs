using System;
using System.Text.RegularExpressions;
using Rest.MercuryCore.Objectivate;

namespace Rest.MercuryCore.Utilities
{
    public static class StringExtension
    {
        public static ExitName ConvertToExitName (this string source)
        {
            switch (source.ToUpper ())
            {
                case "W":
                case "WEST":
                    source = "WEST";
                    break;
                case "E":
                case "EAST":
                    source = "EAST";
                    break;
                case "N":
                case "NORTH":
                    source = "NORTH";
                    break;
                case "S":
                case "SOUTH":
                    source = "SOUTH";
                    break;
                case "U":
                case "UP":
                    source = "UP";
                    break;
                case "D":
                case "DOWN":
                    source = "DOWN";
                    break;
                default:
                    source = "None";
                    break;
            }
            return (ExitName) Enum.Parse (typeof (ExitName), source, true);
        }

        public static string SplitByUpperCase (this string source)
        {
            string[] bas = source.Split (" ");
            for (var i = 0; i < bas.Length; i++)
            {
                string[] split = Regex.Split (bas[i], @"(?<!^)(?=[A-Z])");
                bas[i] = string.Join (" ", split);
            }
            return string.Join (" ", bas);
        }
        public static string TrimAllSpace (this string source) => Regex.Replace (source, @"\s+", "");
        public static string SwapAttackerDefence (this string source) => source.Replace ("{Attacker}", "{XXXXXXXXXX}")
            .Replace ("{Defance}", "{Attacker}")
            .Replace ("{XXXXXXXXXX}", "{Defance}");
        public static string LeftPadSpace (this string source, int TotalLength)
        {
            int wordlength = source.Length + ChineseWrods (source);
            int finalSpaceLength = TotalLength - wordlength;
            string space = "";
            for (var i = finalSpaceLength; i > 0; i--)
            {
                space += " ";
            }
            return $"{space}{source}";
        }
        public static string RightPadSpace (this string source, int TotalLength)
        {
            int wordlength = source.Length + ChineseWrods (source);
            int finalSpaceLength = TotalLength - wordlength;
            string space = "";
            for (var i = finalSpaceLength; i > 0; i--)
            {
                space += " ";
            }
            return $"{source}{space}";
        }
        public static int ChineseWrodsLength (this string source) => source.Length + ChineseWrods (source);
        public static (string Key, string Value) SpliteFrom (this string source) => source.SpliteBy ("from");
        public static (string Key, string Value) SpliteColon (this string source) => source.SpliteBy (":");
        public static (string Key, string Value) SpliteComma (this string source) => source.SpliteBy (",");
        public static (string Key, string Value) SpliteSpace (this string source) => source.SpliteBy (" ");
        private static (string Key, string Value) SpliteBy (this string source, string symbol)
        {
            if (symbol.Length > 1) symbol = $"{symbol} ";
            int sp = source.ToUpper ().IndexOf (symbol.ToUpper ());
            if (sp == -1) return (source, "");
            string key = source.Substring (0, sp).Trim ();
            string value = source.Substring (sp + symbol.Length).Trim ();
            return (key, value);
        }
        private static int ChineseWrods (string word)
        {
            System.Text.ASCIIEncoding ascii = new System.Text.ASCIIEncoding ();
            int tempLen = 0;
            byte[] s = ascii.GetBytes (word);
            for (int i = 0; i < s.Length; i++)
            {
                if ((int) s[i] == 63) tempLen++;
            }
            return tempLen;
        }
    }
}
