using System;
using Rest.MercuryCore.CreatureModel;

namespace Rest.MercuryCore.Utilities
{
    public static class IntExtension
    {
        public static string ToCoins (this int source)
        {
            var coins = ToCoinTuple (source);
            if (coins.Gold == 0 && coins.Silver == 0)
            {
                return $"{coins.Copper}枚銅幣";
            }
            else if (coins.Gold == 0)
            {
                return $"{coins.Silver}枚銀幣{coins.Copper}枚銅幣";
            }
            else
            {
                return $"{coins.Gold}枚金幣{coins.Silver}枚銀幣{coins.Copper}枚銅幣";
            }
        }
        public static (int Gold, int Silver, int Copper) ToCoinTuple (this int source)
        {
            int gold = source / 1000000;
            int silver = (source - (gold * 1000000)) / 1000;
            int copper = (source - (gold * 1000000) - (silver * 1000));
            return (Gold: gold, Silver: silver, Copper: copper);
        }
        public static int Magnification (this int source, double mang)
        {
            return Convert.ToInt32 (source * mang);
        }
        public static string ToAlignRightString (this int source, int length)
        {
            int finalSpaceLength = length - source.ToString ().Length;
            string space = "";
            for (var i = finalSpaceLength; i > 0; i--)
            {
                space += " ";
            }
            return $"{space}{source}";

        }
        public static string GetSkillFailedMessage (this int source, BaseSkill selectedSkill)
        {
            ///1:成功，0失敗，-1:HP不足，-2:MAMA不足, -3:MV不足, 9:熱練度增加
            string rtn = default;
            switch (source)
            {
                case 1:
                case -1:
                case -2:
                    return "";
                case -3:
                    return "你太累了。";
                case 0:
                    rtn = selectedSkill.FailedMessage ();
                    break;
                case 9:
                    rtn = selectedSkill.FailedMessage () + "\r\n你從錯誤中學習進步。";
                    break;
                default:
                    rtn = $"Result:{source}未定義。";
                    break;
            }
            return rtn;
        }

    }
}
