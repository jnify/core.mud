using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Rest.MercuryCore.Utilities
{
    public static class ObjectExtension
    {
        private static readonly JsonSerializerOptions jsoptions;
        private readonly static Random rnd = new Random ();

        static ObjectExtension ()
        {
            jsoptions = new JsonSerializerOptions ();
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            //jsoptions.WriteIndented = true;
        }

        public static T DeepClone<T> (T ob)
        {
            var json = JsonSerializer.Serialize (ob, jsoptions);
            var rtn = JsonSerializer.Deserialize<T> (json, jsoptions);
            return rtn;
        }

        public static TargetObject DeepClone<FromObject, TargetObject> (FromObject from)
        {
            var json = JsonSerializer.Serialize (from, jsoptions);
            var rtn = JsonSerializer.Deserialize<TargetObject> (json, jsoptions);
            return rtn;
        }
        public static List<T> Random<T> (this List<T> source)
        {
            int index = 0;
            var item = default (T);
            var tmpSourceArray = new T[source.Count ()];
            source.CopyTo (tmpSourceArray);
            List<T> SourceOfCopy = tmpSourceArray.ToList ();
            List<T> rtn = new List<T> ();
            while (SourceOfCopy.Count > 0)
            {
                index = rnd.Next (SourceOfCopy.Count);
                item = SourceOfCopy[index];
                rtn.Add (item);
                SourceOfCopy.RemoveAt (index);
            }
            return rtn;
        }
        public static T[] Random<T> (T[] source)
        {
            var tmp = source.ToList ();
            return Random<T> (tmp).ToArray ();
        }

        public static T[] Push<T> (this T[] source, T value)
        {
            var index = source.Length;
            Array.Resize (ref source, source.Length + 1);
            source[index] = value;
            return source;
        }
    }
}
