using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;

namespace Rest.MercuryCore.Quest
{
    public class QuestCollection
    {
        private readonly object grudgesLock = new object ();
        private readonly IFirebase<QuestInfo> db;
        public QuestCollection (IFirebase<QuestInfo> db)
        {
            this.db = db;
            Quests = new List<QuestInfo> ();
            Load ();
        }

        public List<QuestInfo> Quests { get; set; }

        public void Load ()
        {
            var tmp = db.GetALL ("QuestInfo").GetAwaiter ().GetResult ();
            if (tmp != null)
            {
                foreach (var item in tmp)
                {
                    item.Value.ID = item.Key;
                    Quests.Add (item.Value);
                }
            }
        }
        public void InsertOrUpdate (string QuestName, string key, string value)
        {
            CreateQuests (QuestName, key);
            var q = Quests.Where (x => x.ID == QuestName).FirstOrDefault ();
            q.Data[key] = value;
            db.InsertOrUpdate (q, false);
        }
        public string Get (string QuestName, string key)
        {
            CreateQuests (QuestName, key);
            return Quests.Where (x => x.ID == QuestName).FirstOrDefault ().Data[key];
        }
        public void Remove (string QuestName, string key)
        {
            if (!Quests.Any (x => x.ID == QuestName))
            {
                var quest = Quests.Where (x => x.ID == QuestName).FirstOrDefault ();
                if (quest.Data.ContainsKey (key))
                {
                    lock (grudgesLock)
                    {
                        if (quest.Data.ContainsKey (key)) quest.Data.Remove (key);
                        db.InsertOrUpdate (quest);
                    }
                }
            }
        }
        private void CreateQuests (string head, string key)
        {
            if (string.IsNullOrWhiteSpace (head)) return;
            if (!Quests.Any (x => x.ID == head))
            {
                lock (grudgesLock)
                {
                    if (!Quests.Any (x => x.ID == head))
                    {
                        Quests.Add (new QuestInfo () { ID = head });
                    }
                }
            }
            var quest = Quests.Where (x => x.ID == head).FirstOrDefault ();
            if (!string.IsNullOrWhiteSpace (key))
            {
                //先建立quest.Data
                if (quest.Data == null)
                {
                    lock (grudgesLock)
                    {
                        if (quest.Data == null)
                        {
                            quest.Data = new Dictionary<string, string> ();
                            quest.Data.Add (key, "");
                        }
                    }
                }
                //處理Key
                if (!quest.Data.ContainsKey (key))
                {
                    lock (grudgesLock)
                    {
                        if (!quest.Data.ContainsKey (key))
                        {
                            quest.Data.Add (key, "");
                        }
                    }
                }
            }
        }
    }
}
