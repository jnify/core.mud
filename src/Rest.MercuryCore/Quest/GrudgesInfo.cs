using System;
using System.Collections.Generic;
using System.Linq;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;

namespace Rest.MercuryCore.Quest
{
    public class GrudgesInfo : IFirebaseObject
    {
        private readonly object grudgesLock = new object ();
        private readonly IFirebase<GrudgesInfo> grud;
        public GrudgesInfo (IFirebase<GrudgesInfo> grud)
        {
            this.grud = grud;
            Load ();
        }

        public string ID
        {
            get => "GrudgesInfo";
            set { }
        }

        public Dictionary<string, List<string>> Grudges = new Dictionary<string, List<string>> ();

        public void Load ()
        {
            var tmp = grud.Get (ID).GetAwaiter ().GetResult ();
            if (tmp != null) Grudges = tmp.Grudges;
        }

        public (bool IsGrudges, string GrudgesName) CheckGrudges (MobInfo mob, string UserName)
        {
            List<string> list = new List<string> () { UserName };
            return CheckGrudges (mob, list);
        }

        public (bool IsGrudges, string GrudgesName) CheckGrudges (MobInfo mob, List<string> UserNames)
        {
            (bool IsGrudges, string GrudgesName) rtn = default;
            string key = mob.BornInRegion;
            //先確認Regional的仇
            if ((Grudges.ContainsKey (key) && Grudges[key].Any (x => UserNames.Contains (x))))
            {
                rtn.IsGrudges = true;
                rtn.GrudgesName = Grudges[key].Where (x => UserNames.Contains (x)).FirstOrDefault ();
            }

            //再確認本身的仇
            if (mob.MobSettings.Grudges == GrudgesAndResolve.AttackTheMobOnly && mob.BeAttackWith.Keys.Any (x => UserNames.Contains (x)))
            {
                rtn.IsGrudges = true;
                rtn.GrudgesName = mob.BeAttackWith.Keys.Where (x => UserNames.Contains (x)).FirstOrDefault ();
            }

            key = GetKey (mob); //剩同名的仇
            if (key != null && Grudges.ContainsKey (key) && Grudges[key].Any (x => UserNames.Contains (x)))
            {
                rtn.IsGrudges = true;
                rtn.GrudgesName = Grudges[key].Where (x => UserNames.Contains (x)).FirstOrDefault ();
            }
            return rtn;
        }

        public void Add (MobInfo mob, string UserName)
        {
            string key = GetKey (mob);
            if (!string.IsNullOrWhiteSpace (key))
            {
                CreateGrudgesList (key);
                var list = Grudges[key];
                if (!list.Any (x => x == UserName))
                {
                    Grudges[key].Add (UserName);
                    grud.InsertOrUpdate (this);
                }
            }
        }
        public void Remove (MobInfo mob, string UserName)
        {
            string key = GetKey (mob);
            if (Grudges.ContainsKey (key) && Grudges[key].Any (x => x == UserName))
            {
                lock (grudgesLock)
                {
                    if (Grudges.ContainsKey (key) && Grudges[key].Any (x => x == UserName))
                    {
                        Grudges[key].RemoveAll (x => x == UserName);
                        if (Grudges[key].Count () == 0) Grudges.Remove (key);
                        grud.InsertOrUpdate (this);
                    }
                }
            }
        }
        private void CreateGrudgesList (string key)
        {
            if (!Grudges.ContainsKey (key))
            {
                lock (grudgesLock)
                {
                    if (!Grudges.ContainsKey (key))
                    {
                        Grudges.Add (key, new List<string> ());
                    }
                }
            }
        }
        private static string GetKey (MobInfo mob)
        {
            string rtn = default;
            switch (mob.MobSettings.Grudges)
            {
                case GrudgesAndResolve.MobDiedToPPLDied: //Mob死亡後會記仇，記在同名身上(DB)，PPL被該Mob殺死後解除
                    rtn = mob.Name;
                    break;
                case GrudgesAndResolve.Reginal_MobDiedToPPLDied: //Mob死亡後會被整個Region記仇，PPL死亡後解除
                    rtn = mob.BornInRegion;
                    break;
                case GrudgesAndResolve.Reginal: //不記仇
                case GrudgesAndResolve.AttackTheMobOnly: //記在mob本身上
                    break;
                default:
                    Console.WriteLine ($"{mob.MobSettings.Grudges}沒寫到");
                    break;
            }
            return rtn;
        }
    }
}
