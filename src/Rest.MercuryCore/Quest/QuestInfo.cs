using System.Collections.Generic;
using System.Text.Json.Serialization;
using Rest.MercuryCore.Interface;

namespace Rest.MercuryCore.Quest
{
    public class QuestInfo : IFirebaseObject
    {
        [JsonIgnore]
        public string ID { get; set; }
        public Dictionary<string, string> Data { get; set; }
    }
}
