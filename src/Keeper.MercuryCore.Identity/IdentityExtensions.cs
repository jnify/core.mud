﻿using Firebase.OPT;
using Keeper.MercuryCore;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Pipeline;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Quest;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IdentityServiceCollectionExtensions
    {
        public static IServiceCollection AddInMemoryIdentity (this IServiceCollection services)
        {
            services.AddSingleton<IUserManager, InMemoryUserManager> ();
            return services;
        }

        public static IServiceCollection AddFireBase (this IServiceCollection services)
        {
            services.AddSingleton<IFirebase<UserInfo>, Firebase_api<UserInfo>> ();
            services.AddSingleton<IFirebase<MobInfo>, Firebase_api<MobInfo>> ();
            services.AddSingleton<IFirebase<GrudgesInfo>, Firebase_api<GrudgesInfo>> ();
            services.AddSingleton<IFirebase<QuestInfo>, Firebase_api<QuestInfo>> ();
            return services;
        }

        public static IServiceCollection<IPipeline> UseLogin (this IServiceCollection<IPipeline> services)
        {
            services.AddSingleton<LoginManager> ();
            services.Use<IdentityMiddleware<LoginManager>> ();

            return services;
        }
    }
}
