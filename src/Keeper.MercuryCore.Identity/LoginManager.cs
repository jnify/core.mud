﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions;
using Rest.MercuryCore.EventActions.Interface;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.Identity
{
    public class LoginManager : IIdentityManager
    {
        private readonly ILogger<LoginManager> logger;
        private readonly IUserManager userManager;
        private readonly IFirebase<UserInfo> userinfo;
        private readonly IActionLoader actionLoader;
        private const string LoginNamePattern = "^[\\w]{3,20}$";
        private readonly Random rnd = new Random ();
        public LoginManager (ILogger<LoginManager> logger, IUserManager userManager, IFirebase<UserInfo> userinfo, IActionLoader actionLoader)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.userinfo = userinfo;
            this.actionLoader = actionLoader;
        }

        public async Task<AuthenticateResult> AuthenticateAsync (IServiceProvider channelProvider)
        {
            var actionEffective = channelProvider.GetRequiredService<IActionUserEffect> ();
            var channel = channelProvider.GetRequiredService<ITextChannel> ();
            var wordmgr = channelProvider.GetRequiredService<IWorldManager> ();
            var usermgr = channelProvider.GetRequiredService<IUserManager> ();
            string userliststring = string.Join (", ", usermgr.GetAllUser ().Select (x => x.Name));
            string userLoginliststring = string.Join (", ", usermgr.GetAllUser (true).Select (x => x.Name));
            UserInfo user = new UserInfo (actionEffective);

            var welcome = TextUtil.GetFromFile ("DocumentHelp", "Welcome.hlp")
                .Replace ("{StartTime}", wordmgr.WorldInfo.StartTime.ToString ("yyyy-MM-dd HH:mm:ss"))
                .Replace ("{PPLLIST}", userliststring)
                .Replace ("{PPLINLOGINLIST}", userLoginliststring)
                .Replace ("{COUNT}", usermgr.GetAllUser ().Count ().ToString ());

            await channel.SendLineAsync (welcome);

            bool isUsernameValid = false;

            Regex rgx = new Regex (LoginNamePattern, RegexOptions.Singleline);
            try
            {
                TimeSpan tsTimeout = TimeSpan.FromMinutes (1);
                while (!isUsernameValid)
                {
                    await channel.SendLineAsync ("請輸入你的名字：");
                    this.logger.LogDebug ("Receiving username");

                    user.Name = await channel.ReceiveLineAsync (tsTimeout);
                    user.Name = user.Name.Trim ();
                    if (string.IsNullOrWhiteSpace (user.Name)) return AuthenticateResult.Cancelled;
                    this.logger.LogDebug ("Username received: {SubmittedUsername}", user.Name);

                    isUsernameValid = rgx.IsMatch (user.Name);

                    if (!isUsernameValid)
                    {
                        await channel.SendLineAsync ($"請輸入\u001b[36m3~20\u001b[0m以內的字元成為你的名字。");
                    }
                    else
                    {
                        this.logger.LogDebug ($"Username {user.Name} accepted");

                        if (this.userManager.CheckUserInSync (user.Name))
                        {
                            this.logger.LogDebug ("User found");

                            await channel.SendLineAsync ("請輸入密碼：");
                            string password = await channel.ReceiveLineAsync (tsTimeout);
                            password = TextUtil.EncodePassword (user.Name, password);

                            if (this.userManager.CheckUserInSync (user.Name, password))
                            {
                                user = userManager.GetUser (user.Name);
                                user.AuditLog.LastLoginedDateTime = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                                user.AuditLog.LastLoginedIP = channel.IP;
                                user.SetEffectionAction (actionEffective);

                                BindingActionEvent (user.EQArea);
                                BindingActionEvent (user.Things);

                                this.logger.LogInformation ("{Username} logged in successfully", user.Name);
                                await channel.SetUser (user);
                                return AuthenticateResult.Success (user.Name);
                            }
                            else
                            {
                                await channel.SendLineAsync ("密碼錯誤。");
                                this.logger.LogWarning ("{Username} login failed", user.Name);
                                isUsernameValid = false;
                            }
                        }
                        else
                        {
                            bool goToNext = false;

                            this.logger.LogDebug ("No existing user found");
                            await channel.SendLineAsync ($"這個名字聽起來很陌生，要使用[[1;97m{user.Name}[0m]做為你的名字嗎[36m[Y/N][0m？");

                            //Confirm User Name
                            while (!goToNext)
                            {
                                string answer = await channel.ReceiveLineAsync (tsTimeout);
                                switch (answer.ToUpperInvariant ())
                                {
                                    case "Y":
                                        goToNext = true;
                                        break;
                                    case "N":
                                        goToNext = true;
                                        isUsernameValid = false;
                                        break;
                                    default:
                                        await channel.SendLineAsync ($"你要使用[[1;97m{user.Name}[0m]做為你的名字嗎[36m[Y/N][0m？");
                                        break;
                                }
                            }
                            //---------------------------------------------------------------------------------------------------
                            if (!isUsernameValid) continue;

                            //---------------------------------------------------------------------------------------------------
                            await DecidePassword (user, channel, tsTimeout);
                            await DecideSettingName (user, channel, tsTimeout);
                            await DecideRace (user, channel, tsTimeout);
                            await DecideGender (user, channel, tsTimeout);
                            await DecideCareer (user, channel, tsTimeout);
                            await DecideAttribute (user, channel, tsTimeout);
                            //---------------------------------------------------------------------------------------------------
                            goToNext = false;

                            if (isUsernameValid)
                            {
                                //Create a new user.
                                string dt = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                                //setup default attribute.
                                user.AuditLog = new Audit ()
                                {
                                    CreateDateTime = dt,
                                    LastLoginedDateTime = dt
                                };

                                //HP =  原HP + CON*0.6~CON*0.7
                                user.StateMax = new BasicAndUpgradeSetting ()
                                {
                                    HP = user.Attribute.Constitution * 10 + 300,
                                    MV = user.Attribute.Dexterity * 10 + 300,
                                    Mana = user.Attribute.Intelligent * 10 + 300
                                };
                                user.BasicAttack = 10;
                                user.BasicDefence = 1;
                                user.StateCurrent = user.StateMax.Copy;

                                user.Points.Reviviscence = rnd.Next (18, 25);
                                user.Points.Practice = rnd.Next (user.Attribute.Wisdom, user.Attribute.Wisdom + 30);

                                await this.userManager.CreateUserAsync (user);
                                await userinfo.InsertOrUpdate (user);
                                await channel.SetUser (user);
                                this.logger.LogInformation ("New user {Username} registered", user.Name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.userManager.RemoveUser (user.Name);
                logger.LogInformation ("User Idel more than 30 second. " + ex.Message);
                await channel.SendLineAsync ("你考慮的時間太久囉～～先想好再來一次吧～～");
                isUsernameValid = false;
                return AuthenticateResult.Cancelled;
            }
            return AuthenticateResult.Success (user.Name);
        }
        private void BindingActionEvent (Dictionary<string, CommonObject> eQArea)
        {
            foreach (var item in eQArea)
            {
                if (!string.IsNullOrWhiteSpace (item.Value.ActionKey))
                {
                    var action = actionLoader.GetCommonObjectAction (item.Value.ActionKey);
                    if (action != null) item.Value.SetActionCommand (action);
                }
            }
        }
        private void BindingActionEvent (List<CommonObject> things)
        {
            things.ForEach (x =>
            {
                if (!string.IsNullOrWhiteSpace (x.ActionKey))
                {
                    var action = actionLoader.GetCommonObjectAction (x.ActionKey);
                    if (action != null) x.SetActionCommand (action);
                }
            });
        }

        private async Task DecideAttribute (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            Dictionary<string, int[]> MaxAttributesByCareer = new Dictionary<string, int[]> ();
            //Str, Int, Wis, Dex, Con
            int[] MinAttribute = new int[] { 8, 8, 8, 8, 8 };
            MaxAttributesByCareer.Add (CreatureCareer.Magic.ToString (), new int[] { 14, 20, 20, 18, 14 });
            MaxAttributesByCareer.Add (CreatureCareer.Musician.ToString (), new int[] { 16, 18, 20, 18, 16 });
            MaxAttributesByCareer.Add (CreatureCareer.Thief.ToString (), new int[] { 18, 16, 18, 20, 18 });
            MaxAttributesByCareer.Add (CreatureCareer.Fighter.ToString (), new int[] { 20, 14, 14, 14, 20 });
            if (!MaxAttributesByCareer.ContainsKey (user.Career.ToString ())) await Task.FromCanceled (new CancellationToken (true));

            var maxAttr = MaxAttributesByCareer[user.Career.ToString ()];
            List<int> attrNum = new List<int> ();
            for (int i = 0; i < 5; i++)
            {
                var loops = maxAttr[i] - MinAttribute[i];
                while (loops > 0)
                {
                    attrNum.Add (i);
                    loops--;
                }
            }

            bool goToNext = false;
            Random rnd = new Random ();
            while (!goToNext)
            {
                int extraPoint = rnd.Next (25, 31);
                var rndAttrs = attrNum.Random ().GetRange (0, extraPoint);
                int[] basic = (int[]) MinAttribute.Clone ();
                rndAttrs.ForEach (x =>
                {
                    basic[x]++;
                });

                user.Attribute = new Attr ()
                {
                    Strength = basic[0],
                    Intelligent = basic[1],
                    Wisdom = basic[2],
                    Dexterity = basic[3],
                    Constitution = basic[4],
                };
                string msgAttr = $"您的屬性：\n體質：{user.Attribute.Constitution}　　";
                msgAttr += $"敏捷：{user.Attribute.Dexterity}　　";
                msgAttr += $"智力：{user.Attribute.Intelligent}　　";
                msgAttr += $"力量{user.Attribute.Strength}　　";
                msgAttr += $"知識：{user.Attribute.Wisdom}\r\n";
                msgAttr += $"這樣子的屬性你滿意嗎？[36m[Y/N][0m？";
                await channel.SendLineAsync (msgAttr);

                string answer;
                bool subGoToNext = false;
                while (!subGoToNext)
                {
                    answer = await channel.ReceiveLineAsync (tsTimeout);
                    switch (answer.ToUpperInvariant ())
                    {
                        case "Y":
                            goToNext = true;
                            subGoToNext = true;
                            break;
                        default:
                            subGoToNext = true;
                            break;
                    }
                }
            }
        }
        //DecideSettingName
        private async Task DecideSettingName (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            string msg = "請取一個你在這個世界的化名，禁止使用不雅的化名，我們有權可以不經警告直接刪除使用不雅文字的角色。\r\n";
            msg += "請問你的化名是？";
            await channel.SendLineAsync (msg);
            user.Settings.Name = await channel.ReceiveLineAsync (tsTimeout);
            while (string.IsNullOrEmpty (user.Settings.Name))
            {
                await DecideSettingName (user, channel, tsTimeout);
            }
        }
        private async Task DecidePassword (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            bool goToNext = false;
            while (!goToNext)
            {
                await channel.SendLineAsync ($"請輸入你的密碼：");
                user.Password = await channel.ReceiveLineAsync (tsTimeout);
                await channel.SendLineAsync ($"請輸入你的確認密碼：");
                string pwdConf = await channel.ReceiveLineAsync (tsTimeout);
                if (!user.Password.Equals (pwdConf))
                {
                    await channel.SendLineAsync ("您輸入的確認密碼不符，請重新輸入。\r\n");
                }
                else
                {
                    user.Password = TextUtil.EncodePassword (user.Name, user.Password);
                    goToNext = true;
                }
            }
        }

        private async Task DecideGender (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            bool goToNext = false;
            while (!goToNext)
            {
                string msg = "請選擇你的性別：\r\n";
                msg += "\t[1mM[0male\t男性\r\n";
                msg += "\t[1mF[0memale\t女性\r\n";
                msg += "\t[1mN[0meutral\t無性別";
                await channel.SendLineAsync (msg);
                string strGender = await channel.ReceiveLineAsync (tsTimeout);
                strGender = MatchGender (strGender);
                if (Enum.TryParse<CreatureGender> (strGender, true, out var gender))
                {
                    user.Gender = gender;
                    goToNext = true;
                }
                else
                {
                    await channel.SendLineAsync ("沒有這個選項。");
                }
            }
        }
        private string MatchGender (string strGender) => strGender.ToUpperInvariant ()
        switch
        {
            "M" => "Male",
            "F" => "Female",
            "N" => "Neutral",
            _ => strGender
        };

        private async Task DecideCareer (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            bool goToNext = false;
            while (!goToNext)
            {
                string msg = "請選擇你的職業：\r\n";
                msg += $"\t[1mM[0magic\t{CreatureCareer.Magic.GetDescriptionText()}\r\n";
                msg += $"\t[1mB[0mard\t{CreatureCareer.Musician.GetDescriptionText()}\r\n";
                msg += $"\t[1mT[0mhief\t{CreatureCareer.Thief.GetDescriptionText()}\r\n";
                msg += $"\t[1mF[0mighter\t{CreatureCareer.Fighter.GetDescriptionText()}";
                await channel.SendLineAsync (msg);
                string strCareer = await channel.ReceiveLineAsync (tsTimeout);
                strCareer = MatchCareer (strCareer);
                if (Enum.TryParse<CreatureCareer> (strCareer, true, out var career))
                {
                    user.Career = career;
                    goToNext = true;
                }
                else
                {
                    await channel.SendLineAsync ("沒有這個職業。");
                }
            }
        }
        private string MatchCareer (string strCareer) => strCareer.ToUpperInvariant ()
        switch
        {
            "M" => "Magic",
            "B" => "Musician",
            "T" => "Thief",
            "F" => "Fighter",
            _ => strCareer
        };

        private async Task DecideRace (UserInfo user, ITextChannel channel, TimeSpan tsTimeout)
        {
            bool goToNext = false;
            while (!goToNext)
            {
                string msg = "請選擇你的種族：\r\n";
                msg += "\t[1mH[0muman\t人類";
                await channel.SendLineAsync (msg);
                string strRace = await channel.ReceiveLineAsync (tsTimeout);
                strRace = MatchRace (strRace);
                if (Enum.TryParse<CreatureRace> (strRace, true, out var race))
                {
                    user.Race = race;
                    goToNext = true;
                }
                else
                {
                    await channel.SendLineAsync ("沒有這個選項。\r\n");
                }
            }
        }
        private string MatchRace (string strRace) => strRace.ToUpperInvariant ()
        switch
        {
            "H" => "Human",
            _ => strRace
        };

    }
}
