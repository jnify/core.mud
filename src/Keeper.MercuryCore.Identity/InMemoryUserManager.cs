﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.OPT;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.Identity
{
    public class InMemoryUserManager : IUserManager
    {
        private readonly object lockUserGroup = new object ();

        private Dictionary<string, UserInfo> users = new Dictionary<string, UserInfo> ();
        private Dictionary<string, GroupObject> UserGroups = new Dictionary<string, GroupObject> ();

        private readonly object userLock = new object ();

        private readonly IFirebase<UserInfo> userinfo;

        public InMemoryUserManager (IFirebase<UserInfo> userinfo, IWorldManager wordMng)
        {
            wordMng.GetGroupUsers = GetGroupUsers;
            this.userinfo = userinfo;
        }
        private List<UserInfo> GetGroupUsers (string UserName)
        {
            var group = GetGroup (UserName);
            if (group == default) return new List<UserInfo> () { };
            return users.Where (userDic => group.UserGroup.Contains (userDic.Key)).Select (x => x.Value).ToList ();
        }

        public bool CheckUserInSync (string username, string password = null)
        {
            username = NormaliseUserName (username);
            var info = userinfo.Get (username).Result;

            if (info == null)
                return false;
            else
                lock (userLock)
                {
                    //先把房間號碼改為負值，避免一上線就被掃到
                    if (info.Geography.RoomID > 0)
                        info.Geography.RoomID = info.Geography.RoomID * -1;
                    info.Name = username;
                    info = RemoveAttribute (info);
                    if (password == info.Password || password == null)
                    {
                        if (!this.users.ContainsKey (info.Name)) this.users.Add (info.Name, info);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
        }

        public string NormaliseUserName (string username)
        {
            if (string.IsNullOrWhiteSpace (username)) return "";
            return $"{username.First().ToString().ToUpper()}{username.Substring(1).ToLowerInvariant()}";
        }
        private UserInfo RemoveAttribute (UserInfo info)
        {
            info.Status &= ~CreatureStatus.Fighting;
            return info;
        }
        public Task<bool> CreateUserAsync (UserInfo user)
        {
            return Task.Run (() =>
            {
                lock (this.userLock)
                {
                    string userName = NormaliseUserName (user.ID);
                    user.Name = userName;
                    user.ID = userName;

                    if (this.users.ContainsKey (userName))
                    {
                        return false;
                    }
                    else
                    {
                        this.users.Add (userName, user);

                        return true;
                    }
                }
            });
        }
        private static string Normalise (string username) => username.ToUpperInvariant ();
        public UserInfo GetUser (string username)
        {
            username = NormaliseUserName (username);
            if (users.ContainsKey (username))
            {
                return users[username];
            }
            else
            {
                return default;
            }
        }
        public List<UserInfo> GetRoomUser (int RoomID)
        {
            return users.Where (x => x.Value.Geography.RoomID == RoomID).Select (x => x.Value).ToList ();
        }

        public List<UserInfo> GetRoomUser (UserInfo user)
        {
            return GetRoomUser (user.Geography.RoomID);
        }

        public List<UserInfo> GetAllUser (bool InUserLogin = false)
        {
            //RoomID為負數者：處於登入狀態。
            if (InUserLogin)
                return users.Where (x => x.Value.Geography.RoomID < 0).Select (x => x.Value).ToList ();
            else
                return users.Where (x => x.Value.Geography.RoomID > 0).Select (x => x.Value).ToList ();
        }

        public void RemoveUser (string mvUserName)
        {
            mvUserName = NormaliseUserName (mvUserName);
            if (users.ContainsKey (mvUserName))
            {
                users[mvUserName] = null;
                users.Remove (mvUserName);
            }
        }

        public bool CreateGroup (string GroupName, string UserName)
        {
            foreach (var item in UserGroups)
            {
                //已有組隊的，不能再Creaet group.
                if (item.Value.UserGroup.Contains (UserName)) return false;
            }

            //隊名不能相同
            if (UserGroups.ContainsKey (GroupName)) return false;

            lock (lockUserGroup)
            {
                if (UserGroups.ContainsKey (GroupName)) return false;
                var GroupObj = new GroupObject (UserName);
                UserGroups.Add (GroupName, GroupObj);
            }
            return true;
        }

        public bool JoinGroup (string GroupName, string UserName)
        {
            //沒建立隊伍，無法加入。
            if (!UserGroups.ContainsKey (GroupName)) return false;
            foreach (var item in UserGroups)
            {
                //已在隊伍中
                if (item.Value.UserGroup.Contains (UserName)) return false;
            }
            return UserGroups[GroupName].AddMember (UserName);
        }

        public bool LeaveGroup (string UserName)
        {
            foreach (var item in UserGroups)
            {
                if (item.Value.UserGroup.Contains (UserName))
                {
                    if (item.Value.Leader == UserName)
                    {
                        //隊長離開，整個解散
                        UserGroups.Remove (item.Key);
                        return true;
                    }
                    else
                    {
                        return item.Value.RemoveMember (UserName);
                    }
                }
            }
            return false;
        }
        public GroupObject GetGroup (string UserName)
        {
            foreach (var item in UserGroups)
            {
                if (item.Value.UserGroup.Contains (UserName))
                {
                    return item.Value;
                }
            }
            return default;
        }

    }
}
