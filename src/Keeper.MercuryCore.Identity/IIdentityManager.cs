﻿using System;
using System.Threading.Tasks;
using Keeper.MercuryCore.Session;

namespace Keeper.MercuryCore.Identity
{
    public interface IIdentityManager
    {
        Task<AuthenticateResult> AuthenticateAsync (IServiceProvider sessionProvider);
    }
}