﻿namespace Keeper.MercuryCore.Identity
{
    public struct IdentityInfo
    {
        public IdentityInfo (string username, bool isNew, int level = 0) : this ()
        {
            this.Username = username;
            this.IsNew = isNew;
            this.Level = level;
        }

        public string Username
        {
            get;
            private set;
        }

        public bool IsNew
        {
            get;
            private set;
        }

        public int Level { get; set; }
    }
}