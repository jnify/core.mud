﻿using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Pipeline;

namespace Keeper.MercuryCore.CommandLoop
{
    public interface ICommandHandler
    {
        string Name { get; }
        string AbbrName { get; }
        DenyRestSleep DenyCommand { get; }

        Task Handle (ICommandLoop loop, CommandInfo info);
    }
}