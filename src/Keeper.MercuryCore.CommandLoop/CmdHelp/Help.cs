using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class HelpCommandHandler : ICommandHandler
    {
        public string Name => "HELP";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.None;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();

            if (info.ParameterValues.Count () == 0)
            {
                var msg = TextUtil.GetFromFile ("DocumentHelp", "Help.hlp");
                return channel.SendLineAsync (msg);
            }
            else
            {
                var files = Directory.GetFiles ("DocumentHelp", "*.hlp");
                foreach (string fileName in files)
                {
                    var fname = Path.GetFileNameWithoutExtension (fileName);
                    if (string.Compare (fname, info.ParameterString, true) == 0)
                    {
                        var msg = TextUtil.GetFromFile (fileName);
                        return channel.SendLineAsync (msg);
                    }
                }
            }
            return channel.SendLineAsync ($"找不到{info.ParameterString}的說明檔。");
        }
    }
}
