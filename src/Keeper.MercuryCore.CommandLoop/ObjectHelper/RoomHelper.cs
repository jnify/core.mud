using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.ObjectHelper
{
    public class RoomHelper
    {
        public async Task BuildDoorAsync (ITextChannel channel, ICommandLoop loop, List<string> paras)
        {
            ExitName exit = ExitName.Up;
            int roomID = 1;
            if (paras.Count () > 0)
            {
                if (!Enum.TryParse<ExitName> (paras[0], true, out exit))
                {
                    await channel.SendLineAsync ("錯誤：你要在哪個方向開門？");
                    return;
                }
                if (paras.Count () > 1) { roomID = int.Parse (paras[1]); };
                //取得所在房間
                var user = channel.ChannelUser;
                var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
                if (room.Exits.Where (x => x.Dimension == exit).Count () > 0)
                {
                    await channel.SendLineAsync ($"錯誤：{exit.GetDescriptionText()}已經有門囉。");
                    return;
                }
                else
                {
                    var newexit = room.Exits.ToList ();
                    newexit.Add (new ExitSetting ()
                    {
                        RoomID = roomID,
                            gateStatus = GateStatus.Open,
                            Dimension = exit
                    });
                    room.Exits = newexit.ToArray ();
                    await channel.SendLineAsync ($"{exit.GetDescriptionText()}的門建好囉。");
                }
            }
            else
            {
                await channel.SendLineAsync ("錯誤：你要在哪個方向開門？");
            }
        }

        public async Task RemoveDoorAsync (ITextChannel channel, ICommandLoop loop, List<string> paras)
        {
            ExitName exit = ExitName.Up;
            if (paras.Count () > 0)
            {
                if (!Enum.TryParse<ExitName> (paras[0], true, out exit))
                {
                    await channel.SendLineAsync ("錯誤：你想移走哪個方向的門？");
                    return;
                }
                //取得所在房間
                var user = channel.ChannelUser;
                var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
                var listExit = room.Exits.ToList ();
                listExit.Remove (listExit.Where (x => x.Dimension == exit).FirstOrDefault ());
                room.Exits = listExit.ToArray ();
                await channel.SendLineAsync ($"{exit.GetDescriptionText()}的門移走囉。");
            }
            else
            {
                await channel.SendLineAsync ("錯誤：你想移走哪個方向的門？");
            }
        }

    }
}
