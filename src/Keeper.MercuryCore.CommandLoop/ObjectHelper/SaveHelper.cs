using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Firebase.OPT;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.ObjectHelper
{
    public class SaveHelper
    {
        private const string path = "DocumentRoom";
        public async Task Save (ITextChannel channel, ICommandLoop loop, string target, List<string> paras = null)
        {
            switch (target)
            {
                case "ROOM":
                    int RoomID = 0;
                    if (int.TryParse (paras.FirstOrDefault (), out RoomID) && RoomID > 0)
                    {
                        await SaveRoom (channel, loop, RoomID);
                    }
                    else
                    {
                        await channel.SendLineAsync ($"房間不存在。");
                    }
                    break;
                case "MOB":
                    var user = channel.ChannelUser;
                    var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
                    IFirebase<MobInfo> mobmgr = loop.Provider.GetService<IFirebase<MobInfo>> ();
                    room.Mobs.ForEach (x =>
                    {
                        mobmgr.InsertOrUpdate (x);
                    });
                    break;
            }

        }

        private async Task SaveRoom (ITextChannel channel, ICommandLoop loop, int RoomID)
        {
            var jsoptions = new JsonSerializerOptions ();
            jsoptions.Converters.Add (new JsonStringEnumConverter (JsonNamingPolicy.CamelCase));
            jsoptions.WriteIndented = true;

            Room room = loop.Provider.GetService<IWorldManager> ().GetRoom (RoomID);
            var t = JsonSerializer.Serialize (room, jsoptions);

            await channel.SendLineAsync ($"還沒做完。");
        }
    }
}
