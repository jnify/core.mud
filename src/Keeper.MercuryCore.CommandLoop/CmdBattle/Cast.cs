using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class CastCommandHandler : ICommandHandler
    {
        protected readonly object LockSpell = new object ();
        private const string regSpells = @"^'(.*)'(.*)";
        private static SkillCategory[] SpellsSkillCategory = new SkillCategory[]
        {
            SkillCategory.Fire,
            SkillCategory.Water,
            SkillCategory.Ligth,
            SkillCategory.Dark,
            SkillCategory.Wind,
            SkillCategory.Lightning,
            SkillCategory.Earth,
            SkillCategory.Poison,
            SkillCategory.Saint,
            SkillCategory.Evil
        };
        private Dictionary < (string, string), SkillName > spellSkillNames = new Dictionary < (string, string), SkillName > ();

        public string Name => "Cast";
        public string AbbrName => "C";
        private readonly Random rnd = new Random ();
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            if (spellSkillNames.Count () == 0) lock (LockSpell) if (spellSkillNames.Count () == 0)
            {
                FillUpSpellNames ();
            }

            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var godOfWare = loop.Provider.GetService<IGodOfWar> ();

            if (user.EQArea.ContainsKey (EquipmentAllowPosition.HandLeft.ToString ()) && user.EQArea.ContainsKey (EquipmentAllowPosition.HandRight.ToString ()))
                return channel.SendLineAsync ("你至少得空下一隻手來。");

            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("施什麼法？目標？");

            var commands = ParsCommand (info.ParameterString);
            SkillName? skillName = GetSkillName (commands);
            if (!skillName.HasValue) return channel.SendLineAsync ("什麼？！");

            PracticedSkill skillattr = user.GetSkill (skillName.Value);
            if (skillattr == null) return channel.SendLineAsync ("你無法做到！");

            BaseSkill skill = _GetSkill (skillattr);
            if (skill == null) return channel.SendLineAsync ("這不是個法術。");

            //驗證施法對象
            IUserManager usrMng = loop.Provider.GetService<IUserManager> ();
            Creature targetCreature = default;
            CommonObject targetObj = default;
            SpellsAttribute spellattr = skillattr.SpellsAttr;
            switch (spellattr.TargetType)
            {
                case CastTargetType.CreatureAttack:
                    if (string.IsNullOrWhiteSpace (commands.prap)) return channel.SendLineAsync ("施給誰？");
                    targetCreature = room.GetMob (commands.prap);
                    if (targetCreature == default) return channel.SendLineAsync ("這裏沒這個人！");
                    break;
                case CastTargetType.CreatureEffect:
                    if (spellattr.effectType == EffectiveType.None) return channel.SendLineAsync ("這個法術沒任何效果！");
                    if (string.IsNullOrWhiteSpace (commands.prap)) targetCreature = user;
                    targetCreature = targetCreature ?? room.GetMob (commands.prap);
                    targetCreature = targetCreature ?? usrMng.GetUser (commands.prap);
                    break;
                case CastTargetType.CommonObject:
                    if (string.IsNullOrWhiteSpace (commands.prap)) return channel.SendLineAsync ("施給哪一個物品？");
                    targetObj = user.GetThing (commands.prap);
                    targetObj = targetObj ?? room.GetThing (commands.prap);
                    //todo:要檢查物品是否可以被附魔
                    break;
            }
            //IGodOfWar
            var godOfWar = loop.Provider.GetService<IGodOfWar> ();
            if (user.StartBattle (targetCreature))
            {
                channel.BroadcastToRoom (skill.spellsAttribute.CastMessage, user.Geography.RoomID);
                //channel.BroadcastToRoom()
                var AttResult = user.DoAttack (skill);
                if (AttResult.Success)
                {
                    AttResult.PublicMessage = $"[1;31m{user.UserDescriptionAndID}開始攻擊{2}{targetCreature.Settings.Name}！！！[0m\r\n{AttResult.PublicMessage}";
                }
                else
                {
                    user.EndBattle ();
                }
                return godOfWar.MessageCombat (user, targetCreature.Name, AttResult);
            }
            else
            {
                user.EndBattle ();
                return channel.SendLineAsync ($"你手腳太慢，攻擊的對象已經GG了。");
            }
        }

        private BaseSkill _GetSkill (PracticedSkill selectedSkillAttr)
        {
            var skillClassName = $"Skill{selectedSkillAttr.Skillname.ToString()}";
            var classes = Assembly.GetAssembly (typeof (BaseSkill)).GetTypes ().Where (t =>
                t.Namespace == "Rest.MercuryCore.SkillDefinition" && t.Name == skillClassName).FirstOrDefault ();
            return (BaseSkill) Activator.CreateInstance (classes, selectedSkillAttr);
        }

        private SkillName? GetSkillName ((string SpellNameFirst, string SpellNameSecond, string prap) commands)
        {
            var key = spellSkillNames.Keys.Where (x =>
                x.Item1.StartsWith (commands.SpellNameFirst, true, CultureInfo.InvariantCulture)).FirstOrDefault ();
            if (key != default && spellSkillNames.ContainsKey (key))
                return spellSkillNames[key];
            else
                return null;
        }

        private (string SpellNameFirst, string SpellNameSecond, string prap) ParsCommand (string line)
        {
            (string SpellNameFirst, string SpellNameSecond, string prap) spellCommands = ("", "", "");
            var grp = Regex.Match (line, regSpells).Groups;
            if (grp.Count > 1)
            {
                var spResult = grp[1].ToString ().SpliteSpace ();;
                spellCommands = (spResult.Key, spResult.Value, grp[2].ToString ().Trim ());
            }
            else
            {
                var cmds = line.SpliteSpace ();
                spellCommands = (cmds.Key, "", cmds.Value);
            }
            return spellCommands;
        }
        private void FillUpSpellNames () => Enum.GetValues<SkillName> ().ToList ().Where (skillName => SpellsSkillCategory.Contains (skillName.GetAttribute<SkillAttribute> ().skillCategory)).ToList ().ForEach (skillName =>
        {
            var spellName = skillName.ToString ().SplitByUpperCase ().SpliteSpace ();
            spellSkillNames.Add (spellName, skillName);
        });
    }
}
