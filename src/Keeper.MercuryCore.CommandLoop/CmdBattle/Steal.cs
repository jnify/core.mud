using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class StealCommandHandler : ICommandHandler
    {
        public string Name => "STEAL";
        public string AbbrName => Name;
        private readonly Random rnd = new Random ();
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var godOfWare = loop.Provider.GetService<IGodOfWar> ();

            PracticedSkill skillattr = user.GetSkill (SkillName.Steal);
            if (skillattr == null) return channel.SendLineAsync ($"什麼？！");

            if (info.ParameterValues.Count () < 3)
                return channel.SendLineAsync ("你想從誰的身上偷東西呢？");
            var command = info.ParameterString.SpliteFrom ();
            if (string.IsNullOrWhiteSpace (command.Value)) return channel.SendLineAsync ("你想從誰的身上偷東西呢？(Steal Coin from SomeMob)");

            Creature defance = room.GetMob (command.Value);
            if (defance == null) return channel.SendLineAsync ("這裹沒這個人。");

            SkillSteal skill = new SkillSteal (skillattr);
            var skillResult = skill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);

            if (skillResult != 1)
                return channel.SendLineAsync (TextUtil.GetSkillExecuteFailedMessage (skill, skillResult, user.UserDescriptionAndID, defance.UserDescriptionAndID, null));

            //敏愈高，警覺性愈高
            var defanceDex = defance.GetCreatureAttribute ().Dexterity;
            if (rnd.Next (1, 100) < defanceDex)
                return channel.SendMessageToUser ($"{defance.UserDescriptionAndID}緊緊的盯著你的一舉一動。", user.Name);

            if (string.Compare (command.Key, "coin", true) == 0)
            {
                int coin = 0;
                if (defance.Coins > 0) coin = rnd.Next (defance.Coins / 10, defance.Coins / 3);
                user.Coins += coin;
                if (coin == 0)
                    channel.SendLineAsync ($"你的技術還不到家，什麼都沒偷到。");
                else
                    channel.SendLineAsync ($"得手了，總共{coin.ToCoins()}。");
            }
            else
            {
                var targetObj = defance.GetThing (command.Key);
                if (targetObj == null)
                    channel.SendLineAsync ($"你掏了老半天，才發現掏不到你要的東西。");
                else
                {
                    var isSuccess = defance.RemoveByID (targetObj.ID, out targetObj);
                    if (isSuccess)
                    {
                        user.Things.Add (targetObj);
                        channel.SendLineAsync ($"得手了，{targetObj.ShortName}順利入袋。");
                    }
                    else
                    {
                        channel.SendLineAsync ($"你的技術還不到家，什麼都沒偷到。");
                    }
                }
            }
            return MissBeDiscovered (channel, defance, user);
        }

        private Task MissBeDiscovered (ITextChannel channel, Creature defance, UserInfo user)
        {
            //todo:有轉移攻擊技能的，應該要能反擊才是
            //在戰鬥中被偷，不反擊
            if (defance.Status.HasFlag (CreatureStatus.Fighting)) return Task.CompletedTask;
            //Wis小於10，被偷不會反擊
            var defanceWis = defance.GetCreatureAttribute ().Wisdom;

            if (rnd.Next (6, 50) < defanceWis)
            {
                //todo:PPL的朂後一擊，不會顯示出來。
                channel.BroadcastToArea ($"{defance.Settings.Name} 大叫：{user.UserDescriptionAndID}這個沒血沒淚的小偷！！", defance);
                if (defance.StartBattle (user))
                {
                    string startBettelMsg = $"\r\n[1;31m{defance.UserDescriptionAndID}開始攻擊(5){user.UserDescriptionAndID}。[0m";
                    channel.BroadcastToRoom (startBettelMsg, defance.Geography.RoomID);
                }
                return Task.CompletedTask;
            }
            else
            {
                return Task.CompletedTask;
            }
        }
    }
}
