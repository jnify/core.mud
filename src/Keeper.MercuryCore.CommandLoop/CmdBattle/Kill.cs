using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class KillCommandHandler : ICommandHandler
    {
        public string Name => "KILL";
        public string AbbrName => "K";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);

            MobInfo targetMob = default;
            string paraMobName = info.ParameterString;
            if (user.Status.HasFlag (CreatureStatus.Fighting))
            {
                //處於攻擊狀態
                return ContinueAttack (channel, user, room, paraMobName);
            }
            else
            {
                //處於非攻擊狀態
                if (string.IsNullOrWhiteSpace (paraMobName)) return channel.SendLineAsync ($"你想要攻擊誰？");
                if (!room.Mobs.Any (x => x.Name.ToUpperInvariant ().StartsWith (paraMobName.ToUpperInvariant ()))) return channel.SendLineAsync ($"你要攻擊的對象不在這裏。");
                targetMob = room.Mobs.Where (x => x.Name.ToUpperInvariant ().StartsWith (paraMobName.ToUpperInvariant ())).FirstOrDefault ();
                if (user.StartBattle (targetMob))
                {
                    var AttResult = user.DoAttack ();
                    if (AttResult.Success)
                    {
                        AttResult.PublicMessage = $"[1;31m{user.UserDescriptionAndID}開始攻擊(4){targetMob.Settings.Name}！！！[0m\r\n{AttResult.PublicMessage}";
                    }
                    else
                    {
                        user.EndBattle ();
                    }
                    return SendMessage (channel, user.Name, "Mob", room.RoomID, AttResult);
                }
                else
                {
                    user.EndBattle ();
                    return channel.SendLineAsync ($"你手腳太慢，攻擊的對象已經GG了。");
                }
            }
        }

        private Task SendMessage (ITextChannel channel, string Attacker, string Defance, int RoomID,
            (bool Success, string PublicMessage, string PrivateAttackerMessage, string PrivateDefenceMessage) AttResult)
        {
            Task taskPublic = default;
            List<Task> tasks = new List<Task> ();

            if (!string.IsNullOrWhiteSpace (AttResult.PublicMessage))
            {
                taskPublic = channel.BroadcastToRoom (AttResult.PublicMessage, RoomID);
            }

            if (!string.IsNullOrWhiteSpace (AttResult.PrivateAttackerMessage))
            {
                tasks.Add (channel.SendMessageToUser (AttResult.PrivateAttackerMessage, Attacker, false));
            }

            if (!string.IsNullOrWhiteSpace (AttResult.PrivateDefenceMessage))
            {
                tasks.Add (channel.SendMessageToUser (AttResult.PrivateDefenceMessage, Defance, false));
            }

            if (taskPublic != null)
            {
                return taskPublic.ContinueWith (x =>
                {
                    Task.WhenAll (tasks);
                });
            }
            else
            {
                return Task.WhenAll (tasks);
            }
        }

        private Task ContinueAttack (ITextChannel channel, UserInfo user, Room room, string paraMobName)
        {
            if (string.IsNullOrEmpty (paraMobName) && user.FightingInfo.FightingWith != null)
            {
                //檢查原攻擊是否還在
                if (room.Mobs.Any (x => x.ID == user.FightingInfo.FightingWith.ID))
                {
                    var AttResult = user.DoAttack ();
                    return SendMessage (channel, user.Name, "Mob", room.RoomID, AttResult);
                }
                else
                {
                    string message = $"你攻擊的對象 {user.FightingInfo.FightingWith.Settings.Name} 不在這裏！！";
                    user.EndBattle ();
                    return channel.SendLineAsync (message);
                }
            }
            else
            {
                //處於攻擊狀態，不能轉換目標，需要有Change技能才能轉換攻擊目標
                return channel.SendLineAsync ($"你不是正忙著攻擊 {user.FightingInfo.FightingWith.Settings.Name} 嗎？");
            }
        }
    }
}
