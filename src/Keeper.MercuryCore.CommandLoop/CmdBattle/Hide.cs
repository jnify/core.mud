using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class HideCommandHandler : ICommandHandler
    {
        public string Name => "HIDE";
        public string AbbrName => Name;
        private readonly Random rnd = new Random ();
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;

            PracticedSkill skillattr = user.GetSkill (SkillName.Hide);
            var skillCategory = user.GetSkillCategory (skillattr);
            if (skillattr == null) return channel.SendLineAsync ($"什麼？！");

            var skill = new SkillHide (skillattr);
            var skillResult = skill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);

            if (skillResult != 1)
                return channel.SendLineAsync (TextUtil.GetSkillExecuteFailedMessage (skill, skillResult, user.UserDescriptionAndID, "", null));

            //最多900秒
            DateTime effEndTime = DateTime.Now.Add (TimeSpan.FromSeconds (skillCategory.Level * 20));
            UserEffectiveSettings effsetting = new UserEffectiveSettings (EffectiveType.Hide, effEndTime);
            string msg = user.AddEffective (effsetting);
            return channel.SendLineAsync (msg);
        }
    }
}
