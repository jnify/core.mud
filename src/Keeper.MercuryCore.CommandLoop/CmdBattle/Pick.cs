using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class PickCommandHandler : ICommandHandler
    {
        public string Name => "PICK";
        public string AbbrName => Name;
        private readonly Random rnd = new Random ();
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user.Geography.RoomID);

            if (string.IsNullOrEmpty (info.ParameterString))
                return channel.SendLineAsync ("你要解哪裏的鎖？");

            string ParameterString = info.ParameterString;
            if (ParameterString.Length == 1)
                ParameterString = TextUtil.GetExitName (info.ParameterString);

            if (ExitName.TryParse (ParameterString, true, out ExitName door))
            {
                channel.SendMessageToUser ("你試著解鎖......", user.Name);
                return channel.SendLineAsync (PickDoor (wordMng, user, room, door));
            }

            var box = room.GetThing (info.ParameterString);
            if (box != null)
                if (box.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                {
                    channel.SendMessageToUser ("你試著解鎖......", user.Name);
                    return channel.SendLineAsync (Unlock (user, box, box.ShortName).Message);
                }

            return channel.SendLineAsync ($"你想做什麼？");
        }

        private (string Message, bool isSuccess) Unlock (UserInfo user, BaseObjectCanLock box, string objName)
        {
            if (box.gateStatus != GateStatus.Locked)
                return ($"{objName}並沒有上鎖", false);

            PracticedSkill skillattr = user.GetSkill (SkillName.PickLock);
            var skillCategory = user.GetSkillCategory (skillattr);
            if (skillattr == null) return ("你不知道怎麼解鎖。", false);

            var skill = new SkillPickLock (skillattr);
            var skillResult = skill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);

            if (skillResult != 1)
                return (TextUtil.GetSkillExecuteFailedMessage (skill, skillResult, user.UserDescriptionAndID, "", null), false);

            if (skillCategory.Level < box.KeyNumber)
                return (TextUtil.GetSkillExecuteFailedMessage (skill, 0, user.UserDescriptionAndID, "", null), false);

            box.gateStatus = GateStatus.Close;
            return (skill.Message (), true);
        }
        private string PickDoor (IWorldManager wordMng, UserInfo user, Room room, ExitName door)
        {
            if (!room.Exits.Any (x => x.Dimension == door) ||
                !room.Exits.Where (x => x.Dimension == door).FirstOrDefault ().AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose))
                return $"{door.GetDescriptionText()}沒有門！";

            var doorsetting = room.Exits.Where (x => x.Dimension == door).FirstOrDefault ();
            if (!doorsetting.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                return $"{door.GetDescriptionText()}上沒有鎖！";

            //訊息以操作目的為主，對向門只是順帶而已
            var rtn = Unlock (user, doorsetting, door.GetDescriptionText ());
            if (rtn.isSuccess)
            {
                //對向門
                var OpsiteRoom = wordMng.GetRoom (doorsetting.RoomID);
                if (OpsiteRoom != null)
                {
                    var OpsiteDoorSetting = OpsiteRoom.Exits.Where (x => x.RoomID == user.Geography.RoomID).FirstOrDefault ();
                    if (OpsiteDoorSetting != null) OpsiteDoorSetting.gateStatus = GateStatus.Close;
                }
            }
            return rtn.Message;
        }

    }
}
