using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class CircleCommandHandler : ICommandHandler
    {
        public string Name => "CIRCLE";
        public string AbbrName => "CI";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var godOfWare = loop.Provider.GetService<IGodOfWar> ();

            PracticedSkill skillattr = user.GetSkill (SkillName.Circle);
            if (skillattr == null) return channel.SendLineAsync ($"什麼？！");

            if (info.ParameterValues.Count () == 0 &&
                (user.FightingInfo == null || user.FightingInfo.FightingWith == null))
                return channel.SendLineAsync ("先找個目標吧？");

            Creature defance = default;
            if (user.FightingInfo != null && user.FightingInfo.FightingWith != null && info.ParameterValues.Count () > 0)
            {
                defance = room.GetMob (info.ParameterString);
                if (user.FightingInfo.FightingWith.ID != defance.ID) return channel.SendLineAsync ("你正忙著呢！？");
            }

            defance = defance ?? room.GetMob (info.ParameterString);
            defance = defance ?? user.FightingInfo.FightingWith;

            if (defance == null) return channel.SendLineAsync ("這裹沒這個人。");

            SkillCircle skill = new SkillCircle (skillattr);

            //IGodOfWar
            var godOfWar = loop.Provider.GetService<IGodOfWar> ();
            bool InFighting = user.Status.HasFlag (CreatureStatus.Fighting);
            if (user.StartBattle (defance))
            {
                var AttResult = user.DoAttack (skill);
                if (AttResult.Success)
                {
                    if (!InFighting)
                        AttResult.PublicMessage = $"[1;31m{user.UserDescriptionAndID}開始攻擊(3){defance.Settings.Name}！！！[0m\r\n{AttResult.PublicMessage}";
                }
                else
                {
                    user.EndBattle ();
                }
                return godOfWar.MessageCombat (user, defance.Name, AttResult);
            }
            else
            {
                user.EndBattle ();
                return channel.SendLineAsync ($"你手腳太慢，攻擊的對象已經GG了。");
            }
        }
    }
}
