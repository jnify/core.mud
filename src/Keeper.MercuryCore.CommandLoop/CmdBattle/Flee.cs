using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class FleeCommandHandler : ICommandHandler
    {
        public string Name => "Flee";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            Random rnd = new Random ();
            var channel = loop.Provider.GetService<ITextChannel> ();
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var comTool = loop.Provider.GetService<ICommonTools> ();
            var user = usrMng.GetUser (channel.ChannelUser.Name);
            if (!user.Status.HasFlag (CreatureStatus.Fighting)) { return channel.SendLineAsync ($"又沒有人攻擊你，跑什麼跑？"); }

            PracticedSkill skillattr = user.GetSkill (SkillName.Flee);
            //等級10以下，預設給25%的逃跑熟練度
            if (user.Level <= 10)
                skillattr = skillattr ?? new PracticedSkill () { Proficiency = 25 };

            //等級超過10，必需要自己學會逃跑
            if (skillattr == null) return channel.SendLineAsync ("你忘記怎麼逃跑了！！");

            SkillFlee skill = new SkillFlee (skillattr);
            var skillResult = skill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);
            if (skillResult < 0)
            {
                //點數不夠
                string message = TextUtil.GetConsumeNotEnoughString (skillResult).Replace ("{Attacker}", user.UserDescriptionAndID);
                return channel.SendLineAsync (message);
            }
            else if (skillResult == 0)
            {
                //發動技能失敗
                string message = skill.FailedMessage (user.GetWeaponRightHand ()).Replace ("{Attacker}", user.UserDescriptionAndID);
                return channel.BroadcastToRoom (message, user.Geography.RoomID);
            }
            else if (skillResult == 9)
            {
                //發動技能失敗，但進步了
                string message = skill.FailedMessage (user.GetWeaponRightHand ()).Replace ("{Attacker}", user.UserDescriptionAndID);
                message += "\r\n你從錯誤中學習進步。";
                return channel.SendLineAsync (message);
            }
            else
            {
                IWorldManager wordMng = loop.Provider.GetService<IWorldManager> ();
                Room room = wordMng.GetRoom (user.Geography.RoomID);
                var mobs = room.Mobs.Where (x => x.FightingInfo != null && x.FightingInfo.FightingWith != null && x.FightingInfo.FightingWith.Name == user.Name);
                if (mobs != null)
                {
                    mobs.ToList ().ForEach (mob => mob.EndBattle ());
                }
                user.EndBattle ();
                var exitSettings = room.Exits[rnd.Next (room.Exits.Count ())];
                return comTool.MovementAsync (msgMgr, wordMng, usrMng.GetRoomUser (exitSettings.RoomID), usrMng.GetRoomUser (user.Geography.RoomID), user, room, exitSettings.Dimension, true);
            }
        }
    }
}
