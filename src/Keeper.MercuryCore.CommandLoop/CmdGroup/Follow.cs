using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class FollowCommandHandler : ICommandHandler
    {
        public string Name => "FOLLOW";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var user = channel.ChannelUser;
            //todo:要看得見，才能Follow
            if (string.IsNullOrWhiteSpace (info.ParameterString))
            {
                //未輸入User Name
                if (string.IsNullOrWhiteSpace (user.Actions.FollowWith))
                {
                    //沒有跟人
                    return channel.SendLineAsync ("你要跟著誰？！");
                }
                else
                {
                    //有跟人
                    var followwith = usrMng.GetUser (user.Actions.FollowWith);
                    if (followwith == null)
                    {
                        //跟的人未上線
                        user.Actions.UnFollow ();
                        return channel.SendLineAsync ("你要跟著誰？！");
                    }
                    else
                    {
                        return channel.SendLineAsync ($"你正跟著{followwith.Name}走。");
                    }
                }
            }
            else
            {
                //開始跟隨人
                if (string.Compare (info.ParameterString, "self", true) == 0 || string.Compare (info.ParameterString, user.Name, true) == 0)
                {
                    //停止跟隨
                    user.Actions.UnFollow ();
                    return channel.SendLineAsync ($"你停止跟隨。");
                }

                var followwith = usrMng.GetUser (info.ParameterString);
                if (followwith == null)
                {
                    return channel.SendLineAsync ("這裏沒這個人！");
                }
                else
                {
                    user.Actions.Follow (followwith);
                    return channel.SendLineAsync ($"你開始跟著{followwith.Name}走。");
                }
            }
        }
    }
}
