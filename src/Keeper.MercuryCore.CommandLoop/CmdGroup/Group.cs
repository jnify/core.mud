using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class GroupCommandHandler : ICommandHandler
    {
        public string Name => "GROUP";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var user = channel.ChannelUser;

            if (info.ParameterValues.Count () == 0)
            {
                return channel.SendMessageToUser (GetUserGroupList (usrMng, user.Name), user.Name);
            }
            else
            {
                string verb = info.ParameterValues.First ().ToUpper ();
                switch (verb)
                {
                    case "CREATE":
                        return channel.SendLineAsync (CreateGroup (usrMng, user.Name));
                    case "LEAVE":
                        return LeaveGroup (channel, usrMng, user.Name);
                    default:
                        var particUser = usrMng.GetUser (verb);
                        if (particUser == null) return channel.SendLineAsync ("這裏沒有這個人！");
                        if (user.Name == particUser.Name) return channel.SendLineAsync ("你已經在隊伍裹了！");
                        return JoinGroup (channel, usrMng, user.Name, particUser.Name);
                }
            }

        }

        private string GetUserGroupList (IUserManager usrMng, string UserName)
        {
            var UserGroup = usrMng.GetGroup (UserName);
            if (UserGroup == null) return "你還沒有組隊！";
            var leader = usrMng.GetUser (UserGroup.Leader);
            string rtn = $"由{leader.UserDescriptionAndID}帶領的{UserGroup.GroupName}\r\n";
            rtn += "團隊成員有：";
            UserGroup.UserGroup.ForEach (x =>
            {
                var user = usrMng.GetUser (x);
                if (user != null)
                    rtn += $"\r\n{user.UserDescriptionAndID}\t {user.StateCurrent.HP}HP, {user.StateCurrent.Mana}Mana, {user.StateCurrent.MV}MV";
            });
            return rtn;
        }

        private Task LeaveGroup (ITextChannel channel, IUserManager usrMng, string UserName)
        {
            var UserGroup = usrMng.GetGroup (UserName);
            if (UserGroup == null) return channel.SendLineAsync ("你還沒有組隊！");
            var tmpUserList = UserGroup.UserGroup.ToList ();
            if (UserGroup.Leader == UserName)
            {
                if (usrMng.LeaveGroup (UserName))
                {
                    return channel.BroadcastToUsers ("你的隊伍解散了。", tmpUserList);
                }
                else
                {
                    return channel.SendLineAsync ("你沒辦法解散你的隊伍！！");
                }
            }
            else
            {
                if (usrMng.LeaveGroup (UserName))
                {
                    var user = usrMng.GetUser (UserName);
                    return channel.BroadcastToUsers ($"{user.UserDescriptionAndID}離開了隊伍。", tmpUserList);
                }
                else
                {
                    return channel.SendLineAsync ("你離不開你的隊伍！！");
                }
            }
        }

        private Task JoinGroup (ITextChannel channel, IUserManager usrMng, string GroupLeaderName, string participantName)
        {
            var user = usrMng.GetUser (participantName);

            var particGroup = usrMng.GetGroup (participantName);
            if (particGroup != null) return channel.SendLineAsync ($"{user.Name}已經在別人的隊伍裏了！");

            var LeaderGroup = usrMng.GetGroup (GroupLeaderName);
            if (LeaderGroup == default || LeaderGroup.Leader != GroupLeaderName)
                return channel.SendLineAsync ("你還不是隊長。");

            if (LeaderGroup.AddMember (participantName))
            {
                return channel.BroadcastToUsers ($"{user.UserDescriptionAndID}加入了{LeaderGroup.GroupName}。", LeaderGroup.UserGroup);
            }
            else
            {
                return channel.SendLineAsync ($"{user.Name}加不進去你的隊伍！");
            }
        }

        private static string CreateGroup (IUserManager usrMng, string userName) =>
            usrMng.CreateGroup (Guid.NewGuid ().ToString (), userName) ? "隊伍建立完成。" : "你已經有隊伍了唷！";
    }
}
