using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SellCommandHandler : ICommandHandler
    {
        public string Name => "SELL";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var loadCommonObjDef = loop.Provider.GetService<ILoadCommonObject> ();
            var loadMobDef = loop.Provider.GetService<IMobDefinationLoader> ();
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要賣什麼？");
            string SellObjectName = default;
            string vendorName = default;

            //sell xxx
            //sell xxx to vendor
            //先找依有沒有to關鍵字找出SellObjectName及vendorName
            var fromPosition = info.ParameterString.IndexOf ("to", StringComparison.InvariantCultureIgnoreCase);
            if (fromPosition == -1)
            {
                SellObjectName = info.ParameterString;
            }
            else
            {
                SellObjectName = info.ParameterString.Substring (0, fromPosition).Trim ();
                vendorName = info.ParameterString.Substring (fromPosition + 5).Trim ();
            }

            var targetObject = user.GetThing (SellObjectName);
            if (targetObject == null) return channel.SendLineAsync ("你沒這樣東西！想買空賣空？");

            MobInfo vendor;
            if (string.IsNullOrWhiteSpace (vendorName))
            {
                vendor = room.Mobs.Where (x => x.Role == MobRole.Vendor).FirstOrDefault ();
            }
            else
            {
                vendor = room.GetMob (vendorName);
            }
            if (vendor == null) return channel.SendLineAsync ("你找不到老闆");
            if (vendor.Role != MobRole.Vendor) return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：你找錯人了！");
            if (targetObject.Pricing == 0) return channel.SendLineAsync ($"{vendor.Settings.Name}對{targetObject.ShortName}不屑一顧。");
            var mobdef = loadMobDef.GetDef (vendor.Name);
            if (mobdef.AllowSellingTypes == null || mobdef.AllowSellingTypes.Count () == 0)
                return channel.SendLineAsync ($"{vendor.Settings.Name}對任何東西都沒有興趣。");
            if (!mobdef.AllowSellingTypes.Any (x => x == targetObject.ObjectType))
                return channel.SendLineAsync ($"{vendor.Settings.Name}對{targetObject.ShortName}一點興趣也沒有。");

            var isSuccess = user.RemoveByID (targetObject.ID, out var _tmp);
            if (!isSuccess)
                return channel.SendLineAsync ($"你沒辦法賣掉{targetObject.ShortName}。");

            int price = Convert.ToInt32 (Math.Round ((targetObject.Pricing * 0.8m)));
            user.Coins += price;

            string msg = $"你以{price.ToCoins()}賣掉了{targetObject.ShortName}。";
            return channel.SendLineAsync (msg);
        }
    }
}
