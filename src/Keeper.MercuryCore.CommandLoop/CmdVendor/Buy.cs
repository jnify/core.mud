using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class BuyCommandHandler : ICommandHandler
    {
        public string Name => "BUY";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var loadCommonObjDef = loop.Provider.GetService<ILoadCommonObject> ();
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要買什麼？");
            string buyObjectName = default;
            string vendorName = default;

            //buy xxx
            //buy xxx from vendor
            //先找依有沒有from關鍵字找出buyObjectName及vendorName
            var fromPosition = info.ParameterString.IndexOf ("from", StringComparison.InvariantCultureIgnoreCase);
            if (fromPosition == -1)
            {
                buyObjectName = info.ParameterString;
            }
            else
            {
                buyObjectName = info.ParameterString.Substring (0, fromPosition).Trim ();
                vendorName = info.ParameterString.Substring (fromPosition + 5).Trim ();
            }

            MobInfo vendor;
            if (string.IsNullOrWhiteSpace (vendorName))
            {
                vendor = room.Mobs.Where (x => x.Role == MobRole.Vendor).FirstOrDefault ();
            }
            else
            {
                vendor = room.GetMob (vendorName);
            }
            if (vendor == null) return channel.SendLineAsync ("你找不到老闆");
            if (vendor.Role != MobRole.Vendor) return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：你找錯人了！");
            CommonObjectDefined def = null;
            foreach (var menu in vendor.Menu)
            {
                var defTmp = loadCommonObjDef.GetDef (menu.SystemCode);
                if (defTmp != null)
                    if (defTmp.Name.ToUpper ().StartsWith (buyObjectName.ToUpper ()))
                    {
                        def = defTmp;
                        break;
                    }
            }
            if (def == null)
                return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：我沒有在賣這樣物品！");

            //檢查錢
            if (user.Coins < def.Pricing)
                return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：很抱歉, 多賺點錢再來吧！");

            CommonObject targetObj;
            targetObj = loadCommonObjDef.GenerateCommonObject (def.SystemCode);
            if (targetObj == null)
                return channel.SendLineAsync ($"商業之神 告訴你：這個東西[{def.SystemCode}]，我做不出來。");

            //交易開始
            user.Coins -= def.Pricing;
            user.AddThing (targetObj);
            string msg = $"你花了{def.Pricing.ToCoins()}買下了{def.ShortName}";
            return channel.SendLineAsync (msg);
        }
    }
}
