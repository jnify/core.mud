using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class ListCommandHandler : ICommandHandler
    {
        public string Name => "LIST";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var loadCommonObjDef = loop.Provider.GetService<ILoadCommonObject> ();
            MobInfo vendor;
            //找出Vendor
            if (string.IsNullOrEmpty (info.ParameterString))
            {
                vendor = room.Mobs.Where (x => x.Role == MobRole.Vendor).FirstOrDefault ();
            }
            else
            {
                vendor = room.GetMob (info.ParameterString);
            }
            if (vendor == null) return channel.SendLineAsync ("你找不到老闆");
            if (vendor.Role != MobRole.Vendor) return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：你找錯人了！");

            if (vendor.Menu.Count () == 0)
                return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：我還不知道要賣什麼！不如你先賣點東西給我吧？");

            //找出定義
            List<CommonObjectDefined> objDef = new List<CommonObjectDefined> ();

            vendor.Menu.ForEach (menu =>
            {
                var def = loadCommonObjDef.GetDef (menu.SystemCode);
                if (def != null)
                    objDef.Add (def);
                else
                    Console.WriteLine ($"載入銷售資料[{menu.SystemCode}]，不存在。");
            });
            //LeftPadSpace
            //[價格   中文名(英文名)]
            string msg = "販售清單：";
            if (objDef.Count () == 0)
            {
                return channel.SendLineAsync ($"{vendor.Settings.Name} 告訴你：國王的新衣，買不買？");
            }
            else
            {
                //先算出最長的字串
                int maxLength = 0;
                objDef.ForEach (def =>
                {
                    int len = $"{def.GetShortDescription()}（{def.Name}）".ChineseWrodsLength ();
                    if (len > maxLength) maxLength = len;
                });

                objDef.ForEach (def =>
                {
                    string price = def.Pricing.ToString ().LeftPadSpace (7);
                    string desc = $"{def.GetShortDescription()}（{def.Name}）".RightPadSpace (maxLength);
                    msg += $"\r\n[{price}  {desc}]";
                });
            }
            return channel.SendLineAsync (msg);
        }
    }
}
