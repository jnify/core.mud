﻿using System.Collections.Generic;
using System.Linq;

namespace Keeper.MercuryCore.CommandLoop.Parsing
{
    public struct CommandInfo
    {
        public CommandInfo (string name, IEnumerable<string> parameterValues)
        {
            this.Name = name;
            this.ParameterValues = parameterValues;
            this.IsValid = true;
        }

        public static CommandInfo Invalid => new CommandInfo ();

        public string Name
        {
            get;
            set;
        }

        public IEnumerable<string> ParameterValues
        {
            get;
            private set;
        }

        public string ParameterString
        {
            get
            {
                if (ParameterValues.Count () > 0)
                    return string.Join (" ", ParameterValues).Trim ();
                else
                    return "";
            }
        }

        public bool IsValid
        {
            get;
            private set;
        }
    }
}
