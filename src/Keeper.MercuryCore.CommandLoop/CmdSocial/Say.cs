using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SayCommandHandler : ICommandHandler
    {
        public string Name => "SAY";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            UserInfo user = channel.ChannelUser;

            var content = info.ParameterString;
            if (!string.IsNullOrEmpty (content))
            {
                var message = $"{user.UserDescriptionAndID}說：{content}";
                return channel.BroadcastToRoom (message, user.Geography.RoomID);
            }
            else
            {
                return channel.SendLineAsync ("你想說什麼？");
            }
        }
    }
}
