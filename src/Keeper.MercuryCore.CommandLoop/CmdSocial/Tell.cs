using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class TellCommandHandler : ICommandHandler
    {
        public string Name => "TELL";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);

            if (info.ParameterValues.Count () == 0) { return channel.SendLineAsync ("你要和誰說話？"); }
            if (info.ParameterValues.Count () == 1) { return channel.SendLineAsync ("你想和對方說什麼？"); }

            var Receipon = loop.Provider.GetService<IUserManager> ().GetUser (info.ParameterValues.First ());
            if (Receipon == null) { return channel.SendLineAsync ("沒有這個人。"); }
            if (Receipon.Name == user.Name) { return channel.SendLineAsync ("神經病才自言自語吧？"); }

            var message = string.Join ("", info.ParameterValues.Skip (1));
            var messageToReceipt = $"[1;33m{user.UserDescriptionAndID}對{Receipon.UserDescriptionAndID}說：{message}[0m";
            channel.SendLineAsync ($"[1;33m你對{Receipon.Settings.Name}說：{message}[0m");
            return channel.SendMessageToUser (messageToReceipt, Receipon.Name);
        }
    }
}
