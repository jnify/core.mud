using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class AskCommandHandler : ICommandHandler
    {
        public string Name => "ASK";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);

            if (info.ParameterValues.Count () == 0) { return channel.SendLineAsync ("你要問誰？"); }
            if (info.ParameterValues.Count () == 1) { return channel.SendLineAsync ("你想問對方什麼？"); }
            var Receipon = loop.Provider.GetService<IUserManager> ().GetUser (info.ParameterValues.First ());
            if (Receipon == null) { return channel.SendLineAsync ("沒有這個人。"); }
            if (Receipon.Name == user.Name) { return channel.SendLineAsync ("神經病才自問自答吧？"); }

            var message = string.Join ("", info.ParameterValues.Skip (1));
            message = $"{user.UserDescriptionAndID}問{Receipon.UserDescriptionAndID}：{message}";
            return channel.BroadcastToRoom (message, room.RoomID);
        }
    }
}
