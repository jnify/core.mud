using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.ObjectHelper;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.LoadCommonObjects;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SYSTEM_WAITING_COMMAND_TIMEOUTCommandHandler : ICommandHandler
    {
        public string Name => "SYSTEM_WAITING_COMMAND_TIMEOUT";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        private readonly ILogger<AdminCommandHandler> logger;
        public SYSTEM_WAITING_COMMAND_TIMEOUTCommandHandler (ILogger<AdminCommandHandler> logger)
        {
            this.logger = logger;
        }

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            if (!user.SetIdel ())
            {
                return channel.SendLineAsync ($"再不動一動，你的身體要發霉了～");
            }
            else
            {
                IFirebase<UserInfo> userinfo = loop.Provider.GetService<IFirebase<UserInfo>> ();
                IUserManager usrMng = loop.Provider.GetService<IUserManager> ();
                var quitmsg = TextUtil.GetFromFile ("DocumentHelp", "QuitMessage.hlp");
                user.Clear ();
                userinfo.InsertOrUpdate (user);
                loop.IsRunning = false;
                usrMng.RemoveUser (user.Name);
                channel.BroadcastToRoom ($"{user.UserDescriptionAndID}漸漸的消失在你的眼前！！", user.Geography.RoomID, user.Name);
                return Task.CompletedTask;
                //return Task.FromCanceled (new CancellationToken (true));
            }
        }
    }
}
