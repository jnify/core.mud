using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.ObjectHelper;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.LoadCommonObjects;
using Rest.MercuryCore.Loader.Mob;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class AdminCommandHandler : ICommandHandler
    {
        public string Name => "ADMIN";
        public string AbbrName => "A";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        private readonly ILogger<AdminCommandHandler> logger;
        public AdminCommandHandler (ILogger<AdminCommandHandler> logger)
        {
            this.logger = logger;
        }

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            UserInfo u = channel.ChannelUser;
            IMessageManager msgmgr = loop.Provider.GetService<IMessageManager> ();
            IUserManager usrMng = loop.Provider.GetService<IUserManager> ();

            if ((u.Level > 95 || u.Name == "Roy") && info.ParameterValues.Count () > 0)
            {
                try
                {
                    string verb;
                    string target;
                    List<string> paras;
                    if (tryParsVerb (info.ParameterValues, out verb, out target, out paras))
                    {
                        switch (verb)
                        {
                            case "CREATE":
                                switch (target)
                                {
                                    case "DOOR":
                                        return new RoomHelper ().BuildDoorAsync (channel, loop, paras);
                                    case "MOB":
                                        IWorldManager worldManager = loop.Provider.GetService<IWorldManager> ();
                                        LoadCommonObject loadCommonObj = loop.Provider.GetService<LoadCommonObject> ();
                                        MobGenerate genMob = loop.Provider.GetService<MobGenerate> ();
                                        var mobloader = loop.Provider.GetService<IMobDefinationLoader> ();
                                        var md = mobloader.GetDef (paras.First ());
                                        genMob.Generate (md, u.Geography.RoomID);
                                        return channel.BroadcastToRoom ($"{md.Settings.Name}突然出現在你面前。", u.Geography.RoomID);
                                    default:
                                        return channel.SendLineAsync ($"還無法建立{target}唷。");
                                }
                            case "REMOVE":
                                switch (target)
                                {
                                    case "DOOR":
                                        return new RoomHelper ().RemoveDoorAsync (channel, loop, paras);
                                    default:
                                        return channel.SendLineAsync ($"還無法移除{target}唷。");
                                }
                            case "SAVE":
                                return new SaveHelper ().Save (channel, loop, target);
                            case "FILLLIFE":
                                var user = usrMng.GetUser (target);
                                user.Livelihood.Thirsty = 100;
                                user.Livelihood.Full = 100;
                                user.StateCurrent.HP = user.StateMax.HP;
                                user.StateCurrent.Mana = user.StateMax.Mana;
                                user.StateCurrent.MV = user.StateMax.MV;
                                user.CancelEffective (EffectiveType.Dying);
                                return channel.SendLineAsync ("Done");
                            default:
                                return channel.SendLineAsync ($"指令{verb}還不支援唷。");
                        }
                    }
                    else
                    {
                        return channel.SendLineAsync ($"命令不正確。");
                    }
                }
                catch (Exception ex)
                {
                    logger.LogInformation ("Exception:" + ex.Message);
                    return channel.SendLineAsync (ex.Message);
                }
            }
            else
            {
                return channel.SendLineAsync ($"請輸入[36mHelp[0m以獲得幫助。");
            }
        }

        private bool tryParsVerb (IEnumerable<string> parameterValues,
            out string verb,
            out string target,
            out List<string> paras)
        {
            if (parameterValues.Count () > 1)
            {
                verb = parameterValues.First ().ToUpperInvariant ();
                parameterValues = parameterValues.Skip (1);
                target = parameterValues.First ().ToUpperInvariant ();
                parameterValues = parameterValues.Skip (1);
                paras = parameterValues.ToList ();
                return true;
            }
            else
            {
                paras = default;
                verb = target = null;
                return false;
            }
        }
    }
}
