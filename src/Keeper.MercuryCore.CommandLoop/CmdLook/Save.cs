using System.Threading.Tasks;
using Firebase.OPT;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SaveCommandHandler : ICommandHandler
    {
        public string Name => "SAVE";
        public string AbbrName => "SAVE";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {

            IFirebase<UserInfo> userinfo = loop.Provider.GetService<IFirebase<UserInfo>> ();
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var task1 = userinfo.InsertOrUpdate (user);
            var task2 = channel.SendLineAsync ("存檔完成。");
            return Task.WhenAll (task1, task2);
        }
    }
}
