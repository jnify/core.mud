using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class InventoryCommandHandler : ICommandHandler
    {
        public string Name => "INVENTORY";
        public string AbbrName => "INV";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;

            string rtn = "你" + ((Creature) user).GetLookThings ();
            return channel.SendLineAsync (rtn);
        }
    }
}
