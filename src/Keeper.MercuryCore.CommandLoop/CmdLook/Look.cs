using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class LookCommandHandler : ICommandHandler
    {
        public string Name => "LOOK";
        public string AbbrName => "L";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        private readonly Random rnd = new Random ();

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var comTool = loop.Provider.GetService<ICommonTools> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user);
            //看房間
            if (info.ParameterValues.Count () == 0)
            {
                var users = usrMng.GetRoomUser (user);
                return DisplayRoomInformation (channel, comTool, user, users, room);
            }

            //看某東西的裏面
            if (info.ParameterValues.Count () > 1)
            {
                string secondPara = info.ParameterValues.FirstOrDefault ().ToUpperInvariant ();
                if (secondPara == "I" || secondPara == "IN")
                {
                    info.Name = "LOOKIN";
                    var handlerLookup = loop.Provider.GetServices<ICommandHandler> ().ToDictionary (x => x.Name.ToUpperInvariant ());
                    handlerLookup.TryGetValue (info.Name, out var handler);
                    CommandInfo cmd = new CommandInfo (info.Name, info.ParameterValues.Skip (1));
                    return handler.Handle (loop, cmd);
                }
            }

            //看物品
            string objName = info.ParameterString;
            CommonObject obj;
            obj = room.GetThing (objName);
            obj = obj ?? user.GetThing (objName);
            if (obj != null) return channel.SendLineAsync (obj.GetLookDescription ());

            //看Mob
            Creature creature = room.GetMob (objName);
            if (creature != null) return DisplayCreatureInformation (channel, user, creature);

            //看PPL
            creature = usrMng.GetUser (objName);
            if (creature != null) return DisplayCreatureInformation (channel, user, creature);

            //看方向
            if (isLookingExits (objName, out ExitName exitname))
            {
                var exit = room.Exits.Where (x => x.Dimension == exitname).FirstOrDefault ();
                if (exit != null) return DisplayExitInformation (channel, exit, wordMng.GetRoom (exit.RoomID));
            }

            return channel.SendLineAsync ("你從這裡看不出什麼特別的...");
        }

        private Task DisplayExitInformation (ITextChannel channel, ExitSetting exit, Room nextRoom)
        {
            if (exit.gateStatus != GateStatus.Open)
                return channel.SendLineAsync ($"{exit.Dimension.GetDescriptionText()}的門關著。");

            if (nextRoom != null)
                return channel.SendLineAsync ($"{exit.Dimension.GetDescriptionText()}通往{nextRoom.Geography.RoomTitle}。");
            else
                return channel.SendLineAsync ($"{exit.Dimension.GetDescriptionText()}通往未知。");
        }

        private Task DisplayCreatureInformation (ITextChannel channel, UserInfo user, Creature creature)
        {
            bool DisplayThing = CanDisplayThing (user, channel);
            string descript = creature.GetLookDescription (DisplayThing, false, user.Level > 95);
            return channel.SendLineAsync (descript);
        }

        private bool CanDisplayThing (UserInfo user, ITextChannel channel)
        {
            if (user.Career != CreatureCareer.Thief) return false;

            PracticedSkill skill = user.GetSkill (SkillName.Peek);
            if (skill == null) return false;

            if (skill.Proficiency > 94) return rnd.Next (0, 100) < 94;

            SkillPeek peek = new SkillPeek (skill);
            var result = peek.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);
            switch (result)
            {
                case 1:
                    return true;
                case 9:
                    channel.SendMessageToUser ("你從錯誤中學習進步。", user.Name);
                    return false;
                default:
                    return false;
            }
        }

        private Task DisplayRoomInformation (ITextChannel channel, ICommonTools comTool, UserInfo user, List<UserInfo> roomUsers, Room room)
        {
            var descript = comTool.DescriptionBuilder (room, user, roomUsers);
            return channel.SendLineAsync (descript);
        }

        private bool isLookingExits (string objname, out ExitName exitname)
        {
            exitname = objname.ConvertToExitName ();

            return exitname != ExitName.None;
        }
    }
}
