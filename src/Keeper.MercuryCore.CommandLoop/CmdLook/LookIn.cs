using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class LookInCommandHandler : ICommandHandler
    {
        public string Name => "LOOKIN";
        public string AbbrName => "LN";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var user = usrMng.GetUser (channel.ChannelUser.Name);
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            CommonObject containerObj = null;
            string objname = info.ParameterString;
            containerObj = room.GetThing (objname);
            containerObj = containerObj ?? user.GetThing (objname);
            if (containerObj == null) return channel.SendLineAsync ("這裏沒這個東西。");
            if (containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock) && containerObj.gateStatus == GateStatus.Locked)
                return channel.SendLineAsync ($"{containerObj.ShortName}鎖住了。");
            if (containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose) && containerObj.gateStatus != GateStatus.Open)
                return channel.SendLineAsync ($"{containerObj.ShortName}是關著的。");

            if (containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGetFrom) || containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanPutIn))
            {
                string descript = default;
                //Key為Description, Value為數量
                var objDictionary = containerObj.Things.GroupBy (obj => obj.GetDescriptionOnTheGround ()).
                ToDictionary (group => group.Key, group => group.Count ()).ToList ();
                if (objDictionary.Count () > 0)
                {
                    //取出最大數量
                    int maxNumLength = objDictionary.Max (x => x.Value).ToString ().Length;
                    objDictionary.ForEach (dic =>
                    {
                        string objDesc = dic.Key;
                        string numStr = dic.Value.ToAlignRightString (maxNumLength);
                        if (dic.Value > 1)
                            objDesc = $"({numStr}){objDesc}";
                        else
                            objDesc = dic.Key.PadLeft (dic.Key.Length + maxNumLength + 2);

                        descript += $"\r\n{objDesc}";
                    });
                }
                else
                {
                    descript = $"{containerObj.ShortName}裏頭空空如也。";
                }
                return channel.SendLineAsync (descript);
            }

            if (containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink) || containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanEat))
            {
                var loadCommonObj = loop.Provider.GetService<ILoadCommonObject> ();
                var def = loadCommonObj.GetDef (containerObj.SystemCode);
                var maxCount = def.AllowExecuteTimes;
                var percent = TextUtil.GetPercentage (containerObj.AllowExecuteTimes, maxCount);
                if (containerObj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink))
                    return channel.SendLineAsync (GetDrinkMessage (percent, containerObj));
                else
                    return channel.SendLineAsync (GetEatMessage (percent, containerObj));
            }
            return channel.SendLineAsync ("你確定要看那「裡面」的東西？");
        }

        private string GetDrinkMessage (int percent, CommonObject obj)
        {
            string prefix = obj.ShortName;
            if (percent > 90) return $"{prefix}看起來滿滿都是乾淨的的液體。";
            if (percent > 70) return $"{prefix}看起來有許多乾淨的的液體。";
            if (percent > 50) return $"{prefix}看起來大約還有一半乾淨的的液體。";
            if (percent > 30) return $"{prefix}看起來乾淨的的液體不多了。";
            if (percent > 0) return $"{prefix}只剩下些許乾淨的的液體。";
            return $"{prefix}是空的。";
        }

        private string GetEatMessage (int percent, CommonObject obj)
        {
            string prefix = obj.ShortName + "看起來";
            if (percent > 90) return $"{prefix}完好如初。";
            if (percent > 70) return $"{prefix}還有蠻多能吃的。";
            if (percent > 50) return $"{prefix}大約還一半的樣子。";
            if (percent > 30) return $"{prefix}還剩下一些。";
            if (percent > 0) return $"{prefix}剩沒幾口。";
            return $"{prefix}沒辦法吃了。";
        }
    }
}
