﻿using System;
using System.Linq;
using System.Reflection;
using Keeper.MercuryCore;
using Keeper.MercuryCore.CommandLoop;
using Keeper.MercuryCore.CommandLoop.AutoCommand;
using Keeper.MercuryCore.Util;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CommandLoopServiceCollectionExtensions
    {
        public static IServiceCollection AddCommandLoop (this IServiceCollection services, Action<IServiceCollection<ICommandLoop>> servicesAction)
        {
            var collectionWrapper = new WrappedServiceCollection<ICommandLoop> (services);

            servicesAction (collectionWrapper);

            return services;
        }

        public static IServiceCollection<ICommandLoop> AddCommandHandler (this IServiceCollection<ICommandLoop> services)
        {
            var classes = Assembly.GetExecutingAssembly ().GetTypes ().Where (t =>
                t.Namespace == "Keeper.MercuryCore.CommandLoop.AutoCommand" && t.Name.EndsWith ("CommandHandler")).ToList ();

            classes.ForEach (t =>
            {
                services.AddHandler (t);
            });

            return services;
        }

        public static IServiceCollection<ICommandLoop> AddHandler<T> (this IServiceCollection<ICommandLoop> services)
        where T : class, ICommandHandler
        {
            services.AddSingleton<T> ();
            services.AddSingleton<ICommandHandler> (provider => provider.GetRequiredService<T> ());

            return services;
        }

        public static IServiceCollection<ICommandLoop> AddHandler (this IServiceCollection<ICommandLoop> services, Type serviceType)
        {
            services.AddSingleton (serviceType);
            services.AddSingleton<ICommandHandler> (provider => (ICommandHandler) provider.GetRequiredService (serviceType));

            return services;
        }
    }
}