using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class ScoreCommandHandler : ICommandHandler
    {
        public string Name => "SCORE";
        public string AbbrName => "SC";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var wordmgr = loop.Provider.GetService<IWorldManager> ();
            var room = wordmgr.GetRoom (user.Geography.RoomID);
            StringBuilder sb = new StringBuilder ().Clear ();
            sb.Append ($"{user.Gender.GetDescriptionText()}{user.Race.GetDescriptionText()}{user.Career.GetDescriptionText()}");
            sb.Append ($"等級：{user.Level}");
            sb.Append ($"\r\n目前時間：{wordmgr.WorldInfo.SystemDateTime.ToString()}");
            sb.Append ("\r\n" + ((Creature) user).GetLookAttribute ());
            sb.Append ("\r\n" + user.DisplayActionEffect ());
            sb.Append ($"\r\n你{user.Status.CreaturePositionDescription()}。");

            return channel.SendLineAsync (sb.ToString ());
        }
    }
}
