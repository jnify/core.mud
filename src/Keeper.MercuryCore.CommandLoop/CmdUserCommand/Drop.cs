using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class DropCommandHandler : ICommandHandler
    {
        public string Name => "DROP";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            string objname = default;
            if (info.ParameterValues.Count () > 0)
            {
                if (string.Compare (info.ParameterString, "all", true) == 0)
                {
                    //Drop all
                    string dropmsg = default;
                    List<string> idList = user.Things.Where (obj => obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrop)).
                    Select (obj => obj.ID).ToList ();
                    idList.ForEach (id =>
                    {
                        user.RemoveByID (id, out var obj);
                        room.AddThing (obj);
                        dropmsg += $"\r\n{user.UserDescriptionAndID}丟掉了{obj.ShortName}。";
                    });
                    if (!string.IsNullOrWhiteSpace (dropmsg))
                        return channel.SendLineAsync (dropmsg);
                    else
                        return channel.SendLineAsync ("能丟的都丟了。");
                }
                else
                {
                    objname = info.ParameterString;
                    var targetObject = user.GetThing (objname);
                    if (targetObject != null)
                    {
                        var isSuccess = user.RemoveByID (targetObject.ID, out var obj);
                        if (isSuccess)
                        {
                            room.AddThing (obj);
                            return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}丟掉了{obj.ShortName}。", room.RoomID);
                        }
                        else
                        {
                            return channel.SendLineAsync ("你沒辦法丟掉這個東西。");
                        }
                    }
                    else
                    {
                        return channel.SendLineAsync ("你沒有這樣東西。");
                    }
                }
            }
            else
            {
                return channel.SendLineAsync ("你想丟掉什麼？");
            }
        }
    }
}
