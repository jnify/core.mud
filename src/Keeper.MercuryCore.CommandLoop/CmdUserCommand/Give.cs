using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.EventActions;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Quest;
using Rest.MercuryCore.Utilities;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class GiveCommandHandler : ICommandHandler
    {
        public string Name => "GIVE";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            IUserManager usrMng = loop.Provider.GetService<IUserManager> ();
            IWorldManager worldManager = loop.Provider.GetService<IWorldManager> ();
            QuestCollection quest = loop.Provider.GetService<QuestCollection> ();
            ActionMob actionmob = loop.Provider.GetService<ActionMob> ();
            var mobloader = loop.Provider.GetService<IMobDefinationLoader> ();
            UserInfo user = channel.ChannelUser;
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要給什麼東西？");
            if (info.ParameterValues.Count () == 1) return channel.SendLineAsync ("東西要給誰？");

            var objName = info.ParameterValues.First ();
            var obj = user.GetThing (objName);
            if (obj == null) return channel.SendLineAsync ("你沒有這樣東西。");

            var toName = info.ParameterValues.Skip (1).First ();
            var toUser = usrMng.GetUser (toName);
            if (toUser != null) return GiveObjectToPPL (channel, user, obj, toUser);

            var room = worldManager.GetRoom (user.Geography.RoomID);
            var toMob = room.GetMob (toName);
            if (toMob != null) return GiveObjectToMob (channel, user, obj, toMob, mobloader, quest, actionmob, room);
            return channel.SendLineAsync ("這裹沒有這個人。");

        }

        private Task GiveObjectToPPL (ITextChannel channel, UserInfo FromUser, CommonObject obj, UserInfo ToUser)
        {
            if (ToUser.Geography.RoomID != FromUser.Geography.RoomID) return channel.SendLineAsync ("這裹沒有這個人。");

            var isSuccess = FromUser.RemoveByID (obj.ID, out var transferObj);
            if (isSuccess)
            {
                ToUser.AddThing (transferObj);
                return channel.BroadcastToRoom ($"{FromUser.UserDescriptionAndID}拿了{transferObj.ShortName}給{ToUser.UserDescriptionAndID}", FromUser.Geography.RoomID);
            }
            else
            {
                return channel.SendLineAsync ($"{obj.Name}離不開你！！");
            }
        }

        private Task GiveObjectToMob (ITextChannel channel, UserInfo FromUser, CommonObject obj, MobInfo ToMob,
            IMobDefinationLoader mobloader, QuestCollection quest, ActionMob actionmob, Room room)
        {
            var isSuccess = FromUser.RemoveByID (obj.ID, out var transferObj);
            if (isSuccess)
            {
                ToMob.AddThing (transferObj);
                channel.BroadcastToRoom ($"{FromUser.UserDescriptionAndID}拿了{transferObj.ShortName}給{ToMob.UserDescriptionAndID}", FromUser.Geography.RoomID);

                //處理Quest
                var mobdef = mobloader.GetDef (ToMob.Name);
                var QuestName = mobdef.QuestScriptName;

                if (mobdef != null && mobdef.QuestScript != null)
                {
                    var settings = quest.Get (QuestName, FromUser.Name);
                    Dictionary<string, string[]> QuestSetting = new Dictionary<string, string[]> ();
                    QuestSetting = mobdef.QuestScript;
                    if (QuestSetting.ContainsKey (settings))
                    {
                        var steps = QuestSetting[settings]; //receive:PotionNewbieMV
                        var CorrectItemSystemCode = steps[0].SpliteColon (); //第一行必需為receive，要判斷收到的東西對不對
                        if (string.Compare (CorrectItemSystemCode.Key, "receive", true) == 0 && CorrectItemSystemCode.Value == transferObj.SystemCode)
                        {
                            steps = steps.Skip (1).ToArray ();
                            ActionKeyParameter actparam = new ActionKeyParameter (FromUser, ToMob, room);
                            actionmob.ExecuteSteps (steps, actparam);
                        }
                    }
                }
                return Task.CompletedTask;
            }
            else
            {
                return channel.SendLineAsync ($"{obj.Name}離不開你！！");
            }
        }
    }
}
