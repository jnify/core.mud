using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class FillCommandHandler : ICommandHandler
    {
        public string Name => "FILL";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        private readonly Random rnd = new Random ();

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var loadCommonObjDef = loop.Provider.GetService<ILoadCommonObject> ();

            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("什麼東西要裝水？");

            //Get object have water.
            bool canFill = room.Things.Any (x => x.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink) &&
                !x.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGet));
            if (!canFill) return channel.SendLineAsync ("你找不到水源。");

            var obj = user.GetThing (info.ParameterString);
            if (obj == null) return channel.SendLineAsync ("你沒有那樣東西！");
            if (!obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink) || obj.ActionKey != "Drink_Water")
                return channel.SendLineAsync ("它不能裝水！");

            var def = loadCommonObjDef.GetDef (obj.SystemCode);
            if (def.AllowExecuteTimes == obj.AllowExecuteTimes)
                return channel.SendLineAsync ($"{obj.ShortName}還是滿的。");

            if (def.DestroyWhenZeroQuota && (rnd.Next (1, def.AllowExecuteTimes) == 1))
            {
                user.RemoveByID (obj.ID, out var tmp);
                if (string.IsNullOrWhiteSpace (def.SuccessMessage))
                    return channel.SendLineAsync ($"{obj.ShortName}突然消失了。");
                else
                    return channel.SendLineAsync (def.SuccessMessage.Replace ("{Attacker}", user.UserDescriptionAndID));
            }

            obj.AllowExecuteTimes = def.AllowExecuteTimes;
            return channel.SendLineAsync ($"你把{obj.ShortName}裝滿了水。");
        }
    }
}
