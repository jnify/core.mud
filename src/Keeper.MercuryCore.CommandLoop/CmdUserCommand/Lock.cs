using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class LockCommandHandler : ICommandHandler
    {
        public string Name => "LOCK";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user.Geography.RoomID);
            if (string.IsNullOrEmpty (info.ParameterString))
                return channel.SendLineAsync ("你要鎖哪裏？");

            string ParameterString = info.ParameterString;
            if (ParameterString.Length == 1)
                ParameterString = TextUtil.GetExitName (info.ParameterString);

            if (ExitName.TryParse (ParameterString, true, out ExitName door))
                return channel.SendLineAsync (LockDoor (wordMng, user, room, door));

            var box = room.GetThing (info.ParameterString);
            if (box != null)
                if (box.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                    return channel.SendLineAsync (lockBox (user, box, box.ShortName).Message);
                else
                    return channel.SendLineAsync ($"你想做什麼？");

            return channel.SendLineAsync ($"你想做什麼？");
        }
        private (string Message, bool isSuccess) lockBox (UserInfo user, BaseObjectCanLock box, string objName)
        {
            var unlockResult = box.Unlock (user);
            switch (unlockResult)
            {
                case UnlockResult.Success:
                    box.gateStatus = GateStatus.Locked;
                    return ($"你把{objName}鎖起來了。", true);
                case UnlockResult.Locked:
                    return ($"{objName}早就鎖上了。", false);
                case UnlockResult.NoLocked:
                    return ($"{objName}需要先關上。", false);
                default:
                    return ($"{objName}需要鑰匙。", false);
            }
        }

        private string LockDoor (IWorldManager wordMng, UserInfo user, Room room, ExitName door)
        {
            if (!room.Exits.Any (x => x.Dimension == door) ||
                !room.Exits.Where (x => x.Dimension == door).FirstOrDefault ().AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose))
                return $"{door.GetDescriptionText()}沒有門！";

            var doorsetting = room.Exits.Where (x => x.Dimension == door).FirstOrDefault ();
            if (!doorsetting.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                return $"{door.GetDescriptionText()}沒有鎖！";
            var rtn = lockBox (user, doorsetting, door.GetDescriptionText ());
            if (rtn.isSuccess)
            {
                //對向門
                var OpsiteRoom = wordMng.GetRoom (doorsetting.RoomID);
                if (OpsiteRoom != null)
                {
                    var OpsiteDoorSetting = OpsiteRoom.Exits.Where (x => x.RoomID == user.Geography.RoomID).FirstOrDefault ();
                    if (OpsiteDoorSetting != null) OpsiteDoorSetting.gateStatus = GateStatus.Locked;
                }
            }
            return rtn.Message;
        }
    }
}
