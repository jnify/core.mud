using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class StandCommandHandler : ICommandHandler
    {
        public string Name => "STAND";
        public string AbbrName => "ST";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            if (user.Status.HasFlag (CreatureStatus.Stand))
            {
                return channel.SendLineAsync ("你想跟僵屍比一比誰比較直嗎?");
            }
            else
            {
                string msg = default;
                if (user.Status.HasFlag (CreatureStatus.Sleep))
                {
                    msg = $"{user.UserDescriptionAndID}醒過來且站了起來。";
                }
                else // if (user.Status.HasFlag (CreatureStatus.Rest))
                {
                    msg = $"{user.UserDescriptionAndID}站了起來。";
                }
                user.Status &= ~CreatureStatus.Sleep;
                user.Status &= ~CreatureStatus.Rest;
                user.Status |= CreatureStatus.Stand;
                return channel.BroadcastToRoom (msg, user.Geography.RoomID);
            }
        }
    }
}
