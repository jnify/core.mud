using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class PromptCommandHandler : ICommandHandler
    {
        public string Name => "PROMPT";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public PromptCommandHandler () { }

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            IdentityInfo idi = loop.Provider.GetService<IStateManager> ().GetIdentityInfo ();
            UserInfo user = loop.Provider.GetService<IUserManager> ().GetUser (idi.Username);
            if (info.ParameterValues.Count () == 0)
            {
                return loop.Provider.GetService<ITextChannel> ().SendLineAsync ($"目前設定：{user.Settings.Prompt}");
            }
            else
            {
                if (info.ParameterString == "ALL")
                {
                    user.Settings.Prompt = "*生命%h 精神%m 體力%v*";
                }
                else
                {
                    user.Settings.Prompt = string.Join (" ", info.ParameterValues);
                }
                return loop.Provider.GetService<ITextChannel> ().SendLineAsync ("修改完成。");
            }
        }
    }
}
