using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class OpenCommandHandler : ICommandHandler
    {
        public string Name => "OPEN";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user.Geography.RoomID);
            if (string.IsNullOrEmpty (info.ParameterString))
                return channel.SendLineAsync ("你要開哪一個門？");

            string ParameterString = info.ParameterString;
            if (ParameterString.Length == 1)
                ParameterString = TextUtil.GetExitName (info.ParameterString);

            if (ExitName.TryParse (ParameterString, true, out ExitName door))
                return channel.SendLineAsync (OpenDoor (wordMng, user, room, door));

            var box = room.GetThing (info.ParameterString);
            if (box != null && box.AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose))
                return channel.SendLineAsync (OpenBox (user, box, box.ShortName));

            return channel.SendLineAsync ($"你想做什麼？");

        }

        private string OpenBox (UserInfo user, BaseObjectCanLock box, string objName)
        {
            switch (box.gateStatus)
            {
                case GateStatus.Open:
                    return $"{objName}已經開了。";
                case GateStatus.Locked:
                    return $"{objName}還鎖著呢！";
                default:
                    box.gateStatus = GateStatus.Open;
                    return $"你打開了{objName}。";
            }
        }

        private string OpenDoor (IWorldManager wordMng, UserInfo user, Room room, ExitName door)
        {
            if (!room.Exits.Any (x => x.Dimension == door) ||
                !room.Exits.Where (x => x.Dimension == door).FirstOrDefault ().AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose))
                return $"{door.GetDescriptionText()}沒有門！";

            var doorsetting = room.Exits.Where (x => x.Dimension == door).FirstOrDefault ();
            switch (doorsetting.gateStatus)
            {
                case GateStatus.Locked:
                    return $"{door.GetDescriptionText()}的門鎖住了！";
                case GateStatus.Open:
                    return $"{door.GetDescriptionText()}的門己經開到不能再開了。";
                default:
                    //操作本身
                    doorsetting.gateStatus = GateStatus.Open;
                    //對向門
                    var OpsiteRoom = wordMng.GetRoom (doorsetting.RoomID);
                    if (OpsiteRoom != null)
                    {
                        var OpsiteDoorSetting = OpsiteRoom.Exits.Where (x => x.RoomID == user.Geography.RoomID).FirstOrDefault ();
                        if (OpsiteDoorSetting != null) OpsiteDoorSetting.gateStatus = GateStatus.Open;
                    }
                    return $"吱～嘎～{door.GetDescriptionText()}的門打開了。";
            }
        }
    }
}
