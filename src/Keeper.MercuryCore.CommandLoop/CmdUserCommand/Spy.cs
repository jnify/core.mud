using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SpyCommandHandler : ICommandHandler
    {
        public string Name => "SPY";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var comTool = loop.Provider.GetService<ICommonTools> ();
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你並沒有查到任何異狀！");

            ExitName exit = info.ParameterValues.First ().ConvertToExitName ();
            if (exit == ExitName.None) return channel.SendLineAsync ("你並沒有查到任何異狀！");

            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user.Geography.RoomID);
            var exitSetting = room.Exits.Where (door => door.Dimension == exit).FirstOrDefault ();
            if (exitSetting == null) return channel.SendLineAsync ("你並沒有查到任何異狀！");
            if (exitSetting.gateStatus != GateStatus.Open) return channel.SendLineAsync ("這門是關著的。");
            var targetRoom = wordMng.GetRoom (exitSetting.RoomID);

            var targetRusers = usrMng.GetRoomUser (targetRoom.RoomID);
            var descript = comTool.DescriptionBuilder (targetRoom, user, targetRusers);
            channel.SendMessageToUser (descript, user.Name);

            //檢查有沒有Spy技能
            PracticedSkill skillattr = user.GetSkill (SkillName.Spy);
            if (skillattr != null)
            {
                //有Spy技能
                var skill = new SkillSpy (skillattr);
                var skillResult = skill.CanExecute (user.CooldownAction, user.GettingImprovement, user.StateCurrent, user.IncreaseSkillTimes);
                if (skillResult == 9) channel.SendMessageToUser ("你的偵察技能進步了。", user.Name);
                if (skillResult == 1) return Task.CompletedTask; //spy成功，直接結束
            }
            //無偵察技能，或是偵察失敗，會被主動攻擊、或記仇的Mob攻擊而停留在該房間
            //處理記仇
            var UserNames = new List<string> () { user.Name };
            var mobs = targetRoom.Mobs.Where (mob => !mob.Status.HasFlag (CreatureStatus.Fighting)).ToList ();
            foreach (var mob in mobs)
            {
                var result = wordMng.CheckGrudges (mob, UserNames);
                if (result.IsGrudges && mob.StartBattle (user))
                {
                    user.ChangeRoom (targetRoom);
                    string startBettelMsg = $"\r\n[1;31m{mob.UserDescriptionAndID}開始攻擊(D){user.UserDescriptionAndID}。[0m";
                    return channel.BroadcastToRoom (startBettelMsg, targetRoom.RoomID);
                }
            }

            //處理主動攻擊
            var mobAutoAtt = targetRoom.Mobs.Where (mob => mob.MobSettings.AutoAttackUser && !mob.Status.HasFlag (CreatureStatus.Fighting)).FirstOrDefault ();
            if (mobAutoAtt != null && mobAutoAtt.StartBattle (user))
            {
                user.ChangeRoom (targetRoom);
                string startBettelMsg = $"\r\n[1;31m{mobAutoAtt.UserDescriptionAndID}開始攻擊(E){user.UserDescriptionAndID}。[0m";
                return channel.BroadcastToRoom (startBettelMsg, targetRoom.RoomID);
            }
            return Task.CompletedTask;
        }

    }
}
