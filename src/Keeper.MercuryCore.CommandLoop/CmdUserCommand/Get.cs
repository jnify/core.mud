using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class GetCommandHandler : ICommandHandler
    {
        public string Name => "GET";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            //get all
            //get all from abc
            //get xxx
            //get xxx from abc
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            string targetObjectName = default;
            string fromObjectName = default;
            BaseObjectContainer fromObject;
            string fromObjectShortName = default;
            bool isGetFromRoom = false;

            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要拿什麼東西？");
            targetObjectName = info.ParameterValues.First ();

            if (info.ParameterValues.Count () > 1 && (string.Compare (info.ParameterValues.Skip (1).First (), "from", true) == 0))
            {
                if (info.ParameterValues.Count () == 2) return channel.SendLineAsync ("你要從哪裏拿東西？");
                fromObjectName = info.ParameterValues.Skip (2).First ();
            }

            if (string.IsNullOrEmpty (fromObjectName))
            {
                fromObject = room;
                isGetFromRoom = true;
            }
            else
            {
                //決定要從哪一個物件拿取，先找身上的容器，再找房間內的容器。
                var obj = user.GetThing (fromObjectName);
                obj = obj?? room.GetThing (fromObjectName);
                if (obj == null) return channel.SendLineAsync ("這裏沒有這個容器讓你拿東西");
                if (!obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGetFrom))
                    return channel.SendLineAsync ($"你沒辦法從{obj.ShortName}拿出任何東西。");

                if (obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock) && obj.gateStatus == GateStatus.Locked)
                    return channel.SendLineAsync ($"{obj.ShortName}鎖住了。");
                if (obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose) && obj.gateStatus != GateStatus.Open)
                    return channel.SendLineAsync ($"{obj.ShortName}是關著的。");

                fromObject = (BaseObjectContainer) obj;
                fromObjectShortName = obj.ShortName;
            }

            if (string.Compare (targetObjectName, "all", true) == 0)
            {
                var objIDs = fromObject.Things.Where (x => x.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGet)).Select (x => (x.ID, x.ShortName)).ToList ();
                if (objIDs.Count () > 0)
                {
                    objIDs.ForEach (objID =>
                    {
                        var isSuccess = fromObject.RemoveByID (objID.ID, out var targetObject);
                        if (isSuccess)
                        {
                            user.AddThing (targetObject);
                            if (isGetFromRoom)
                                channel.BroadcastToRoom ($"{user.UserDescriptionAndID}撿起了{targetObject.ShortName}。", room.RoomID);
                            else
                                channel.BroadcastToRoom ($"{user.UserDescriptionAndID}從{fromObjectShortName}拿出{targetObject.ShortName}。", room.RoomID);
                        }
                        else
                        {
                            if (isGetFromRoom)
                                channel.SendLineAsync ($"你的動作太慢，這裏沒有{objID.ShortName}了。");
                            else
                                channel.SendLineAsync ($"你的動作太慢，{fromObjectShortName}裏沒有{objID.ShortName}了。");
                        }
                    });
                    return Task.CompletedTask;
                }
                else
                {
                    if (isGetFromRoom)
                        return channel.SendLineAsync ($"這裏沒有任何能撿的東西。");
                    else
                        return channel.SendLineAsync ($"{fromObjectShortName}裏頭空空如也。");
                }
            }
            else
            {
                var obj = fromObject.GetThing (targetObjectName);
                if (obj == null)
                {
                    if (isGetFromRoom)
                        return channel.SendLineAsync ("這裏沒這個東西。");
                    else
                        return channel.SendLineAsync ($"{fromObjectShortName}裏沒這個東西。");
                }
                var isSuccess = fromObject.RemoveByID (obj.ID, out var targetObject);
                if (isSuccess)
                {
                    user.AddThing (targetObject);
                    if (isGetFromRoom)
                        return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}撿起了{targetObject.ShortName}。", room.RoomID);
                    else
                        return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}從{fromObjectShortName}拿出{targetObject.ShortName}。", room.RoomID);
                }
                else
                {
                    if (obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGet))
                        return channel.SendLineAsync ($"你的動作太慢，{obj.ShortName}被人捷足先登了。");
                    else
                        return channel.SendLineAsync ($"你拿不走{obj.ShortName}。");
                }
            }
        }
    }
}
