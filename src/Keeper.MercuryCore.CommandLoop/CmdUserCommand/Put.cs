using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class PutCommandHandler : ICommandHandler
    {
        public string Name => "PUT";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            IUserManager usrMng = loop.Provider.GetService<IUserManager> ();
            IWorldManager worldManager = loop.Provider.GetService<IWorldManager> ();
            UserInfo user = channel.ChannelUser;
            var room = worldManager.GetRoom (user.Geography.RoomID);
            string objName = default;
            string toObjectName = default;
            //Put xxx into yyy, put xxx in yyy
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要放什麼東西？");
            objName = info.ParameterValues.First ();

            if (info.ParameterValues.Count () == 1) return channel.SendLineAsync ("你打算放到哪裏？");
            if (string.Compare (info.ParameterValues.Skip (1).First (), "in", true) == 0 ||
                string.Compare (info.ParameterValues.Skip (1).First (), "into", true) == 0
            )
            {
                if (info.ParameterValues.Count () == 2) return channel.SendLineAsync ("你打算放到哪裏？");
                toObjectName = info.ParameterValues.Skip (2).First ();
            }
            else
            {
                toObjectName = info.ParameterValues.Skip (1).First ();
            }

            var obj = user.GetThing (objName);
            if (obj == null) return channel.SendLineAsync ("你沒有這樣東西。");

            var toObject = user.GetThing (toObjectName);
            toObject = toObject ?? room.GetThing (toObjectName);
            if (toObject == null) return channel.SendLineAsync ("這裏沒有這樣東西。");

            if (!toObject.AcceptVerbs.HasFlag (ObjectActionsVerb.CanPutIn))
                return channel.SendLineAsync ($"你沒辦法把{obj.ShortName}放進{toObject.ShortName}");

            if (obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanPutIn))
                return channel.SendLineAsync ($"你不能夠把{obj.ShortName}放進{toObject.ShortName}");

            var isSuccess = user.RemoveByID (obj.ID, out var transferObj);
            if (!isSuccess) return channel.SendLineAsync ($"{obj.ShortName}離不開你！！");

            toObject.AddThing (transferObj);
            return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}把{transferObj.ShortName}放進{toObject.ShortName}", user.Geography.RoomID);

        }
    }
}
