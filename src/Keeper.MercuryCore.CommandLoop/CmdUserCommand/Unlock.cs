using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class UnlockCommandHandler : ICommandHandler
    {
        public string Name => "UNLOCK";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var user = channel.ChannelUser;
            var room = wordMng.GetRoom (user.Geography.RoomID);

            if (string.IsNullOrEmpty (info.ParameterString))
                return channel.SendLineAsync ("你要解哪裏的鎖？");

            string ParameterString = info.ParameterString;
            if (ParameterString.Length == 1)
                ParameterString = TextUtil.GetExitName (info.ParameterString);

            if (ExitName.TryParse (ParameterString, true, out ExitName door))
                return channel.SendLineAsync (UnlockDoor (wordMng, user, room, door));

            var box = room.GetThing (info.ParameterString);
            if (box != null)
                if (box.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                    return channel.SendLineAsync (UnlockBox (user, box, box.ShortName).Message);
                else
                    return channel.SendLineAsync ($"你想做什麼？");

            return channel.SendLineAsync ($"你想做什麼？");
        }
        private (string Message, bool isSuccess) UnlockBox (UserInfo user, BaseObjectCanLock box, string objName)
        {
            var unlockResult = box.Unlock (user);
            switch (unlockResult)
            {
                case UnlockResult.Success:
                    box.gateStatus = GateStatus.Close;
                    return ($"{objName}的鎖打開了。", true);
                case UnlockResult.NoLocked:
                    return ($"{objName}並沒有上鎖。", false);
                default:
                    return ($"{objName}需要鑰匙。", false);
            }
        }

        private string UnlockDoor (IWorldManager wordMng, UserInfo user, Room room, ExitName door)
        {
            if (!room.Exits.Any (x => x.Dimension == door) ||
                !room.Exits.Where (x => x.Dimension == door).FirstOrDefault ().AcceptVerbs.HasFlag (ObjectActionsVerb.CanOpenClose))
                return $"{door.GetDescriptionText()}沒有門！";

            var doorsetting = room.Exits.Where (x => x.Dimension == door).FirstOrDefault ();
            if (!doorsetting.AcceptVerbs.HasFlag (ObjectActionsVerb.CanLock))
                return $"{door.GetDescriptionText()}上沒有鎖！";

            //訊息以操作目的為主，對向門只是順帶而已
            var rtn = UnlockBox (user, doorsetting, door.GetDescriptionText ());
            if (rtn.isSuccess)
            {
                //對向門
                var OpsiteRoom = wordMng.GetRoom (doorsetting.RoomID);
                if (OpsiteRoom != null)
                {
                    var OpsiteDoorSetting = OpsiteRoom.Exits.Where (x => x.RoomID == user.Geography.RoomID).FirstOrDefault ();
                    if (OpsiteDoorSetting != null) OpsiteDoorSetting.gateStatus = GateStatus.Close;
                }
            }
            return rtn.Message;
        }
    }
}
