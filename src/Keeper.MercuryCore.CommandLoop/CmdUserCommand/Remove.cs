using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class RemoveCommandHandler : ICommandHandler
    {
        public string Name => "REMOVE";
        public string AbbrName => "RM";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            if (info.ParameterValues.Count () == 0)
            {
                return channel.SendLineAsync ("你要脫下哪一個裝備？");
            }
            else
            {
                var user = channel.ChannelUser;
                var obj = user.GetEQThing (info.ParameterString);
                if (obj == null)
                {
                    return channel.SendLineAsync ("你沒有那樣東西。");
                }
                else
                {
                    user.AddThing (obj);
                    user.EQArea.Remove (user.EQArea.Where (x => x.Value.ID == obj.ID).First ().Key);
                    string msg = $"{user.UserDescriptionAndID}卸下了{obj.ShortName}";
                    return channel.BroadcastToRoom (msg, user.Geography.RoomID);
                }
            }
        }

    }
}
