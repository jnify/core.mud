using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class PracticeCommandHandler : ICommandHandler
    {
        public string Name => "PRACTICE";
        public string AbbrName => "PRACT";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            string msg = default;
            //pract
            //pract from MobName
            //pract skillname from mobname
            var result = info.ParameterString.SpliteFrom ();
            string skillName = result.Key;
            string mobname = result.Value;
            if (string.IsNullOrWhiteSpace (skillName) || string.IsNullOrWhiteSpace (mobname))
            {
                //輸入的參數，直接當成Master Name
                if (string.IsNullOrWhiteSpace (mobname)) mobname = skillName;
                MobInfo master = null;
                if (!string.IsNullOrWhiteSpace (mobname)) master = room.GetMob (mobname);

                if (master == null)
                    msg = DisplaySkillPracticedFromUser (user);
                else
                    msg = DisplaySkillPracticeFromMaster (user, room, master);
            }
            else
            { //向導師學習技能
                msg = PractSkillFromMob (user, room, skillName, mobname);
            }
            return channel.SendLineAsync (msg);
        }
        private string DisplaySkillPracticedFromUser (UserInfo creature)
        {
            string msg = "技能列表：";
            creature.PracticeSkills.Sort (delegate (PracticedSkill x, PracticedSkill y)
            {
                var xskill = x.SkillAttr;
                var yskill = y.SkillAttr;
                if (xskill.skillCategory == yskill.skillCategory) return 0;
                else return xskill.skillCategory.CompareTo (yskill.skillCategory);
            });
            string skillsystemName = default;
            bool sameLine = true;
            creature.PracticeSkills.ForEach (pract =>
            {
                var skillAttr = pract.SkillAttr;
                if (skillsystemName != skillAttr.skillCategory.ToString ())
                {
                    msg += $"\r\n{skillAttr.skillCategory.GetDescriptionText()}技能";
                    skillsystemName = skillAttr.skillCategory.ToString ();
                    sameLine = true;
                }
                string eName = pract.Skillname.ToString ().SplitByUpperCase ().LeftPadSpace (18);
                string cName = skillAttr.Description.LeftPadSpace (12);
                if (sameLine)
                {
                    msg += $"\r\n{eName}{cName} {pract.Proficiency}%   ";
                    sameLine = false;
                }
                else
                {
                    msg += $"{eName}{cName} {pract.Proficiency}%";
                    sameLine = true;
                }
            });
            msg += $"\r\n你一共學了{creature.PracticeSkills.Count()}種技能，還有{creature.Points.Practice}次練習能力。";
            return msg;

        }
        private string DisplaySkillPracticeFromMaster (UserInfo user, Room room, MobInfo Trainer)
        {
            if (Trainer.Role != MobRole.Trainer)
                return $"{Trainer.Settings.Name} 告訴你：啊？你想對我做什麼？";

            if (!TextUtil.IsSameCareer (user.Career, Trainer.Career))
                return $"{Trainer.Settings.Name} 告訴你：你並不是我這個公會中的人！";

            return DoDisplaySkillPracticeFromMaster (user, Trainer);
        }
        private string PractSkillFromMob (UserInfo user, Room room, string skillName, string TrainerName)
        {
            //Get Trainer
            MobInfo Trainer = null;
            if (string.IsNullOrWhiteSpace (TrainerName))
            {
                Trainer = room.Mobs.Where (x => x.Role == MobRole.Trainer).FirstOrDefault ();
                if (Trainer == null) return ("這裏沒有任何導師。");
            }
            else
            {
                Trainer = room.GetMob (TrainerName);
                if (Trainer == null) return ("你找不到這個導師。");
            }

            if (Trainer.Role != MobRole.Trainer)
                return $"{Trainer.Settings.Name} 告訴你：啊？你想對我做什麼？";

            if (!TextUtil.IsSameCareer (user.Career, Trainer.Career))
                return $"{Trainer.Settings.Name} 告訴你：你並不是我這個公會中的人！";

            if (user.Points.Practice == 0) return "你的練習點數不夠！";
            return DoPractSkillFromMob (user, Trainer, skillName);
        }
        private string DoPractSkillFromMob (UserInfo user, MobInfo master, string skillname)
        {
            skillname = skillname.TrimAllSpace ();
            var haveSkill = SkillName.TryParse (skillname, true, out SkillName MasterSkill);
            if (!haveSkill) return $"{master.Settings.Name} 告訴你：我沒有什麼可以教你的！(2)";

            var masterPractSkill = master.GetSkill (MasterSkill);
            if (masterPractSkill == null) return $"{master.Settings.Name} 告訴你：我沒有什麼可以教你的！(3)";

            var UserCurrentAttribute = user.GetCreatureAttribute ();

            var masterSkillAttr = masterPractSkill.SkillAttr;
            var userLearnSkillCategory = user.GetSkillCategory (masterPractSkill);
            if (userLearnSkillCategory == null) return $"你的要先學會技能{masterSkillAttr.skillCategory}才行。";

            if (userLearnSkillCategory.Level < masterSkillAttr.LearnSkillMinLevel)
                return $"你的技能{masterSkillAttr.skillCategory}要先達到{masterSkillAttr.LearnSkillMinLevel}級才行。";

            int increaseNumber = UtilityExperence.IncreaseProficiency (UserCurrentAttribute);
            var pract = user.GetSkill (MasterSkill);

            if (pract == null)
            {
                pract = new PracticedSkill () { Skillname = MasterSkill, Proficiency = 0 };
                user.PracticeSkills.Add (pract);
            }

            if (pract.Proficiency >= masterPractSkill.Proficiency)
                return $"你的技能{masterSkillAttr.Description}已經超越過我了。";

            if (pract.Proficiency >= masterSkillAttr.ProficiencyMax)
                return $"你的技能{masterSkillAttr.Description}無法再進步了。";

            pract.Proficiency += increaseNumber;
            //不能超過熟練度上限
            if (pract.Proficiency >= masterSkillAttr.ProficiencyMax) pract.Proficiency = masterSkillAttr.ProficiencyMax;
            user.Points.Practice--;

            return $"你的{masterSkillAttr.Description}技能進步了。";
        }
        private string DoDisplaySkillPracticeFromMaster (UserInfo user, Creature master)
        {
            string msg = "技能列表：";
            master.PracticeSkills.Sort (delegate (PracticedSkill x, PracticedSkill y)
            {
                var xskill = x.SkillAttr;
                var yskill = y.SkillAttr;
                if (xskill.skillCategory == yskill.skillCategory) return 0;
                else return xskill.skillCategory.CompareTo (yskill.skillCategory);
            });
            string skillsystemName = default;
            bool sameLine = true;
            master.PracticeSkills.ForEach (masterPract =>
            {
                var masterSkillAttr = masterPract.SkillAttr;
                var userSkillPract = user.GetSkill (masterPract.Skillname);
                int userProficiency = 0;
                if (userSkillPract != null) userProficiency = userSkillPract.Proficiency;
                if (skillsystemName != masterSkillAttr.skillCategory.ToString ())
                {
                    msg += $"\r\n{masterSkillAttr.skillCategory.GetDescriptionText()}技能";
                    skillsystemName = masterSkillAttr.skillCategory.ToString ();
                    sameLine = true;
                }
                string eName = masterPract.Skillname.ToString ().SplitByUpperCase ().LeftPadSpace (18);
                string cName = masterSkillAttr.Description.LeftPadSpace (12);
                if (sameLine)
                {
                    msg += $"\r\n";
                    sameLine = false;
                }
                else
                {
                    sameLine = true;
                }
                msg += $"{eName}{cName} {userProficiency}%   ";
            });
            msg += $"\r\n你一共學了{user.PracticeSkills.Count()}種技能，還有{user.Points.Practice}次練習能力。";
            return msg;
        }
    }
}
