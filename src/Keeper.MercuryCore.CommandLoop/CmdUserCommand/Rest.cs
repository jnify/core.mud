using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class RestCommandHandler : ICommandHandler
    {
        public string Name => "REST";
        public string AbbrName => "R";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying | DenyRestSleep.DenyFighting;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            if (user.Status.HasFlag (CreatureStatus.Rest))
            {
                return channel.SendLineAsync ("你已經正在休息了，不是嗎？難道還有其他的事情讓你累得如此糊塗？");
            }
            else
            {
                user.Status &= ~CreatureStatus.Stand;
                user.Status &= ~CreatureStatus.Sleep;
                user.Status |= CreatureStatus.Rest;
                return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}坐下來休息休息。", user.Geography.RoomID);
            }
        }
    }
}
