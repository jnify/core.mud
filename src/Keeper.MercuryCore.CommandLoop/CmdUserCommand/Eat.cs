using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class EatCommandHandler : ICommandHandler
    {
        public string Name => "EAT";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            string actionverb = "吃";
            var verb = ObjectActionsVerb.CanEat;
            if (string.IsNullOrWhiteSpace (info.ParameterString)) return channel.SendLineAsync ($"你想{actionverb}什麼？");

            var obj = user.GetThing (info.ParameterString);
            if (obj == null) return channel.SendLineAsync ($"你沒有那個東西。");

            if (!obj.AcceptVerbs.HasFlag (verb)) return channel.SendLineAsync ($"你一點也不想{actionverb}它。");

            ActionKeyParameter actionpara = new ActionKeyParameter (user, room);
            obj.ReActCommand (actionpara, obj);
            return Task.CompletedTask;
        }
    }
}
