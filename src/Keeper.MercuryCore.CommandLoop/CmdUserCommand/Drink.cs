using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class DrinkCommandHandler : ICommandHandler
    {
        public string Name => "DRINK";
        public string AbbrName => "DR";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            CommonObject obj = default;

            //試著找水喝
            if (string.IsNullOrWhiteSpace (info.ParameterString))
            {
                obj = room.Things.Where (x => x.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink)).FirstOrDefault ();
                obj = obj ?? user.Things.Where (x => x.AcceptVerbs.HasFlag (ObjectActionsVerb.CanDrink)).FirstOrDefault ();
            }
            else
            {
                obj = user.GetThing (info.ParameterString);
            }

            if (CheckVerb (obj, ObjectActionsVerb.CanDrink, "喝", out string Message))
            {
                ActionKeyParameter actionpara = new ActionKeyParameter (user, room);
                obj.ReActCommand (actionpara, obj);
                return Task.CompletedTask;
            }
            else
            {
                return channel.SendLineAsync (Message);
            }
        }

        private bool CheckVerb (CommonObject obj, ObjectActionsVerb objAcceptVerb, string verb, out string message)
        {
            if (obj == null)
            {
                message = $"你想{verb}什麼！";
                return false;
            }
            else
            {
                if (!obj.AcceptVerbs.HasFlag (objAcceptVerb))
                {
                    message = $"它不是用來{verb}的！！！ ";
                    return false;
                }
                else
                {
                    message = default;
                    return true;
                }
            }
        }
    }
}
