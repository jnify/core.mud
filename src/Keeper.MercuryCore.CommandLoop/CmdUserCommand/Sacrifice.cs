using System;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SacrificeCommandHandler : ICommandHandler
    {
        public string Name => "SACRIFICE";
        public string AbbrName => "SAC";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        private readonly static Random rnd = new Random ();

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            if (string.IsNullOrWhiteSpace (info.ParameterString))
            {
                return channel.SendLineAsync ($"你想奉獻什麼？");
            }
            else
            {
                var obj = room.GetThing (info.ParameterString);
                if (obj == null)
                {
                    return channel.SendLineAsync ($"這裏沒那個東西。");
                }
                else
                {
                    if (!obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanGet))
                    {
                        return channel.SendLineAsync ($"你不能奉獻它！！");
                    }
                    else
                    {
                        if (!room.RemoveByID (obj.ID, out var rtn))
                        {
                            return channel.SendLineAsync ("你手腳太慢了。");
                        }
                        else
                        {
                            int addType = rnd.Next (2);
                            channel.BroadcastToRoom ($"{user.UserDescriptionAndID}向[1;37m旅者之神但丁[0m奉獻{rtn.ShortName}", user.Geography.RoomID, user.Name);
                            rtn = null;

                            switch (addType)
                            {
                                case 0:
                                    int min = user.StateMax.HP / 20;
                                    int max = user.StateMax.HP / 10;
                                    int num = rnd.Next (min, max);
                                    user.StateCurrent.HP += num;
                                    if (user.StateCurrent.HP > user.StateMax.HP) user.StateCurrent.HP = user.StateMax.HP;
                                    return channel.SendLineAsync ("[1;37m旅者之神但丁[0m接受了你的奉獻, 祂恢復你的一些生命以作為獎勵。");
                                case 1:
                                    min = user.StateMax.MV / 20;
                                    max = user.StateMax.MV / 10;
                                    num = rnd.Next (min, max);
                                    if (user.StateCurrent.MV > user.StateMax.MV) user.StateCurrent.MV = user.StateMax.MV;
                                    return channel.SendLineAsync ("[1;37m旅者之神但丁[0m接受了你的奉獻, 祂恢復你的一些體力以作為獎勵。");
                                default:
                                    min = user.StateMax.Mana / 20;
                                    max = user.StateMax.Mana / 10;
                                    num = rnd.Next (min, max);
                                    user.StateCurrent.Mana += num;
                                    if (user.StateCurrent.Mana > user.StateMax.Mana) user.StateCurrent.Mana = user.StateMax.Mana;
                                    return channel.SendLineAsync ("[1;37m旅者之神但丁[0m接受了你的奉獻, 祂恢復你的一些精神以作為獎勵。");
                            }
                        }
                    }
                }
            }
        }
    }
}
