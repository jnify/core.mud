using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Loader.Mob;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.SkillDefinition;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class TrainCommandHandler : ICommandHandler
    {
        public string Name => "TRAIN";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyFighting | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var mobloader = loop.Provider.GetService<IMobDefinationLoader> ();
            string msg = default;
            //train
            //train from MobName
            //train skillname from mobname
            var result = info.ParameterString.SpliteFrom ();
            string skillName = result.Key;
            string mobname = result.Value;
            if (string.IsNullOrWhiteSpace (skillName) || string.IsNullOrWhiteSpace (mobname))
            {
                //輸入的參數，直接當成Master Name
                if (string.IsNullOrWhiteSpace (mobname)) mobname = skillName;
                MobInfo master = null;
                if (!string.IsNullOrWhiteSpace (mobname)) master = room.GetMob (mobname);
                //train
                //train from yyy
                //結構不為train xxx from yyy的話，顯示技能分類
                if (master == null)
                    msg = DisplaySkillCategoriesLearned (user);
                else
                    msg = DisplayTrainerSkillCategory (user, room, mobloader, master);
            }
            else
            { //向導師學習技能
                msg = TrainSkillSystem (user, room, mobloader, skillName, mobname);
            }
            return channel.SendLineAsync (msg);
        }

        private string DisplaySkillCategoriesLearned (Creature creature)
        {
            string msg = "技能系統：";
            string skillTitleName = "名稱";
            msg += $"\r\n{skillTitleName.LeftPadSpace(20)}  等級";
            creature.LearnedSkillSystem.ForEach (x =>
            {
                string word = $"{x.skillCategory.GetDescriptionText()}({x.skillCategory.ToString()})";
                msg += $"\r\n{word.LeftPadSpace(20)}  {x.Level}";
            });
            return msg;
        }

        private string DisplayTrainerSkillCategory (UserInfo user, Room room, IMobDefinationLoader mobloader, MobInfo Trainer)
        {

            if (Trainer.Role != MobRole.Trainer)
                return $"{Trainer.Settings.Name} 告訴你：啊？你想對我做什麼？";

            if (!TextUtil.IsSameCareer (user.Career, Trainer.Career))
                return $"{Trainer.Settings.Name} 告訴你：你並不是我這個公會中的人！";

            var md = mobloader.GetDef (Trainer.Name);
            var skills = md.LearnedSkillSystem;
            string msg = default;
            skills.ForEach (x =>
            {
                var userSkillSystem = user.LearnedSkillSystem.Where (y => y.skillCategory == x.skillCategory).FirstOrDefault ();
                var masterSkillSystem = md.LearnedSkillSystem.Where (y => y.skillCategory == x.skillCategory).FirstOrDefault ();
                if (userSkillSystem == null || (userSkillSystem.Level < masterSkillSystem.Level))
                {
                    int skillLevel = 1;
                    int currentExp = 0;
                    if (userSkillSystem != null)
                    {
                        skillLevel = userSkillSystem.Level + 1;
                        currentExp = userSkillSystem.Experience;
                    }
                    int SkillMaxLevel = SkillLimitation.GetSkillCategoryLimit (user, masterSkillSystem.skillCategory);
                    if (skillLevel <= SkillMaxLevel)
                    {
                        bool isEnoughGoToNextLevel = UtilityExperence.CheckNextLevelExperence (skillLevel, currentExp);
                        if (isEnoughGoToNextLevel)
                            msg += $"{Trainer.Settings.Name} 告訴你：只要 {(md.TrainingFeeBase * skillLevel * 10).ToCoins()} 枚我就教你下一級的{x.skillCategory.GetDescriptionText()}技能({x.skillCategory})。\r\n";
                        else
                            msg += $"{Trainer.Settings.Name} 告訴你：還需要 {UtilityExperence.GetNextLevelNeedExperence (skillLevel, currentExp)} 點經驗值，你的{x.skillCategory.GetDescriptionText()}技能({x.skillCategory})就可以再昇一級。\r\n";
                    }
                }
            });
            if (string.IsNullOrWhiteSpace (msg)) return $"{Trainer.Settings.Name} 告訴你：我沒有什麼可以教你的！！！";;
            return msg;
        }
        private string TrainSkillSystem (UserInfo user, Room room, IMobDefinationLoader mobloader, string skillsystemname, string TrainerName)
        {

            var Trainer = room.GetMob (TrainerName);
            if (Trainer == null) return "這裏沒有這個人";

            if (Trainer.Role != MobRole.Trainer)
                return $"{Trainer.Settings.Name} 告訴你：啊？你想對我做什麼？";

            if (!TextUtil.IsSameCareer (user.Career, Trainer.Career))
                return $"{Trainer.Settings.Name} 告訴你：你並不是我這個公會中的人！";

            if (!Enum.TryParse<SkillCategory> (skillsystemname.TrimAllSpace (), true, out var skillCategory))
                return $"{Trainer.Settings.Name} 告訴你：ㄟ... 那不是你能夠學的 %^&#$#%^...."; //無此技能設定

            var MasterSkillCategory = Trainer.LearnedSkillSystem.Where (x => x.skillCategory == skillCategory).FirstOrDefault ();
            if (MasterSkillCategory == null)
                return $"{Trainer.Settings.Name} 告訴你：我沒有什麼可以教你的！"; //該導師不會該技能

            //找出User要Train到哪一個Level
            var skillsystem = user.LearnedSkillSystem.Where (x => x.skillCategory == skillCategory).FirstOrDefault ();
            int skillLevel = 1;
            if (skillsystem != null) skillLevel = skillsystem.Level + 1;

            if (skillLevel > MasterSkillCategory.Level)
                return $"{Trainer.Settings.Name} 告訴你：我沒有什麼可以教你的！！"; //超出該導師技能上限

            int SkillMaxLevel = user.GetSkillCategoryMaxLevel (skillCategory);
            if (skillLevel > SkillMaxLevel)
                return $"{Trainer.Settings.Name} 告訴你：你在{skillCategory.GetDescriptionText()}的技能方面無法再進步了。"; //超出技能上限

            //算出Training費用
            var md = mobloader.GetDef (Trainer.Name);
            var trainFee = md.TrainingFeeBase * skillLevel * 10;
            if (user.Coins < trainFee)
            {
                return $"{Trainer.Settings.Name} 告訴你：很抱歉, 多賺點錢再來吧！";
            }

            if (skillLevel == 1)
            {
                //等級1不用檢查昇級所需
                var learnedSkillSystemattribute = new LearnedSkillCategory (skillCategory, skillLevel, 0) { };
                user.LearnedSkillSystem.Add (learnedSkillSystemattribute);
            }
            else
            {
                bool isEnoughGoToNextLevel = UtilityExperence.CheckNextLevelExperence (skillLevel, skillsystem.Experience);
                if (!isEnoughGoToNextLevel)
                    return $"{Trainer.Settings.Name} 告訴你：你在{skillCategory.GetDescriptionText()}技能方面的經驗尚嫌不足。";

                user.Coins -= trainFee; //付錢
                var userSkillsystem = user.LearnedSkillSystem.Where (x => x.skillCategory == skillCategory).FirstOrDefault ();
                userSkillsystem.Level++;
                userSkillsystem.Experience = UtilityExperence.ConductSkillSystemExperence (skillLevel, skillsystem.Experience);
            }
            string msg = $"你付了{trainFee.ToCoins()}的費用.\r\n";
            msg += $"{Trainer.Settings.Name} 告訴你：你在{skillCategory.GetDescriptionText()}技能方面晉升一級！";

            if (skillLevel == SkillMaxLevel)
                msg += $"\r\n{Trainer.Settings.Name} 告訴你：你在{skillCategory.GetDescriptionText()}的技能方面無法再進步了。"; //超出技能上限

            return msg;
        }
    }
}
