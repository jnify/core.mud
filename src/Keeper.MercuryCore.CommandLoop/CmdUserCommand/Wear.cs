using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class WearCommandHandler : ICommandHandler
    {
        public string Name => "WEAR";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            if (info.ParameterValues.Count () == 0) return channel.SendLineAsync ("你要穿戴什麼？");

            var user = channel.ChannelUser;
            var obj = user.GetThing (info.ParameterString);
            if (obj == null) return channel.SendLineAsync ("你沒有那樣東西。");

            if (user.Level < obj.Level) return channel.SendLineAsync ($"你必須等到 {obj.Level} 級以後才能使用。");

            Task wear = null;
            if (!obj.AcceptVerbs.HasFlag (ObjectActionsVerb.CanWear)) return channel.SendLineAsync ("你身上沒有適合的地方可以戴上這玩意兒。");

            if (obj.Type != WeaponType.None)
            {
                wear = WareWeapon (channel, user, obj);
            }
            else
            {
                wear = WareDefence (channel, user, obj);
            }
            return wear.ContinueWith ((task) =>
            {
                CalculatorAttackVolume (user);
            });

        }

        private void CalculatorAttackVolume (UserInfo user)
        {

        }

        private Task WareDefence (ITextChannel channel, UserInfo user, CommonObject obj)
        {
            string msg = default;
            string wearVerb = default;
            switch (obj.Position)
            {
                case EquipmentAllowPosition.Body: //穿
                case EquipmentAllowPosition.Gloves:
                case EquipmentAllowPosition.Cape:
                case EquipmentAllowPosition.Boots:
                    wearVerb = "穿";
                    break;
                case EquipmentAllowPosition.Neck: //圍在XX上
                    wearVerb = "圍";
                    break;
                default:
                    wearVerb = "戴";
                    break;
            }

            //移除身上現有的裝備
            switch (obj.Position)
            {
                case EquipmentAllowPosition.Head:
                case EquipmentAllowPosition.Ligth:
                case EquipmentAllowPosition.Body:
                case EquipmentAllowPosition.Gloves:
                case EquipmentAllowPosition.Cape:
                case EquipmentAllowPosition.Belt:
                case EquipmentAllowPosition.Leggings:
                    msg += RemoveEquipmentFromBodyToThings (user, obj.Position);
                    msg += MoveEquipmentFromThingsToEQarea (user, wearVerb, obj);
                    break;
                case EquipmentAllowPosition.Ear:
                    msg += RemoveEquipmentHave2Position (user, obj, EquipmentAllowPosition.Ear_1);
                    msg += MoveEquipmentFromThingsToEQarea (user, wearVerb, obj, EquipmentAllowPosition.Ear_1);
                    break;
                case EquipmentAllowPosition.Neck:
                    msg += RemoveEquipmentHave2Position (user, obj, EquipmentAllowPosition.Neck_1);
                    msg += MoveEquipmentFromThingsToEQarea (user, wearVerb, obj, EquipmentAllowPosition.Neck_1);
                    break;
                case EquipmentAllowPosition.Finger:
                    msg += RemoveEquipmentHave2Position (user, obj, EquipmentAllowPosition.Finger_1);
                    msg += MoveEquipmentFromThingsToEQarea (user, wearVerb, obj, EquipmentAllowPosition.Finger_1);
                    break;
                case EquipmentAllowPosition.Bracelet:
                    msg += RemoveEquipmentHave2Position (user, obj, EquipmentAllowPosition.Bracelet_1);
                    msg += MoveEquipmentFromThingsToEQarea (user, wearVerb, obj, EquipmentAllowPosition.Bracelet_1);
                    break;
                default:
                    msg += $"沒寫到{obj.Position}這個部位。";
                    break;
            }
            user.EQArea = user.EQArea.OrderBy (x => x.Key).ToDictionary (o => o.Key, p => p.Value);
            return channel.BroadcastToRoom (msg, user.Geography.RoomID);
        }

        private string MoveEquipmentFromThingsToEQarea (UserInfo user, string verb, CommonObject obj, EquipmentAllowPosition copyPosition = EquipmentAllowPosition.None)
        {
            if (!user.EQArea.ContainsKey (obj.Position.ToString ()))
            {
                user.RemoveByID (obj.ID, out var nouse);
                user.EQArea.Add (obj.Position.ToString (), obj);
                return $"{user.UserDescriptionAndID}把{obj.ShortName}{verb}在{obj.Position.GetDescriptionText()}上。";
            }
            else if (copyPosition != EquipmentAllowPosition.None && !user.EQArea.ContainsKey (copyPosition.ToString ()))
            {
                user.RemoveByID (obj.ID, out var nouse);
                user.EQArea.Add (copyPosition.ToString (), obj);
                return $"{user.UserDescriptionAndID}把{obj.ShortName}{verb}在{copyPosition.GetDescriptionText()}上。";
            }
            else
            {
                return $"你沒有多餘的{obj.Position.GetDescriptionText()}可以裝備。";
            }
        }

        private string RemoveEquipmentHave2Position (UserInfo user, CommonObject obj, EquipmentAllowPosition copy)
        {
            //如果兩個位置都滿，則移除右邊
            if (user.EQArea.ContainsKey (obj.Position.ToString ()) && user.EQArea.ContainsKey (copy.ToString ()))
            {
                return RemoveEquipmentFromBodyToThings (user, obj.Position);
            }
            return "";
        }

        private string RemoveEquipmentFromBodyToThings (UserInfo user, EquipmentAllowPosition _position)
        {
            if (user.EQArea.ContainsKey (_position.ToString ()))
            {
                var removeObject = user.EQArea[_position.ToString ()];
                user.AddThing (removeObject);
                user.EQArea.Remove (_position.ToString ());
                return $"{user.UserDescriptionAndID}把{removeObject.ShortName}拿了下來。\r\n";
            }
            return "";
        }

        private Task WareWeapon (ITextChannel channel, UserInfo user, CommonObject obj)
        {
            string msg = default;
            //取得已裝備件數
            int count = user.EQArea.Where (x => x.Value.Type != WeaponType.None).Count ();
            //取得欲裝備之單雙手屬性
            bool is2HandWeapon = _is2HandWeapon (obj.Type);

            //兩隻手都有拿裝備時，先移除右手的裝備
            if (count > 1)
                msg += RemoveEquipmentFromBodyToThings (user, EquipmentAllowPosition.HandRight);

            //裝備雙手武器時移除雙手的裝備
            if (is2HandWeapon)
            {
                msg += RemoveEquipmentFromBodyToThings (user, EquipmentAllowPosition.HandRight);
                msg += RemoveEquipmentFromBodyToThings (user, EquipmentAllowPosition.HandLeft);
            }

            //開始裝備，先處理右手
            if (!user.EQArea.Keys.Contains (EquipmentAllowPosition.HandRight.ToString ()))
            {
                msg += MoveWeaponFromThingsToBody (user, obj, EquipmentAllowPosition.HandRight);
            }
            else if (!user.EQArea.Keys.Contains (EquipmentAllowPosition.HandLeft.ToString ()))
            {
                msg += MoveWeaponFromThingsToBody (user, obj, EquipmentAllowPosition.HandLeft);
            }
            else
            {
                msg += $"{user.UserDescriptionAndID}沒有第三隻手可以拿武器！！";
            }
            user.EQArea = user.EQArea.OrderBy (x => x.Key).ToDictionary (o => o.Key, p => p.Value);

            return channel.BroadcastToRoom (msg, user.Geography.RoomID);
        }

        private string MoveWeaponFromThingsToBody (UserInfo user, CommonObject obj, EquipmentAllowPosition targetPosition)
        {
            user.RemoveByID (obj.ID, out var nouse);
            user.EQArea.Add (targetPosition.ToString (), obj);
            return $"{user.UserDescriptionAndID}把{obj.ShortName}拿在{targetPosition.GetDescriptionText()}作為武器。";
        }

        private bool _is2HandWeapon (WeaponType t)
        {
            switch (t)
            {
                case WeaponType.Sword2hd:
                case WeaponType.Hammer:
                case WeaponType.Staff:
                    return true;
                default:
                    return false;
            }
        }
    }
}
