using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class EquipmentCommandHandler : ICommandHandler
    {
        public string Name => "EQUIPMENT";
        public string AbbrName => "EQ";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var msg = ((Creature) user).GetLookEquipment ();
            return channel.SendLineAsync ("你" + msg);
        }
    }
}
