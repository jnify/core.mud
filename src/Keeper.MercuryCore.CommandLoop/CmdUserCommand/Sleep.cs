using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SleepCommandHandler : ICommandHandler
    {
        public string Name => "SLEEP";
        public string AbbrName => "SL";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyFighting | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            if (user.Status.HasFlag (CreatureStatus.Sleep))
            {
                return channel.SendLineAsync ("你睡得正香甜呢！什麼！你在夢中也還要睡! 不會是做了....");
            }
            else
            {
                return channel.BroadcastToRoom ($"{user.UserDescriptionAndID}躺下來睡覺。", user.Geography.RoomID).ContinueWith ((x) =>
                {
                    user.Status &= ~CreatureStatus.Stand;
                    user.Status &= ~CreatureStatus.Rest;
                    user.Status |= CreatureStatus.Sleep;
                });
            }
        }
    }
}
