using System;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class VisibleCommandHandler : ICommandHandler
    {
        public string Name => "Visible";
        public string AbbrName => "Vis";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var user = channel.ChannelUser;
            user.CancelHideSneakEffect ();
            return channel.SendLineAsync ("你靜悄悄不動聲色的顯露了身形。");

        }
    }
}
