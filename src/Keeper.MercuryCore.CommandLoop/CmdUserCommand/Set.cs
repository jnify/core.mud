using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SetCommandHandler : ICommandHandler
    {
        public string Name => "SET";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public SetCommandHandler () { }

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            if (info.ParameterValues.Count () > 0)
            {
                if (info.ParameterValues.Count () == 1)
                {
                    return UpdateBoolAttribute (channel,
                        user.Settings,
                        info.ParameterValues.FirstOrDefault ());
                }
                else
                {
                    return UpdateBoolAttribute (channel,
                        user.Settings,
                        info.ParameterValues.FirstOrDefault (),
                        string.Join (" ", info.ParameterValues.Skip (1)));
                }
            }
            else
            {
                return buildSettings (channel, user);
            }
        }

        private Task UpdateBoolAttribute (ITextChannel channel, UserConfiguration setting, string attributeName, string value = default)
        {
            var props = typeof (UserConfiguration).GetProperties ();
            PropertyInfo prop = null;
            foreach (var p in props)
            {
                if (p.Name.ToUpperInvariant () == attributeName.ToUpperInvariant ())
                {
                    prop = p;
                    break;
                }
            }
            if (prop != null)
            {
                switch (prop.PropertyType.Name)
                {
                    case "Boolean":
                        bool resultbool = (bool) prop.GetValue (setting);
                        prop.SetValue (setting, !resultbool);
                        return channel.SendLineAsync ($"設定已{TextUtil.GetColorWord(!resultbool)}。");
                    case "Int32":
                        if (int.TryParse (value, out var intValue))
                        {
                            prop.SetValue (setting, intValue);
                            return channel.SendLineAsync ($"已設定完成。");
                        }
                        else
                        {
                            return channel.SendLineAsync ($"它需要是一個數字！");
                        }
                    case "String":
                        if (string.IsNullOrEmpty (value))
                        {
                            return channel.SendLineAsync ("你要改成什麼值呢？");
                        }
                        else
                        {
                            prop.SetValue (setting, value);
                            return channel.SendLineAsync ($"已設定完成。");
                        }
                    default:
                        return channel.SendLineAsync ($"資料{prop.PropertyType.Name}未設定。請洽管理人員。");
                }
            }
            else
            {
                return channel.SendLineAsync ("沒有這個設定啦！");
            }
        }

        private Task buildSettings (ITextChannel channel, UserInfo user)
        {
            string msg = "目前設定值如下：\r\n\r\n";
            msg += GetAttrDescription ("AutoHelpFighting", TextUtil.GetColorWord (user.Settings.AutoHelpFighting));
            msg += GetAttrDescription ("AutoFighting", TextUtil.GetColorWord (user.Settings.AutoFighting));
            msg += GetAttrDescription ("AutoEatDrink", TextUtil.GetColorWord (user.Settings.AutoEatDrink));
            msg += GetAttrDescription ("AutoGet", TextUtil.GetColorWord (user.Settings.AutoGet));

            if (user.Settings.Wimpy == 0)
                msg += GetAttrDescription ("WimpyA");
            else
                msg += GetAttrDescription ("WimpyB", TextUtil.GetColorWord ($"{user.Settings.Wimpy}%"));
            msg += GetAttrDescription ("Prompt", TextUtil.GetColorWord (user.Settings.Prompt, TextUtil.ColorFront.Green));
            msg += "\r\n";
            msg += $"描述：{user.Settings.Description}\r\n";
            msg += $"名稱：{user.Settings.Name}\r\n";
            msg += $"自述：{user.Settings.DetailDescription}";

            return channel.SendLineAsync (msg);
        }

        private string GetAttrDescription (string attrName, string Value = default) => attrName
        switch
        {
            "AutoHelpFighting" => $"{MakeUpToLength(attrName)}\t（{Value}）自動幫助隊友攻擊\r\n",
            "AutoFighting" => $"{MakeUpToLength(attrName)}\t（{Value}）於作戰狀態下，自動攻擊\r\n",
            "AutoEatDrink" => $"{MakeUpToLength(attrName)}\t（{Value}）餓了、渴了，自動找食物吃、自動喝水\r\n",
            "AutoGet" => $"{MakeUpToLength(attrName)}\t（{Value}）算動拾取物品\r\n",
            "WimpyA" => $"{MakeUpToLength("Wimpy")}\t你死戰到底，絕不逃跑\r\n",
            "WimpyB" => $"{MakeUpToLength("Wimpy")}\t在生命值剩下{Value}自動逃跑\r\n",
            "Prompt" => $"{MakeUpToLength(attrName)}\t提示條：{Value}\r\n",
            _ => ""
        };
        private string MakeUpToLength (string word)
        {
            const int maxLength = 16;
            return $"{word,maxLength}";
        }
    }
}
