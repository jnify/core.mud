using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;
using Rest.MercuryCore.WorldModel;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SearchCommandHandler : ICommandHandler
    {
        public string Name => "SEARCH";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            if (string.IsNullOrEmpty (info.ParameterString))
            {
                return channel.SendLineAsync ("你要找什麼？");
            }
            else
            {
                var user = channel.ChannelUser;
                var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
                var thing = room.GetThing (info.ParameterString);
                if (thing != null)
                {
                    if (thing.AcceptVerbs.HasFlag (ObjectActionsVerb.CanSearch))
                    {
                        ActionKeyParameter actionpara = new ActionKeyParameter (user, room);
                        string msg = thing.ReActCommand (actionpara);
                        if (msg != default)
                        {
                            return channel.BroadcastToRoom (msg, user.Geography.RoomID);
                        }
                        else
                        {
                            return Task.CompletedTask;
                        }
                    }
                    else
                    {
                        return channel.SendLineAsync ("你沒辦法找到任何東西。");
                    }
                }
                else
                {
                    return channel.SendLineAsync ("這裏沒那樣東西。");
                }
            }
        }
    }
}
