using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class SkillCommandHandler : ICommandHandler
    {
        public string Name => "SKILLS";
        public string AbbrName => "SKILL";
        public DenyRestSleep DenyCommand => DenyRestSleep.None;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            var user = channel.ChannelUser;
            var room = loop.Provider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);

            string msg = "技能列表：";
            user.PracticeSkills.Sort (delegate (PracticedSkill x, PracticedSkill y)
            {
                var xskill = x.SkillAttr;
                var yskill = y.SkillAttr;
                if (xskill.skillCategory == yskill.skillCategory) return 0;
                else return xskill.skillCategory.CompareTo (yskill.skillCategory);
            });
            string skillsystemName = default;
            user.PracticeSkills.ForEach (pract =>
            {
                var skillAttr = pract.SkillAttr;
                if (skillsystemName != skillAttr.skillCategory.ToString ())
                {
                    msg += $"\r\n{skillAttr.skillCategory.GetDescriptionText()}";
                    skillsystemName = skillAttr.skillCategory.ToString ();
                }
                string eName = pract.Skillname.ToString ().SplitByUpperCase ().LeftPadSpace (18);
                string cName = skillAttr.Description.LeftPadSpace (12);
                string consumeMV = $"{skillAttr.Consume.MV.ToString().LeftPadSpace(3)} 體力";
                string consumeMana = $"{skillAttr.Consume.Mana.ToString().LeftPadSpace(3)} 精神力";
                msg += $"\r\n{eName}{cName}  {consumeMana} {consumeMV}";
            });
            msg += $"\r\n你一共學了{user.PracticeSkills.Count()}種技能，還有{user.Points.Practice}次練習能力。";

            return channel.SendLineAsync (msg);
        }

    }
}
