﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Identity;
using Keeper.MercuryCore.Pipeline;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.Internal
{
    [Flags]
    public enum DenyRestSleep
    {
        None = 0, //允許全部
        DenyRest = 1, //休息時禁止
        DenySleep = 2, //睡覺時禁止
        DenyFighting = 4, //戰鬥時禁止
        DenyDying = 8 //瀕死時禁止

    }
    public class CommandLoopMiddleware : IMiddleware
    {
        private readonly ICommonTools commonTools;
        public CommandLoopMiddleware (ICommonTools commontool)
        {
            this.commonTools = commontool;
        }

        private class LoopInstance : ICommandLoop
        {
            public bool IsRunning { get; set; }
            public IServiceProvider Provider { get; set; }
            private DateTime BlockTillDT = DateTime.MinValue;
            internal void SetDelayInMilliseconds (int delayMilliseconds)
            {
                BlockTillDT = DateTime.Now.Add (TimeSpan.FromMilliseconds (delayMilliseconds));
            }
            public double BlockInMilliseconds
            {
                get
                {
                    var ts = BlockTillDT - DateTime.Now;
                    if (ts < TimeSpan.Zero) return 0;
                    return ts.TotalMilliseconds;
                }
            }
        }

        public Func<Task> BuildHandler (IServiceProvider serviceProvider, Func<Task> next)
        {
            var channel = serviceProvider.GetRequiredService<ITextChannel> ();
            var parser = serviceProvider.GetRequiredService<ICommandParser> ();
            var handlerLookup = serviceProvider.GetServices<ICommandHandler> ().ToDictionary (x => x.Name.ToUpperInvariant ());
            var handerMapping = serviceProvider.GetServices<ICommandHandler> ().ToDictionary (x => x.AbbrName.ToUpperInvariant (), x => x.Name.ToUpperInvariant ());
            return async () =>
            {
                var loopInstance = new LoopInstance
                {
                IsRunning = true,
                Provider = serviceProvider
                };
                ICommandHandler handler = null;
                UserInfo user = channel.ChannelUser;
                user.BindDelayInMilliseconds (loopInstance.SetDelayInMilliseconds);

                InitalUserLoginEvent (serviceProvider, user);

                while (loopInstance.IsRunning)
                {
                    var commandLine = await channel.ReceiveLineAsync ();
                    int multipleTimeCmd = 1;

                    if (commandLine.IndexOf (" ") != -1)
                    {
                        string _intString = commandLine.Substring (0, commandLine.IndexOf (" "));
                        if (int.TryParse (_intString, out multipleTimeCmd))
                            commandLine = commandLine.Substring (commandLine.IndexOf (" ") + 1);
                        else
                            multipleTimeCmd = 1;
                    }

                    while (multipleTimeCmd > 0 && loopInstance.IsRunning)
                    {
                        multipleTimeCmd--;
                        await ExecuteCommandLineAsync (loopInstance, parser, commandLine, handerMapping, handlerLookup, handler, channel);
                        if (multipleTimeCmd > 0) Thread.Sleep (300);
                    }
                }
            };
        }

        private async Task ExecuteCommandLineAsync (LoopInstance loopInstance, ICommandParser parser, string commandLine,
            Dictionary<string, string> handerMapping, Dictionary<string, ICommandHandler> handlerLookup, ICommandHandler handler,
            ITextChannel channel)
        {
            var blocktime = loopInstance.BlockInMilliseconds;
            if (blocktime > 0)
                Thread.Sleep (Convert.ToInt32 (blocktime));

            var info = parser.Parse (commandLine);

            if (info.IsValid)
            {
                if (handerMapping.TryGetValue (info.Name.ToUpperInvariant (), out var fullcmd))
                {
                    info.Name = fullcmd;
                }

                if (handlerLookup.TryGetValue (info.Name.ToUpperInvariant (), out handler))
                {
                    if (!BlockedCommand (channel, handler, channel.ChannelUser))
                    {
                        await handler.Handle (loopInstance, info);
                    }
                }
                else
                {
                    await channel.SendLineAsync ($"什麼事也沒發生...");
                }
            }
            else
            {
                await channel.SendLineAsync ("");
            }
        }

        private void InitalUserLoginEvent (IServiceProvider serviceProvider, UserInfo user)
        {
            ITextChannel channel = serviceProvider.GetRequiredService<ITextChannel> ();
            IStateManager sessionState = serviceProvider.GetService<IStateManager> ();
            IdentityInfo idi = sessionState.Get<IdentityInfo> ();

            if (user.Level > 95)
            {
                idi.Level = user.Level;
                sessionState.Set (idi);
            }
            if (user.Geography.RoomID < 0)
                user.Geography.RoomID = user.Geography.RoomID * -1; //回到離開的房間

            //Build login message.
            string WelcomeMessage = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n突然螢幕變得黑暗, 有一股很強的力量把你吸進去....\n\n\n";
            var room = serviceProvider.GetService<IWorldManager> ().GetRoom (user.Geography.RoomID);
            var roomusers = serviceProvider.GetService<IUserManager> ().GetRoomUser (user.Geography.RoomID);
            WelcomeMessage += commonTools.DescriptionBuilder (room, user, roomusers);

            user.LastMessageDatetime = user.LastMessageDatetime.LastMessageDatetimeComplete (); //不讓Elf的Job顯示Prompt
            channel.BroadcastToRoom ($"{user.Name}出現在你面前。", user.Geography.RoomID, user.Name);
            if (user.Status.HasFlag (CreatureStatus.Dying))
            {
                channel.SendLineAsync ("你正昏迷不醒！！");
            }
            else if (user.Status.HasFlag (CreatureStatus.Sleep))
            {
                channel.SendLineAsync ("你在睡覺，什麼也看不見！");
            }
            else
            {
                channel.SendLineAsync (WelcomeMessage);
            }
        }

        private bool BlockedCommand (ITextChannel channel, ICommandHandler handler, UserInfo user)
        {
            //瀕死時
            if (user.Status.HasFlag (CreatureStatus.Dying) && handler.DenyCommand.HasFlag (DenyRestSleep.DenyDying))
            {
                channel.SendLineAsync ("你傷的太重！！");
                return true;
            }

            //戰鬥時
            if (user.Status.HasFlag (CreatureStatus.Fighting) && handler.DenyCommand.HasFlag (DenyRestSleep.DenyFighting))
            {
                channel.SendLineAsync ("你正忙戰鬥呢！");
                return true;
            }

            //睡覺時
            if (user.Status.HasFlag (CreatureStatus.Sleep) && handler.DenyCommand.HasFlag (DenyRestSleep.DenySleep))
            {
                channel.SendLineAsync ("哦？在睡夢中嗎？");
                return true;
            }

            //休息時
            if (user.Status.HasFlag (CreatureStatus.Rest) && (handler.DenyCommand.HasFlag (DenyRestSleep.DenyRest)))
            {
                channel.SendLineAsync ("嗯... 你太舒服起不來了...");
                return true;
            }
            return false;
        }

    }
}
