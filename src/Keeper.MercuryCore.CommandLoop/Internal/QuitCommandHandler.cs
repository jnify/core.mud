﻿using System.Threading;
using System.Threading.Tasks;
using Firebase.OPT;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Utilities;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class QuitCommandHandler : ICommandHandler
    {
        public string Name => "QUIT";
        public string AbbrName => Name;
        public DenyRestSleep DenyCommand => DenyRestSleep.None;

        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            var channel = loop.Provider.GetService<ITextChannel> ();
            IUserManager usrMng = loop.Provider.GetService<IUserManager> ();
            var user = usrMng.GetUser (channel.ChannelUser.Name);
            if (user.Status.HasFlag (CreatureStatus.Fighting))
            {
                loop.Provider.GetService<ITextChannel> ().SendLineAsync ("你還在戰鬥中，無法離開。");
                return Task.CompletedTask;
            }
            if (user.Status.HasFlag (CreatureStatus.Dying))
            {
                loop.Provider.GetService<ITextChannel> ().SendLineAsync ("你還沒真的死啦！！");
                return Task.CompletedTask;
            }

            var quitmsg = TextUtil.GetFromFile ("DocumentHelp", "QuitMessage.hlp");
            loop.Provider.GetService<ITextChannel> ().SendLineAsync (quitmsg);
            user.Clear ();
            IFirebase<UserInfo> userinfo = loop.Provider.GetService<IFirebase<UserInfo>> ();
            userinfo.InsertOrUpdate (user);
            usrMng.RemoveUser (user.Name);
            loop.IsRunning = false;
            channel.BroadcastToRoom ($"{user.UserDescriptionAndID}漸漸的消失在你的眼前！！", user.Geography.RoomID, user.Name);
            //return Task.FromCanceled (new CancellationToken (true));
            return Task.CompletedTask;
        }
    }
}
