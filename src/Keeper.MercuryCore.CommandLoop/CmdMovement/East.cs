using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Keeper.MercuryCore.CommandLoop.Internal;
using Keeper.MercuryCore.CommandLoop.Parsing;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Rest.MercuryCore.CreatureModel;
using Rest.MercuryCore.God.Interface;
using Rest.MercuryCore.Interface;
using Rest.MercuryCore.Objectivate;

namespace Keeper.MercuryCore.CommandLoop.AutoCommand
{
    public class EastCommandHandler : ICommandHandler
    {
        public string Name => "EAST";
        public string AbbrName => "E";
        public DenyRestSleep DenyCommand => DenyRestSleep.DenyFighting | DenyRestSleep.DenyRest | DenyRestSleep.DenySleep | DenyRestSleep.DenyDying;
        public Task Handle (ICommandLoop loop, CommandInfo info)
        {
            ITextChannel channel = loop.Provider.GetService<ITextChannel> ();
            var usrMng = loop.Provider.GetService<IUserManager> ();
            var msgMgr = loop.Provider.GetService<IMessageManager> ();
            var comTool = loop.Provider.GetService<ICommonTools> ();
            var user = usrMng.GetUser (channel.ChannelUser.Name);
            var wordMng = loop.Provider.GetService<IWorldManager> ();
            var room = wordMng.GetRoom (user.Geography.RoomID);
            var exit = ExitName.East;
            List<UserInfo> users = default;
            var targetRoom = room.Exits.Where (x => x.Dimension == exit).FirstOrDefault ();
            if (targetRoom != null) users = usrMng.GetRoomUser (targetRoom.RoomID);
            return comTool.MovementAsync (msgMgr, wordMng, users, usrMng.GetRoomUser (user.Geography.RoomID), user, room, exit);
        }
    }
}
