﻿using System;
using Keeper.MercuryCore.Middleware;
using Keeper.MercuryCore.Session;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Keeper.MercuryCore.TestHarness
{
    class Program
    {
        static void Main (string[] args)
        {
            var host = new HostBuilder ()
                .ConfigureServices (services => services
                    .AddLogging (logging => logging.SetMinimumLevel (LogLevel.Trace))
                    .AddInMemoryIdentity ()
                    .AddFireBase ()
                    .AddCommandLoop (cmd => cmd
                        .AddVerbObjectParser ()
                        .AddCommandHandler ()
                    )
                    //.RegisterServiceFirebaseDI ()
                    .LoadCoreObjects ()
                    .LoadWorldObjects ()
                    .LoadCoreElfObjects ()
                    .AddSingleton<ISession, UserCommonSession> ()
                )
                .ConfigureSerilog (config => config
                    .WriteTo.LiterateConsole ()
                    .WriteTo.RollingFile (".\\Logs\\TestHarness-{Date}.log", fileSizeLimitBytes : 1024 * 1024)
                    .MinimumLevel.Verbose ())
                .ConfigurePipeline (pipeline =>
                {
                    int port;
                    int.TryParse (Environment.GetEnvironmentVariable ("PORT"), out port);
                    if (port == 0) port = 4444;
                    pipeline.AddTcpEndpoint ("EndUser", options => options.Port = port)
                        .UseTelnetChannel ()
                        .UseUtf8Channel ()
                        .UseVirtualTerminalChannel ()
                        .UseMotd ((MotdOptions opt) =>
                        {
                            opt.Message = $"-------Welcome to {port}: v0.0.0.4 Beta-------";
                        })
                        .UseLogin ()
                        .UseCommandLoop ();
                    //pipeline.Use ((provider, next) => () => Run (provider));
                })
                .Build ();

            host.Run ();
        }
    }
}
